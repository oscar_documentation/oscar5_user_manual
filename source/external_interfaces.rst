External Interfaces (Plugins)
*****************************

The current analysis codes with established *rapyds* interfaces are listed :ref:`below <codes_with_plugins>`.

.. _codes_with_plugins:
.. list-table:: Codes with a defined *rapyds* interface
   :header-rows: 1
   :class: tight-table

   * - Analysis Code
     - Supported Versions
     - Area
     - Notes
   * - Serpent [#ext]_
     - 2.1.23 to 2.1.29
     - Neutronics (Monte Carlo)
     -
   * - MCNP [#ext]_
     - 5.1, 6.1, 6.2
     - Neutronics, Particle Transport (Monte Carlo)
     -
   * - MGRAC [#int]_
     - 1.x, 2.x
     - Neutronics (Nodal Diffusion) [#th]_
     -
   * - COGENT [#int]_
     - 0.x
     - Neutronics, Particle Transport (Monte Carlo)
     - Both the code and the interface is still under heavy development.
   * - DeCART2D [#ext]_
     - 1.x
     - Neutronics (2D MOC)
     - Current version of interface restricted to prismatic HTR designs.
   * - CAPP [#ext]_
     - 1.x
     - Neutronics (Finite Element Diffusion) [#th]_
     - Limited to analysis of prismatic HTR designs.
   * - RELAP [#ext]_
     - 5.x
     - Thermal Hydraulics (System) [#lim]_
     - Limited to the passing of neutronics parameters, and some basic output processing.
   * - GAMMA+ [#ext]_
     -
     - Thermal Hydraulics (System) [#lim]_
     - See above.



.. [#ext] Externally developed code, which is distributed and licensed separately. Only interfaces
          are available.

.. [#int] In house developed analysis code.

.. [#th] Also supports embedded thermal hydraulic solvers, if available.

.. [#lim] Application level interface only, as the *rapyds* data model does not contain all the required
          elements.

See also :ref:`plugin_support` for more information on the capabilities of these plugins.

Adding Interfaces
=================

Interfaces are developed using a common API, and can be added without access to, or modification of
the base *rapyds* source. This system uses a template
engine, which is used to produce input from the various *rapyds* :ref:`data elements <Data Model>`
and :ref:`application parameters <general_app_parameters>`.
The plugin interface is illustrated :ref:`below <rapyds_plugins>`.

.. _rapyds_plugins:
.. figure:: images/rapyds-plugins.png

   Schematic illustration of the RAPYDS plugin interface.

   Regions in blue denote system elements, while red regions denote parts specific to the plugin.

The only requirement for the functionality of a plugin, is the implementation of application input templates, for all
:ref:`application modes <Applications>` the target code should support, as well as the associated recipe(s), which is
used to process output into a standardized data object. To avoid a lot of duplication in these templates,
templates can also be developed at the data element level.

.. A comprehensive plugin development guide will be added in the near future.