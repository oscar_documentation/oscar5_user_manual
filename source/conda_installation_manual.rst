.. role:: bash(code)
    :language: console

.. role:: py(code)
    :language: python

Installation with Conda
************************

This section covers the installation of **OSCAR-5** using the cross platform **Conda** package manager.

Introduction
------------

The following sections gives a little bit of background on the tools and terms you will encounter during the installation.


What is Anaconda and Conda?
+++++++++++++++++++++++++++

`Anaconda <https://www.anaconda.com>`__ is a provider of data science platforms, with a particular focus on Python and
its associated infrastructure. It has both open source (community), and enterprise offerings. At the heart of the
platform is Conda, which is a multi platform repository of Python packages and related software. It is however more
that just another Python repository (such as PyPI), and also provides the additional infrastructure to build, package and
run Python based, and other, software. It is therefore a very attractive platform to build and distribute the
**OSCAR-5** system.

See the `conda documentation <https://docs.conda.io/projects/conda/en/latest/index.html>`__ for more information.

What is a "conda package"?
++++++++++++++++++++++++++

This usually refers to either a Python package (which is a collection of modules), a software library, or an executable
program. When :bash:`conda` is used to install a package, its specified list of dependencies are also automatically
installed.

What is a "conda environment"?
++++++++++++++++++++++++++++++

In Conda, an environment is a standalone selection of Python packages, system libraries, support programs, and
a python interpreter. Installing or changing packages in one environment will not affect the base installation, or other
environments. This is a very useful mechanism to isolate programs, and pin (or fix) versions of dependencies. A practical
example is to a have an established stable production environment (used for official calculations say) which is
rarely updated, and another more dynamic environment were new features are tested and used.

More information can be found `here <https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html>`__.

.. note::

   One can think as a conda environment as small "containerized" working space. However, it is not a true container as
   one would create with for example Docker, since it still depends on the underlying operating system. Thus, software
   installed and used through Conda remains system dependent.

What is a "conda channel"?
++++++++++++++++++++++++++

Conda channels are the locations where packages are stored. When installing a package, channels are automatically
searched for the most appropriate version, and all its dependencies. Channels can be remote (specified by a URL), or
local (specified by a directory). After installing Anaconda, a number of default channels will be available, but you
can add additional ones. For example, `Conda forge <https://conda-forge.org>`__ is a community channel made up of
thousands of contributors. The **OSCAR-5** packages are currently hosted on the `necsa-rrt <https://anaconda.org/necsa-rrt>`__
channel, whose contents is summarized in the following table:

.. list-table:: Contents of the **OSCAR-5** installation channel.
   :header-rows: 1
   :class: tight-table

   * - Package
     - Description
   * - `oscar5_nodal_package`
     - The nodal diffusion solver MGRAC and associated support codes.
   * - `rapyds`
     - The Python pre- and post processing platform used in **OSCAR-5**.
   * - `compose`
     - The model preparation package, which is used to prepare cross sections for the nodal diffusion solver MGRAC.
   * - `rapyds_extensions`
     - A collection of Python extensions written in `C++`. It is required by the `rapyds` package.
   * - `rapyds_data`
     - External data requirements for the `rapyds` package.
   * - `oscar5_docs`
     - Package containing the complete documentation tree of the **OSCAR-5** system. It is simply a static version
       of the online documentation, which can be used for offline viewing.
   * - `oscar4_docs`
     - User manuals for the nodal package.

Additional information on Conda channels can be found in the
`documentation <https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/channels.html>`__.

Installing and updating conda packages
++++++++++++++++++++++++++++++++++++++

Any conda package is installed using the following command basic syntax:

.. code-block:: console

   conda install <package_name> -c <channel>

where channel is the preferred channel from which the package should be installed. If it is omitted, then *Conda* will
try to install it from the currently configured default channels. See the
`Conda documentation <https://docs.anaconda.com/anaconda/user-guide/tasks/install-packages>`__ for more information.

The install mechanism can also be used to update a package, or change to a particular version. To see what versions
of a package is available, use the `search` mechanism:

.. code-block:: console

   conda search <package_name> -c <channel>

Then, to install a particular version, use

.. code-block:: console

   conda install <package_name>=<version> -c <channel>

To reinstall a particular version, add the following flag:

.. code-block:: console

   conda install <package_name>=<version> -c <channel> --force-reinstall

For example, the following sequence will list the available versions of the `oscar5_nodal_package`, and install the
specified version:

.. code-block:: console

   conda search oscar5_nodal_package -c necsa-rrt
   conda install oscar5_nodal_package=1.2 -c necsa-rrt


Initial installation procedure
++++++++++++++++++++++++++++++

The installation procedure and prerequisites depends on the release package you received:

.. tabularcolumns:: |\Y{0.15}|\Y{0.30}|\Y{0.35}|\Y{0.25}|

.. list-table:: Package dependent prerequisites and installation instructions
   :header-rows: 1
   :class: tight-table

   * - Package
     - Contents
     - Prerequisites
     - Instructions
   * - Pre-packaged Installer
     - A single platform dependent installer
     -
     - `Using the Self Contained Installer`_
   * - Live installation using Conda
     -  1. The helper script `install_oscar5.py`
        2. Package repository URL containing the **OSCAR-5** binary packages
     -  1. `Anaconda3`_
        2. A live internet connection which can access standard Conda repositories and the provided URL.
     - Follow the instructions outlined in `Live Installation`_
   * - Partial live installation using Conda
     -  1. The helper script `install_oscar5.py`
        2. An archive containing the **OSCAR-5** binary packages
     -  1. `Anaconda3`_
        2. A live internet connection which can access standard Conda repositories
     -  First follow the instructions outlined in `Creating a Local Package Repository`_, then proceed to
        `Live Installation`_


You should also have a license file `oscar_license.ini`, which will be placed in the correct location during the
installation process.


Prerequisites
-------------

Anaconda3
+++++++++

This is a platform independent Python interpreter and package management system. It can be downloaded
form the `Anaconda homepage <https://www.anaconda.com/distribution/>`__. The package comes as a self extracting
installer, and additional installation instructions can be found on the download page.

.. note::

   It is not required when using the self extracting installer package, as a lightweight version of the `conda` package
   manager is included in the package.


Intel Fortran Redistributable Package
+++++++++++++++++++++++++++++++++++++

The correct redistributable can be obtained from
`this link <http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/15799/ww_ifort_redist_msi_2019.5.281.zip>`__
. This package contains both 32 and 64 bit installers, but you only need to install the package relevant to your operating
system (that is, 64 bit if you are running 64 bit Windows).

.. note::

   The Intel redistributable no longer seems to be required, since Intel has made most of their libraries available
   as *Conda* packages. Only install it if you experience runtime errors.

Git
+++

The version control client Git is required if you want direct access to our code development repositories, or the
benchmarks in the validation set.

.. list-table:: Obtaining Git
   :header-rows: 1

   * - Operating System
     - Installation Instructions
   * - Windows
     - Download installer form `<https://git-scm.com>`__
   * - Linux (Debian)
     - :bash:`sudo apt-get install git`
   * - Linux (CentOS or RHEL Based)
     - :bash:`sudo yum install git`

Alternatively Git can be installed with :bash:`conda`, i.e. :bash:`conda install git`.

You will also need to create a `GitLab <https://gitlab.com>`__ account, then send us you username so that we can add you
to the relevant projects.

To enable easy access to our repositories, `create an ssh key <https://docs.gitlab.com/ee/ssh/>`_ and add it to your
GiLab profile. Alternatively, checkout will be performed using HTTPS and you will be prompted for your GitLab password.

.. note::

   Since moving to Conda distribution, GitLab access is no longer required in order to use any of the components in
   **OSCAR-5**. However, it remains the best platform to host collaborative projects such as benchmarks and other
   reactor models. Also, if you plan to contribute development work, GitLab access and Git is essential.


Using the Self Contained Installer
----------------------------------


The installer is easy to use, and will guide you through the process step by step. Once installation is complete, the
license file needs to be placed in the correct root directory:

    1. Open a `Anaconda Prompt (oscar-5)` terminal from the start menu.

    2. In the prompt, type the following:

       .. code-block:: console

          copy C:\Path\to\oscar_license.ini  %CONDA_PREFIX%


.. note::

   If the `visualization test <Testing Your Installation>`_ fails, the installer probably contains a broken version of
   the *vtk* package for Windows. This is a *Conda* bug
   which would hopefully be resolved soon. In the mean time, the issue can be resolved by installing *vtk* through the standard
   python distribution tools. Open an `Anaconda Prompt (oscar-5)` terminal from the start menu, and type the
   following:

   .. code-block:: console

      pip install vtk=8.1.2 --force-reinstall -I


Creating a Local Package Repository
-----------------------------------

Create a new directory, which I will refer to as the PACKAGE_ROOT, and extract the provided archive to this directory.
After the extraction, the contents should resemble the following

.. code-block:: console

   PACKAGE_ROOT
            |_ channeldata.json
            |_ index.html
            |_ noarch
            |_ win-64

where *win-64* might be replaced by *linux-64* or *osx-64* depending on your operating system. This directory will be
passed as the *channel* flag when performing the `Live Installation`_.

Live Installation
-----------------

.. note::

   Make sure that the license file is in the same directory as the install script.

This describes how to install all the **OSCAR-5** packages, together with their dependencies. It assumes that
`Anaconda3`_ is already installed. The following must be performed in an *anaconda prompt*:

    - On Windows, select *Anaconda Prompt* from the Start menu
    - On Linux or OSX, open any terminal. Depending on the options chosen during installation, Anaconda might already
      available. If the the :bash:`(base)` qualifier in the prompt is not visible, type :bash:`conda activate` to enable it.
      If this fails, try

The *install_oscar5.py* script is indented to help with installation process, especially if you are planning to install
and use sources from out GitLab code repositories (that is, set up a partial development environment). If you are not
planning on modifying any sources, use of the script is not necessary, and you can proceed directly to
`Manual installation without using the install script`.

Before proceeding, place the *install_oscar5.py* script somewhere on your local drive.

.. attention::

   It is a good idea to update Conda before starting the installation process. So, in the *Anaconda Prompt*, type

   .. code-block:: console

      conda update conda

.. note::

   The install script will only install packages for which you have a valid license!

Basic Installation
++++++++++++++++++

In the *Anaconda Prompt*, change to the directory containing the installation script, and run

.. code-block:: console

   python install_oscar5.py --channel <repository> --name <name>

where *<repository>* is the provided URL, our conda channel `necsa-rrt <https://anaconda.org/necsa-rrt>`__, or full
path to the local PACKAGE_ROOT from `Creating a Local Package Repository`_,
and *<name>* is the Conda environment in which the packages should be installed (defaults to *rapyds*).

For example, installing form a local repository into the *rapyds* environment:

.. code-block:: console

   python install_oscar5.py --channel C:\path\to\my\package_root --name rapyds

or, to install from a (local) server:

.. code-block:: console

   python install_oscar5.py --channel http://address_of_server/tag --name rapyds

Python development installation
+++++++++++++++++++++++++++++++

In the *Anaconda Prompt*, change to the directory containing the installation script, and run

.. code-block:: console

   python install_oscar5.py --channel <repository> --name <name> --dev rapyds,apps -D <dest_dir>

where:

    - *<repository>* is the provided URL or full path to the local PACKAGE_ROOT from `Creating a Local Package Repository`_
    - *<name>* is the Conda environment in which the packages should be installed (defaults to *rapyds*)
    - *<dest_dir>* full path to the directory in which code repositories should be cloned (defaults to the current
      directory).

After the packages are installed, you will be prompted to activate the environment and run the install script again:

.. code-block:: console

   conda activate <name>
   python install_oscar5.py  -D <dest_dir> --setup

The installation is now complete. You can proceed to `Testing Your Installation`_.

Manual installation without using the install script
++++++++++++++++++++++++++++++++++++++++++++++++++++

This covers the steps required to preform the installation without the helper script. It is useful if you would like
more insight in how Conda works.

Step 1: Set up repository channels
==================================

In the open *Anaconda Prompt*, run

.. code-block:: console

   conda config --get channels

If the entry *conda-forge* is *not* present add it with:

.. code-block:: console

   conda config --append channels conda-forge

Step 2: Create an environment
=============================

Next, create a
`conda environment <https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`__ which
will contain the **OSCAR-5** installation:

.. code-block:: console

   conda create --name <name>

Where, *<name>* can be any string entry. The "classic" value (used in the documentation and tutorials) is *rapyds*, e.g

.. code-block:: console

   conda create --name rapyds

Step 3: Install packages
========================

Finally, install all the packages to the newly created environment:

.. code-block:: console

   conda install rapyds compose oscar5_nodal_package -n <name> -c <repository>

where *<name>* is the environment name created in the previous step, and *<repository>* is either the
the local PACKAGE_ROOT from `Creating a Local Package Repository`_, or URL where the packages are located.

You can now proceed to `Installing the License File`_, and then to `Testing Your Installation`_.

Installing the License File
---------------------------

The license file *oscar_license.ini* needs to placed withing the root of your target *Conda* environment.
This is required when performing the `manual installation <Manual installation without using the install script>`__, or when replacing
an expired license file.

On Windows:

    1. Open a `Anaconda Prompt` terminal from the start menu.

    2. Make sure that you are in the correct environment:

       .. code-block:: console

          conda activate <name>

    3. Copy the license file:

       .. code-block:: console

          copy C:\Path\to\oscar_license.ini  %CONDA_PREFIX%

On Unix (Linux or OSX):

    1. Within any terminal window, make sure that you are working in the correct environment:

       .. code-block:: console

          conda activate <name>

    2. Copy the license file:

       .. code-block:: console

          cp /path/to/oscar_license.ini  $CONDA_PREFIX


Testing Your Installation
-------------------------

The following is a quick test to insure that the python components of the installation are working correctly. In an
*Anaconda Prompt* activate the environment (if you have not done so already):

.. code-block:: console

   conda activate <name>

where *<name>* is the environment you created during the installation process (default is *rapyds*). Now start an
interactive interpreter session:

.. code-block:: console

   python

After the console starts, type the following

>>> from core import *

If this finishes without errors, it usually means that the system can find all dependencies and you can safely continue.
See the `Troubleshooting`_ section if this fails.

Next, type

>>> from material_library.moderators import LightWater
>>> m = LightWater(1 * units.g / units.cc)
>>> print(m)

You should see a table giving the isotopic composition of water. Finally, let's check the display engine:

>>> from csg import *
>>> p = primitives.Cube()
>>> p.show()

If all is well, a window should pop up displaying a spinning cube (you might have to zoom out using the center mouse button).

Installing Optional Packages
----------------------------

This section covers the installation of optional (third party) packages.

FreeCAD
+++++++

The Open Source CAD software suite `FreeCAD <https://www.freecadweb.org>`__ is used in the exporting and documentation
of models. It can be installed using *conda*:

.. code-block:: console

   conda activate <name>
   conda install -c freecad/label/qt5.9 freecad

where *<name>* is your **OSCAR-5** environment.

.. attention::

   It is important to use the *label/qt5.9* channel, as the default repository contains bugs which prevents the FreeCAD
   library to be imported.

.. note::

   Since FreeCAD is also under heavy development, there is a good change that you might encounter a lot
   of incompatible package dependencies during the above installation. If this happens, simply install it
   in its own conda environment:

    .. code-block:: console

       conda deactivate
       conda create -n freecad
       conda install -c freecad/label/qt5.9 freecad -n freecad

   The link with FreeCAD should still work, provided it is installed in an environment with name *freecad* or
   *freecad-dev*.

YADE
++++

The discrete element modelling package `YADE <https://yade-dem.org/doc/>`__ is used in the generation of explicit
particle distributions for HTR designs. A packaged version is hosted on our *conda* channel, and can be installed using:

.. code-block:: console

   conda activate <name>
   conda install -c <repository> yade

where *<name>* is your **OSCAR-5** environment, and *<repository>* is either the
the local PACKAGE_ROOT from `Creating a Local Package Repository`_, or URL where the packages are located.

.. note::

   It is recommended that the packaged version of YADE be used on your local (host) machine, as it is guaranteed to be
   compatible with the *conda* version of **OSCAR-5**. When running YADE remotely,
   the `official installation methods <https://yade-dem.org/doc/installation.html>`__ can be followed on the target machine.


Configuring the IDE
-------------------

You can use any text editor to work with the Python components of **OSCAR 5**. However, to get the most out of the
system, it is recommended that you use a Python aware Integrated Development Environment (IDE). This section describes
how to set up our recommended IDE `PyCharm <https://www.jetbrains.com/pycharm/>`_, which can be downloaded
`here <https://www.jetbrains.com/pycharm/promo/anaconda/>`_.


.. note::

   The free community edition is more than sufficient as an interface tool for the **OSCAR 5** system.


Installation
++++++++++++

Simply follow the installer steps. All default values are fine, but feel free to change the installation location. Once
installed, you can click the option to launch the program, or launch it later from the start menu.

When launching for the first time, you can choose a theme and install some optional packages (all of which can be skipped).
There is no need to install *Miniconda* as you already have a fully fledged Anaconda installation, so skip this option
if it is presented.

Setting the interpreter
+++++++++++++++++++++++

The interpreter can only be set when a project is available, so lets start by opening **rapyds**. Choose *File > Open*,
then browse to *INSTALLATION_PREFIX\\oscar5\\rapyds*. Open the settings panel *File > Settings*, and browse to
*Project:rapyds > Project Interpreter*. Look at the value in the *Project Interpreter* box. If the interpreter option
*Python 3.7 (<name>)* (where *<name>* is your installation environment) is listed, select it, and you are done.
Otherwise, add it using the following steps:

    1. Press the gear icon on the left of the selection box and choose *Add...*.
    2. Make sure *Conda Environment* is selected on the left and choose *Existing environment*
    3. Press the browse button to the left of the *Interpreter* box, browse to the root directory were Anaconda was
       installed, and then select the *python.exe* file located under the *envs\\<name>* sub directory.
    4. Make sure the *Make available to all projects* box is ticked, then press *OK*.
    5. Finally, make sure that *Python 3.7 (<name>)*  is selected as the *Project Interpreter* before exiting the
       settings dialog.

.. note::

   If you are unsure of your Anaconda installation directory, open an *Anaconda Prompt* and type

   .. code-block:: console

      conda activate <name>
      echo %CONDA_PREFIX%

   The displayed path is the root directory which contains your Python interpreter, and the path before *envs\\<name>*
   is the root Anaconda installation directory.

Customizing the terminal
++++++++++++++++++++++++

.. note::

   In newer versions of the IDE, the terminal is correctly set up once the Conda env is selected, and
   it is not necessary to implement the steps below.

To ensure that the environment is sourced correctly when you open a terminal, the default terminal command needs to be
changed.

Open the settings panel, *File > Settings*. Then, in the *Tools > Terminal* panel, enter the following in the
*Shell path* box:

.. code-block:: console

  cmd.exe "/K" <ANACONDA_INSTALL_ROOT>\Scripts\activate.bat <ANACONDA_INSTALL_ROOT>\envs\<name>

where, *<ANACONDA_INSTALL_DIR>* should be replaced with your Anaconda installation root.


.. note::

   If your path include spaces, you should enclose the entire path in double quotes.


Locating Git
++++++++++++

.. attention::

   This step is only required if PyCharm warns that it could not configure *Git*.

If you installed *Git* using *conda*, the IDE might not detect it automatically, and you will have to enter the path
manually. Open *File > Settings*, then navigate to *Version Control > Git*. In the *Path to Git executable*, enter

.. code-block:: console

   <ANACONDA_INSTALL_DIR>\Library\bin\git.exe

where, *<ANACONDA_INSTALL_DIR>* should be replaced with your Anaconda installation root. Press the *Test* button to
make sure that everything is working.

Troubleshooting
---------------

Error on importing :py:`core`
+++++++++++++++++++++++++++++

    1. :py:`from core import *` raises either a :py:`ModuleNotFound` or :py:`ImportError` exception with the message
       :py:`No module named 'core'`:

       - Did you activate the correct Conda environment prior to starting the shell (e.g. :bash:`conda activate <name>`)?
       - For a development installation did you perform the :bash:`--setup` step **after** activating the environment?

    2. :py:`from core import *` produces a  a :py:`ModuleNotFound` or :py:`ImportError` exception with a message
       referring to missing libraries (:bash:`*.so` or :bash:`*.DLL`):

       - Try deactivating and re-activating the environment:

        .. code-block:: console

           conda deactivate
           conda activate <name>

       - If the error persist please let us know by either logging an issue on GitLab, or by sending us an email. Please
         include the full Tracback of the error message.

    3. An error is produced referring to a missing MKL library (this usually happens on Linux). This appears to be a
       `numpy` problem, and can be fixed by upgrading `numpy` to the latest version:

       .. code-block:: console

          conda install numpy=1.2

Error on visualisation
++++++++++++++++++++++

    1. In Windows, the :py:`p.show()` command raises a DLL not found error:

       - This is a bug in certain versions of conda, or more precisely, the **vtk** package. First try to install an
         earlier version of **vtk**:

         .. code-block:: console

            conda activate <name>
            conda install vtk=8.1


        If this fails to resolve the problem, remove the conda provided **vtk**, and use the pip one instead:

         .. code-block:: console

            conda activate <name>
            pip install vtk=8.1.2 --force-reinstall -I


Error on running MGRAC
++++++++++++++++++++++

    1. On the Windows platform, if running **mgrac** causes missing DLL errors when trying to access the *MKL* libraries,
       install the correct version using the following:

       .. code-block:: console

         conda activate <name>
         conda install mkl=2019.5 -c intel






