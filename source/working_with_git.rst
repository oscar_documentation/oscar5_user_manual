Working with Git
****************

This section contains a very brief introduction to the version control system *Git*.

Working with Git in the PyCharm IDE
-----------------------------------

Apart from source file editing and execution, the IDE also makes it easier to work with version control systems like
Git. This section gives a quick introduction on how to work with Git in the IDE.

Adding files
++++++++++++

When creating new files through the IDE, you will be prompted to add them to the repository (unless they are filtered
out by the *gitignore* rules). You can always manually add any file by right clicking on it in the *Project* view,
and selecting *Git > Add*.

Committing local changes
+++++++++++++++++++++++++

This will write changes to your local repository. Right click on a project (or sub directory) in the *Project* view,
then select *Git > Commit Directory*. A window will pop open, in which you can view and select which changes should be
part of the commit. You should also add a message briefly describing the changes that were made.

.. note::

   You may want to uncheck the *Perform code analysis* box. Otherwise PyCharm will analise all you source files for
   errors and syntax violations, which can be quite time consuming.

Pulling updates from the server
+++++++++++++++++++++++++++++++

Right click the project root (or sub directory) you wish to update, then select *Git > Repository > Pull*. Press
*Pull* on the next dialog to initiate the update. Note that the IDE will automiatically try to merge any local changes
with that from the server, and present you with detailed merge options if it can not be easily resolved.

.. attention::

   You should always commit local changes before pulling from a remote branch!

Pushing updates to the server
+++++++++++++++++++++++++++++

This will push all your **committed** changes to the source repository, making them available to other team members.
Right click on the project root in the *Project* tree, and select *Git > Repository > Push*. A dialog will appear showing
all your local commits. Confirm by pressing *Push*.

.. note::

   It is always a good idea to preform a pull before pushing to the server.

.. attention::

   Only changes already :ref:`committed <Committing local changes>` to your local repository can be pushed!

Adding a new repository
+++++++++++++++++++++++

From the *VCS* menu, select *Git > Clone...*. The *URL* field is the location of the remote repository, which
can be obtained by copying the *Clone with SSH* address in the GitLab project page:

.. image:: images/gitlab-clone.png

The *Directory* field is the local directory where the repository will be cloned. This can be any (writeable) location,
and PyCharm will automatically create any missing directories.

.. note::

   The repository will be cloned **directly** into this local directory, that is, no additional sub directory is created.

Press *Clone* to initiate the process. After completion, PyCharm will prompt you to open the directory. Select *Yes*
and then *Attach* to add it to your list of projects.

.. note::

   Some versions of PyCharm might refuse to attach the project (something about *Non standard layout*), and then
   asks if you wish to open it in a new window. Select *Yes*. You can then either work with this project in the
   new window, or close it, then open it again from *File > Open Recent* and choose *Attach*. This usually
   (for some reason) works the second time around.


Tips and Tricks
---------------

This section lists some advanced `git` features that can be used from the command prompt.

How to reset your local repository
++++++++++++++++++++++++++++++++++

If something terrible goes wrong in your local repository, you can reset it to the current server version using the
following sequence of commands.

.. code-block:: console

   cd path/to/my/reactor
   git fetch --all
   git reset --hard origin/master

If you want to reset to a specific branch, replace the last line with

.. code-block:: console

   git reset --hard origin/<branch_name>

.. attention::

   This will overwrite **all** local changes, even if they were committed. To preserve uncommitted changes, you
   can stash them prior to the reset using:

   .. code-block:: console

      git stash

   then restore them with

   .. code-block:: console

      git stash pop

   after resetting the repository.





