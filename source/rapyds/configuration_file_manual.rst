Configuring Hosts
*****************

Introduction
============

Configuration files are used to configure target codes on local and remote hosts. It is also used to set
access credentials to remote hosts, as well as some basic defaults for cluster environments.

The system first processes the *target_modes.cfg* file, located in the *rapyds* root directory, then any
file passed via the ``config_file`` parameter or *--config-file* command line argument. The latter file
then overrides or extends values defined in the former. It is therefore recommended not to modify
the system *target_modes.cfg* directly, but rather create a number of additional configuration file(s) for all hosts
you intend to run codes on.

Configuration file syntax
-------------------------

Configuration file input uses the standard *cfg* or (*ini* on Windows) syntax:

    - Section headers are enclosed in square brackets:

      .. code-block:: ini

         [SECTION]

    - Parameters in a section are set using keyword value pairs, separated by a colon or equal sign:

      .. code-block:: ini

         key1 : value1
         key2 = value2

    - Comments can be inserted anywhere using the hash '#' character

Host configuration
==================

A remote host is configured under the **[HOST]** section, and accepts the following parameters:

.. parameter:: name
   :type: :term:`string`
   :default: None

   Host name. Either a fully qualified domain name, or ip address. This parameter is required.

.. parameter:: username
   :type: :term:`string`
   :default: None

   Username on host machine. Optional, the system will prompt for it when required.

.. parameter:: password
   :type: :term:`string`
   :default: None

   User password. Optional, the system will prompt for it when required.

.. parameter:: directory
   :type: :term:`string`
   :default: None

   Base working directory on the host machine.  Defaults to the user's home directory on the host.

.. parameter:: protocol
   :type: options
   :default: None

   Defines how jobs are executed on the host. Available options are:

        - *SSH* for basic remote execution,
        - *SSH_Batch* for detached remote execution, and
        - *PBS* or *PBS_pro* for clusters running a portable batch system.

   .. note::

      In normal *SSH* mode, your local terminal wil have ownership of the remote task, and closing it will stop
      execution. To avoid this, use the *SSH_Batch* mode instead.

If :param:`protocol` is *PBS* or *PBS_pro* the following parameters can also be set:

.. parameter:: queue
   :type: :term:`string`
   :default: None

   The default queue jobs will be submitted on. Will use the default queue configured on the cluster if not set.

.. parameter:: project
   :type: :term:`string`
   :default: None

   Project name from which resources should be assigned. Passed via the *-P* switch in the job submission script.

.. parameter:: hook
   :type: :term:`string`
   :default: None

   Command that will be added to any submission script. Useful for setting environment variables or sourcing modules.

.. parameter:: spool
   :type: :term:`directory`
   :default: '/var/spool/PBS/spool'

   Directory were output is spooled. Required for correct functioning of the remote `peek` mode. Contact the system
   administrator if you are not sure what this path is.

.. attention::

   A configuration file can only have one **[HOST]** entry!


Simple host configuration example
---------------------------------

The following example configures a simple remote host for running jobs via *ssh*:

.. code-block:: ini

   [HOST]
   name : my.remote.machine
   username : joe
   protocol : SSH

Example of configuring a remote cluster running a PBS
-----------------------------------------------------

The following example shows how to configure a remote cluster:

.. code-block:: ini

    [HOST]
    name = hpc.somewhere.net
    username = ABC-TGB
    password = *8927(k
    directory = scratch
    protocol = PBS
    project = INDY0788
    queue = guest
    hook = module add mpi/openmpi-1.8.8-gnu


Target mode configuration
=========================

Each target code supported by the system needs to have a section in the configuration file, with the section header
corresponding to the name with which this mode will be identified,  that is, when passed to the ``target_mode`` parameter
or *--target-mode* command line flag. The parameters that can be set in the configuration file, are split into:

    1. Parameters that only need to be set once, that is, they do not depend on the execution host. These are usually
       set within the  system *target_modes.cfg* file.
    2. Parameters that depend on the execution host, for instance, the location of data files and executables. These
       are set within host specific configuration files, which will inherit the base (static) configurations from
       *target_modes.cfg*.

Static target mode configuration
--------------------------------

The following table lists the parameters which form the basis of how the system interacts with a target code.

.. parameter:: root
   :type: :term:`directory`
   :default: None

   Root path of input templates. Input templates define how the system creates input for the target code. This
   parameter is required.

.. parameter:: manage_own_inventory
   :type: :term:`bool`
   :default: False

   Flag indicating if the target code has an inventory management system. If ``False`` (the default), the system
   will use its own code independent inventory management for the target mode.

.. _link_internal_cycle_critical_configurations:

.. parameter:: internal_cycle_critical_configurations
   :type: :term:`bool`
   :default: False

   Flag indicating if the target code automatically calculates critical configurations at the end of depletion steps.
   If ``False`` (the default), the system will generate critical calculations at the end of each required depletion step,
   which can then be executed after completion of the burnup calculation.

.. parameter:: dynamic_banks
   :type: :term:`bool`
   :default: True

   Flag indicating if the target code can dynamically move banks during an irradiation history. If ``False``,
   irradiation histories will be broken into pieces with constant bank, with new input files generated at each step
   where the bank changes. The burnup calculation will then chain these calculations together in order to complete the
   irradiation history.

.. parameter:: state_parameter_maps
   :type: Custom
   :default: None

   Remap unsupported :term:`state` parameter values to ones supported by the target code. This is only applicable to
   target codes that have internal cross section models, which does not respond directly to the state perturbation
   mechanisms.

   The entry has the following syntax:

   .. code-block:: ini

      {SP1} <- UP1[sf], UP2[sf], ... ;{SP2} <- UP1[sf], UP2[sf], ...

   where:

      - *SPi* are :term:`state` parameter values supported by the target code.
      - *UPi* are :term:`state` parameter values that are not supported internally by the target code.
      - *sf* is an optional scaling factor that will be applied to the value when applying the mapping. It is either
        a scalar value, or dimensioned if *SP* and *UP* have different dimensions.

   For example:

   .. code-block:: ini

      state_parameter_maps = poison_concentration <- reflector_purity[10.0]

   will map `reflector_purity` to the the soluble poison concentration state parameter `poison_concentration`. This
   means that, if `reflector_purity` state is defined to be :math:`x`, it will trigger `poison_concentration` as
   :math:`10x` in the target code.

   .. note::

      Only valid :term:`state` tags are allowed.

   .. attention::

      This mapping should also be applied when creating homogenized mixtures.

.. note::

   For local configurations, you can use ``$`` to access environmental variables, and ``%`` to access
   the following known system paths:

        - TEMPLATES: The built in *rapyds/templates* directory
        - DATA: Location of the *rapyds/data* directory
        - RAPYDS: Root installation of the *rapyds* package


The following example adds the target codes MCNP and MGRAC:

.. code-block:: ini

    [MCNP]
    root = '%TEMPLATES/mcnp'
    dynamic_banks = False

    [MGRAC]
    root = '%TEMPLATES/OSCAR4_v1.0/MGRAC'
    manage_own_inventory = True
    internal_cycle_critical_configurations = True

Modifications of these static parameters should only appear in your local configuration file if:

    1. You have customized the templates and moved them to a new location, or
    2. You have implemented a new target mode or an alias of an existing target mode. Aliases are useful if you want to
       target different versions that use the same input structure, for instance

       .. code-block:: ini

          [MCNP6]
          executable = '/to/bin/mcnp6'

          [MCNP6.2]
          root : '%TEMPLATES/mcnp6'
          dynamic_banks = False
          executable = '/to/bin/mcnp6.2'


Host specific target mode configuration
---------------------------------------

The following table lists all parameters that depend on the execution host:

.. parameter:: executable
   :type: :term:`filepath`
   :default: None

   Full path to target code executable on the host.

.. parameter:: version
   :type: :term:`string`
   :default: None

    String giving the version of the executable. Use the standard major.minor.update notation, e.g. '2.1.23'.

    .. note::

       Although this parameter is optional, the system occasionally checks for this parameter to activate or
       deactivate certain target code specific features.

.. parameter:: library
   :type: :term:`filepath`
   :default: None

   Full path to cross section library or directory file on the host.

.. parameter:: library_filter
   :type: :term:`regex`
   :default: None

   Filter used to reduce cross section library. This parameter can be used to select only particular evaluations or
   isotopes from a large library file. It accepts any standard regular expression, and will only extract
   entries for which this expression returns a math. For instance, in an ACE directory file, the following will only
   extract entries ending in '7*c':

   .. code-block:: ini

      library_filter = '(\d+.7\dc)'

   Use the pipe '|' to string together expressions. For instance, the following will also match natural element
   entries from any evaluation, and thermal libraries ending in '1*t':

   .. code-block:: ini

      library_filter = '(\d+.7\dc)|(\d+000.\d{2}c)|(*.1\dt)'


.. parameter:: thermal_libraries
   :type: custom
   :default: None

   Description of compositions for which thermal scattering data is available. Although the system does have a
   mechanism to automatically deduce thermal libraries for certain isotopes, it is better to standardize how thermal
   compositions are treated using this parameter. It contains a list of compositions, separated using ';',
   and the composition has the form:

    .. code-block:: none

        {COMPOSITION}({ISOTOPE1} -> '{TAG1}', {ISOTOPE2} -> '{TAG2}', ...)

   where:

        - ``COMPOSITION`` is the tag used to identify the composition. This tag is passed to the ``thermal_composition``
          parameter of material instances.
        - ``ISOTOPEi`` are standard :term:`isotope` identifiers, e.g. H-1.
        - ``TAGi`` are the library tag (without extension) as it appears in the library file.

   For example, the following defines light and heavy water:

       .. code-block:: ini

          thermal_libraries = H2O(H-1 -> 'lwtr');D2O(H-1 -> 'lwtr',H-2 -> 'hwtr')

   Although you can give your compositions any name, it is recommended (for compatibility) to use the following set of
   thermal composition tags:

   +-----------+------------------+
   |Tag        |Description       |
   +===========+==================+
   |*H2O*      |Light water       |
   +-----------+------------------+
   |*D2O*      |Heavy water       |
   +-----------+------------------+
   |*ZrH*      |Zirconium Hydride |
   +-----------+------------------+
   |*Be*       |Natural beryllium |
   +-----------+------------------+
   |*BeO*      |Beryllium Oxide   |
   +-----------+------------------+
   |*Graphite* |Graphite          |
   +-----------+------------------+

   .. attention::

     Compositions are separated using a semi colon ``;``, while thermal isotopes within the composition are
     delimited using a comma ``,``.


.. parameter:: library_path
   :type: :term:`directory`
   :default: None

   Full path to directory containing data files on the host. It is usually set to the directory containing all additional
   data files.

.. parameter:: decay
   :type: :term:`filepath`
   :default: None

   Full path to isotope decay data on the host . It is only required for target codes needing additional decay data, which cannot be
   derived from :param:`library_path`.

.. parameter:: fission_product_yields
   :type: :term:`filepath`
   :default: None

   Full path to fission product yield data on the host. It is only required for target codes needing additional yield data,
   which cannot be derived from :param:`library_path`.

.. attention::

   All paths must be **absolute** on the target host. If no **[HOST]** is specified, the local host is assumed.


Defining additional modes
-------------------------

You can create a new mode, based on an existing mode, using the `like` keyword. This is useful,
for instance if you want to use two versions of the same code. For example,

.. code-block:: ini

   [SERPENT29]
   like = SERPENT
   version = 2.1.29
   executable = '/shared/bin/sss2.1.29'

will add an additional target mode using the same input templates, but a different version.

.. note::

   The input templates will branch based on this version tag to ensure that only input options available in this
   version are used.

Complete host configuration file example
========================================

The following listing gives a complete example of a remote host configuration file. In this particular example, we
configure the target modes *MCNP*, *MCNP6*, and *SERPENT*:

.. code-block:: ini

    [HOST]
    name = 172.18.14.51
    username = francois
    directory = scratch
    protocol = PBS

    [SERPENT]
    executable = '/shared/bin/sss2.1.23-m2'
    version = '2.1.23'
    library_path = '/shared/libraries/transmutation'
    library = '/shared/libraries/ace/sss_endfb7.xsdata'
    decay = '/shared/libraries/transmutation/endfb7.dec'
    fission_product_yields = '/shared/libraries/transmutation/endfb7.nfy'
    thermal_libraries = H2O(H-1 -> 'lwe7');ZrH(H-1 -> 'h/zr',Zr-Nat -> 'zr/h'); D2O(H-1 -> 'lwe7',H-2 -> 'hwe7'); Be(Be-9 ->'be')

    [MCNP6]
    like = MCNP
    executable = '/shared/bin/mcnp6.1.1'
    version = '6.1.1'
    library = '/shared/libraries/ace/xsdir'
    library_filter = 'r(\d*.7\dc)|(\d+000.\d{2}c)|(.*w.*\.1\dt)|(gr.*\.1\dt)|(be\.1\dt)'
    library_path = '/shared/libraries/ace/xdata'
    thermal_libraries = H2O(H-1 -> 'lwtr');ZrH(H-1 -> 'h/zr',Zr-Nat -> 'zr/h'); D2O(H-1 -> 'lwtr',H-2 -> 'hwtr'); Be(Be-9 ->'be'); Graphite(C-Nat -> 'grph')

    [MCNP]
    executable = '/shared/bin/mcnp5.1.51'
    version = '5.1.51'
    library = '/shared/libraries/ace/xsdir'
    library_filter = 'r(\d*.7\dc)|(\d+000.\d{2}c)|(.*w.*\.1\dt)|(gr.*\.1\dt)|(be\.1\dt)'
    library_path = '/shared/libraries/ace/xdata'
    thermal_libraries = H2O(H-1 -> 'lwtr');ZrH(H-1 -> 'h/zr',Zr-Nat -> 'zr/h'); D2O(H-1 -> 'lwtr',H-2 -> 'hwtr'); Be(Be-9 ->'be'); Graphite(C-Nat -> 'grph')



