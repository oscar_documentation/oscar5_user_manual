.. role:: py(code)
   :language: py

General Application Command Line Interface (CLI)
************************************************

Currently, the most common way to interact with an application is using the unified Command Line Interface (CLI).
Although each application mode has a slightly different interface, they all share a number of common options and modes,
which is described here.

Applications are launched from the command line by passing the input script to the python interpreter. The easiest way
to achieve this, is using the :command:`oscar5` (or :command:`rapyds-cli`) wrapper script, which has the following
signature:

.. code-block:: console

   $oscar5 MY_REACTOR.package.my_module [app options] [mode [mode options]]

Here, we assume that you are running the script from the root *MY_APPS* directory. The number of sub-packages to get
to your module can be of any length, separated by a '.'

.. note::

   The '.py' extension of the input module is omitted.

Although the :command:`oscar5` script simply wraps a :command:`python -m` call, it also adds some basic :command:`bash`
:command:`<tab>` and :command:`<tab><tab>` auto completion features.

.. note::

   Tab completion is currently only available on :command:`bash` shells, and not in Windows :command:`cmd` terminals.
   Extended tab completion is under development for :command:`PowerShell`, which will then also make this feature
   available on the Windows operating system.

The available :command:`<app options>` and :command:`modes` are described in `Setting or modifying parameters using command line arguments`_
and `Application modes`_ respectively. They can also be viewed by passing the :command:`--help` flag:

.. code-block:: console

   $oscar5 MY_REACTOR.package.my_module --help

.. attention::

   If you are working in a terminal or *Anaconda Prompt*, remember to activate the correct environment first:

   .. code-block:: console

      $ conda activate rapyds


Setting or modifying parameters using command line arguments
------------------------------------------------------------

This section describes the available :command:`app options`.

.. option:: --log-level <str>

   Determines the amount of log information printed to the log file. The following options are supported, listed from
   the most to least amount of log information:

   .. list-table:: Log level options.
      :class: tight-table
      :widths: 1 2

      * - **DEBUG**
        - Prints a lot of additional information, which typically only makes sense for the developers. Use this level
          if additional information is required in order to track down a problem.
      * - **INFO**
        - Prints some additional runtime information to the log file.
      * - **WARN**
        - Only print warnings and errors.
      * - **ERROR**
        - Print only errors.

   The default value is **INFO**.


Most of the :ref:`general <general_app_parameters>` parameters can be customized on the command line using the following flags:


.. option:: --config-file <str>

   Targets :attr:`app_param.config_file`.

.. option:: --target-mode <str>

   Targets :attr:`app_param.target_mode`. This flag can be used to run the same module using multiple target codes.

.. option:: --doppler-broadening

   Targets :attr:`app_param.doppler_broadening`.

.. option:: --source-iteration <int>

   Targets :attr:`app_param.source_iteration`.

.. option:: --max-iteration <int>

   Targets :attr:`app_param.max_iteration`.

.. option:: --particles <int>

   Targets :attr:`app_param.particles`.

.. option:: --convergence_criteria <float>

   Targets :attr:`app_param.convergence_criteria`.

.. option:: --error-tolerance <float>

   Targets :attr:`app_param.error_tolerance`.

.. option:: --cross-section-library <str>

   Targets :attr:`app_param.cross_section_library`.

.. option:: --decay-data-library <str>

   Targets :attr:`app_param.decay_data_library`.

.. option:: --fission-product-yields <str>

   Targets :attr:`app_param.fission_product_yields`.

For instance, to set the configuration file :

.. code-block:: console

   $ oscar5 python -m MY_REACTOR.cases.my_app --config-file my_config.cfg

.. note::

   Values specified on the command line are **optional**, and will override the parameter values set in the input module.

.. attention::

   Since these parameters generally affect all sub modes, the are specified before any mode on the command line.

Application modes
-----------------

All applications have numerous sub, or run, modes, each with its own set of command line flags. The following
table summarizes the general modes:

.. table:: Common application modes.
   :class: tight-table

   ================  ==================================================================================================
   Run Mode          Description
   ================  ==================================================================================================
   archive_          Compress the entire contents of :attr:`app_param.working_directory` into a single archive file.
   clean_            Clears files and directories from the :attr:`app_param.working_directory`.
   visualization_    Visualize the underlying model.
   pre_              Pre-processing and input creation.
   execute_          Execution using :attr:`app_param.target_mode`.
   post_             Post-process results from output file(s).
   run_              Create input, launch application and wait for results to be available.
   doc_              Generate and view attached document (if activated).
   ================  ==================================================================================================


All the run modes have their own help flag, listing the available options for that particular mode. For instance,

.. code-block:: console

    $oscar5  MY_REACTOR.package.my_module execute --help

.. _archive:

Archive mode
++++++++++++

This mode is used to compress the entire :attr:`app_param.working_directory` into a single archive file. It accepts the following
command line arguments:

.. program:: archive

.. option:: --format <str>

   Compression format. The available formats is determined by the `shutil archive <https://docs.python.org/2/library/shutil.html>`__
   module, and depends on which compression libraries are available on your system:

     - 'zip': ZIP file (if the :command:`zlib` module or external zip executable is available).
     - 'tar': uncompressed tar file.
     - 'gztar': gzip'ed tar-file (if the :command:`zlib` module is available).
     - 'bztar': bzip2'ed tar-file (if the :command:`bz2` module is available).

   The default value is 'tar`.

.. option:: --name <str>

   Archive file name. Defaults to :attr:`app_param.project_name`. An extension will be automatically appended depending on
   :option:`archive --format`.

For example, the following will create a zip archive :attr:`app_param.project_name`.zip:

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module archive --format zip


.. _clean:

Clean mode
++++++++++

This mode clears all generated input and output from the :attr:`app_param.working_directory`. It accepts the following
command line flags:

.. program:: clean

.. option:: --keep_tree

   Delete only files, that is, keep directory structure.

.. option:: --remote_host

   Also clear input and output files on the remote host.


.. _visualization:

Visualization mode
++++++++++++++++++

Three dimensional visualization of the underlying model. Accepts the following command line flags:

.. program:: visualization

.. option:: --interactive

   Launch interactive `VTK <http://www.vtk.org>`__ based viewer.

.. option:: --parts

   When combined with the interactive flag, displays a more sophisticated interactive viewer with a tree showing
   model components.

   .. note::

      This will become the default view mode in future releases, as the materials based viewer will be deprecated.

.. option:: --freecad

   Export model to `FreeCAD <https://www.freecadweb.org>`__.

If no flags are passed, a simple `VTK <http://www.vtk.org>`__ view window is displayed.

For example, to view the model in an interactive frame:

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module visualization --interactive --parts

.. note::

   The FreeCAD export requires that the FreeCAD python API is in the python path. This mode is also disabled for
   certain models.

.. _pre:

Pre-processing mode
+++++++++++++++++++

Process input data, and create input for the :attr:`app_param.target_mode`. Accepts the following command line flags:

.. program:: pre

.. option:: --force

   Overwrite any existing input file(s). If this flag is omitted, existing input files will be used.

   .. attention::

      Since input depends on the remote host, be sure to add this flag if :attr:`app_param.config_file` is changed!

Input file(s) are written to a :attr:`app_param.target_mode` sub directory under :attr:`app_param.working_directory`.

.. note::

   Use the :command:`pre` mode if you want to view or modify input files before program execution.

.. _execute:

Execution mode
++++++++++++++

This mode is used to execute :attr:`app_param.target_mode` executables on the on the target host, and examine running applications.
The following flags can be used to customize :ref:`application parameters <general_app_parameters>` related to code
execution:

.. program:: execute

.. option:: --force

   Overwrite any existing input file(s), and also ignore any existing input.

.. option:: --executable <str>

   Targets :attr:`app_param.executable`.

.. option:: --arguments <str>

   Targets :attr:`app_param.arguments`.

.. option:: --use-rsync

   Targets :attr:`app_param.use_rsync`.

.. option:: --threads <int>

   Targets :attr:`app_param.threads`.

.. option:: --node <int>

   Targets :attr:`app_param.nodes`.

.. option:: --queue <str>

   Targets :attr:`app_param.queue`.

.. option:: --walltime <float>

   Targets :attr:`app_param.walltime`.

.. note::

   This mode will create input files if none exist, so there is no need to run in :command:`pre` mode first. However,
   if you plan to modify input files, use :command:`pre` mode, and **don't** pass the :option:`--force` flag to
   :command:`execute` mode.

For example, the following will launch the application on the host defined in @a my_config using 20 threads:

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module --config-file my_config.cfg execute --threads 20


For running applications, the following flags can be used:

.. option:: --status

   Get the status of a running application.

.. option:: --peek

   Stream the output from a running application.

   .. attention::

      When running on a remote host, make sure that the :attr:`spool` parameter is set correctly.

.. option:: --kill

   Kill a running application.

For example,

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module --config-file my_config.cfg execute --status


.. _post:

Post-processing mode
++++++++++++++++++++

Copy (sync) output files from the run host, parse results from output files and generate results summary. Accepts the
following command line flags:

.. program:: post

.. option:: --force

   Fore re-processing of output. Will generate a new result archive even if an existing is found.

.. option:: --use-rsync

   Use :command:`rsync` to copy files from a remote host. Equivalent to setting the :attr:`app_param.use_rsync` parameter. Only
   relevant if a remote host is used.

.. option:: --show

   Initiate result viewer GUI (if available).

.. option:: --compare <str>

   Compare results with another target mode. Output for the target mode must already exist.

For example, the following will simply process output and generate a result archive:

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module post

.. attention::

   If you passed a configuration file to the execute_ mode, be sure to use the same one in this mode, otherwise
   results will not be retrieved from the correct host:

   .. code-block:: console

       $oscar5  MY_REACTOR.package.my_module --config-file my_config.cfg post

.. _run:

Run mode
++++++++

This essentially combines the pre_, execute_ and post_ modes. Supports all the execute_ mode flags. For example,

.. code-block:: console

   $oscar5  MY_REACTOR.package.my_module run --force

will, recreate the input, execute the application and post process the result.

.. attention::

   When running on a remote host, for example,

   .. code-block:: console

      $oscar5  MY_REACTOR.package.my_module --config-file my_config.cfg run --threads 24

   the application will wait (pining the remote machine periodically) until execution on the remote host has finished,
   before copying output files back to the local machine. Thus, the terminal on the local machine must not be closed,
   and access to the remote machine must be maintained.

.. _doc:

Document generation mode
++++++++++++++++++++++++

Create and view the attached documentation.

.. attention::

   This mode is only available if the :ref:`docgen parameter set <docgen_parameters>` has been activated. See
   :ref:`Activating documentation input`.

Accepts the following command line flags:

.. program:: doc

.. option:: --preview

   Only create the document :ref:`source tree <documentation_source_tree>` (if it does not exist), and expand macros
   without generating targets.

   .. note::

      Is equivalent to running the standalone :ref:`docgen <docgen>` application in :ref:`pre <docgen_pre>` mode.

.. option:: --skip-html

   Don't generate HTML output.

.. option:: --skip-pdf

   Don't generate LaTeX output.

.. option:: --force

   Force running the pre-processor, and rebinding of variables.

.. option:: --show

   View the generated output. On most platforms, it will be opened in a browser window.

Nested application modes
------------------------

Many applications contain sub applications, which are then exposed in the CLI as additional modes.
