.. _critical_case:
Steady State Neutron Flux Calculations
**************************************

This application mode is used for calculating the equilibrium fission source distribution (and related output
values), at a fixed model state.

.. note::

   The name of the application is a bit misleading, as it is not intended only to be used for models in a critical
   state. In fact, it is frequently used to calculate :math:`k_{eff}` for sub- and super critical configurations.
   However, most of the other outputs (such as power and flux distributions), only make sense for a critical state.

.. _critical_case_typical_use_cases:

Typical Use Cases
=================

  1. Calculate the effective multiplication factor :math:`k_{eff}`.
  2. Compute the equilibrium power distribution, and maximum power density (peaking factor).
  3. Compute various flux distributions (and other responses).
  4. Calculate kinetics data (e.g. :math:`\beta_{eff}`).

.. _critical_case_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager critical_case MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'projects'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --configuration <str>

   The configuration module that contains the model used in this script.

.. option:: --mco <str>

   Name (or path) of the model configuration archive that should be used in this script.

   .. attention::

      This option can not be specified when :option:`--configuration` is also specified.

.. option:: --time-stamp <str>

   Time point at which facility state should be recovered (see :attr:`app_param.time_stamp`). Must be a
   valid :term:`datetime`.

A typical input module has the following form:

.. code-block:: py

   import applications.critical_case as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   ccp = app.parameters()

   # Set some base application parameters
   cpp.time_stamp = '01-01-2020 00:00:00'

   # Set and modify the model
   cpp.model = model
   cpp.model.set_banks(control.percent_extracted(50))
   # etc

   # Application specific
   cpp.power = 1 * units.MW
   # etc

   if __name__ == '__main__':
    app.main(parameters=cpp)


.. _critical_case_parameters:

Additional Input Parameters
===========================

In the following `ccp` denotes a `critical_case` parameter set, created as follows:

.. code-block:: py

   import applications.critical_cases as app

   cpp = app.parameters()

.. current_parameter_set:: cpp
   :synopsys: applications.critical_case.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`, as well as the following:

.. parameter:: power
   :type: :term:`power`
   :default: 1 W

   The power used to normalize flux levels.

.. parameter:: power_maps
   :type: :term:`bool`
   :default: True

   Flag indicating that channel power distributions should be calculated.

.. parameter:: mass_map
   :type: :term:`bool`
   :default: False

   If the flag is set, a summary of the current core loading will be written (as a comment block) to the input
   file.


.. parameter:: kinetics_parameters
   :type: :term:`bool`
   :default: False

   Flag indicating that kinetics parameters should be calculated.

.. parameter:: fueled_primitive_powers
   :type: :term:`bool` or :term:`tuple`
   :module: cpp.model

   Set the mesh for detailed power distribution calculations. If set to True, the default mesh (if available) will be
   used. Otherwise two integers can be specified, with the values denoting the number of radial and axial meshes
   each fueled primitive will be subdivided by.

   How these numbers are interpreted depends on the fuel type present. For example, if the model contains plate
   assemblies

   >>> cpp.model.fueled_primitive_powers = 6, 8

   will de divide each plate length into 6 equal pieces and 8 axial pieces. Thus, power will be estimated at 48
   positions in each plate. When pin or dispersed fuel type assemblies are used, the radial subdivision is ignored,
   and pins or compacts are only subdivided axially.

   To calculate only the average power in each primitive (plate, pin or compact), use

   >>> cpp.model.fueled_primitive_powers = 0, 0

   .. note::

      For nodal codes, which typically rely on pre-tabulated data for power reconstruction, the subdivision data is
      ignored, and the system only checks if this flag was set.

.. py:function:: add_response(r, tag=None)

   Adds a response that should be calculated based on the equilibrium flux distribution.

   :param r: The target response object.
   :param tag: Re-specify the response tag.
   :type tag: :term:`string`


.. _critical_case_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`.

.. _crytical_case_typical_cli:

Typical command line usage
--------------------------

.. code-block:: console

   oscar5 MY_REACTOR.projects.my_case --target-mode <MODE> --config-file <CONFIG> run --threads 24

.. _critical_case_output_tokens:

Output Tokens
=============

This application mode hase an extensive set of output tokens, which are grouped by topic in the following sections.
See also :ref:`app_output_tokens` for general guidelines on using these tokens.

.. py:currentmodule:: critical_case

General output tokens
---------------------

.. py:function:: multiplication_factor(**kwargs)

   Extract the effective multiplication factor :math:`k_{eff}`.

.. py:function:: critical_position(bank_names=None, **kwargs)

   Extract the bank position(s) used in the calculation.

   :param bank_names: Specify which bank position(s) should be extracted. If not specified,
      the positions of all the banks will be retrieved. These names must correspond to the model bank names, as
      defined in :attr:`model.banks`.
   :type bank_names: :term:`string` or :term:`list` [:term:`string`]

   The return type depends on the value of `bank_names`:

      1. If a single :term:`string` value, a single :term:`travel` value, given that bank's position.

      2. If it is a list of bank names, a :term:`dict` mapping these names to their :term:`travel` values.

      3. If None (the default), a :term:`dict` mapping all :attr:`model.banks` to their :term:`travel` values.

   .. note::

      If :attr:`app_param.bank_search` was not set, this will simply return the input bank positions, as defined
      using :func:`model.set_banks`.

Output tokens related to power distribution
-------------------------------------------

.. py:function:: power_distribution(**kwargs)

   Extracts the power fraction map. Will return a :term:`labeledgrid` or :term:`hexagonal grid`, depending on the core
   configuration. Entries are fractions in percentages.

.. note::

   The following parameters are only available if :attr:`cpp.model.fueled_primitive_powers` was set.

.. py:function:: average_power_density(**kwargs)

   The average power density over all fueled regions, with the size of the volumes determined by the
   :attr:`model.fueled_primitive_powers` parameter.


.. py:function:: average_heat_flux(**kwargs)

   The average heat flux over all fueled regions, with the size of the areas determined by the
   :attr:`model.fueled_primitive_powers` parameter.

.. py:function:: maximum_power_density(**kwargs)

   The maximum power density over all fueled regions, with the size of the volumes determined by the
   :attr:`model.fueled_primitive_powers` parameter.

.. py:function:: maximum_power_density_channel(**kwargs)

   The core channel (or position) where the maximum power density occurred.

.. py:function:: maximum_heat_flux(**kwargs)

   The maximum heat flux over all fueled regions, with the size of the areas determined by the
   :attr:`model.fueled_primitive_powers` parameter.

.. py:function:: maximum_heat_flux_channel(**kwargs)

   The core channel (or position) where the maximum heat flux occurred.

.. py:function:: maximum_primitive_power(**kwargs)

   The maximum fueled primitive (plate, pin or fueled compact) power.

.. py:function:: maximum_primitive_power_channel(**kwargs)

   The core channel (or position) where the maximum primitive power occurred.

.. py:function:: maximum_primitive_power_index(**kwargs)

   The index of the primitive with maximum power. See the :ref:`assembly type <Choosing an Assembly Class>`
   documentation on how primitives are indexed.

.. py:function:: primitive_power_shape(position, slice='z', metric='total', total=None, **kwargs)

   Create a one dimensional slice through a calculated power distribution.

   :param position: The core channel from which the distribution should be extracted.
   :param slice: The coordinate along which the slice should be taken. Must be one of the standard cartesian coordinates
     :math:`x,y,z`. For :term:`hexagonal grid` layouts, the :math:`x, y` coordinates are interpreted as the continuous
     versions of :math:`q, r` indices in the axial coordinate system.
   :param metric: How values in the coordinate plane perpendicular to `slice` should be treated. The following options are
      allowed:

      .. table:: Slice accumulation options.
         :class: compact-table
         :align: center

         +---------------------------+--------------------------------------------------------------------------+
         | Option                    | Description                                                              |
         +===========================+==========================================================================+
         | `'total'`                 | Sum over all values in the perpendicular plane.                          |
         +---------------------------+--------------------------------------------------------------------------+
         | `'average'`               | Average over all values in the perpendicular plane.                      |
         +---------------------------+--------------------------------------------------------------------------+
         | `'max_density'`           | At the position with maximum power density.                              |
         +---------------------------+--------------------------------------------------------------------------+
         | `'max_flux`'              | At the position with maximum heat flux.                                  |
         +---------------------------+--------------------------------------------------------------------------+

   :param total: Use this parameter to indicate that a summation should be performed along a certain dimension. It can
     be any of the two coordinate values spanning the plane perpendicular to `slice`.

   This will return two values, the first is a list of :term:`length` values denoting the coordinate positions along
   which the `slice` is taken, and the second a list of :term:`power` values.

   For example, to extract the total axial shape, use

   >>> data = critical_case.primitive_power_shape('A1', slice='z', metric='total')

   Or, to get the axial shape along the hottest pin

   >>> data = critical_case.primitive_power_shape('A1', slice='z', metric='max_density')

   .. note::

      Since plates are usually segmented, the `total` parameter must be used to extract axial power shapes. For
      example, to get the total axial power shape in the hottest plate, assuming the plate extends along the
      :math:`x`-axis,

      >>> data = critical_case.primitive_power_shape('A1', slice='z', metric='max_density', total='x')


Output tokens related to flux distribution
------------------------------------------

.. py:function:: average_channel_thermal_flux(flt=None, **kwargs)

   Extract the average thermal (:math:`<0.625` eV) flux distribution in the core.

   :param flt: Limit the extraction to certain positions. Specified as a grid, with non-empty positions indicating core
     channels for which values are required. For example,

     .. code-block:: py

        flt = \
         [[_, 'A', 'B', 'C'],
          [1,   _,  1,    _],
          [2,   1,  _,    _],
          [3,   _,  _,    1]]

     will only extract fluxes in positions `'1A'`, `'2B'` and `'3C'`.
   :type flt: :term:`labeledgrid` or :term:`hexagonal grid`

   Returns a :term:`labeledgrid` or :term:`hexagonal grid` containing the average flux in each channel.

.. py:function:: average_channel_fast_flux(flt=None, **kwargs)

   Similar to :func:`critical_case.average_channel_thermal_flux`, except that the fast flux (:math:`> 0.82085` MeV)
   is extracted.

.. py:function:: core_flux_map(**query, **kwargs)

   Extract core flux distributions. Similar to :func:`critical_case.average_channel_thermal_flux` and
   :func:`critical_case.average_channel_fast_flux`, but with more control over the range of flux values extracted.

   :param **query: Sequence of :ref:`distribution query <querying_distributions>` values, which are used to specify
      exactly which flux values are extracted.

   Returns a :term:`labeledgrid` or :term:`hexagonal grid` with the queried flux values in each channel.

   For example, to extract flux in a specify energy and axial range:

   >>> epi=critical_case.core_flux_map(e_min=0.625 * units.eV, e_max=0.82085 * units.MeV, z_min=-30 * units.cm, z_max=30 * units.cm)

.. py:function:: core_flux_distribution(channel, **query, **kwargs)

   Extract detailed flux distribution within a core channel.

   :param channel: Core position.
   :param **query: Sequence of :ref:`distribution query <querying_distributions>` values, which are used to specify
      exactly which flux values are extracted.

   The returned value depends on the `**query`: If a :term:`list` of values is requested, a :term:`list` is returned.
   Otherwise a singe flux value is returned.

   For example, to extract axial thermal flux shape at specified points:

   >>> th = core_flux_distribution(channel='A1', e_max=0.625 * units.eV, z=[-15 * units.cm, -10 * units.cm, -5 * units.cm])


Kinetics parameter related output tokens
----------------------------------------

.. note::

   The following output parameters are only available if the :attr:`cpp.kinetics_parameters` flag was set.

.. py:function:: beta_effective(**kwargs)

   Extract the calculated :math:`\beta_{eff}` value.

.. py:function:: delayed_fractions(**kwargs)

   Extract the :math:`\beta` for each delayed precursor group. Returns a list of values, with length equal to the
   number of delayed group.

   .. attention::

      Currently, the returned values will sum to :math:`\beta_{eff}`, that is, they are not normalized.

.. py:function:: generation_time(**kwargs)

   Extract the average neutron generation time :math:`\Lambda`. Returned value has :term:`time` dimensions.

.. py:function:: decay_constants(**kwargs)

   Extract the decay constant (:math:`\lambda`) for each delayed precursor group.

.. _critical_case_examples:

Examples
========
