.. _bank_worth_application:

Calculating Bank or Rod Worths
******************************

Introduction
============

This application mode is used to estimate the reactivity worth of control structures in the core. It first performs a
base static criticality calculation, then a single perturbed criticality for each control structure. The reactivity is
then deduced as the difference between these two calculations.

Creating an input module
========================



Application parameters
======================

Apart from the :ref:`standard application parameters <Setting application parameters>`, this application accepts the
following parameters as input:


.. parameter:: base_position
   :type: :term:`travel` or :term:`dict`
   :default: `fully_extracted`


   The position of the banks for the base calculation. This is either a single :term:`travel` instance
   (meaning all banks are at the same position), or a :term:`dict` object (with bank names as keys), containing a
   position for each individual bank.

   For example, to fully extract all the banks,

   .. code-block:: py

      parameters.base_position = control.fully_extracted()

   or a different position per bank,

   .. code-block:: py

      parameters.base_position = {'bank_A' : control.fully_extracted(), 'bank_B': control.percentage_inserted(50)}

   .. note::

      Here the bank names refer to :ref:`configured model banks <Parameters related to control element configuration>`.

Without any additional input, the application will automatically calculate worths for all the banks
:ref:`configured in the model <Parameters related to control element configuration>`. You can customize this in two ways:

    1. By :ref:`redefining the model banks <Parameters related to control element configuration>`, that is, regrouping
       the control positions. For example, to automatically calculate the reactivity estimates for each rod, simply
       assign a unique bank name to each control rod or position.

    2.




