Data Types
**********


.. glossary::

   directory
        String value describing a valid **directory** on your system. Relative paths will always be relative from
        where the script is **executed**. Use the `path_relative_to` utility to create paths relative to the
        calling module:

        .. code-block:: py

           import core.utilities

           rel_path = core.utilities.path_relative_to(__file__, '../../my_rel_path')

        Environmental variables can be used by pre-pending a '$', as in *$HOME/path/*.

        .. warning::
           If you are working in a docker container, paths must either point to a valid volume mount, or a directory
           in the container itself.

   filepath
        String value describing a valid **file** location on your system. Relative paths will always be relative from
        where the script is executed.

        Environmental variables can be used by pre-pending a '$', as in *$HOME/path/my_file.txt*.

        .. warning::
           If you are working in a docker container, paths must either point to a valid volume mount, or a directory
           in the container itself.

   grid
       This data type represents a two dimensional array of values. It is constructed from a :term:`list` of lists,
       with each row a new list. For example,

       .. code-block:: py

           g = \
           [['a', 'b', 'b', 'd'],
            ['a', 'b', 'b', 'd'],
            ['a', 'b', 'b', 'd']]


       They can be used to specify rectangular (:term:`labeledgrid`), or hexagonal (:term:`hexagonal grid`) layouts.

       .. note::

          When using the grid to specify hexagonal layouts, the offset approach as outlined in
          :func:`HexagonalGrid.to_grid` should be followed.

   labeledgrid
        This data type is constructed from a :term:`list` of lists, with each row a new list. The first row is interpreted
        as column headers, and the first entry in subsequent rows are used as row labels. Thus, to create a
        :math:`m\times n` grid, you would specify a list containing :math:`m+1` lists, each of length :math:`n+1`. For
        example:

        .. code-block:: py

           from core import *

           g = \
           [['-', 'A', 'B', 'C', 'D'],
            ['1', 'a', 'b', 'b', 'd'],
            ['2', 'a', 'b', 'b', 'd'],
            ['3', 'a', 'b', 'b', 'd']]

           lb = utilities.create_labeled_grid(g)

        Row and column labels can be any :term:`string` or :term:`integer`. Values in the grid can be accessed using
        row and column labels:

        >>> lb[0][0]
        'a'

        or the row and column label:

        >>> lb['1A']
        'a'

        .. note::

           For most parameters expecting a labeled grid, the `utilities.create_labeled_grid` is not neccesary, as
           the system will automatically construct one from the raw data ``g``.

   hexagonal grid
        Hexagonal grid layouts are specified using the `axial <https://www.redblobgames.com/grids/hexagons/#coordinates-axial>`__
        :math:`(q, r)` coordinate system. This system is more convenient for circular layouts than the traditional grid
        based specification, although the latter is also supported (see :func:`HexagonalGrid.to_grid` and
        :func:`HexagonalGrid.from_grid`).

        For grids whose elements have a vertex on the :math:`x`-axis, the coordinate layout is shown in `flat_top`_,
        while grids with vertices on the :math:`y`-axis, is shown in `pointy_top`_.

        .. _flat_top:
        .. figure:: images/hex-x.png
           :scale: 25%
           :align: center

           Axial coordinates for a :math:`x`-axis aligned hexagonal grid.

        .. _pointy_top:
        .. figure:: images/hex-y.png
           :scale: 25%
           :align: center

           Axial coordinates for a :math:`y`-axis aligned hexagonal grid.

        Hexagonal grid elements are specified using the `utilities.HexagonalGrid` component, which has the following
        interface:

        .. code-block:: py

           grid = utilities.HexagonalGrid(orientation='x')
           grid[q] = r0, [e1, e2, ...]

        where,

          - `orientation` determines the orientation of the grid in the :math:`xy` plane. The following options are
            supported:

            .. _hexagonal_grid_orientation:
            .. table:: Hexagonal grid orientation options.
               :align: center
               :class: tight-table

               +--------------------------------------+-----------------------------------------------------+
               | Options                              |  Result                                             |
               +======================================+=====================================================+
               | 'x', 'flat' or `ALIGNMENT.X_AXIS`    | The lattice elements will have a vertex on the      |
               |                                      | :math:`x`-axis (see `flat_top`_).                   |
               +--------------------------------------+-----------------------------------------------------+
               | 'y', 'pointy' or `ALIGNMENT.y_AXIS`  | The lattice elements will have a vertex on the      |
               |                                      | :math:`y`-axis (see `pointy_top`_).                 |
               +--------------------------------------+-----------------------------------------------------+

          - `q` is the :math:`q` value in the axial coordinate system, and `r0` the *smallest* :math:`r` coordinate.
            The :term:`list` then specifies the actual grid entries, so that, :math:`(q, r_0)` contains `e1`,
            :math:`(q, r_0+1)` contains `e2` etc.

        This is best illustrated with some simple examples:

        .. code-block:: py

           grid = utilities.HexagonalGrid(orientation='x')
           grid[1] = -1, ['A', 'B']
           grid[0] = -1, ['C', 'D', 'E']
           grid[-1] = 0, ['F', 'G']

        will produce the following:

        .. _flat top example:
        .. figure:: images/hex-x-exp.png
           :scale: 25%
           :align: center

        For the other orientation,

        .. code-block:: py

           grid = utilities.HexagonalGrid(orientation='y')
           grid[1] = -1, ['A', 'B']
           grid[0] = -1, ['C', 'D', 'E']
           grid[-1] = 0, ['F', 'G']

        is equivalent to:

        .. _pointy top example:
        .. figure:: images/hex-y-exp.png
           :scale: 22%
           :align: center

        Grid elements are accessed using their :math:`(q,r)` coordinates, for example

        >>> grid[0][0]
        'D'

        >>> grid[-1][0]
        'A'

        Labels can also be assigned to grid entries, by passing a second list after the entry list when filling
        the grid:

        .. code-block:: py

           grid = utilities.HexagonalGrid()
           grid[1] = -1, ['A', 'B'], ['L1', 'L2']
           grid[0] = -1, ['C', 'D', 'E'], ['L3', 'L4', 'L5']
           grid[-1] = 0, ['F', 'G'], ['L6', 'L7', 'L8']

        In this case, elements can also be accessed using the label:

        >>> grid['L4']
        'D'

        The `utilities.HexagonalGrid` has a number of useful methods, which are summarized below.

        .. py:function:: HexagonalGrid.set_label(q, r, val)

           Sets the grid label at :math:`(q,r)`.

        .. py:function:: HexagonalGrid.fold()

           Symmetrically folds the grid across the :math:`q=0` axis. This allows one to only specify one halve
           (e.g. :math:`q>0` or :math:`q<0`) of a symmetric hexagonal grid.

        .. py:function:: HexagonalGrid.to_grid(filler=None, row_labels=None, column_labels=None)

           Converts the hexagonal layout to an equivalent rectangular grid.

           :param filler: What to place in rectangular grid for entries not in the actual hexagonal grid.
           :param row_labels: Optional row labels that should be assigned to the grid. If not specified, simple integer
                labels will be generated automatically.
           :param column_labels: Optional column labels that should be assigned to the grid. If not specified, simple integer
                labels will be generated automatically.

           This will return a :term:`labeledgrid`. For the `flat_top`_ case, the grid conversion will offset the
           columns. For the `flat top example`_:

           >>> print(grid.to_grid(filler=_))
                 1 2 3
               1 F C -
               2 G D A
               3 - E B

           In the `pointy_top`_ case, rows are offset, as illustrated below for the `pointy top example`_

           >>> print(grid.to_grid(filler=_))
               1 2 3
             1 C A -
             2 F D B
             3 - G E

        .. py:function:: HexagonalGrid.from_grid(grid)

           Construct the :math:`(q,r)` coordinate system from an equivalent rectangular grid.

           :param grid: Any square rectangular mesh.
           :type grid: :term:`labeledgrid` or :term:`list`

           The offset convention as described in :func:`HexagonalGrid.to_grid` should be followed.

        .. note::

           Since a `utilities.HexagonalGrid` will be automatically constructed from a rectangular grid using
           :func:`HexagonalGrid.from_grid`, hexagonal layouts can be specified just like in :term:`labeledgrid`.
           However, some operations like circular shuffling or element wise rotations, are easier to interpret and
           input using the axial :math:`(q,r)` coordinate system.

   isotope
       Any isotope identifier. Isotopes can be specified using any one of the following formats:

        - 'Element-A' as in 'U-235'
        - 'Z-Element-A' as in '92-U-235'
        - 'ZAID' as in '92235'

       In all the above, the '-' can be omitted, that is, 'U235' is also valid. To specify meta stable isotopes, append
       the character 'm' in any of the above notations, e.g. 'Am-242m'.

       Natural elements are specified by appending the '-Nat' to the element symbol, or just the element itself, e.g.
       'U-Nat' or 'U'.

   dict
        A standard python dictionary. Dictionary elements are collected using curly brackets ``{}``. A ``:`` is used to
        specify key value pairs, e.g. the following creates a dictionary with two elements:

        >>> my_dict = {'key1': 1, 'key2': 2}
        >>> my_dict['key1']
        1

        Keys are usually :term:`string` or :term:`integer` instances, but can be any object with a valid ``hash``
        method. Values can be of any type.

   list
        A standard python `list <http://introtopython.org/lists_tuples.html#Lists>`_. Values in brackets denotes the
        expected type in the list. If omitted, any type is allowed.

        Python list are constructed using square brackets `[ ]`. List element are separated using a comma ','.

        >>> l = [1, 2, 3]
        >>> len(l)
        3

   tuple
       A standard python `tuple <https://docs.python.org/2.7/tutorial/datastructures.html#tuples-and-sequences>`_.
       Tuples are constructed using round brackets `()`:

       >>> t = (1, 2)

       The brackets can also be omitted, that is

       >>> t = 1, 2
       >>> len(t)
       2

       .. note::

          Unlike :term:`list` objects, tuples cannot be extended or assigned to. Thus they retain the values with
          which they were constructed. Thus, tuples can be viewed as a kind of *static* list.

   bool
        A standard python boolean type. Either `True` or `False`.

   string
        A standard python string literal, that is, any set of characters enclosed with ' ' or " ".

        >>> my_string = 'apple'

   regex
       A standard python regular expression. This is used to find and match patterns in other string. For example,

       >>> '9000\.\d2c'

       will match ``'9000.12c'`` and ``'9000.99c'``.

       A full explanation is beyond the scope of this manual. The following online resources is a good place to start:

         1. The `Official Python Documentation <https://docs.python.org/3/howto/regex.html>`__
         2. A nice fairly comprehensive `Ultimate Guide <https://towardsdatascience.com/the-ultimate-guide-to-using-the-python-regex-module-69aad9e9ba56>`__
         3. Regex `Cheat Sheet <https://www.dataquest.io/blog/regex-cheatsheet>`__

   format

       A string that describes how a data value should be rendered as a string. It is a standard python
       `format specification <https://docs.python.org/3/library/string.html#formatspec>`__. The most commonly
       used cases are:

       1. **'{:.<d>f}'**: To format a real value with *d* decimal places, e.g

         >>> '{:.3f}'.format(1)
         '1.000'

       2. **'{:.<d>(e|E)}'**: To format a real value with *d* decimal places in scientific notation, e.g

         >>> '{:.5E}'.format(1)
         '1.00000E+00'

   integer
        A signed integer.

        >>> a = 1
        >>> b = -10

   float
        Any real number. Accepts all standard notation using `.` as a decimal:

        >>> a = 1.0
        >>> b = 1.056E-5

   area
        A quantity with area (:math:`L^2`) dimensions, e.g.

        >>> a = 1.0 * units.cm * 2.0 * units.cm

   volume
       A quantity with :math:`L^3` dimensions, e.g

       >>> a = 15.0 * units.cc
       >>> b = 1.0 * units.cm * 3.0 * units.cm * 50.0 * units.mm
       >>> a == b
       True


   length
        A quantity with length dimensions, e.g.

        >>> a = 8 * units.cm
        >>> b = 5 * units.feet

   time
       A quantity with time dimensions, e.g.

       >>> a = 2.0 * units.days
       >>> b = 50 * units.seconds

   temperature
       A quantity of temperature dimensions, e.g

       >>> a = 600 * units.K
       >>> b = 75 * units.degC

   power
       A quantity with power dimensions, e.g.

       >>> a = 2.0 * units.kW

   pressure
      A quantity with pressure dimensions, e.g.

      >>> a = 1.0 * units.bar

   energy
      A quantity with energy dimensions, e.g.

      >>> a = 1.0 * units.J
      >>> b = 5.6 * units.eV

   mass
     A quantity with mass dimensions, e.g.

     >>> a = 1 * units.g
     >>> b = 2 * units.lb

   density
      A quantity with mass density units, for example

      >>> a = 1.0 * units.g / units.cc
      >>> print(a.to(units.kg / units.m / units.m / units.m))
      1000.0 kilogram / meter ** 3

   color
      A :term:`tuple` of 3 :term:`integer` values, denoting the red, green and blue value of the color. The integer
      values must be between 0 to 255.

      >>> red = (255, 0, 0)

      The `utilities.Palette` object collects a number of standard colors:

      >>> from core import *
      >>> print(utilities.Palette.Brown)
      (165, 42, 42)

   datetime
       Either a python `datetime <https://docs.python.org/2/library/datetime.html>`_ object, or any
       `valid string describing a date and time <http://dateutil.readthedocs.io/en/stable/parser.html>`_. For
       consistency, it is best to stick with a `ISO <http://www.cl.cam.ac.uk/~mgk25/iso-time.html>`_ format,
       e.g. ``'YYYY-MM-DD hh:mm:ss'`` or without the ``'-'`` and ``':'`` characters ``'YYYYMMDD hhmmss'``.

   placeholder
      Tags used in maps to denote positions that are empty, or will be filled by some other mechanism. The
      :py:mod:`core.place_holders` module contains the following predefined tokens:

      ==========  ==================================
      Token       Description
      ==========  ==================================
      `_`          Empty place holder.
      `_p`         Place holder with index 0.
      `_pi`        Place holder with index :math:`i`.
      ==========  ==================================

   component
       Any :class:`core.assembly.Component` instance. In input modules, components are usually obtained from the
       assembly library:

       .. code-block:: py

          from ..model import assemblies
          cmp = assemblies.MY_REACTOR_component_name()

   material
       Any :class:`core.material.Material` instance. Materials represent mixtures of isotopes, as well as additional
       meta data describing the mixture's state. More details can be found in :ref:`Specifying materials`.

   loadable
       Any `Component` with non-static features which are tracked in a history file. Apart form fuel elements,
       this also includes irradiation rigs, control elements and even reflector elements if poison buildup or
       radiation damage is explicitly tracked.

   loader
       A callable (function of function object) which return a **unique** :term:`component` instance. Must accept
       the `name` keyword arguments. All attributes of the `assembly` container are valid, e.g

       .. code-block:: py

          from ..model import assemblies

          # valid assembly loader
          cmp_loader = assemblies.MY_REACTOR_component_name

   material_tag
       Tag object used to classify a material. See :ref:`Specifying materials` for a complete list.

   bundle_tag
       Tag denoting a fueled or burnable bundle. Bundle tags are defined in the `core.assembly` module. Available tags
       are:

        =========================  ==============================================================
        Bundle Tag                 Description
        =========================  ==============================================================
        `fuel`                      Standard fissionable fuel bundle.
        `ba`                        Burnable absorber bundle.
        `clad`                      Bundle used to flag and activate clad material.
        `reflector`                 Bundle used to flag and activate reflector material.
        `absorber`                  Control structure (used to deplete control rods).
        `detector`                  Used to specify parts of a detector component that will be
                                    activated.
        =========================  ==============================================================

        These tags are available through the `burn_bundle` class in the :mod:`core.assembly` module:

        >>> from core.assembly import burn_bundle
        >>> burn_bundle.fuel

        Each bundle can be further customized by specifying an index, e.g.

        >>> burn_bundle.fuel(1)

        will create a `fuel` bundle tag with index 1 (the default index is 0). This is used when an assembly contains
        burnable components that have different designs, or distinct material compositions (e.g. MOX pin bundles).

   state_parameter
      State parameter tag. The following table list the available tags:

      =========================   ==============================================================
      State Tag                   Description
      =========================   ==============================================================
      `fuel_temperature`           Average temperature of fissionable material.
      `clad_temperature`           Average temperature of fuel material cladding.
      `moderator_temperature`      Average moderator temperature.
      `reflector_temperature`      Average reflector temperature.
      `reflector_purity`           Measure of reflector efficiency (e.g. heavy water purity)
      `reflector_density`          Density of the reflector.
      `moderator_density`          Average moderator density.
      `poison_concentration`       Soluble absorber concentration.
      `pressure`                   Coolant pressure.
      `inlet_temperature`          Temperature of coolant inlet.
      `flow_velocity`              Average velocity of the coolant flow.
      =========================   ==============================================================

      These tags are accessed through the `parameter` class in the :mod:`core.state` module:

      >>> import core.state as state
      >>> state.parameter.fuel_temperature(65 * units.degC)

   state

      The complete model state, which is specified as a :term:`tuple` or :term:`list` of :term:`state_parameter` tags.
      For example, to set the fuel and moderator temperature:

      >>> from core import *
      >>> state = (state_parameters.fuel_temperature(65 * units.degC),
      >>>          state_parameters.moderator_temperature(40 * units.degC))

   boundary_condition
       Tag used to denote a boundary condition. The following table lists all the boundary conditions defined in the
       :mod:`core.boundary_conditions`:

       =========================   ==================================================================================
       Tag                         Description
       =========================   ==================================================================================
       `vacuum`                    Nothing crossing this boundary ever returns
       `black`                     Synonym for `vacuum`
       `reflective`                Mirror boundary condition, that is, everything crossing this boundary returns with
                                   reflected angles
       `reflexive`                 Synonym for `reflective`
       `periodic`                  Periodic boundary condition.
       `isotropic`                 Angular flux is reflected isotropically
       `white`                     Synonym for `isotropic`
       `albedo`                    Tag denoting albedo boundary condition, that is, incoming current over outgoing
                                   current.
       =========================   ==================================================================================

       .. note::

          When the diffusion approximation is used `white` and `reflective` are equivalent.

   travel
       Bank position token. The following table list the tokens available in the :mod:`core.control` module:

       .. table:: Bank positions.
          :class: tight-table

          ============================    ========================================    =====================================
          Token                           Argument                                    Description
          ============================    ========================================    =====================================
          `fully_inserted()`               None                                        Move to fully inserted position.
          `fully_extracted()`              None                                        Move to fully inserted position.
          `percentage_inserted(x)`         Floating point value between 0 and 100.     Move to :math:`x\%` inserted.
          `percentage_extracted(x)`        Floating point value between 0 and 100.     Move to :math:`x\%` inserted.
          `travel_distance(x)`             :term:`length` value                        Move a travel distance of :math:`x`.
                                                                                       Negative values denote insertion.
          `notches(x)`                     :term:`integer` or :term:`float`            Notch scaling is set using the
                                                                                       `set_notch(v)` method, where `v`
                                                                                       is the absolute movement per notch.
                                                                                       Negative values denote insertion.
          ============================    ========================================    =====================================

   orientation
       Tags used to denote an object's orientation. They are collected in the :class:`core.geometry.orientation` class,
       which contains the following:

       ==================  =====================================================
       Tag                 Description
       ==================  =====================================================
       `horizontal`        Orientation parallel to the :math:`x`-axis.
       `vertical`          Orientation parallel to the :math:`y`-axis.
       `north`             Orientation in the positive :math:`y` direction.
       `south`             Orientation in the negative :math:`y` direction.
       `east`              Orientation in the negative :math:`x` direction.
       `west`              Orientation in the positive :math:`x` direction.
       ==================  =====================================================

   region
       Any :term:`CSG` region. This can be either a :ref:`single primitive <Primitive regions>` or
       :ref:`composite <Regularizing set operations>`. For example, the following are all valid regions:

       >>> from csg import *
       >>> p = primitives.Cuboid()
       >>> q = primitives.Cylinder(radius=1 * units.mm)
       >>> c = p & ~q

   affine_transformation

       In it's most general from, and affine transformation is a :math:`4\times4` matrix, with the first
       :math:`3\times 3` entries :math:`R_{ij}` specifying the rotation matrix, and the translation :math:`b_j`
       specified in column 4:

        .. math::

           \begin{bmatrix}
            R_{11} & R_{12} & R_{13} & b_1 \\
            R_{21} & R_{22} & R_{23} & b_2 \\
            R_{31} & R_{32} & R_{33} & b_3 \\
            0 & 0 & 0 & 1 \\
           \end{bmatrix}

        Affine transformations transform points :math:`r=(x,y,z)` to :math:`r'=(x',y',z')` as follows:

        .. math::

            r' = R r + b.

        Note that translations are applied **after** the rotation. The :mod:`core.geometry` module contains routines
        to construct the most :ref:`common transformations <Transformations>`.

   point
       A three or two dimensional point. Can be specified using a :term:`tuple` or :term:`list` of three or two
       numbers with length dimensions. For example ``(1 * units.mm, 0.5 * units.mm, -1.0 * units.m)``.

       .. note::

          If only two values are specified, it is assumed to be the :math:`x` and :math:`y` coordinates respectively,
          and the :math:`z` coordinate is assumed to be 0 (that is, the point is on the :math:`xy`-plane).

   predicate
       A callable which takes a single argument and return a :term:`bool`. Python
       `lambdas <http://book.pythontips.com/en/latest/lambdas.html>`__ are useful methods for creating predicates
       on the fly. For example, ``lambda x : x == 0 | x == 2`` will return `True` if and only if the value is equal
       to 1 or 2.