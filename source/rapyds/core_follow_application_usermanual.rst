.. _core_follow:
Core Follow or Multi-Step Depletion Calculations
************************************************


General calculation that computes and tracks the material changes in a model during irradiation. This can be as simple
as a depletion calculation at constant power, or a more complicated irradiation history taking model changes (e.g
bank positions, rig movements) into consideration. The primary goal of this application mode is to update
assembly inventories, but it also facilitates the calculation of (optional) critical states during the irradiation.

.. note::

   This application is restricted to irradiation over a period where the core layout is fixed. That is, assemblies
   in the main core grid can not be shuffled during the calculation. Thus it corresponds to a typical operational cycle.

.. _core_follow_typical_use_cases:

Typical Use Cases
=================

  1. Update assembly material compositions to current plant state, using real plant operational data.
  2. Monitor model performance during burnup by comparing with real critical configurations.

.. _core_follow_creating_input_module:

Creating an Input Module
========================

The :command:`manager` utility can be used to create a template starting script:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager core-follow CYCLE_NAME [options]

This mode accepts the following optional arguments:

.. option:: --start-time <str>

   Specify the cycle start time. Must be a valid :term:`datetime` parameter. Initiates the :attr:`cf.start_time`
   parameter.

.. option:: --plant-data <str>

   Full path to the file that contains the cycle progression data. Sets the :param:`plant_data` parameter.

.. option:: --configuration <str>

   Configuration module this application's model is based on.


For example, the following will create the module :command:`C001` in the :command:`core_follow` package:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager core-follow C001 --start-time '1977-10-30 08:00:00'


.. _core_follow_parameters:

Additional Input Parameters
===========================

In the following `rod_cal` denotes a `rod_calibrations` parameter set, created as follows:

.. code-block:: py

   import applications.core_follow as app

   cf = app.parameters()

.. current_parameter_set:: cf
   :synopsys: applications.core_follow.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`, as well as the following:

.. parameter:: cycle_name
   :type: :term:`string`

   For this application, it is an alias for :attr:`app_param.project_name`.

.. parameter:: start_time
   :type: :term:`datetime`

   Calender date and time indicating were the depletion calculation should start. For this application, it is an alias for
   :attr:`app_param.time_stamp`.

.. parameter:: irradiation_history

    Sequence defining how, and under what conditions, material activation will occur. The irradiation steps can be set
    manually (see :ref:`irradiation_history`), or it can be constructed using the plant data processing
    tool.

    This parameter can also point to a pre generated and archived irradiation history file.

    .. note::

       Only steps with :param:`critical_bank` or :param:`critical_power` set, will be considered and edited as critical
       cases.

.. parameter:: plant_processing_module

   Specify the module that is used to interpret data in the :attr:`cf.plant_data` file. This is usually the
   `process_plant_data` module found in the `core_follow` directory:

   .. code-block:: py

      import process_plant_data

      parameters.plant_processing_module = process_plant_data

   .. note::

      This parameter is only required if the plant data is processed through the `plant data <Processing plant data>`_
      mode.

.. parameter:: plant_data
   :type: :term:`directory`

   Specify the location of the (raw) plant data file. This is usually a text file with rows and columns of data, which
   can be interpreted by the :attr:`cf.plant_processing_module`.


   .. note::

      This parameter is only required if the plant data is processed through the `plant data <Processing plant data>`_
      mode.


.. _core_follow_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`.

The following
table list the additional modes available, or the modes that have a custom role for this application:

====================================  ==================================================================================
 Run Mode                             Description
====================================  ==================================================================================
:ref:`plant_data <cf_plant_data>`     View and modify the plant data defining the cycle procession.
:ref:`post <cf_post>`                 Collect all results.
:ref:`critical-cases <cf_cases>`      Perform snap shot calculations at all the steps with critical configurations
                                      defined
====================================  ==================================================================================

More details on each run mode are given in the sections below.

.. _core_follow_typical_cli:

Typical usage sequence
----------------------

The following list the typical sequence in which the available modes should be used :

.. code-block:: console

   $oscar5 MY_REACTOR.core_follow.my_module plant-data
   $oscar5 MY_REACTOR.core_follow.my_module --target-mode MODE --config-file my_config.cfg execute --threads 24
   $oscar5 MY_REACTOR.core_follow.my_module --target-mode MODE --config-file my_config.cfg post
   $oscar5 MY_REACTOR.core_follow.my_module critical-cases --target-mode MODE --config-file my_config.cfg execute --threads 24
   $oscar5 MY_REACTOR.core_follow.my_module critical-cases --target-mode MODE --config-file my_config.cfg post

.. note::

   The last two steps are only required for target codes which can not calculate the critical cases during the depletion
   calculation. That is, modes for which :ref:`internal_cycle_critical_configurations <link_internal_cycle_critical_configurations>`
   is False (the default). In particular, it is not required for calculations using :term:`MGRAC`.

.. _cf_plant_data:

plant-data
----------

This will launch the interactive data viewer and step generation tool, which can be used to define or modify the
:param:`irradiation_history` for the cycle.

.. attention::

   This mode is **only** available if the :param:`plant_processing_module` and :param:`plant_plant` were set correctly.

.. _cf_post:

post
----

Collects all results from the main depletion calculation and the critical calculations. It accepts all the
standard :ref:`post processing <post>` options. The :command:`--show` command will display a summary of certain
core parameters throughout the cycle.

.. _cf_cases:

critical-cases
--------------

Calculate the cases that were flagged as critical in the :attr:`cf.irradiation_history`. This a complete sub-application
with all the :ref:`General Application Command Line Interface (CLI)` options and run modes. The
top level :ref:`run modes <Application modes>` applies to *all* critical configurations, for example,

.. code-block:: console

   $oscar5 MY_REACTOR.core_follow.my_module critical-cases --target-mode MODE --config-file my_config.cfg execute --threads 24

will launch all the case. In addition, each individual critical case can be accessed as a sub application:

.. code-block:: console

   $oscar5 MY_REACTOR.core_follow.my_module critical-cases CYCLE-2009.1.3.19.55.12 --config-file my_config.cfg execute --threads 24

will only run the critical case at the specified time point.

.. note::

   The :command:`--help` flag will list all available critical cases:

   .. code-block:: console

      $oscar5 MY_REACTOR.core_follow.my_module --target-mode MODE critical-cases --help

.. attention::

   This is option is only activated for target codes which can not calculate the critical cases during the depletion
   calculation. That is, modes for which :ref:`internal_cycle_critical_configurations <link_internal_cycle_critical_configurations>`
   is False (the default).

.. _core_follow_output:

Output Tokens
=============

The following lists all output tokens for this application mode that can be used in the report generation system.


.. py:currentmodule:: core_follow

Time points
-----------

.. py:function:: cycle_progression(**kwargs)

   Extract the list of points in the irradiation history at which material distributions were updated. This returns a
   list of :term:`time` values, with the zero point at the beginning of the irradiation history, and the last entry the
   total duration of the irradiation history.

.. py:function:: cycle_time_points(**kwargs)

   Extract the list of points in the irradiation history at which material distributions were updated. This returns a
   list of :term:`datetime` values, with the first point the irradiation history start date and time.


.. py:function:: critical_time_points(**kwargs)

   Extract the list of points in the irradiation history at which were the model was assumed to be in a critical state.
   This returns a list of :term:`datetime` values, with the first point the irradiation history start date and time.

.. py:function:: critical_cases(**kwargs)

   Extract the list of points in the irradiation history at which were the model was assumed to be in a critical state.
   This returns a list of :term:`time` values, with the zero point at the beginning of the irradiation history, and the
   last entry the total duration of the irradiation history.

Mass distributions
------------------

.. py:function:: total_mass(isotope='U-235', at=None, **kwargs)

   Extract the **total** mass at each depletion step over the irradiation history.

   :param isotope: Specify the isotope for which masses should be extracted.
   :type isotope: :term:`isotope`
   :param at: Limit the steps at which data should be extracted. Either a single integer index, or :term:`list` of
     integer indices. Instead of indices a target (or list of) :term:`datetime` or :term:`time` value can also be
     specified. In this case, the masses at the beginning of the irradiation step containing the target time is extracted.

   When a single value for `at` is specified, a single :term`mass` quantity is returned. Otherwise a list equal to the number
   of points in `at` is returned. The default is to return masses at all steps.

   .. note::

      Depending on the target code used, masses for all isotopes might not be available for certain isotopes at all
      steps. Since, regardless of target code used, material distributions are always stored at the beginning and
      end of a cycle, information for all isotopes are guaranteed to be available at these points.


.. py:function:: u235_mass(at=None, **kwargs)

   Equivalent to :code:`core_follow.total_mass('U-235', at, **kwargs)`.


.. py:function:: boc_u235_mass(**kwargs)

   Equivalent to :code:`core_follow.total_mass('U-235', at=BOC, **kwargs)`.

.. py:function:: eoc_u235_mass(**kwargs)

   Equivalent to :code:`core_follow.total_mass('U-235', at=EOC, **kwargs)`.

.. py:function:: average_xenon(at=None, **kwargs)

   Extracts the core average Xenon at the critical positions.

   :param at: Limit the steps at which data should be extracted. Either a single integer index, or :term:`list` of
     integer indices. Instead of indices a target (or list of) :term:`datetime` or :term:`time` value can also be
     specified. In this case, the masses at the beginning of the irradiation step containing the target time is extracted.

   Returns a :term:`list` (or single value if `at` is one index) of quantities with number density units.

.. py:function:: mass_map(isotope='U-235', at=None, **kwargs)

   Extract the mass *distribution* at each depletion step over the irradiation history.

   The parameters have exactly same as effect as described in :func:`core_follow.total_mass`. The same comment regarding
   the availability of mass distributions also applies.

   Returns a :term:`list` of :term:`labeledgrid` or :term:`hexagonal grid` instances.

.. py:function:: u235_mass_map(at=None, **kwargs)

   Equivalent to :code:`core_follow.mass_map(isotope='U-235', at=at, **kwargs)`

.. py:function:: boc_u235_mass_map(**kwargs)

   Equivalent to :code:`core_follow.mass_map(isotope='U-235', at=BOC, **kwargs)`

.. py:function:: eoc_u235_mass_map(**kwargs)

   Equivalent to :code:`core_follow.mass_map(isotope='U-235', at=EOC, **kwargs)`

Control structures
------------------

.. py:method:: bank_positions(bank, at=None, **kwargs)

   Extract the bank positions at each critical configuration point in the irradiation history.

   :param str bank: Extract only data for the specified bank. If specified, a single :term:`travel` instance is returned.
   :param at: Limit the steps at which data should be extracted. Either a single integer index, or :term:`list` of
     integer indices. Instead of indices a target (or list of) :term:`datetime` or :term:`time` value can also be
     specified. In this case, the masses at the beginning of the irradiation step containing the target time is extracted.

   If critical bank searches (:attr:`app_param.bank_search`) were not performed, this will simply return the input bank
   positions.


   .. note::

      If the `bank` parameter is not specified, **and** there are multiple banks defined, a :term:`dict` object with
      bank names as keys, and :term:`travel` instances as values is returned.

.. py:function:: startup_bank(bank, **kwargs)


   Equivalent to :code:`core_follow.bank_positions(bank, at=BOC)`

   .. note::

      To get the initial position of a specific bank use :code:`startup_bank('BANK_NAME')`.

.. py:function:: eoc_bank(bank, **kwargs)


   Equivalent to :code:`core_follow.bank_positions(bank, at=EOC)`


.. py:function:: critical_multiplication_factors(**kwargs)

   Extract the calculated multiplication factors (:math:`k_{eff}`) for the time points at which the model is assumed
   to be in a critical state. Returns a list of values, of length equal to :func:`core_follow.critical_time_points` or
   :func:`core_follow.critical_cases`.

.. _core_follow_examples:

Examples
========


For input that will use existing plant data, the typical contents of the input module is

.. code-block:: py

   import applications.core_follow as app
   from ..configurations.my_conf import model
   from . import process_plant_data
   from core import *

   # Create parameter set
   cf = app.parameters()

   # Set and the model
   cf.model = model

   # Set some base application parameters
   cf.cycle_name = 'CYCLE_1'
   cf.start_time = '01-01-2020 00:00:00'

   # Application specific
   # path to raw plant data
   cf.plant_data = utilities.path_relative_to(__file__, '../plant_data/CYCLE_1/CYCLE_1.csv')
   # where the processed step data should be saved
   cf.irradiation_history = utilities.path_relative_to(__file__, '../plant_data/CYCLE_1/CYCLE_1.pkl')
   # how the raw plant data should be saved
   cf.plant_processing_module = process_plant_data

   if __name__ == '__main__':
    app.main(parameters=cf)

If the irradiation steps are specified manually:

.. code-block:: py

   import applications.core_follow as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   cf = app.parameters()

   # Set and the model
   cf.model = model

   # Set some base application parameters
   cf.cycle_name = 'CYCLE_1'
   cf.start_time = '01-01-2020 00:00:00'

   # Specify the irradiation steps
   # First step without critical position
   cf.irradiation_history.add_step(duration=0.5 * units.days,
                                   power=10 * units.MW,
                                   average_banks={'bank_1': control.percentage_extracted(55),
                                                  'bank_2': control.percentage_extracted(65)})

   # Second step with critical position
   cf.irradiation_history.add_step(duration=1.5 * units.days,
                                   power=10 * units.MW,
                                   average_banks={'bank_1': control.percentage_extracted(60),
                                                  'bank_2': control.percentage_extracted(70)},
                                   critical_power=10.2 * units.MW,
                                   critical_banks={'bank_1': control.percentage_extracted(62),
                                                  'bank_2': control.percentage_extracted(72)})

   # Decay step
   cf.irradiation_history.add_decay_step(duration=0.5 * units.days)

   # etc

   if __name__ == '__main__':
    app.main(parameters=cf)