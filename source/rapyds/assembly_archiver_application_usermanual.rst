.. _assembly_archiver:
Viewing and Archiving Assemblies
********************************

This application mode is used to save, visualize and check assembly models.

.. _assembly_archiver_typical_use_cases:
Typical Use Cases
=================

  1. To test, view and archive a complete model configuration.

.. _assembly_archiver_creating_input_module:
Creating an Input Module
========================

The **manager** utility can be used create a basic template build module for the component. It has the following
signature:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager assembly MODULE_NAME [options]

where ``MODULE_NAME`` is the name of build module. The following options are allowed:

.. option:: --base-cls <str>

   Base assembly class this component will use. Currently, the following options are allowed:

    - Assembly : For a :ref:`Generic assembly`

    - Control : For a generic :ref:`Control assembly`

    - Pool : For a :ref:`Pool or ex-core structure`

.. option:: --base-name <str>

   Assembly type name. The will also be used to name the assembly archive for this component. It is recommended that
   you prepend your reactor's name, e. g. ``'MY_REACTOR-some-fairly-descriptive-name'`` . This makes it easier to
   recognize components in multi reactor application sets.

.. option:: --description <str>

   Short description of the component.

.. option:: --mcnp-deck <str>

   Pull cells from an existing input deck. The value past to this call is the location of the input deck *relative* to
   the target module.

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'model'.

.. option:: --description <str>

   A short description of the input module.

A typical input module has the following form:

.. code-block:: py

   import applications.assembly_archiver as app
   from core import *
   from csg import *
   from ..model import assemblies

   def build():

       asm = assembly.Assembly(name='MY_REACTOR_assembly_type')

       # add cells etc

       return asm

   if __name__ == '__main__':
    app.main(source=build)


See also :ref:`Basic structure of an assembly build module`.

.. attention::

   The `build` method must be passed to the `app.main` method using the `source` keyword.

.. _assembly_archiver_parameters:
Additional Input Parameters
===========================

This primary function is to define and store a complete assembly geometry structure, thus the application mode has no other
parameters to set.

.. _assembly_archiver_cli:

Command Line Usage
==================

The assembly build module is used on the command line to save the assembly archive, and perform additional tests.

.. code-block:: console

   $ oscar5 MY_REACTOR.model.my_module [options] <mode> [mode options]

Since this application has no special parameters to set, only the :command:`--log-level` flag is supported. The
supported application run modes are summarized below.


.. _assembly_archiver_visualization:

visualization
-------------

Works just like the standard :ref:`visualization`, with the same command line flags.


.. _assembly_archiver_archive:

archive
-------

Process the assembly build script and save the resulting structure in a binary archive. This will create
an :attr:`asm.name`.asm file in the model directory. Apart from the basic cell structure defined in the build script,
this also calculates a polyhedral mesh, which is used in many visualization routines and geometric algorithms (e.g.
cell volume calculation).

.. note::

   Currently the polyhedral mesh generation is performed using the infinite precision kernels from the
   `CGAL <https://www.cgal.org>`_ collection of algorithms. This ensures that all geometric operations are robust,
   but also requires a fair amount of computing resources and time.


This mode accepts the following arguments:

.. option:: --force

    Flag that forces the regeneration of the archive, even if the system detects that the source script has not
    changed.

.. option:: --check-intersections

     Check for intersecting cells in the generated polyhedral mesh. Will print a list of intersecting cell indices,
     or 'No intersections` if there are no pair wise intersection of cells.

.. option:: --no-polyhedra

     Skip the (expensive) calculation of the polyhedral mesh. This flag is useful if changes ooccured that won't
     affect the assembly structure. This includes material updates or any other update to the physical *contents*
     of cells.

.. option:: --search-tree

     Generate a search tree data structure that is used for point location, and adds missing cells to the assembly.

     .. note::

        This option requires that the background of the assembly was defined in the module with the `set_background`
        call.

.. option:: --verbose<int>

      Set the verbosity level for the search tree generation algorithm.

.. option:: --complete-universe

    Creates missing background cells using the older ``'decompose'`` algorithm.

    .. note::

        This option requires that the background of the assembly was defined in the module with the `set_background`
        call.

.. _assembly_archiver_plotter:

plotter
-------

Creates two dimensional plots of slices through the model. Accepts all the :ref:`plotter application <geometry_plotter>`
:ref:`command line arguments <geometry_plotter_cli>`. Note that since only the
:ref:`plotter execution <geometry_plotter_execute>` is supported, the mode can be excluded from the command line
sequence. For example, the following will create four plots, two in the :math:`xy`-plane, and one each in the other
coordinate planes:

.. code-block:: console

   $ oscar5 MY_REACTOR.model.my_module plotter --frame 8.1 8.1 100.0 --pixels 10 --x 0.0 --y 0.0 -- z 0.0 -30.0

.. attention::

   Standalone image generation (without the use of target codes) is only available if the archive was generated with
   the :option:`--search-tree` flag. Otherwise, the system will try to use one of the supported target codes.

.. _assembly_archiver_check_model:

check-model
-----------

This checks your model by running a simple simulation using the specified target code (or plugin). It is a critical
step in ensuring that the model is compatible, before running simulations with the specified plugin.

This modes uses the :ref:`model checker command line interface <model_checker_cli>`. For example,

.. code-block:: console

  $ oscar5 MY_REACTOR.model.my_module check-model --target-mode MCNP --config-file server.cfg --particles 1200000 execute --threads 24 --force


.. _assembly_archiver_typical_cli:

Typical command line usage
--------------------------

The following gives the typical command line use sequence:

.. code-block:: console

   $ oscar5 MY_REACTOR.model.my_module archive --check-intersections
   $ oscar5 MY_REACTOR.model.my_module visualization --interactive

