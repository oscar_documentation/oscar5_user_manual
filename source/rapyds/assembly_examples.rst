Assembly Construction Examples
==============================

This section includes some complete assembly build modules, illustrating the different assembly types. The highlighted
sections shows input that is unique to that assembly instance.


.. _basic_assembly_example:

Basic assembly
--------------

.. literalinclude:: /validation/MNR/model/beryllium_reflector.py

.. _control_assembly_example:

Control assembly
----------------


.. literalinclude:: /validation/ETRR2/model/absorber_plate.py
   :emphasize-lines: 30, 138-140

.. _plate_type_fuel_assembly_example:

Plate fuel assembly
-------------------
.. literalinclude:: /validation/SAFARI_1/model/leu_fuel_assembly.py
   :emphasize-lines: 26-63, 84-85

.. _plate_type_fuel_assembly_with_absorbers_example:

Plate fuel assembly with burnable absorbers
-------------------------------------------

.. literalinclude:: /validation/OPAL/model/standard_fuel.py
   :emphasize-lines: 25, 36-100, 143, 147
