.. role:: py(code)
    :language: python

Generic Assembly Types
======================

These are basic assembly framework classes, with no predefined structure.

Generic assembly
----------------

Basic assembly class with no pre-defined cells. It is available in the :mod:`core.assembly` module, and is used as
follows:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = assembly.Assembly(name='MY_REACTOR_assembly_base_name')

This class exposes the following set of parameters, which are common to all assembly types:

.. current_parameter_set:: assembly
   :synopsys: core.assembly.Assembly

.. parameter:: name
   :type: :term:`string`
   :default: Required

   Name associated with the assembly *instance*.

   .. attention::

      In the build module it is always the base, or assembly type name, and will be used to name the assembly
      archive created.

.. parameter:: center
   :type: :term:`point`
   :default: 0,0,0

   The component's center point. This parameter is primarily used when loading the component into a
   `facility <Specifying loadable facilities>`_. Thus, it is relative to the assembly's role in the overall model, and
   not necessarily a reflection of it's center of mass. For instance, if a *consistent* coordinate system is chosen for
   all components, this parameter can be left at :math:`(0,0,0)`. On the other hand, if one component's cells were
   constructed with the fuel center line at :math:`z=0` (for instance), and another assembly with the bottom of the
   assembly at :math:`z=0`, then they cannot have the same zero point.

.. parameter:: state
   :type: :term:`state`

   State perturbations (e.g fuel and moderator temperature) currently applied to the assembly instance. Is best
   modified using the :func:`assembly.set_state` method. If empty, all cells and materials will be at their default state as
   specified during construction.

   .. note::

      This parameter is rarely set within the build module, and is mostly modified by external mechanisms.

.. parameter:: transformation
   :type: :term:`affine_transformation`

   Transformation that should be applied to the assembly instance. If assigning directly to this parameter, any
   existing transformations will be replaced, while the :func:`assembly.add_transformation` method will append the
   transformation.

   .. note::

      This parameter is rarely set within the build module. The one exception is when it is more convenient to
      construct the assembly using a particular orientation, but is is always used in another orientation or
      coordinate system.

.. parameter:: structure
   :type: :term:`list`

   Read only access to the cells that constitute the structure of the assembly. This is simply a :term:`list` of
   :class:`csg.cell.Cell` instances. For example, :py:`len(assembly.structure)`
   gives the total number of cells.

   This parameter should not be directly modified. New cells are added using the :func:`assembly.add_cell` and
   :func:`assembly.add_cells` methods.

.. parameter:: burn_bundles

   Stores objects that manages materials that can be depleted or activated. A burn bundle acts like a material
   exposure mesh, and can be customized without modifying the geometry structure.

   Burn bundles are added and accessed using one of the following *bundle tags*, available in
   :class:`core.assembly.burn_bundle``:

   ===========================   ===============================================================================================
   Tag                           Description
   ===========================   ===============================================================================================
   :py:`burn_bundle.fuel`        Fueled or fissionable material.
   :py:`burn_bundle.ba`          Burnable absorber material typically included in fuel assemblies to control initial reactivity.
   :py:`burn_bundle.clad`        Used for fuel clad activation.
   :py:`burn_bundle.reflector`   Used in the activation of reflector elements (e.g beryllium).
   :py:`burn_bundle.absorber`    Activation of material in control elements or rods.
   ===========================   ===============================================================================================

   .. note::

      The bundles tags are also available in the :mod:`core.assembly` module, with the same name given in the above table,
      but with *_bundle* appended. For example,

      >>> import core.assembly
      >>> core.assembly.burn_bundle.fuel == core.assembly.fuel_bundle()
      True

   Different bundles are accessed using the :py:`[]` method. For example, the following will access the fuel bundle of
   an assembly instance :py:`asm`:

   .. code-block:: py

      bundle = asm.burn_bundles[assembly.burn_bundle.fuel]

   An assembly can have more than one bundle. For example, a typical follower type control element might have a
   :py:`fuel_bundle` to manage fueled regions, and a :py:`absorber_bundle` to manage depletion of the absorber section.

   Bundles are added automatically by many of the assembly types listed below. However, you can always add bundles
   manually. See :ref:`Specifying burn bundles`.

.. parameter:: fuel_bundles

   Synonym for :attr:`assembly.burn_bundles`.

.. parameter:: flags
   :type: :term:`integer`
   :default: 0

   Bit type flags which further customize the assembly's role. The following table lists the flags relevant to the
   heterogeneous assembly library:

   ==========================  =========================================================================================
   Flag                        Description
   ==========================  =========================================================================================
   ``type_flags.self_cooled``  Heat produced by this assembly does not contribute to core thermal power. Assemblies
                               with this flag will also not be affected by model state changes.
   ==========================  =========================================================================================

   Flags are added using the or operator '|'. For example

   .. code-block:: py

      asm.flags |= type_flags.self_cooled

.. _generic_control_type:
Control assembly
----------------

Models any assembly that moves in order to control reactivity. This class is found in the :mod:`core.control` module
and is used as follows:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = control.ControlAssembly(name='MY_REACTOR_assembly_base_name')

It adds the following set of parameters to the ones listed in `Generic assembly`_:

.. current_parameter_set:: control_assembly
   :synopsys: core.control.ControlAssembly

.. parameter:: total_travel_distance
   :type: :term:`length`
   :default: Required

   Total length the control structure moves from fully inserted to fully extracted. This parameter must be specified
   in real length dimensions.

.. parameter:: travel_direction
   :type: :class:`core.control.TravelDirection`
   :default: up

   The direction in which the control structure is *extracted*. Currently, this parameter must be either
   ``core.control.up()`` (the default), or ``core.control.down()``.

.. parameter:: base_position
   :type: :term:`travel`
   :default: Required

   The position in which the control assembly is constructed (that is, the coordinates used to define cells). Typically,
   the assembly is built either fully extracted or fully inserted, but this is not a requirement.

.. _asm_type_pool:

Pool or ex-core structure
-------------------------

Models a reactor pool, or any relevant structure outside the core region. The :mod:`core.pool` module provides
a basic :class:`Pool` class, which only defines a slot for the reactor core, and a :class:`LoadablePool` that can load
additional assemblies using a load map.

The basic pool component is used as follows:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = pool.Pool(name='MY_REACTOR_pool')

It exposes the following additional parameters:


.. current_parameter_set:: pool
   :synopsys: core.pool.Pool

.. parameter:: core_center
   :type: :term:`point`
   :default: :py:`(0, 0, 0)`

   Defines the core center relative to the pool component's center. This parameter is used to correctly place
   the core in the :attr:`pool.core_cell`.

.. parameter:: core_cell
   :type: :class:`csg.cell.Cell`
   :default: Required

   Defines the structure that will contain the core. This must be standard :class:`csg.cell.Cell`. Instead of
   specifying this parameter, the :func:`pool.set_core_cell` method, which takes the same arguments as a cell construction,
   can be used.

   For example, the following will create the same container cell:

   .. code-block:: py

      from csg import *

      region = primitives.RectangularCylinder(width=50 * units.cm, length=60 * units.cm)

      pool.core_cell = Cell(region)

      # equivalently
      pool.set_core_cell(region)

   .. attention::

      This cell will be a normal part of the pool structure, and should not intersect with any other cell.

   .. note::

      This need not be the only cell filled with the core structure. You can flag any cell that will contain
      the core structure by filling it with the special facility ``'core'``,

      .. code-block:: py

         pool.add_cell(region, facility='core')

When your facility has ex-core positions with a grid like structure (e.g. a row of irradiation positions), the
:class:`LoadablePool` class can be used:

.. code-block:: py

   from core.pool import LoadablePool

   def build(*args, **kwargs):

       asm = LoadablePool(name='MY_REACTOR_pool')

As with the basic :class:`Pool` component, this class also facilitates core positioning using the :attr:`pool.core_center`
and :attr:`pool.core_cell` parameters. The following method is used to flag additional grid positions.

.. py:function:: pool.add_load_position(label, region, center)

   Adds an additional load position to the reflector.

   :param str label: The position label. Should be the combination of a row and column label used in the load grid
        specification. For example, if your load grid has a row ``'A'`` and column ``'1'``, then ``'A1'`` will be
        the label for that position.
   :param region: The region (or cell) bounding the position.
   :type region: :term:`region`
   :param center: The position to which assemblies should be moved, that is, targets will be aligned so that
        there center matches this value.
   :type center: :term:`point`

.. attention::

   If using automatically generated (lattice) cores, the core box, core grid, and any other structures not part of loaded
   assemblies, must be part of the pool.

Custom core structures
----------------------

This assembly type is used to define structures that behaves like a core, but does not fit the categories for which
the :ref:`auto core construction <Parameters related to core structure>` applies. Typical examples include cores that
loads assemblies on an irregular grid, or includes complicated in core facilities or control structures.

The :mod:`core.reactor_core` module provides the :class:`UnstructuredCore` class, which is simply an assembly type which can
be loaded with a map. It accepts all the `Generic assembly`_ parameters.

Defining the core structure is similar to any assembly, except that certain cells are marked so that they can later
be loaded with the appropriate assembly when building the :ref:`core configuration <Building Core Configurations>`:

.. current_parameter_set:: unstructured_core
   :synopsys: core.reactor_core.UnstructuredCore

.. py:function:: add_core_position(label, region, center)

   Adds a special cell marked as a core position.

   :param str label: Position label. Should correspond to a label that will be used in the
     :ref:`core_map <Parameters related to core layout>`.
   :param region: Defines the geometry of the core slot. Any valid CSG region, or an existing :class:`Cell` instance.
   :param center: The position to which assemblies should be moved.
   :type center: :term:`point`

   .. note::

      The `label` specified with this call must correspond to the combination of row and column label that
      will be used in the :ref:`core_map <Parameters related to core layout>`.

   .. attention::

      Structures added with this method must not overlap with structures using the normal
      :ref:`add_cell <assemblies_add_cells>` method.