.. _distributions:
Working with Distributions
**************************

In **rapyds** a distribution is a field of value(s) over the (physical) phase space. They are used to store output
from responses, and is also used to specify external sources. This section gives a general overview of the distribution
interface, as well as the various distributions available in **rapyds**.

.. _dist_base_parameters:
Instantiating Distributions
===========================

In general, a distribution simply models a functional :math:`f:R^m \to R`. Which components of the underlying phase
space the distribution covers is specified using the :attr:`distribution.variables` parameter.

.. current_parameter_set:: Distribution
   :synopsys: Common distribution parameters.

.. parameter:: variables
   :type: :term:`string` or :term:`list`

   The phase space variable(s) this distribution covers. It is specified by giving one or more (a :term:`list`) of the
   following tokens:

   .. _phase_space_tokens:
   .. list-table:: Phase space variables.
      :header-rows: 1

      * - Symbol
        - Phase Space Component
      * - *x*
        - The :math:`x` coordinate in cartesian geometry.
      * - *y*
        - The :math:`y` coordinate in cartesian geometry.
      * - *z*
        - The :math:`z` coordinate in cartesian geometry.
      * - *r*
        - The radial coordinate is cylindrical geometry.
      * - *theta*
        - The polar angle coordinate in cylindrical geometry.
      * - *e*
        - The energy component.
      * - *t*
        - The time component.

   The number of variables also determines the phase space dimension of the distribution.

.. parameter:: aggregate
   :type: sum, mean, min, or max
   :default: sum

   Determines how ranges, and variables not explicitly specified when :ref:`inserting or retrieving data <querying_distributions>`
   should be handled. For example, if ``sum`` is specified, the total over all dimensions perpendicular
   to specified variables will be used. The options can be described mathematically as follows:

   Let :math:`f` denote the distribution of interest, and suppose its phase space is decomposed as
   :math:`\mathbf{A}\times\mathbf{B}`. Then, a query specifying only variable values in :math:`\mathbf{A}` will yield
   the following:

   - If :attr:`Distribution.aggregate` is ``sum``:

      .. math::

         q(\mathbf{a}=\mathbf{a}_0) = \int_{\mathbf{B}}d\mathbf{b}\, f(\mathbf{a}=\mathbf{a}_0, \mathbf{b})

   - If :attr:`Distribution.aggregate` is ``average``:

      .. math::

         q(\mathbf{a}=\mathbf{a}_0) =\frac{1}{|\mathbf{B}|} \int_{\mathbf{B}}d\mathbf{b}\, f(\mathbf{a}=\mathbf{a}_0, \mathbf{b})

   - If :attr:`Distribution.aggregate` is ``min``:

      .. math::

         q(\mathbf{a}=\mathbf{a}_0) = \min_{\mathbf{b}\in\mathbf{B}}f(\mathbf{a}=\mathbf{a}_0, \mathbf{b})

   - If :attr:`Distribution.aggregate` is ``max``:

      .. math::

         q(\mathbf{a}=\mathbf{a}_0) = \max_{\mathbf{b}\in\mathbf{B}}f(\mathbf{a}=\mathbf{a}_0, \mathbf{b})



.. _querying_distributions:
Accessing Distribution Values
=============================

.. current_parameter_set:: Distribution
   :synopsys: Common distribution parameters.

.. py:function:: get_value(**query)

   Retrieve a value from the distribution.

   :param query: The phase space coordinates at which a value from the distribution should be retrieved. Takes the
      form of keyword value pairs, with keys listed in :ref:`phase space tokens <phase_space_tokens>`, followed by
      the corresponding phase space value.

      Instead of a value, a range can also be specified, by passing the keywords ``<v>_min`` and\or ``<v>_max`` (here,
      ``<v>`` must be replaced by one of the :ref:`phase space tokens <phase_space_tokens>`).


   For example, if ``dist`` is a distribution covering the :math:`x` coordinate and energy domains, then,
   assuming ``dist.aggregate='sum'``:

   .. list-table:: Query examples.
      :header-rows: 1
      :class: tight-table

      * - Syntax
        - Description
      * - ``dist.get_value(x=5 * units.cm, e=10 * units.eV)``
        - Retrieve the value at :math:`x` equal to 5 cm and energy 10 eV
      * - ``dist.get_value(x=5 * units.cm, e_max=100 * units.eV)``
        - Retrieve the value at :math:`x` equal to 5 cm, integrated over all energies less than 100 eV
      * - ``dist.get_value(x=5 * units.cm, e_min=10 * units.eV, e_max=100 * units.eV)``
        - Retrieve the value at :math:`x` equal to 5 cm, integrated over all energies less than 100 eV, and greater
          than 10 eV
      * - ``dist.get_value(x=5 * units.cm)``
        - Retrieve the value at :math:`x` equal to 5 cm, integrated over the energy domain
      * - ``dist.get_value()``
        - The integral of the distribution over its entire domain

   .. note::

      How range and unspecified values are treated, is determined by the :attr:`Distribution.aggregate` parameter.


.. py:function:: set_value(value, **at)

   Insert a value into the distribution at a particular point.

   :param value: Value to insert (or score).
   :param at: The phase space coordinates at which a value should be inserted. Takes the
      form of keyword value pairs, with keys listed in :ref:`phase space tokens <phase_space_tokens>`, followed by
      the corresponding phase space value.

   When the phase space point values are integers, this call will set the nodes or moments defining the distribution.
   How the insertion at not discrete points are handled, depends on the distribution type, and/or the interpolation
   rules used.

   .. attention::

      Unlike :func:`Distribution.get_value`, all phase space variables must be specified, and ranges
      are not allowed.


.. py:function:: slice(coord, **query)

   Produces a slice through the distribution along a specified coordinate in phase space.

   :param coord: The :ref:`phase space token <phase_space_tokens>` along which the slice should be taken.
   :param query: Specify how the other variables should be treated. Similar syntax as used in
      :func:`Distribution.get_value`.

   :returns: Two lists, the first a set op points in the variable `coord`, and the second the distribution values along
      these points, as determined by `query` and :attr:`Distribution.aggregate`.

   .. attention::

      The variable specified with `coord` can not also appear in `query`.

   .. note::

      The return value is suitable for use as the `data` value when using the :ref:`plotting directive <plot_lines>`.


Distribution Containers
=======================

This section lists the distribution types available in the :mod:`distributions` module.

.. current_parameter_set:: distributions
   :synopsys: Module containing

.. py:class:: Flat(**kwargs)

   Represents a trivial distribution (e.g. without any shape), over the phase space region.

.. py:class:: Discrete(**kwargs)

   Represents a piece-wise constant one dimensional distribution. Its domain is represented by a sequence of adjacent
   (but disjoint) intervals or bins.

   :param kwargs: Can be used to (optionally) initiate any of the distribution's parameters (e.g.
      :attr:`Distribution.variables`, :attr:`Distribution.aggregate` or :attr:`Discrete.bins`).

Apart from the :ref:`standard distribution parameters <dist_base_parameters>`, :class:`distribution.Discrete` has
the following extra parameters:

.. current_parameter_set:: Discrete
   :synopsys: Common distribution parameters.

.. parameter:: bins
   :type: :term:`list` or :term:`tuple`

   The bin structure. Can be specified as a *monotonically increasing* list of bin bounds, or, for equal sized bins,
   in the form ``(v_min, v_max, n_bins)``, where ``v_min, v_max`` is the minimum and maximum value respectively, and
   ``n_bins`` the number of bins.

   .. attention::

      When specifying the bin bounds explicitly, the upper and lower bounds must also be given. Thus,
      for :math:`n` bins, there should be :math:`n+1` bounds.

   For example, the following will create a discrete energy distribution with two bins:

   .. code-block:: py

      d = distributions.Discrete(variables='e')
      d.bins = [1.0E-11 * units.MeV, 6.25E-7 * units.MeV, 20.0 * units.MeV]

   When calling :func:`Distribution.get_value` or :func:`Distribution.set_value` on a `Discrete` distribution, bin index
   values can also be passed. For example, in the above distribution

   .. code-block:: py

      d.get_value(e=19 * units.MeV) == d.get_value(e=1)
      True

   .. note::

      For equal sized bins, it is recommend that the ``(v_min, v_max, n_bins)`` is used, as certain plugins may only
      support this option.

.. current_parameter_set:: distributions
   :synopsys: Module containing

.. py:class:: Interpolated(**kwargs)

   Represents a continuous one dimensional distribution, anchored at a discrete set of nodes in phase space, and using a
   specified interpolation rule.

Apart from the :ref:`standard distribution parameters <dist_base_parameters>`, :class:`distribution.Interpolated` has
the following extra parameters:

.. current_parameter_set:: Interpolated
   :synopsys: Common distribution parameters.

.. parameter:: nodes
   :type: :term:`list` or :term:`tuple`

   The points in phase space at which the distribution is anchored. Similar to :attr:`Discrete.bins`, with the same
   syntax.

.. parameter:: interpolation
   :type: linear, quadratic or cubic

   How values in between :attr:`Interpolated.nodes` are deduced. Either piecewise linear, quadratic or cubic splines.


.. current_parameter_set:: distributions
   :synopsys: Module containing

.. py:class:: Tensor(*dist, **kwargs)

   Create a distribution over a multi-dimensional phase space by combining separate distributions, using an outer (or
   tensor) product approach.

   :param dist: A sequence of `Distributions` that will be added to the outer product.
   :param kwargs: Specify the distributions to combine using their :ref:`phase space tokens <phase_space_tokens>`
      as keys. This pattern can only be used if each distribution is one dimensional (see the examples below).

   When using discrete (bin) distributions (such as :class:`distributions.Discrete`), then the tensor effectively
   subdivides each bin with another bin structure from a different portion of phase space.

   The domain of the tensor can be described mathematically as follows:

   If :math:`f_i:A_i\to R`, :math:`i=1,\ldots,n` are distributions on phase space :math:`A_i`, then the tensor
   :math:`T` is a distribution on the direct sum :math:`A_1\oplus\ldots\oplus A_n`. In particular, the individual
   phase spaces :math:`A_i` must be perpendicular, that is, the :attr:`Distribution.variables` of each of the distributions
   specified using `dist` (or `kwargs`) must be disjoint.

   For example, the following will create three energy bins, and further subdivide each energy bin into 10 spatial bins:

   .. code-block:: py

      d = Tensor(Discrete(variables='e', bins=[1.0E-11 * units.MeV, 6.25E-7 * units.MeV, 20.0 * units.MeV]),
                 Discrete(variables='x', bins=(-1.0 * units.cm, 1.0 * units.cm, 10)))

   The above can also be produced using the following equivalent keyword based construction:

   .. code-block:: py

      d = Tensor(e=Discrete(bins=[1.0E-11 * units.MeV, 6.25E-7 * units.MeV, 20.0 * units.MeV]),
                 x=Discrete(bins=(-1.0 * units.cm, 1.0 * units.cm, 10)))

   .. attention::

      The :attr:`Distribution.variables` in this case is deduced automatically, and should not be set explicitly.


.. py:function:: cartesian_mesh(x=None, y=None, z=None, transformation=None)

   Create a rectangular mesh distribution. This is a convenience function that creates a special
   :class:`distributions.Tensor` instance using :class:`distributions.Discrete` instances.

   :param x: The bin structure along the :math:`x` coordinate.
   :type x: :term:`list` or :term:`tuple`
   :param y: The bin structure along the :math:`y` coordinate.
   :type y: :term:`list` or :term:`tuple`
   :param z: The bin structure along the :math:`z` coordinate.
   :type z: :term:`list` or :term:`tuple`
   :param transformation: Optional transformation that should be applied to the mesh.
   :type transformation: :term:`affine_transformation`

   All the ranges (`x`, `y` or `z`), can be specified as an explicit list of bounds, or a simpler notation for equal
   sized bins (see :attr:`Discrete.bins`). If bounds are not specified along a particular coordinate direction,
   the mesh will be unbounded in this coordinate.

   For example, the following,

   .. code-block:: py

      m = distributions.cartesian_mesh(x=(-1.0 * units.cm, 1.0 * units.cm, 10),
                                       y=[0.0 * units.cm, 1.0 * units.cm, 2.0 * units.cm],
                                       z=(-10 * units.cm, 0 * units.cm, 10))

   is equivalent to

   .. code-block:: py

      m = distribution.Tensor(x=distributions.Discrete(bins=(-1.0 * units.cm, 1.0 * units.cm, 10)),
                              y=distributions.Discrete(bins=[0.0 * units.cm, 1.0 * units.cm, 2.0 * units.cm]),
                              z=distributions.Discrete(bins=(-10 * units.cm, 0 * units.cm, 10)))


.. py:function:: cylindrical_mesh(r, theta=None, axial=None, alignment='z', center=(0, 0, 0), transformation=None)

   Create a cylindrical mesh distribution.

   :param r: The radial bin structure.
   :type r: :term:`list` or :term:`tuple`
   :param theta: The polar angle bin structure. If not specified, no angular bins will be created.
   :type theta: :term:`list` or :term:`tuple`
   :param axial: The bin structure along the coordinate axis perpendicular to the radial plane. This coordinate is
     determined by the `alignment` parameter.
   :type axial: :term:`list` or :term:`tuple`
   :param alignment: Determines how the cylindrical mesh is aligned in the cartesian coordinate system. It can be
      specified by either giving the coordinate along which the `axial` mesh extend, or the radial plane:

      .. list-table:: Cylindrical mesh alignment options.
         :header-rows: 1

         * - Option
           - Description
         * - `'z'` or  `'xy'`
           - The `axial` mesh extends along the :math:`z`-axis, and the radial plane is :math:`xy`.
         * - `'x'` or  `'yz'`
           - The `axial` mesh extends along the :math:`x`-axis, and the radial plane is :math:`yz`.
         * - `'y'` or  `'xz'`
           - The `axial` mesh extends along the :math:`y`-axis, and the radial plane is :math:`xz`.
   :type alignment: :term:`string`
   :param center: Where the cylindrical mesh is centered in the radial plane.
   :type center: :term:`point`
   :param transformation: Optional transformation that should be applied to the mesh.
   :type transformation: :term:`affine_transformation`

   All the ranges (`r`, `theta` or `axial`), can be specified as an explicit list of bounds, or a simpler notation for
   equal sized bins (see :attr:`Discrete.bins`).

   .. note::

      Although `center` contains all three coordinate, only the values for the coordinates on the radial plane is used.
      For example, if `alignment` is **z**, then only the first two (:math:`x` and :math:`y` coordinates of `center` is
      used).

   In the cylindrical mesh, a call to :func:`Distribution.get_value` can use either the polar `r` and `theta` keywords to
   specify radial mesh positions, or the standard cartesian coordinates `x`, `y`, and `z`.


.. py:class:: Distributed(dist=None, sub_tag='bin')

   Models a distribution with components spread over discrete categories. Typical examples are distributions in
   different in-core channels, or ex-core positions.

   .. note::

      This distribution is created automatically when using certain targets (e.g. :class:`attached.InCoreChannels`). It
      is rarely necessary to instantiate, or modify this distribution type.

   :param dist: The distribution that will be used in sub categories. If not specified, the distribution might differ
      from one category to the next.
   :param sub_tag: The tag (or key) that will be used to specify the sub category in queries, e.g.
      :func:`Distribution.get_value`.

   For example, if the categories are core loading positions, a typical query from this distribution has the following
   form:

   .. code-block:: py

      v = dist.get_value(bin='CN', x=9.0 * units.cm, e=0.6 * units.eV)



Attaching Distributions
=======================

Distributions are usually attached to responses, in which case they are expected to be calculated, or sources, where
they describe how particles are produced. In both cases, the distribution can be considered overlay (that is, all model
elements are affected), or attached to particular model element(s), effectively restricting the response or source to
these element(s). The :mod:`attached` module contains some objects which can be used to dynamically find targets
based on certain rules. These are summarized below.

.. current_parameter_set:: attached
   :synopsys: Module that assists in

.. py:class:: InCore(predicate=None)

   Restricted to geometric elements (cells) in the :attr:`model.reactor_core`.

   :param predicate: Function object used to extract cells. Must take a cell instance as argument, and return True
      or False. If not specified, it is assumed that all cells in :attr:`model.reactor_core` are targeted.
   :type predicate: :term:`predicate` or None

   For example, to target all cells with a particular part name:

   .. code-block:: py

      tg = InCore(predicate=lambda c: c.part=='TargetPart')

   .. note::

      If :attr:`model.pool` is set and `predicate` is None, it will target :attr:`model.pool.core_cell`.

.. py:class:: InCoreChannel(channel, predicate=None)

   Restricted to geometric elements (cells) within a specified position in the :attr:`model.core_map`.

   :param channel: Position in the :attr:`model.core_map` to target.
   :param predicate: Function object used to extract cells. Must take a cell instance as argument, and return True
      or False. If not specified, the entire :attr:`component` loaded in `channel` is assumed.
   :type predicate: :term:`predicate` or None

.. py:class:: InCoreChannels(predicate=None)

   Targets all channels (or positions) within the :attr:`model.core_map`.

   :param predicate: Function object used to extract cells. Must take a cell instance as argument, and return True
      or False. If not specified, the entire :attr:`component` loaded in each channel is assumed.

   When this target is used, a :class:`distributions.Distributed` is created automatically, with the `bin` values
   equal to the position labels.

   .. note::

      If `predicate` is set, only the positions in :attr:`model.core_map` for which `predicate` return a non empty
      list of cells, will be present in the created :class:`distributions.Distributed`.


Creating Responses
==================

This section describes how to attach a distribution to a response function, which is then used to trigger
the calculation of these distributions in certain application modes (e.g. :ref:`critical_case <critical_case>`). A
response is created by instantiating a `response.ResponseDistribution` object:

.. code-block:: py

    from core.response import ResponseDistribution

    resp = ResponseDistribution()


.. current_parameter_set:: ResponseDistribution
   :synopsys: Attaching a distribution to a response

.. parameter:: particle
   :default: neutron

   The incident particle type this response is bound to.

.. parameter:: to

   Object used to deduce the model target(s) this response is attached to. Any of the objects listed in
   `Attaching Distributions`_.

   .. note::

      If not specified, the :attr:`ResponseDistribution.distribution` will be estimated over the entire model
      (as an overlay).

.. parameter:: distribution
   :type: `Distribution`

   The function shape of this response. Can be any of the response containers listed in `Distribution Containers`_.

   .. note::

      If not specified, a :class:`distributions.Flat` distribution is assumed.



Creating Sources
================

A distribution can also be attached to a source, which generates particles for the

