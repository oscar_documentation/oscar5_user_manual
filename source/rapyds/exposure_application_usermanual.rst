Creation of Burnup Dependent Material Libraries
***********************************************

Introduction
============

An exposure history (or archive) for an assembly is defined to be the average composition of its burnable materials
over a specified irradiation period. It is primarily used to estimate isotopic compositions at a given exposure. Only
a single composition for each burn bundle is stored at a depletion step. Thus, detailed spatial distribution is lost,
and the calculation is best performed in a simplified manner, using infinite environment approximations.

This application also facilitates lumping of isotopes into a proxy isotope, whose number density is calculated so that
total absorption is preserved.

Typical uses cases:

    - Provide detailed material compositions when importing assemblies based on available burnup estimation.
    - Fill in missing isotopes, or create an equivalent lump isotope, when
      :ref:`de-homogenizing<De-Homogenization Tool (ODEHT) User Manual>` material compositions from the core simulator.

Exposure application input module
=================================

You can use the :command:`create_module` utility to create a basic script in the *exposure* package. For instance,

.. code-block:: console

   $oscar5 MY_REACTOR.create_module exposure exp_type_1 --assembly MY_REACTOR_fuel_type_1 \
           --description 'Burnup dependent material compositions for fuel type 1'

which will create the *exp_type_1* script in *exposure*.

Apart from the standard application parameters, this application accepts the following parameters as input:

.. parameter:: assembly
   :type: :term:`component`
   :default: required

    The target assembly. Burnup will be performed for all burn bundles in this assembly. This parameter is required.

.. parameter:: target_fuel_bundle
   :type: :term:`bundle_tag`
   :default: ``fuel_bundle()``

    Burnable bundle on which exposure tabulation will be performed. Any bundle containing heavy metals can be selected.

.. parameter:: frame
   :type: :term:`region`
   :default: None

    By default the assembly will be used as is, that is, in its own universe surrounded by whatever fill material was
    used during assembly construction. This parameter is used to clip the model. For example,

    .. code-block:: py

       from csg import *

       parameters.frame = primitives.Cuboid(-0.5 * 7.71 * units.cm, 0.5 * 7.71 * units.cm,
                                            -0.5 * 8.1 * units.cm, 0.5 * 8.1 * units.cm,
                                            -10.0 * units.cm, 10.0 * units.cm)

    will only consider part of the assembly bounded by the specified cuboid.

.. parameter:: boundary_conditions
   :type: :term:`boundary_condition`
   :default: ``reflective()``

   Boundary conditions that should be applied on the :param:`frame` boundary.


.. parameter:: microscopic_isotopes
   :type: :term:`list` [ :term:`isotope` ]
   :default: None

   List of isotopes for which a microscopic reaction rate should be calculated. The application will calculate
   microscopic :math:`\sigma_a^i` for all isotopes :math:`i` in this list (averaged over the
   :param:`target_fuel_bundle`), at each depletion step. The *residual* macroscopic absorption at each step
   is defined as:

   .. math::

      \Sigma_r := \Sigma_a - \Sigma_{i} N_i \sigma_a^i

   where :math:`\Sigma_a` is the macroscopic total absorption rate, and :math:`N_i` the number density for
   isotopes :math:`i`. Thus, this explicit list of isotopes should include all isotope **not** desired in the
   residual (or lumped) composition.

.. parameter:: lump_isotope
   :type: :term:`isotope`
   :default: B-10

   Set the default isotope used for lumping. This will be added to :param:`microscopic_isotopes`. Other than that, this
   parameter is not used by the application, and the lumping procedure can always be customized later.


.. parameter:: irradiation_history
   :type: Custom
   :default: Required

   Defines the depletion steps at which exposures will be tabulated. Steps can be added using time,

   .. code-block:: py

      parameters.irradiation_history.add_step(duration=3 * units.days, power=35 * units.kW)

   or burnup units:

   .. code-block:: py

      parameters.irradiation_history.add_step(duration=3 * units.burnup, power=35 * units.kW)

   .. attention::

      Remember to set the power level for each step, as the system has no way of determining base power for a
      single assembly.

   If cumulative steps are given, remember to set the ``step_type`` flag:

   .. code-block:: py

      parameters.irradiation_history.step_type = core.irradiations.cumulative()

   See the :ref:`irradiation history manual<Irradiation histories>` for more information.


Treatment of burnable absorbers and other bundles
-------------------------------------------------

Any burnable absorbers (flagged with the ``ba_bundle()`` tag) and other burn bundles will be automatically depleted
along with the main bundle. To avoid this behavior, the bundles must be explicitly flagged as non burnable. For
instance, to switch off depletion of burnable absorbers:

.. code-block:: py

   parameters.assembly.fuel_bundles[core.assembly.ba_bundle()].depletion_mesh.burnable = False


When burnable absorbers are depleted, the application will also store material compositions for this bundle in a
separate archive. Support for other bundles will be added in future versions.

How results are used
====================

Output from exposure calculations are collected in an ``AssemblyExposureHistory``, and saved directly the
:param:`target_assembly` archive. Thus, it becomes part of the assembly definition, and will be used automatically
by history importers (and other down stream clients) to deduce material compositions. You can access this data by
manually loading the archive:

>>> from core.assembly import AssemblyArchive
>>> arc = AssemblyArchive('/path/to/my_assembly.asm')
>>> exp = arc.get_exposure_history()

If burnable absorbers were also depleted, its exposure archive can also be retrieved:

>>> ba_exp = arc.get_ba_exposure_history()


Running exposure calculations on the command line
=================================================

Command line interaction follows the general application interface described in
:ref:`Running applications from the command line`. The only difference is in the behavior of the **post** mode,
which is described below.

Post processing mode
--------------------

The post processing mode will collect output from the transport code, create an ``AssemblyExposureHistory`` and
store it directly in the target assembly's archive. See `How results are used`_ for instructions on how to access the
data.

