.. _plugin_support:

Plugin Data Model and Application Support
*****************************************



Application Support
===================

Notes on which :ref:`application modes <Applications>` are supported by the built-in target codes (plugins).

.. note::

   Unless otherwise stated, it can be assumed that an application parameter and/or application mode is fully supported.

.. _SERPENT_app_support:

SERPENT
-------

.. table:: SERPENT application parameter support.
   :class: tight-table
   :align: center

   +----------------------------------------------+--------------------------------------------------------------------------+
   | Parameter                                    | Notes                                                                    |
   +==============================================+==========================================================================+
   | :attr:`app_param.particles`                  | Maps to the `NPG` parameter on the `set pop` or `set nps` card.          |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.source_iteration`           | Maps to the `NSKIP` parameter on the `set pop` card.                     |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.max_iteration`              | Maps to the `NGEN` parameter on the `set pop` card.                      |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.bank_search`                | Only supported in a modified version of Serpent 2, which is available    |
   |                                              | upon request.                                                            |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.use_stochastic_mixing`      | Ignored, since temperature treatment is handled by the                   |
   |                                              | :attr:`app_param.doppler_broadening` flag.                               |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.thermal_hydraulic_feedback` | Currently not supported.                                                 |
   +----------------------------------------------+--------------------------------------------------------------------------+



.. table:: Notes on the SERPENT plugin application support.
   :class: tight-table
   :align: center

   +-----------------------------------------+--------------------------------------------------------------------------+
   | Application                             |  Notes                                                                   |
   +=========================================+==========================================================================+
   | :ref:`model_checker <model_checker>`    | Will perform a fixed source simulation, with source sampled uniformly in |
   |                                         | the target region. The region can be customized using the                |
   |                                         | :attr:`check.frame` and :attr:`check.center` parameters.                 |
   |                                         | A number of 2D plots will also created automatically, with resolution    |
   |                                         | determined by the :attr:`app_param.particles` value.                     |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`rod_calibration <rod_calibration>`| Modelled as an irradiation history in modified version, else as a        |
   |                                         | sequence of :ref:`cases <cases>`.                                        |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`core_follow <core_follow>`        | Basic irradiation histories (without control movement or model state     |
   |                                         | changes) are supported for all versions of Serpent 2. Rod movements,     |
   |                                         | bank searches and rig changes during an irradiation history is only      |
   |                                         | supported in a modified version of Serpent v2.1.23 or v2.1.29, which     |
   |                                         | is available upon request.                                               |
   +-----------------------------------------+--------------------------------------------------------------------------+

.. _MCNP_app_support:

MCNP
----

.. table:: MCNP application parameter support.
   :class: tight-table
   :align: center

   +----------------------------------------------+--------------------------------------------------------------------------+
   | Parameter                                    | Notes                                                                    |
   +==============================================+==========================================================================+
   | :attr:`app_param.particles`                  | Maps to the first entry on the `KCODE` or `NPS` cards.                   |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.source_iteration`           | Maps to the third entry on the `KCODE` card.                             |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.max_iteration`              | Maps to the fourth entry on the `KCODE` card.                            |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.bank_search`                | Not supported.                                                           |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.use_stochastic_mixing`      | If not active, the library with the nearest temperature will be used.    |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.doppler_broadening`         | Not supported.                                                           |
   +----------------------------------------------+--------------------------------------------------------------------------+
   | :attr:`app_param.thermal_hydraulic_feedback` | Currently not supported.                                                 |
   +----------------------------------------------+--------------------------------------------------------------------------+

.. table:: Notes on the MCNP plugin application support.
   :class: tight-table
   :align: center

   +-----------------------------------------+--------------------------------------------------------------------------+
   | Application                             |  Notes                                                                   |
   +=========================================+==========================================================================+
   | :ref:`plotter <geometry_plotter>`       | Not supported (plotting routines can not be accessed                     |
   |                                         | non-interactively).                                                      |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`model_checker <model_checker>`    | Will perform a fixed source simulation, with source sampled uniformly in |
   |                                         | the target region. The region can be customized using the                |
   |                                         | :attr:`check.frame` and :attr:`check.center` parameters.                 |
   |                                         | The location of lost particles (if any), can be viewed using the         |
   |                                         | `post --show` command.                                                   |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`rod_calibration <rod_calibration>`| Modelled as a :ref:`cases <cases>` application.                          |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`critical_case <critical_case>`    | :attr:`cpp.kinetics_parameters` are only supported in v6.0 or later.     |
   +-----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`core_follow <core_follow>`        | Only basic irradiation histories (without control movement or model state|
   |                                         | changes) are supported in v6.0 or later.                                 |
   +-----------------------------------------+--------------------------------------------------------------------------+

.. _MGRAC_app_support:

MGRAC
-----


.. table:: MGRAC application parameter support.
   :class: tight-table
   :align: center

   +--------------------------------------------------------------------------------+----------------------------------------+
   | Parameter                                                                      | Notes                                  |
   +================================================================================+========================================+
   | :attr:`app_param.thermal_hydraulic_feedback_options.heat_transfer_correlation` | Options map directly to MGRAC inputs   |
   +--------------------------------------------------------------------------------+----------------------------------------+
   | :attr:`app_param.thermal_hydraulic_feedback_options.safety_correlation'        | Options map directly to MGRAC inputs   |
   +--------------------------------------------------------------------------------+----------------------------------------+



.. attention::

   All applications require that the :attr:`model.nodal_representation` is set correctly.

.. table:: Notes on the MGRAC plugin application support.
   :class: tight-table
   :align: center

   +----------------------------------------+--------------------------------------------------------------------------+
   | Application                            |  Notes                                                                   |
   +========================================+==========================================================================+
   | :ref:`plotter <geometry_plotter>`      | Not supported.                                                           |
   +----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`model_checker <model_checker>`   | Not supported.                                                           |
   +----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`critical_case <critical_case>`   | Power shapes only supported on the pin cell mesh defined during the      |
   |                                        | homogenization step. Flux responses are only available on the nodal mesh,|
   |                                        | but also on pin cell (sub mesh) level in v1.2 or later.                  |
   |                                        | Kinetics parameters are only available in v2.0.                          |
   +----------------------------------------+--------------------------------------------------------------------------+
   | :ref:`core_follow <core_follow>`       | All features, including bank movements, searches, as well as rig         |
   |                                        | movements are supported.                                                 |
   +----------------------------------------+--------------------------------------------------------------------------+