.. _model_checker:

Check Model Compatibility
*************************

This application is used to check if a model is compatible with a particular target code, or plugin. It can also be
used to further isolate and resolve geometry and material errors.

.. attention::

   This application is fully integrated in the :ref:`configuration` and :ref:`assembly_archiver` applications as a sub
   mode, and it is rarely necessary to create a standalone input script.

.. _model_checker_typical_use_cases:

Typical Use Cases
=================

 1. Check if the geometry input is valid for a target code.
 2. Further isolate and resolve errors or incompatibilities with the model geometry.


.. _model_checker_parameters:

Additional Input Parameters
===========================

In the following `check` denotes a `model_checker` parameter set, created as follows:

.. code-block:: py

   import applications.model_checker as app

   check = app.parameters()

.. current_parameter_set:: check
   :synopsys: applications.model_checker.parameters

This application supports all :ref:`parameters related to input generation <general_application_parameters_summary>`,
as well as the following:

.. parameter:: target

   The assembly or core configuration that should be checked. Any of the following is supported:

    - Full :term:`filepath` to an assembly archive (*.asm*) file, or model configuration (*.mco*) file.
    - A :term:`component`.
    - A :ref:`complete model configuration <>`.

   .. note::

      When using the embedded mode in the :ref:`configuration` and :ref:`assembly_archiver` applications, this parameter
      is set automatically.

.. parameter:: frame
   :type: :term:`tuple` [:term:`length`]

   Specify a specific rectangular area to focus the test on. Should be three :term:`length` values, giving the :math:`x`,
   :math:`y` and :math:`z` extend respectively.

   If not specified, the entire bounding box of :attr:`check.target` will be used.

.. parameter:: center
   :type: :term:`point`

   Where the :attr:`check.frame` should be centred.

   .. note::

      This parameter is only used if ::attr:`check.frame` is set.


.. _model_checker_cli:
Command Line Usage
==================

This application supports the following command line flags:

.. option:: --config-file <str>

   Targets :attr:`app_param.config_file`.

.. option:: --target-mode <str>

   Targets :attr:`app_param.target_mode`. See :ref:`plugin_support` for how the application behaves in different target
   codes.

.. option:: --particles <int>

   Targets :attr:`app_param.particles`.

.. option:: --frame <float float float>

   Targets :attr:`check.frame`.

   If not specified, the bounding box of the entire model will be used.

.. option:: --center <float float float>

   Targets :attr:`check.center`.

The standard :ref:`pre <pre>`, :ref:`execute <execute>`, :ref:`post <post>`, and :ref:`run <run>` modes are supported
by this application mode.

.. _model_checker_examples:
Examples
========

The following
