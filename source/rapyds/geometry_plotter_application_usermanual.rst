.. _geometry_plotter:
Create Two Dimensional Plots of Slices Through a Model
******************************************************

Simple driver application used to create plots from planar slices through a model.

.. attention::

   The plotter is fully integrated in the :ref:`configuration` and :ref:`assembly_archiver` applications, and it is not
   rarely necessary to create a standalone input script.


.. _geometry_plotter_typical_use_cases:

Typical Use Cases
=================

  1. Check assembly geometry.
  2. Check model configuration geometry.
  3. Produce plots for documentation purposes (also see the ).

.. _geometry_plotter_parameters:

Additional Input Parameters
===========================

In the following `plotter` denotes a `geometry_plotter` parameter set, created as follows:

.. code-block:: py

   import applications.geometry_plotter as app

   plotter = app.parameters()

.. current_parameter_set:: plotter
   :synopsys: applications.geometry_plotter.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`, as well as the following:

.. parameter:: assembly
   :type: :term:`component` or :term:`filename`

   Specify target assembly or assembly archive name. If not specified the :attr:`app_param.model` will be used when
   producing the plots.

.. parameter:: z
   :type: :term:`length` or :term:`list` [:term:`length`]

   Cut plane positions perpendicular to the :math:`xy` plane. For each value specified here, a new cut plot will
   be created at :math:`z` equal to that value.

.. parameter:: x
   :type: :term:`length` or :term:`list` [:term:`length`]

   Cut plane positions perpendicular to the :math:`yz` plane. For each value specified here, a new cut plot will
   be created at :math:`x` equal to that value.

.. parameter:: y
   :type: :term:`length` or :term:`list` [:term:`length`]

   Cut plane positions perpendicular to the :math:`xz` plane. For each value specified here, a new cut plot will
   be created at :math:`y` equal to that value.

.. parameter:: frame
   :type: :term:`tuple` [:term:`length`]

   Specify the size of the frame in the to which the plot will be restricted.  Must be three :term:`length`
   values, specifying the size of the frame in :math:`x`, :math:`y`, and :math:`z` direction respectively.
   If not set, the bounding box of the entire :attr:`app_param.model` or :attr:`plotter.assembly` will be used.

.. parameter:: center
   :type: :term:`point`

   Set the center of the view :attr:`plotter.frame`. Defaults the center of the bounding box of the
   :attr:`app_param.model` or :attr:`plotter.assembly`.

.. parameter:: pixels
   :type: :term:`integer`

   The number of pixels per square centimeter. Determines the resolution and size of the plot.


.. _geometry_plotter_cli:

Command Line Usage
==================

This application supports the following command line flags:

.. option:: --config-file <str>

   Targets :attr:`app_param.config_file`.

.. option:: --target-mode <str>

   Targets :attr:`app_param.target_mode`. Also determines which plugin will be used if the built-in plotting fails.

.. option:: --frame <float float float>

   The size of the frame which the plot should be restricted to. Three floats given the :math:`x`, :math:`y` and
   :math:`z` coordinate sizes in centimeter.

   If not specified, the bounding box of the entire model will be used.

.. option:: --center <float float float>

   Center position of the frame. Accepts three centimeter values,one for each coordinate. It defaults to :math:`0,0,0`
   if not specified.

.. option:: --x <float ..>

   List (one or more) of cut positions perpendicular to the :math:`x`-axis. Values are assumed to be in centimeter.
   This will generate cuts perpendicular to the :math:`yz` plane.

.. option:: --y <float ..>

   List (one or more) of cut positions perpendicular to the :math:`y`-axis. Values are assumed to be in centimeter.
   This will generate cuts perpendicular to the :math:`xz` plane.

.. option:: --z <float ..>

   List (one or more) of cut positions perpendicular to the :math:`z`-axis. Values are assumed to be in centimeter.
   This will generate cuts perpendicular to the :math:`xy` plane.

.. option:: --pixels <int>

   Number of pixels per square centimeter. The higher this value, the higher the resolution of the generated plots.


.. _geometry_plotter_execute:

execute
-------

Generates the plot image(s). Supports the following options:

.. option:: --threads <int>

   Targets :attr:`app_param.threads`. Specify how many threads the plotting routine, or target code will use.

.. option:: --executable <str>

   Targets :attr:`app_param.executable`.

.. option:: --arguments <str>

   Targets :attr:`app_param.arguments`.

.. option:: --use-sub-code

   Force the use of :command:`--target_mode` instead of the built-in plotter. See the note below.


.. note::

   This application will by default first try to use the built-in plotter. For this to work,
   the :attr:`plotter.assembly` must have been archive with cell indexing active, that is, with the
   :command:`--search-tree` flag, or with mesh completion requested. When plotting a complete configuration, all
   assemblies must have been archived with cell indexing.

   If these conditions are not met, the plotter will resort to the target sub code. This behavior can be forced, by
   adding :option:`--use-sub-code`.


.. _geometry_plotter_examples:

Examples
========

The following illustrates how to use the plotter directly in the python interpreter.

>>> from core import *
>>> from applications.geometry_plotter import GeometryPlotter
>>> app = GeometryPlotter()
>>> app.parameters.assembly = '/full/path/to/my_assembly.asm'
>>> app.parameters.z = 0.0 * units.cm
>>> app.parameters.working_directory = '/full/path/to/where/images/should/be/saved'
>>> app.parameters.pixels = 100
>>> app.execute(threads=2)


