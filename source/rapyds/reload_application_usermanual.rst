.. _reload_application:

Performing Reload Calculations
******************************

The reload application is used to perform a number of *predictive* calculations, and then extract parameters from
these calculations which is used to estimate the safety and performance of the core for the coming cycle. Predictive
calculations are used to estimate the core inventory, and is therefore usually burnup calculations using typical plant
states, or criticality buckling (bank or boron searches). Certain core parameters are then extracted from separate
calculations at various time points in the predictive line(s).

The application's interface is very generic, allowing one to add multiple predictive lines, request an unlimited number
of outputs to be calculated from these lines, and combine the outputs using simple expressions or custom routines
to calculate the core parameters of interest.

Finally, the application also provides the functionality to generate a customizable report as part of the output.

.. _reload_typical_use_cases:
Typical Use Cases
=================

   - Check safety and utilization parameters for an upcoming cycle.

   - Perform a general design study, which requires predictive burnup calculations using different core configurations.

.. _reload_create_input_module:
Creating an Input Module
========================

The :command:`manager` utility can be used to create a template starting script:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager reload CYCLE_NAME [options]

This mode accepts the following optional arguments:

.. option:: --start-time <str>

   Specify the planned cycle start time. Must be a valid :term:`datetime` parameter. Initiates the :attr:`reload.start_time`
   parameter.

.. option:: --planned-cycle-length <str>

   The planned duration of the cycle for which predictive calculations will be performed. A string expression of a
   quantity with :term:`time` dimensions, e.g ``'6 months'``. If no units are specified, the value is assumed to be
   in days.

   Sets the :attr:`reload.planned_cycle_length` parameter.

.. option:: --base <str>

   Base reload module which this module should customize. Simply the module name (without any extensions). This
   module will then use the defaults defined in *base* input, and only modify a few cycle specific parameters.

.. option:: --report-template <str>

   Path to the template set used to generate the report.

.. option:: --configuration <str>

   Configuration module this application's model is based on.


For example, the following will create the module :command:`C003` in the :command:`reload` package, reading input form
:command:`base.py`:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager reload C003 --start-time '1977-10-30 08:00:00' --base base


.. _reload_parameters:
Additional Input Parameters
===========================

In the following, `reload`, denotes the application parameter set, instantiated as follows:

.. code-block:: py

   import applications.reload as app

   reload = app.parameters()

.. current_parameter_set:: reload
   :synopsys: applications.reload.parameters


Apart from the :ref:`standard application parameters <general_app_parameters>`, this application accepts the
following parameters as input:

.. parameter:: cycle_name
   :type: :term:`string`
   :default: Required

   Name of the cycle this reload calculation is predicting. For this application, it is an alias for
   :attr:`app_param.project_name`.

.. parameter:: start_time
   :type: :term:`datetime`
   :default: Required

   Calender date and time indicating were predictive calculations should start. For this application, it is an alias for
   :attr:`app_param.time_stamp`.

.. parameter:: planned_cycle_length
   :type: :term:`time`
   :default: Required

   The planned duration of the cycle. This parameter is used when auto creating irradiation histories, and as an initial
   guess when predicting the cycle length.

.. parameter:: repeat_predictive_step_structure
   :type: :term:`bool`
   :default: False

   Flag indicating that the *entire* cycle pattern should be repeated to fill :attr:`reload.planned_cycle_length`. If False,
   only the last entry in the pattern will be repeated.

.. parameter:: fix_rods_for_last_repeats
   :type: :term:`integer`
   :default: 0

   Switch off critical control searches for the last steps in predictive line calculations. This can be used to avoid problems
   with critical searches near the end of a cycle when reactivity margins drop, or just to extract the reactivity changes itself.

.. parameter:: rods_fixed_post_decay
   :type: :term:`bool` or :term:`time`
   :default: False

   Switch off critical control searches for steps after a shutdown period in the predictive case structure. If True,
   critical searches will be switches off after any decay step. If a :term:`time` value, searches will only be switched
   after after decay steps of duration *less* than this value.

.. parameter:: fixed_position
   :type: :term:`travel`
   :default: fully_extracted

   The fixed bank position that should be used whenever :attr:`reload.fix_rods_for_last_repeats` or
   :attr:`reload.rods_fixed_post_decay` is active. Either a single value, applied to all banks, or a :term:`dict`
   giving a value to each bank. For example,

   .. code-block:: py

      reload.rods_fixed_post_decay = 1 * units.days
      reload.fixed_position = {'control': control.fully_extracted, 'regulating': control.percent_inserted(70)}

.. parameter:: report_template
   :type: :term:`directory`
   :default: Required

   Root path of the report templates that should be used to generate the report for this application. See also
   :attr:`doc_param.report_template`.

.. parameter:: template_pre_processor_filter
   :type: :term:`regex`

   Filter that determines to which files the pre-processor should be applied. See also
   :attr:`doc_param.template_pre_processor_filter`.

   .. attention::

      The filter should be set so that it **excludes** any `.rst` file(s) with directives
      :ref:`directive <rapyds_directives>` which reference bound variables.

.. note::

   The above parameters will be applied to all predictive calculations added using :attr:`reload.add_predictive_calculation`.

In addition to the above parameters, the following methods are used to set input for the application:

.. py:function:: add_predictive_calculation(name, description=None, steps=None, fixed_bank=None, power=None)

   Adds a predictive calculational line. These calculations form the base from which parameters are extracted.

   :param str name: Name used to identify the predictive calculation.
   :param str description: String describing the role of the predictive line.
   :param steps: Either a single time dimensioned value, or a list of time dimensioned values. In the  former, an
            irradiation history with a single steps size is created. For the latter, the specified steps are added,
            then the last step is used to fill the history until :attr:`reload.planned_cycle_length` is reached. Negative
            values can be used to indicate down (decay) times in the sequence.
   :param fixed_bank: Specify a fixed bank position that will be used in each step. This can be a single :term:`travel`
            value, or :term:`dict` instance with a position per bank. Banks without fixed positions will trigger
            a rod search.
   :param power: Fixed power used in irradiation steps. If not specified, the
            :attr:`model.facility_description.design_power` will be used.
   :returns: The parameter set which defines the calculation's input.

   The returned parameter set can be used to customize the calculation by modifying the model, and/or the irradiation
   history.

   Critical bank searches are activated the usual way by setting the :attr:`app_param.bank_search` parameter:

   .. code-block:: py

      base = reload.add_predictive_calculation('Base', steps=[0.1 *units.days, 1 * units.days])
      base.bank_search = True
      base.bank_search.target_keff = 1.003

   which will activate searches for *all* banks. Alternatively, specify the banks that can be activated explicitly:

   .. code-block:: py

      base.bank_search = ['regulating', 'control']

.. py:function:: extract(output_parameter, predictive_line)

   Extract data directly from the predictive line calculation.

   :param output_parameter: Output parameter tag. Any output token from a :ref:`depletion calculation <core_follow_output>`.
   :param predictive_line: The predictive line from which the data should be extracted.
   :returns: A reference to the quantity, which can be added to the report using :attr:`reload.report`.

.. py:function:: calculate(output_parameter, predictive_line, point=0)

   Request the calculation of a (raw) physical quantity, which is typically produced by a snap shot calculation at a
   particular point.

   :param output_parameter: :ref:`Output parameter <app_output_tokens>` tag, as specified by one of the application modes.
   :param predictive_line: The predictive line from which the state of the core should be deduced. Must refer to an
    existing line. Can either be the line reference returned by :attr:`reload.add_predictive_calculation`, the line
    index, or the name used to identify the line.
   :param point: The time point in `predictive_line` at which the calculation is performed. If it is an integer,
       the core state at the *beginning* of that depletion step wil be used. Negative integers are used to
       index steps from the back of the irradiation history. In this case, the state at the *end* of the step will be
       used. Thus, 0 refers to the BOC state, while -1 to the EOC state. Alternatively a :term:`time` quantity can
       be specified (e.g ``1 * units.days``), in which case the state at the *beginning* of the step containing `point`
       will be used.
   :returns: A reference to the quantity, which can be used in expressions or functions which calculate values that
       appear in the report.


   The available `output_parameter` options are documented in each individual application mode. See
   :ref:`app_output_tokens` for a general discussion.

   .. attention::

      Since the requested parameters determine at which points in the predictive line material compositions should be
      saved, all requests should be added **before** executing the predictive line(s).

   These output parameters can be combined using arithmetic operations ``*,+,-,/``, and `maximum` and `minimum` to produce
   the quantity of interest.

   .. attention::

      Use the `maximum` and `minimum` functions, as the built-in python `max`, `max` methods won't work with these
      output tokens.

   .. note::

      As described in :ref:`Changing the underlying application parameters`, any parameter of the generated application
      can be modified. In particular, the :attr:`app_param.target_mode` can be modified, so that the case can be run
      with any available plugin.

.. py:function:: report(quantity, name, description=None, ranges=None, uncertainty=None, fmt=None)

   Adds the requested quantity to the generated report.

   :param quantity: An output parameter (as returned by :attr:`reload.calculate`), expression combining output values,
    or a custom function object calculating the value of interest.
   :param str name: Name that will be used to label the quantity in the report. It is also used as a placeholder when
    designing the report layout.
   :param str description: Any additional information.
   :param tuple ranges: Range in which the parameter should fall. The input is a :term:`tuple` containing the minimum
     and maximum value. Use `None` in an entry to indicate that the value is only bounded from one side, e.g.
     ``(0.5, None)`` for larger than only, and ``(None, 1.5)`` for less than only.
   :param float uncertainty: Typical uncertainty associated with this parameter. This combines modelling uncertainties
     with the expected deviation from plant that is usually deduced from a validation exercise.
   :param fmt: Function that should be used to write the parameter value in the report. Should always return a string
     instance. You can use any standard `python format statement <https://pyformat.info>`__, e.g.
     ``'{:.2f}'.format`` will write floating point values with two decimal places.  For dimensioned quantities, the
     `format_unit` utility can be used.
   :returns: The object that collects all the above parameters.

   .. attention::

      If the `fmt` keyword is specified, referencing this value in the document will always yield a string. In particular,
      the :ref:`var <Simple variable referencing>` documentation role can not contain any formatting statements.

Report Generation
=================

Just like the application itself, the report generation component is very flexible, allowing full customization of
contents, and output formats. Like all other documentation generation functions, the
`sphinx <http://www.sphinx-doc.org/en/master>`__ system is used to build documentation in html and pdf formats.

Structure of the report templates
---------------------------------

The report generation is a slightly specialized case of the standard
:ref:`docgen documentation <documentation_source_tree>` structure, with the intention that the same
structure will be applied to multiple reload cases at different time points. This report template structure consist
of the following components:

    - A collection of text files, with extension `.rst`. These mix basic
      `reStructuredText` markup with data extraction directives. See :ref:`sphinx_and_rst` and
      :ref:`rapyds_sphinx_extensions`.

    - A `conf.py` file which configures how the documentation will be generated using
      `sphinx <http://www.sphinx-doc.org/en/master>`__.


Variable placeholders
+++++++++++++++++++++

Points to a value registered using the :func:`reload.report`. The syntax used in the documentation is

.. code-block:: rst

   ${Variable Name}

where ``Variable Name`` was the `name` used in the :func:`reload.report` call. In addition to these dynamically
added variables, the system also makes the following available:

 ====================== ==============================================================================================
 Variable               Description
 ====================== ==============================================================================================
 cycle_name             Value of the :attr:`reload.cycle_name` parameter.
 planned_cycle_length   Value of the :attr:`reload.planned_cycle_length` parameter. Formatted to be printed in days.
 start_time             Value of the :attr:`reload.start_time` parameter.
 load_map               Current contents of the load map, giving the names of assemblies loaded from the inventory.
                        It expands to a :term:`labeledgrid`.
 ====================== ==============================================================================================

For example, the following

.. code-block:: rst

   This report describes the neutronic performance of the SAFARI-1 reactor, cycle :var:`${cycle_name}`.

Will expand to

.. code-block:: rst

   This report describes the neutronic performance of the SAFARI-1 reactor, cycle C1109-1.

.. attention::

   Only 'raw' variable references, that is, references that are **not** used within the arguments, options or body
   of a :ref:`directive <rapyds_directives>`, should be present in files which are going to pe parsed by the
   pre-processor. Set :attr:`reload.template_pre_processor_filter` parameter to only include files that use **no**
   :ref:`directives <rapyds_directives>`.

.. _reload_cli:
Command Line Usage
==================

The reload application follows the :ref:`standard command line usage <General Application Command Line Interface (CLI)>`:

.. code-block:: console

  $oscar5 MY_REACTOR.reload.my_module [options] [mode [options]]

All :ref:`basic arguments <Setting or modifying parameters using command line arguments>` are accepted. The following
table list the additional modes available, or the modes that have a custom role for this application:

====================================  ==================================================================================
 Run Mode                             Description
====================================  ==================================================================================
:ref:`pre <reload_pre>`               Generate all inputs.
:ref:`execute <reload_execute>`       Launch all calculations.
:ref:`post <reload_post>`             Collect all results.
:ref:`predictive <reload_pred>`       Access the predictive calculations.
:ref:`cases <reload_cases>`           Access all snap-shot cases used to extract additional parameters.
:ref:`prepare-report <reload_prep>`   Copy report templates and resolve place holders and macros.
:ref:`build-report <reload_build>`    Generate the report.
====================================  ==================================================================================

More details on each run mode are given in the sections below.

Typical usage sequence
----------------------

The following list the sequence in which the available modes should be used:

.. code-block:: console

   $oscar5 MY_REACTOR.reload.my_module prediction run
   $oscar5 MY_REACTOR.reload.my_module cases execute
   $oscar5 MY_REACTOR.reload.my_module post
   $oscar5 MY_REACTOR.reload.my_module prepare-report
   $oscar5 MY_REACTOR.reload.my_module build-report

.. _reload_pre:

pre
---

Generate input for *all* predictive lines *and* snap shot calculations. Accepts all flags from the general
:ref:`pre-processing mode <pre>`.

.. _reload_execute:

execute
-------

Executes all predictive line calculations (in the order they were added to the calculation), then all the individual
cases.

Accepts all flags from the general :ref:`execution mode <execute>`.

.. note::

   When the individual case calculations are launched, the will automatically call the :ref:`post processing <post>`
   mode of the predictive line they depend on. Hence inventories will be updated automatically.

.. _reload_post:

post
----

Extract all results and saves a temporary archive collecting all outputs requested using the `report` call.

Accepts all flags from the general :ref:`post mode <post>`.

.. attention::

   Currently, the :option:`--show` and :option:`--compare` is not implement for the top level application.

.. _reload_pred:

predictive
----------

Access all predictive calculations added using :attr:`reload.add_predictive_calculation`.  This sub mode
is a full application in its own right, with all the standard :ref:`modes <Application modes>`. When evoked from this
level, these modes apply to all predictive calculations. For example,

.. code-block:: console

   $oscar5 MY_REACTOR.reload.my_module --target-mode MODE predictive execute --force

will launch all predictive calculations using **MODE**.

Each individual predictive calculation is also available as a sub mode. For example, to launch only the `reactivity`
calculation

.. code-block:: console

   $oscar5 MY_REACTOR.reload.my_module --target-mode MODE predictive reactivity execute --force

This is useful when only certain calculation needs to be repeated.

.. _reload_cases:

cases
-----

Access all additional calculational cases. These were added by the system depending on the outputs requested using the
:attr:`reload.calculate` method. As with the :ref:`predictive mode <reload_pred>`, This sub mode
is a full application in its own right, with all the standard :ref:`modes <Application modes>`. When evoked from this
level, theses modes apply to all cases. For example,

.. code-block:: console

   $oscar5 MY_REACTOR.reload.my_module cases execute

will call the :ref:`execute <execute>` mode for all the auto generated cases.

.. note::

   Without the :command:`--force` flag, only the cases that are not currently running, or have no results available,
   will be executed.

.. attention::

   Any :ref:`command line parameter <Setting or modifying parameters using command line arguments>` set here, will
   overwrite the custom application parameters set during the
   :ref:`result token initiation <Changing the underlying application parameters>`. In particular,

   .. code-block:: console

      $oscar5 MY_REACTOR.reload.my_module cases --target-mode <MODE> execute

   will perform **all** case calculations with the specified plugin.

.. note::

   Since the system tries minimize the number of required calculations, output parameters will be grouped together,
   and there is not a one to one correspondence between outputs requested and calculations performed.

.. attention::

   The predictive calculations must be completed before starting any of these calculations.

.. _reload_prep:

prepare-report
--------------

This mode performs the following steps:

   1. Copy report templates from the :attr:`reload.report_template` path to the local
      :attr:`app_param.working_directory`.

   2. Collect all bound `variables <Variable placeholders>`_ into a binary file `.results.pkl`, which is
      copied to the local document source tree in :attr:`app_param.working_directory`.

   3. Apply the pre-processor to all document source files specified by :attr:`reload.template_pre_processor_filter`,
      replacing all `variables <Variable placeholders>`_ with their corresponding values.

Once this step is complete, the document source tree in :attr:`app_param.working_directory` should resemble the
:ref:`standard autodoc structure <documentation_source_tree>`.

This mode currently accepts no flags, and is used as follows:

.. code-block:: console

   $oscar5 MY_REACTOR.reload.my_module prepare-report

.. attention::

   Results must be collected prior to this step. Thus, first run

   .. code-block:: console

      $oscar5 MY_REACTOR.reload.my_module post

.. note::

   This mode must be executed again if:

    1. Any file in the :attr:`reload.report_template` was modified.

    2. The value of any bound variable was changed (e.g. by rerunning the cases), or a new variable was added.

.. _reload_build:

build-report
------------

Evoke the sphinx system to process the local sources, and generate the documentation. This mode accepts the following
flags:

.. program:: build-report

.. option:: --skip-html

   Don't generate HTML output, that is, generate PDF only.

.. option:: --skip-pdf

   Don't generate PDF output, that is, generate HTML only.

.. _reload_examples:
Examples
========

In the following example, two predictive calculations are performed. The first uses a core configuration, which will
produce the highest power density, while the second includes some fueled rigs, which is limiting in terms of reactivity.
Then peaking factor parameters are extracted from the first line, while reactivity related parameters are extracted
from the second.

The example uses the following directory structure:

.. code-block:: console
   :caption: Reload example directory structure.

    MY_REACTOR
         + - reload
           - c1000.py
           + - report_template
             + - source
               - index.rst
             - conf.py

The contents of some of the files are summarized below.

.. _reload_example:
.. code-block:: python
   :caption: Contents of c1000.py

   import applications.reload as app
   import applications.critical_case as cc
   import applications.core_follow as cf
   from core import *

   from ..configurations.my_base_configuration import model
   from ..configurations.my_rigged_configuration import model as rigged_model

   parameters = app.parameters()

   parameters.start_time = '10/10/2013 00:00:00'
   parameters.planned_cycle_length = 30 * units.days


   parameters.cycle_name = 'C1000'
   parameters.working_directory = utilities.path_relative_to(__file__, 'C1000')
   parameters.report_template = utilities.path_relative_to(__file__, 'report_template')

   # peaking predictive calculation
   peaking = parameters.add_predictive_calculation('peaking',
                                                   'Predictive calculation for the peaking limiting core',
                                                    steps=[0.1 * units.days,
                                                           0.25 * units.days,
                                                           0.75 * units.days,
                                                           1.5 * units.days,
                                                           3.0 * units.days])

   peaking.model = model

   peaking.bank_search = ['control', 'regulating']
   peaking.bank_search.target_keff = 0.99

   # reactivity limiting
   reactivity = parameters.add_predictive_calculation('reactivity',
                                                      'Predictive calculation for the reactivity limiting core',
                                                       steps=[0.1 * units.days,
                                                              0.25 * units.days,
                                                              0.75 * units.days,
                                                              1.5 * units.days,
                                                              3.0 * units.days])

   reactivity.model = rigged_model

   reactivity.bank_search = ['control', 'regulating']
   reactivity.bank_search.target_keff = 1.0

   # ---------------------------------------------------------------------------------------------
   # Extract some trending parameters

   days = parameters.extract(cf.cycle_progression, reactivity)
   banks = parameters.extract(cf.bank_positions(bank='control'), reactivity)

   eoc_u235 = parameters.extract(cf.eoc_u235_mass, reactivity)
   eoc_u235_map = parameters.extract(cf.eoc_u235_mass_map, reactivity)

   parameters.report(days, 'Days')
   parameters.report(banks, 'Bank Positions')
   parameters.report(eoc_u235, 'EOC U-235')
   parameters.report(eoc_u235_map, 'EOC U-235 Map')

   # ---------------------------------------------------------------------------------------------
   # Extract and report peaking factor related parameters

   avg = parameters.calculate(average_power_density, peaking, 1.5 * units.days)     # some time into the cycle
   mx = parameters.calculate(maximum_power_density, peaking, 1.5 * units.days)

   hottest_channel = parameters.calculate(maximum_power_density_channel, peaking, 1.5 * units.days)

   parameters.report(mx / avg, 'Peaking Factor')
   parameters.report(mx, 'Peak Power Density')
   parameters.report(hottest_channel, 'Peak Power Channel')


   total = configure_banks(all=['control', 'regulating'])
   all_in = parameters.calculate(total.banks_inserted(), reactivity, 0)        # keff with rods in
   all_out = parameters.calculate(total.banks_extracted(), reactivity, 0)      # keff with rods out

   beta_eff = 0.00716                                                          # could also use calculated value if available

   parameters.report((all_out - reactivity.bank_search.target_keff)/all_out/reactivity.bank_search.target_keff/beta_eff,
                     'Excess Reactivity')

   parameters.report((all_out - all_in)/all_out/all_in/beta_eff,
                     'Bank Worth')

   if __name__ == '__main__':
      app.main(parameters=parameters)


.. code-block:: python
   :caption: Content of conf.py

   import sys
   from core.utilities import load_file

   project = u'${cycle_name}'
   copyright = u''
   author = u''

   extensions = [
       'matplotlib.sphinxext.plot_directive',
       'sphinx.ext.mathjax',
       'sphinx.ext.autosectionlabel',
       'sphinxcontrib.bibtex',
       'sphinx.ext.intersphinx',
       'autodoc.sphinxext'                   # load the rapyds sphinx extensions
      ]

   # Register the binded variable set
   autodoc_variables = load_file('.results.pkl')

   # etc



.. code-block:: rst
   :caption: Content of index.rst

   ********************************************
   Reload Report for Cycle :var:`${cycle_name}`
   ********************************************

   Summary
   =======

   Some important safety and utilization parameters for :var:`${cycle_name}` are summarized in
   :numref:`summary_table`.

   .. _summary_table:
   .. list-table:: Safety and utilization parameters.
      :header-rows: 1

      * - Parameter
        - Value
        - Unit
      * - Planned cycle length
        - :var:`${planned_cycle_length}, days, {:.1f}`
        - Days
      * - Excess reactivity
        - :var:`${Excess Reactivity}, {:.1f}`
        - Dollar
      * - Total bank worth
        - :var:`${Bank Worth}, {:.1f}`
        - Dollar
      * - Peaking factor
        - :var:`${Peaking factor}, {:.2f}`
        -
      * - Peak power density
        - :var:`${Peak Power Density}, W/cc, {:.1f}`
        - W/cc
      * - Channel with peak power density
        - :var:`${Peak Power Channel}`
        -
      * - EOC U-235 mass
        - :var:`${EOC U-235}, kg, {:.1f}`
        - kg

   Cycle Progression
   =================

   .. plot_lines::
      :x_axis_label: Days
      :x_axis_unit: days
      :y_axis_label: Bank Position

      Control:
         x: ${Days}
         y: ${Bank Positions}


      Estimated critical bank positions.


