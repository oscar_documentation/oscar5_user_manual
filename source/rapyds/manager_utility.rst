Using the Manager Utility
*************************

Introduction
============

The **manager** utility is used to create basic input module templates, as well as manage the various data bases attached
to your model, using a simple command line interface. It is invoked using the following syntax:

.. code-block:: console

   oscar5 MY_REACTOR.manager <mode> [options]

The available modes and options will be described in the following sections:

=================  =====================================================================================================
 Mode               Section
=================  =====================================================================================================
*document*          `Generate Model Documentation`_
*facility*          `Managing the Facility Loading Data Base`_
*inventory*         `Managing the Inventory`_
=================  =====================================================================================================

.. note::

   The available modes and options can always be viewed using the *--help* switch, that is,

   .. code-block:: console

      oscar5 MY_REACTOR.manager --help

   will list all the available modes, and

   .. code-block:: console

      oscar5 MY_REACTOR.manager <mode> --help

   will list all the available options for the specified mode.

Creating Input Modules
======================

.. warning::

   These have not been ported from the individual application manuals yet!

Facility Loading
----------------

Core Follow
-----------

Generate Model Documentation
============================

.. attention::

   The way model documentation is generated is currently being ported to the new approach using sphinx directives, which
   is not compatible with the auto-extraction routine currently implemented in the manager utility. Thus, this command
   will be deprecated, or its purpose re-formulated, in the near future.

This mode produces the model documentation framework. It scans all input in the `model` and `compose` packages and
creates a set of restructured text input files, which is the processed into html (or pdf) output using the documentation
utility :ref:`sphinx <https://www.sphinx-doc.org>`.

.. code-block:: console

   oscar5 MY_REACTOR.manager document [options]

The following options are available:

.. option:: --force

   Force the regeneration of images and drawing sheets.

Managing the Facility Loading Data Base
=======================================

The facility data base simply stores the state of the various facilities at different time points, usually described by
input in the `facility-loading <Facility Loading>`_ package. Entries can be viewed and modified using the *facility*
mode:

.. code-block:: console

   oscar5 MY_REACTOR.manager facility [options]

The following options are available:

.. option:: -d, --data-base <str>

   Full :term:`filepath` to the facility management data base. The default is */path/to/MY_REACTOR/MY-REACTOR_facility*.

.. option:: -i, --info

   Prints the list of time point entries in the data base.

.. option:: -p, --print <str>

   Prints a summary of the facility state at the specified :term:`datetime`.

   .. note::

      The time point can be any value, and is not restricted to points saved in the data base. If a point not in
      the data base is requested, the state at the nearest point *before* the specified point will be provided.

.. option:: -r, --remove <str>

   Remove the specified :term:`datetime` from the data base.

   .. attention::

      In this case, the point must be valid time stamp in the data base, that is, in the list returned by the *--info*
      switch.

.. option:: -b, --backtrack <str>

   Remove all entries in the data base **after** the specified :term:`datetime`.


.. attention::

   Remember to enclose :term:`datetime` values that contains spaces in quotes, that is

   .. code-block:: console

      oscar5 MY_REACTOR.manager facility --print '11-JUN-2019 01:00:00'


Managing the Inventory
======================

An inventory stores the irradiation histories of assemblies with transmutable materials. It is usually updated during
`Core Follow`_ calculations, and assemblies are added using `Facility Loading`_ inputs. Any attached inventory can be
explored or edited using the *inventory* mode:

.. code-block:: console

   oscar5 MY_REACTOR.manager inventory [options]

The following options are available:

.. option:: -p, --path <str>

   Full :term:`directory` path of the inventory. The default is */path/to/MY_REACTOR/MY-REACTOR_inventory*.

.. option:: -r, --report <str>

   Prints a summary of all assembly states in the inventory at the specified :term:`datetime`.

.. option:: -b, --backtrack <str>

   Remove all entries in the inventory **after** the specified :term:`datetime`.

   .. attention::

      This cannot be easily undone, so use with care.

.. option:: -t, --timestamps <str>

   List all the entries of the specified assembly in the inventory. This gives all the time points at which the history
   for the assembly was modified.

   Example:

   .. code-block:: console

      oscar5 MY_REACTOR.manager inventory -t TARGET_ASM




