Glossary
++++++++

.. glossary::

    CSG
        Constructive Solid Geometry. Components constructed form simpler regions using transformations and boolean
        operations. In the contents of this manual, all regions are defined by a set number of parameters, as opposed
        to an arbitrary surface definition (that is, a parametric modeling approach is used).

    MCNP
        Monte Carlo N-Particle Transport Code

    MGRAC
        Multi-Group Reactor Analysis Code

    MTR
        Material Testing Reactor

    homogenized cross section
         Defined as the total reaction rate over a volume :math:`V` divided by the average flux:

         .. math::

            \frac{\Simga_{i\in Isotopes}\int_{V}\, dr N_i(r)\sigma^i_}