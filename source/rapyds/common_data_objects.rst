Application Data
****************

This section collects a number of data containers, or functions that is used in multiple applications.

.. toctree::

   depletion_meshes
   distributions
   irradiation_histories







