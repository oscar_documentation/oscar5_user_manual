Moderator Materials
===================

.. attention::

   The moderator materials here will respond correctly to state (temperature, density, pressure, boron concentration)
   changes, so it is important to use them in models were this behavior is important.

The following are all the pre defined materials currently available in the :mod:`material_library.moderators` package.

.. py:function:: material_library.moderators.LightWater(mass_density=None, temperature=None, pressure=None)

   Pure water.

   :param mass_density: Density of the water.
   :type mass_density: :term:`density`
   :param temperature: Temperature of the water.
   :type temperature: :term:`temperature`
   :param pressure: Water pressure.
   :type pressure: :term:`pressure`

   Either `mass_density` or `temperature` **and** `pressure` must be specified. In the latter case the
   `iapws <https://pypi.python.org/pypi/iapws>`__ steam table implementation will be used.

   Example:

   >>> from material_library.moderators import LightWater
   >>> from core import *
   >>> lwt = LightWater(temperature=60 * units.degC, pressure=1.8 * units.bar)
   >>> print(lwt)
   Material     : H2O
   Type         : moderator
   Mass density : 0.9832 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-1       11.19     %         6.57530e-02
   O-16      88.81     %         3.28765e-02
   Total     100.00              9.86294e-02


.. py:function:: material_library.moderators.BoratedWater(mass_density=None, temperature=None, pressure=None, total_density=None, boron_concentration=746)

   Water with a certain concentration of Boron in it.

   :param mass_density: Density of the water (**excluding** boron salt)
   :type mass_density: :term:`density`
   :param temperature: Temperature of the water.
   :type temperature: :term:`temperature`
   :param pressure: Water pressure.
   :type pressure: :term:`pressure`
   :param total_density: Total solution density (**including** boron salt)
   :type total_density: :term:`density`
   :param float boron_concentration: The amount of boron in parts per million (ppm)

   Either `mass_density` or `temperature` **and** `pressure` must be specified. In the latter case the
   `iapws <https://pypi.python.org/pypi/iapws>`__ steam table implementation will be used.

   If `total_density` is specified, the `mass_density` parameter is ignored.

   Example:

   >>> from material_library.moderators import BoratedWater
   >>> from core import *
   >>> lwt = BoratedWater(temperature=60 * units.degC, pressure=1.8 * units.bar, boron_concentration=600)
   >>> print(lwt)
   Material     : Borated Water
   Type         : moderator
   Mass density : 0.9838 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-Nat     11.18     %         6.57328e-02
   B-Nat     600.0     ppm       3.28972e-05
   O-Nat     88.76     %         3.28684e-02
   Total     100.00              9.86341e-02
