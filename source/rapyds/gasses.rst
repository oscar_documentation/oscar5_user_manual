Gasses
======

.. py:function:: material_library.gasses.DryAir(density=1.20479E * units.g / units.cc)

   Atmospheric air with no water content.

   :param density: Air mass density.
   :type density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.gasses import DryAir
   >>> air = DryAir()
   >>> print(air)
   Material     : Air(dry)
   Type         : void
   Mass density : 0.0012 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     124.0     ppm       7.49040e-09
   N-Nat     75.53     %         3.91212e-05
   O-16      23.18     %         1.05138e-05
   Ar-Nat    1.28      %         2.32966e-07
   Total     100.00              4.98754e-05


.. py:function:: material_library.gasses.Helium(density=0.164 * units.kg / (units.m * units.m * units.m))

   Natural helium.

   :param density: Air mass density.
   :type density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.gasses import Helium
   >>> air = Helium()
   >>> print(air)
   Material     : Helium
   Type         : gas gap
   Mass density : 0.0002 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   He-3      2.0       ppm       6.54921e-11
   He-4      100.00    %         2.46747e-05
   Total     100.00              2.46747e-05


.. py:function:: material_library.gasses.Nitrogen(density=2.86024625 * units.kg / units.m / units.m/ units.m)

   Natural nitrogen composition.

   :param density: Nitrogen mass density.
   :type density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.gasses import Nitrogen
   >>> nit = Nitrogen()
   >>> print(nit)
   Material     : Nitrogen
   Type         : gas gap
   Mass density : 0.0029 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   N-14      99.62     %         1.22541e-04
   N-15      0.38      %         4.35784e-07
   Total     100.00              1.22976e-04

.. py:function:: material_library.gasses.DampAir(density=1.20479E * units.g / units.cc)

   Atmospheric air with water content.

   :param density: Air mass density
   :type density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.gasses import DampAir
   >>> air = DampAir()
   >>> print(air)
   Material     : Air(damp)
   Type         : void
   Mass density : 0.0012 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     124.0     ppm       7.49040e-09
   N-Nat     75.53     %         3.91212e-05
   O-16      23.18     %         1.05138e-05
   Ar-Nat    1.28      %         2.32966e-07
   Total     100.00              4.98754e-05
