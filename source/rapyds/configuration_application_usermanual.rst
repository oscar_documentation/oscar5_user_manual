.. _configuration:

Viewing and Archiving Model Configurations
******************************************

This application mode is used to setup and interact with a complete model, constructed using the parameters described in
:ref:`Building Core Configurations`.

.. _configuration_typical_use_cases:

Typical Use Cases
=================

  1. To test, view and archive a complete model configuration.

.. _configuration_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager configuration MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'configurations'.

.. option:: --description <str>

   A short description of the input module.

A typical input module has the following form:

.. code-block:: py

   import applications.configuration as app
   from core import *
   from ..model import assemblies

   # Create parameter set
   model = app.new()

   # Set facility info
   model.facility_description.name = 'MY_REACTOR'
   model.facility_description.site = 'CORE'
   model.facility_description.type = 'MTR'
   model.facility_description.design_power = 10.0 * units.MW
   model.facility_description.design_pressure = 1.8 * units.bars
   model.facility_description.design_inlet_temperature = 40 * units.degC
   model.facility_description.design_coolant_flow_direction = 'DOWNWARDS'

   #
   model.inventory_manager.inventory = core.utilities.path_relative_to(__file__, '../MY_REACTOR_inventory')
   model.inventory_manager.model_path = core.utilities.path_relative_to(__file__, '../model')
   model.facility_manager = core.utilities.path_relative_to(__file__, '../MY_REACTOR_facility')

   # Set core layout, configure banks, set default state, etc

   if __name__ == '__main__':
    app.main(model=model)


.. _configuration_parameters:

Additional Input Parameters
===========================

This primary function is to define and store a complete model configuration. A new model instance is created by
calling :code:`model=app.new()`, whose parameters are then set as described in the
:ref:`model configuration <Building Core Configurations>`. This application mode has no other parameters to set.

.. _configuration_cli:

Command Line Usage
==================

.. code-block:: console

   $ oscar5 MY_REACTOR.configurations.my_config [options] <mode> [mode options]


Since this application has no special parameters to set, only the :command:`--log-level` flag is supported. The
supported application run modes are summarized below.


.. _conf_visualization:

visualization
-------------

Works just like the standard :ref:`visualization`, with the same command line flags.


.. _conf_archive:

archive
-------

Saves the `model` as a Model Configuration Object (`.mco`) file. It supports the following options:

.. option:: --name <str>

   The archive file name. If not specified, :attr:`model.name` is used.

.. _conf_plotter:

plotter
------

Creates two dimensional plots of slices through the model. Accepts all the :ref:`plotter application <geometry_plotter>`
:ref:`command line arguments <geometry_plotter_cli>`. Note that since only the
:ref:`plotter execution <geometry_plotter_execute>` is supported, the mode can be excluded from the command line
sequence. For example,

.. code-block:: console

   oscar5 MY_REACTOR.configurations.my_config plotter --z 0.0 --x 0.0 --pixels 10 --threads 4 --force

.. _conf_check_model:

check-model
-----------

This checks your model by running a simple simulation using the specified target code (or plugin). It is a critical
step in ensuring that the model is compatible, before running simulations with the specified plugin.

This modes uses the :ref:`model checker command line interface <model_checker_cli>`. For example,

.. code-block:: console

   oscar5 MY_REACTOR.configurations.my_config check-model --particles 128000 --config-file my_config.cfg --target-mode MCNP run --threads 24 --force

.. _configuration_typical_cli:

Typical command line usage
--------------------------

To visually inspect the configuration, then check if the model geometry is compatible with MCNP, use the following:

.. code-block:: console

   oscar5 MY_REACTOR.configurations.my_config visualization --interactive --parts
   oscar5 MY_REACTOR.configurations.my_config check-model --particles 128000 --config-file my_config.cfg --target-mode MCNP run --threads 24 --force

If there were lost particles (geometry errors), use the following the view them:

.. code-block:: console

   oscar5 MY_REACTOR.configurations.my_config check-model --particles 128000 --target-mode MCNP post --show

.. _configuration_examples:

Examples
========
