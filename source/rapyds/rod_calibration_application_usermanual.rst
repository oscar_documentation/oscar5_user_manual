.. _rod_calibration:

Rod Calibration Calculations
****************************

Application mode used to calculate differential and integral control rod worth as a function of insertion depth.

.. _rod_calibration_typical_use_cases:

Typical Use Cases
=================

This application mode is used to:

   1. Simulate a rod worth experiment, using experimentally measured data.

   2. Calculate the integral worth of a control rod as a function of its position (see :func:`rod_cal.auto_sequence`).

.. _rod_calibration_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager rod_calibration MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'projects'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --configuration <str>

   The configuration module that contains the model used in this script.

.. option:: --mco <str>

   Name (or path) of the model configuration archive that should be used in this script.

   .. attention::

      This option can not be specified when :option:`--configuration` is also specified.

.. option:: --time-stamp <str>

   Time point at which facility state should be recovered (see :attr:`app_param.time_stamp`). Must be a
   valid :term:`datetime`.

A typical input module has the following form:

.. code-block:: py

   import applications.rod_calibration as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   rod_cal = app.parameters()

   # Set some base application parameters
   rod_cal.time_stamp = '01-01-2020 00:00:00'

   # Set and modify the model
   rod_cal.model = model
   # etc

   # Application specific input
   rod_cal.rod_steps.base_power = 1 * units.kW         # Reactor power level at which calibrations were performed
   rod_cal.rod_steps.xenon_mode = 'equilibrium'        # Calculate equilibrium xenon mode at above power level

   rod_cal.rod_steps.add_step(critical_banks={'r1': control.percentage_extracted(50),
                                              'r2': control.percentage_extracted(45)},
                              perturbed_banks={'r2': control.percentage_extracted(50)})

   rod_cal.rod_steps.add_step(critical_banks={'r1': control.percentage_extracted(55),
                                              'r2': control.percentage_extracted(50)},
                              perturbed_banks={'r2': control.percentage_extracted(55)})
   # etc

   if __name__ == '__main__':
    app.main(parameters=rod_cal)


.. _rod_calibration_parameters:

Additional Input Parameters
===========================

In the following `rod_cal` denotes a `rod_calibrations` parameter set, created as follows:

.. code-block:: py

   import applications.rod_calibration as app

   rod_cal = app.parameters()

.. current_parameter_set:: rod_cal
   :synopsys: applications.rod_calibration.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`. The rod calibration sequence
is specified by adding rod position at various critical and perturbed positions. The :attr:`rod_cal.rod_steps` parameter
is used for this purpose. Calculation of differential and integral reactivities are handled by the
:attr:`rod_cal.results_viewer`.

.. parameter:: rod_steps

   This parameter is used to specify the calibration sequence. Steps in the sequence are added using the following
   function:

   .. py:method:: rod_steps.add_step(critical_banks, perturbed_banks, critical_state=None)

      Adds a step to the calibration sequence.

      :param critical_banks: The positions of all the banks at the critical set point for this step. Can also be
         set to None if a new critical position is not required.
      :param perturbed_banks: The positions of all the banks for the perturbed (non critical) state of this step.
         Can be set to None if no perturbation occurred in this step (only set point correction).
      :param critical_state: The model :term:`state` at the critical set point for this step. If not specified, it is
         assumed that the state did not change.

      Bank positions are specified as a :term:`dict` option, mapping bank names, to :term:`travel` instances.

      .. note::

         Although all bank/rod positions must be specified for the first `critical_banks`  point, subsequent steps only
         need to specify the perturbed bank(s) position (that is, the bank(s) that changed from the previous point.


      .. attention::

         Since certain target codes, or the templates generating input for them, can remove redundant steps, make sure
         that each position (either critical or perturbed), differs from the previous point in the sequence.

      .. note::

        The :attr:`rod_cal.rod_steps` is just a special kind of :ref:`irradiation history <Irradiation histories>`
        container, so many of the parameters are applicable for this container as well. In particular, consider
        setting :attr:`irr.base_power` and :attr:`irr.xenon_mode` to `'equilibrium'` if the power level is high
        enough that xenon will affect tha accuracy of your critical estimates.



.. parameter:: results_viewer

   Object used to customize how reactivities are calculated, define reference results (if available).

   .. parameter:: target
      :type: :term:`string`
      :module: rod_cal.results_viewer

      The name of the bank or rod that is being calibrated.

   .. parameter:: axial_scale
      :type: :term:`list`
      :module: rod_cal.results_viewer

      Manually specify the rod position scale, which is used on the independent axis of the reactivity curve plots. If
      not specified, it will be deduced automatically based on the :attr:`rod_cal.rod_steps` values.

      The number of entries must be one more than the number of reactivity insertion/removals present in
      :attr:`rod_cal.rod_steps`.

      .. note::

         This only affects the post processing of results, and not the simulation itself.

   .. parameter:: measured_differential_worth
      :type: :term:`list`
      :module: rod_cal.results_viewer

      Specify the measured (or reference) reactivities. The number of entries must be equal to the number of reactivity
      insertion/removals present in :attr:`rod_cal.rod_steps`.

   .. parameter:: beta_eff
      :type: :term:`float`
      :module: rod_cal.results_viewer

      Manually specify a :math:`\beta_{eff}` value, which can be used to scale the reactivities.

   .. parameter:: scale
      :module: rod_cal.results_viewer

      Specify how raw calculated reactivities are scaled. The raw reactivities are calculated as:

      .. math::

         \frac{k_{eff}^p - k_{eff}^b}{k_{eff}^p k_{eff}^b},

      where :math:`k_{eff}^b`, :math:`k_{eff}^p` is the base and perturbed reactivities respectively. There are a number
      of built-in scaling routines, which are summarized below

      +-----------------------+----------------------------------------------------------------------------+
      | Option                | Final results                                                              |
      +=======================+============================================================================+
      | 'to_dollar'           | :math:`\frac{k_{eff}^p - k_{eff}^b}{k_{eff}^p k_{eff}^b \beta_{eff}}`      |
      +-----------------------+----------------------------------------------------------------------------+
      | 'to_pcm'              | :math:`\frac{k_{eff}^p - k_{eff}^b}{k_{eff}^p k_{eff}^b} * 100000`         |
      +-----------------------+----------------------------------------------------------------------------+
      | 'to_mk'               | :math:`\frac{k_{eff}^p - k_{eff}^b}{k_{eff}^p k_{eff}^b} * 1000`           |
      +-----------------------+----------------------------------------------------------------------------+

      Alternatively, a custom scale function can be specified. This function, must take two arguments, the first being
      the raw reactivity value, and the second a pointer to the `rod_cal.results_viewer` object. For example, to extract
      reactivities in cents:

      .. code-block:: py

         def to_cents(raw, res):
             return raw / res.beta_eff * 100.0

         rod_cal.results_viewer.scale = to_cents



.. py:function:: auto_sequence(target, positions, critical=control.search(), target_keff=1.0)

   Create a basic :attr:`rod_cal.rod_steps` sequence, using fixed, or searched bank positions.

   :param target: The name of the bank/rod worth calculations should be performed for.
   :type target: :term:`string`
   :param positions: Sequence of positions for the `target` bank/rod.
   :type positions: :term:`list` [:term:`travel`]
   :param critical: How the critical set points are defined. Either a single :term:`travel` value, in which case
                    it will be applied to all banks/rods (except `target`), or a :term:`dict`, explicitly specifying
                    how each bank will be treated. If `None`, no critical correction will be performed.
   :param target_keff: Target :math:`k_{eff}`, which is used if `critical` contains banks whose position
                    should be searched.
   :type target_keff: :term:`float`

   For example, if the model has three rods (called 'R1', 'R2' and 'R3' respectively), then

   >>> rod_param.auto_sequence('R1', [control.percentage_extracted(x) for x in range(0, 110, 10)])

   would calculate reactivities for rod 'R1' at 10% intervals, and search new critical positions with 'R2' and 'R3'
   to compensate for the reactivity insertion. In this case, the first critical position (with 'R1' at 100% inserted) will
   also be searched.

   In order to keep 'R2' fixed, and only compensate with 'R3', use:

   >>> rod_param.auto_sequence('R1', [control.percentage_extracted(x) for x in range(0, 110, 10)],
   >>>                         critical={'R2': control.percentage_extracted(100), 'R3': control.search()})

   Finally, if a new critical set point should not be calculated, use

   >>> rod_param.auto_sequence('R1', [control.percentage_extracted(x) for x in range(0, 110, 10)], critical=None)

   .. note::

      The option not to search for new critical positions should only be used if the target rod is fairly light, as
      moving too far away from a critical position might skew the delta reactivities.


.. _rod_calibration_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`.

.. _rod_calibration_typical_cli:

Typical command line usage
--------------------------

.. code-block:: console

   oscar5 MY_REACTOR.calibration.my_rod --target-mode MY_TARGET --config-file MY_CONFIG run --force
   oscar5 MY_REACTOR.calibration.my_rod --target-mode MY_TARGET --config-file MY_CONFIG post --show

.. _rod_calibration_output_tokens:

Output Tokens
=============

.. py:currentmodule:: rod_calibration

This application provides the following output variables:

.. py:function:: differential_curve(transform_scale=None, transform_values=None, **kwargs)

   Retrieve the calculated differential reactivity values.

   :param transform_scale: Optional transformation function that will be applied to the independent variable (rod
      movement scale).
   :param transform_values: Optional transformation that will be applied to the reactivities.

   This token (via its :py:`get` method), returns two lists, the first being the rod movement scale, and the second the
   reactivities (as calculated by the :attr:`rod_cal.results_viewer` object).

   .. note::

      The returned value can be used directly as the `data` attribute of a line in the :rst:dir:`plot_lines` documentation
      directive.

.. py:function:: s_curve(transform_scale=None, transform_values=None, **kwargs)

   Retrieve the calculated cumulative reactivity over the specified rod movement.

   :param transform_scale: Optional transformation function that will be applied to the independent variable (rod
      movement scale).
   :param transform_values: Optional transformation that will be applied to the reactivities.

   This token (via its :py:`get` method), returns two lists, the first being the rod movement scale, and the second the
   integral worth (as calculated by the :attr:`rod_cal.results_viewer` object).

.. py:function:: total_worth(**kwargs)

   Retrieve the calculated total worth.

.. py:function:: measured_differential_curve(transform_scale=None, transform_values=None, **kwargs)

   Same as :func:`rod_calibration.differential_curve`, but returns the reference reactivities.

.. py:function:: measured_s_curve(transform_scale=None, transform_values=None, **kwargs)

   Same as :func:`rod_calibration.s_curve`, but returns the reference reactivities.

.. py:function:: critical_multiplication_factors(transform_scale=None, **kwargs)

   Retrieve the effective multiplication factor (:math:`k_{eff}`) at the critical set points.

   :param transform_scale: Optional transformation function that will be applied to the independent variable (rod
      movement scale).

   This token (via its :py:`get` method), returns two lists, the first being the positions of the
   :attr:`rod_cal.results_viewer.target` at each critical set point, and the second the :math:`k_{eff}` values.

   .. note::

      These values only make sense if experimental set points were provided.

.. _rod_calibration_examples:

Examples
========
