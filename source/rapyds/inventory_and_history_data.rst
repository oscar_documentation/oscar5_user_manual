Assembly Inventories
====================

This chapter explains what an assembly inventory is, how it's updated, and how its contents can be explored.

What is the structure of an Inventory?
--------------------------------------

An Inventory is simply a directory that contains a list of (binary) assembly history files. To use a specific inventory,
this :term:`directory` must be assigned to the :attr:`model.inventory_manager.inventory`.

How is an Inventory updated?
----------------------------

Currently, the inventory is updated by two :ref:`application <Applications>` modes:

   1. When new :ref:`batches <Adding assembly batches>` are added in the :ref:`facility_management <facility_management>`
      application. This will create a new assembly history file within the directory.

   2. The inventory is updated by the :ref:`core_follow <core_follow>` application mode, usually with beginning, and
      end of cycle material states. Only assemblies loaded in the core for the specific cycle is updated.

.. note::

   Many other application modes (such as :ref:`reload <reload_application>`), will create local copies (or branches) of an
   inventory. This should be considered as part of that application's output data, and not the model's state.

Exploring inventory contents using the Python API
-------------------------------------------------

The following basic example shows how to open an inventory, check that it has some assemblies in it, load the first assembly,
and check its current U-235 contents:

>>> from inventory.inventories import FlatInventory
>>> inv = FlatInventory('/path/to/MY_REACTOR/My_Inventory', model_path='/path/to/MY_REACTOR/model')
>>> assert (len(inv.assemblies() != 0))
>>> hist = inv.load_history(inv.assemblies()[0])
>>> print(hist.timestamps())
>>> asm = hist.get_at(hist.timestamps()[-1])
>>> print(asm.get_mass('U-235'))

A detailed description of the most important interface elements are given below.

.. py:function:: inventory.inventories.FlatInventory(path, model_path=None)

   Loads an inventory object from disk.

   :param path: Absolute path to to the inventory directory.
   :type path: :term:`directory`
   :param model_path: Absolute path to the directory containing your assembly library (that is, *.asm* files).
   :type model_path: :term:`directory`

   This :class:`FlatInventory` is essentially a :term:`dict` like object, mapping
   assembly names to :class:`AssemblyHistory` instances. The interface of both objects are described below.

   .. note::

      The name reflects the fact that the inventory does not store any relational data from the assembly histories
      it manages. Thus only name, and time based queries are possible.

The created :class:`FlatInventory` instance has the following methods that can be used to query its contents:

.. current_parameter_set:: inv
   :synopsys: inventory.inventories.FlatInventory

.. py:function:: assemblies()

   Returns a :term:`list` of all assembly names currently in the inventory.

.. py:function:: has_assembly(name)

   Checks if an assembly with given name is currently in the inventory.

   :param str name: Target assembly name.

.. py:function:: timestamps()

   Returns a :term:`list` of :term:`datetime` entries at which any assembly within the inventory was updated.

.. py:function:: backtrack(to)

   Remove all entries in the inventory *after* a specified time, essentially resetting the inventory state.

   :param to: Target time point. All entries will be removed after this point.
   :type to: :term:`datetime`

   .. note::

      This is similar to :ref:`manager inventory --backtrack <Managing the Inventory>`, except that assemblies which
      only have entries after this date, will not be removed.

   .. attention::

      This call can not be undone, so use with caution!

.. py:function:: decay_assemblies(target_time, assemblies='all')

   Perform decay (cool down) for assemblies managed by the inventory.

   :param target_time: Time point to which assemblies should be decayed.
   :type target_time: :term:`datetime`
   :param assemblies: Which assemblies should be decayed. Either a :term:`list` of assembly names, or a :term:`predicate`
      that filters assembly names. The keyword 'all' can be used to indicate that no filtering should be applied.

   This will create a new entry at `target_time` in all targeted assemblies, with new material compositions after
   performing radioactive decay from the previous saved point.

   .. note::

      Currently, although the rate equation is exact, only isotopes with neutron cross sections are stored in the
      material compositions.

.. py:function:: load_history(name)

   Opens an assembly history file.

   :param str name: Assembly name.

   :returns: An `hist <Exploring assembly history files using the Python API>`_ instance, whose interface is described
             in the section below.


Exploring assembly history files using the Python API
-----------------------------------------------------

This section describes how to query data from a single assembly history instance, as returned by
:attr:`inv.load_history`.

.. attention::

   None of the assembly history queries listed below will perform any decay calculations. The only way to currently
   decay assemblies to a specific point is through the :attr:`inv.decay_assemblies` call.

.. current_parameter_set:: hist
   :synopsys: inventory.history.AssemblyHistory

.. py:function:: timestamps()

   Return a :term:`list` of :term:`datetime` instances at which this history database was updated.

.. py:function:: get_bol()

   Return the assembly at Beginning of Life (fresh) conditions.

   This returns a complete :ref:`Assembly <Generic assembly>` instance.

.. py:function:: get_at(time=None)

   Retrieve the assembly state at a specific time point in the history.

   :param time: Target time point. If not specified, the state at the last entry is returned.

   This returns a complete :ref:`Assembly <Generic assembly>` instance.

   .. note::

      If `time` is not one of the saved points (as returned by :func:`hist.timestamps`), then the state at the
      closets point less than `time` is returned. In this case, *no decay* to the target time is performed.

.. py:function:: backtrack(to)

   Similar to :func:`inv.backtrack`, except that only this assembly history is affected.

.. py:function:: get_mass(isotope, at=None)

   Retrieve the total mass of an isotope (or element) at a specific point in the history archive.

   :param isotope: Target isotope or element.
   :type isotope: :term:`isotope`
   :param at: At which point the mass should be calculated. If not specified, the last point in the archive will be
     used.
   :type at: :term:`datetime`

   :return: A :term:`mass` value, representing the total mass of `isotope` in the assembly at the closest time point
       less than or equal to `at`.


.. py:function:: get_delta_mass(isotope, begin=None, end=None)

   Retrieve the total mass change of an isotope (or element) between tow points in the history archive.

   :param isotope: Target isotope or element.
   :type isotope: :term:`isotope`
   :param begin: The first reference point. If not specified, the first time point in the assembly history is assumed
      (i.e. :py:`hist.timestamps()[0]`).
   :type begin: :term:`datetime`
   :param end: The second point, which must be after `begin`. If not specified, the last point in the assembly
      history is assumed (i.e. :py:`hist.timestamps()[-1]`).
   :type end: :term:`datetime`

   :return: A :term:`mass` value, representing the total mass change in `isotope` from `begin` to `end`.

.. py:function:: import_inventory(source, steps=None)

   Import saved time points from another assembly history into this one.

   :param source: Another `AssemblyHistory` instance.
   :param steps: Time points from `source` that should be copied to this instance. If not specified, all points returned
     by :attr:`hist.timestamps` will be inserted.


Other ways to query Inventory contents
--------------------------------------

The inventory contents can be view using the CLI via the :ref:`manager utility <Managing the Inventory>`. An inventory
report can also be added to documents using the :ref:`inventory_report <inventory_report>` directive.