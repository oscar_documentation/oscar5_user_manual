.. _facility_management:
Facility Management
*******************

The facility loading application is used to update the location and material properties of named assemblies
tracked by the system. Typical use cases are:

    - Initiate the facility inventory at a particular point in time using material properties calculated from a previous
      version of OSCAR, or from another source.
    - Add new fuel batches.
    - Specify the core loading for a particular cycle.
    - Specify movement of tracked assemblies at a particular point in time.

.. note::

   The core loading saved here will be available to all applications which set the
   :param:`model.inventory_manager.load_time` parameter. The system will look for the closest actual loading prior to
   this date from the facility archive, and use it to populate the core. Thus, once a loading is specified, it need not
   be repeated in any of your time bound scripts.

Creating an input module
========================

The :command:`manager` utility can be used to create a basic template input module. It is used as follows:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager facility_loading <module name> [options]

where *<module name>* is the name of your input script (without any extensions), and must adhere to
`python module naming requirements <https://python.org/dev/peps/pep-0008/#package-and-module-names>`_. The following
options are available:

.. program:: manager

.. option:: --time <str>

   Real (calender) date and time at which the facility change should be implemented. Must be an ISO compatible date
   and time string (see :term:`datetime`).

.. option:: --configuration <str>

   Name of configuration whose model should be used as basis for this project. Should be the name (without any extensions)
   of an existing module in your **configuration** package.

.. option:: --description <str>

   Short description of the input.

For example, the following will create the **batch_41** module in the **facility_loading** package, using the **cold**
configuration module:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager facility_loading batch_41 --time '10-10-2011 00:37:20' --config-file cold --description 'Load fresh assemblies for batch 41'

Application parameters
======================

The following lists all the parameters available for this application mode:

.. parameter:: time
   :type: :term:`datetime`
   :default: Required

   Real (calender) date and time at which facility changes will be saved.

.. parameter:: model
   :type: ``ModelBuilder``
   :default: NA

   The underlying heterogeneous model. If you imported an existing model from **configurations**, you can simply
   pass it to this parameter.

   .. attention::

      For facility movements, the model's :param:`facility_manager` must be set. If new assemblies are added, the
      model's :param:`inventory_manager.inventory` must also be available.


Adding assembly batches
=======================

New fuel assemblies are added in batches, with one assembly type per batch. A batch is added using the following call:

.. py:function:: parameters.add_batch(loader, importer=None)

   :param loader: Assembly type.
   :type loader: :term:`loader`
   :param importer: Object used to set burnable materials for this batch. See `Importers`_. If no not specified, the
        assemblies will be added as built.
   :returns: A **batch** object, which is used to add assembly names.

For example:

.. code-block:: py

   from ..model import assemblies

   batch = parameters.add_batch(assemblies.MY_REACTOR_assembly_type_1)

Named assemblies are added using one of the following:

.. py:function:: batch.add(*names, **kwargs)

   Adds a specified list of assemblies.

   :param str names: Comma separated list of assembly names.
   :param kwargs: Optional keyword arguments that will be passed to the importer. These depends on the importer used,
        and is described below in `Importers`_.


.. py:function:: batch.add_all(source, pattern, **kwargs)

   Adds all assemblies from a specified source.

   :param source: Source from which assembly names should be extracted. Either an existing directory or map with
        assembly names.
   :type source: :term:`directory` or :term:`labeledgrid`
   :param str pattern: Pattern used to filter assembly names. If `source` is a directory, this should be a typical
        file mask, e.g. 'EF*.HIST'. Otherwise, it should be a valid :term:`regex` expression, e.g 'EF.*'.
   :param kwargs: Optional keyword arguments that will be passed to the importer. These depends on the importer used,
        and is described below in `Importers`_.

Multiple fuel batches can be added. When the input module is executed, the system will, for each batch:

    1. Create an assembly instance using the specified `loader` for each name added to the batch.
    2. If an `importer` is specified, it will be used to construct the material mesh for the assembly instances. Any
       additional keyword arguments will be passed to the `importer`.
    3. Each assembly instance will be saved to the inventory at :param:`time`.


Specify a facility loading
==========================

The facility manager application also models the movement of assemblies between facilities. A particular set of
movements are initiated with the following call:

.. py:function:: parameters.create_loading(time=None, tag=None, blank=False)

   Create a loading instance.

   :param time: Real (calender) date and time at which the loading should be saved. If None, the value of
        :param:`time` is used.
   :type time: :term:`datetime`
   :param tag: Tag used to identify the loading. See `Saving the load configuration`_.
   :param bool blank: Force the creation of a clean loading. If the flag is False, the loading of the previous
        step in the archive will be used to initiate this loading.

   :returns: A loading parameter set, which is used to load the core and move assemblies.

For example, the following is used to specify the actual core loading at :param:`time`:

.. code-block:: py

   loading = parameters.create_loading(tag=tags.actual)

Core loading
------------

The object returned by `create_loading` has the following parameters that facilitates loading of the core:

.. parameter:: core_load_map
   :type: :term:`labeledgrid`
   :default: NA

   Specify the core load map. Entries in this map are usually strings (assembly names) or empty :term:`placeholder`
   tokens, but assembly instances are also allowed. String entries should point to names that are available in the
   inventory.

   .. note::

      Unless the `blank` flag was passed to `create_loading`, or there are no saved loadings at previous time steps,
      the :param:`core_load_map` will be initiated with the load map at the point closest to it in the loading archive.

   .. attention::

      When assigning a grid to this parameter, and the existing load map (e.g from a previous time step) is not empty,
      only the entries that are **not** placeholders will be used to update the map.

      For example, suppose the load map was initiated as:

      .. code-block:: py

         loading.core_load_map = \
         [[  -,     '1',     '2'],
           'A',  'E-11', 'F-12' ]]


      Then setting,

      .. code-block:: py

          loading.core_load_map = \
         [[  -,     '1',     '2'],
           'A',       _,  'G-12' ]]

      the resulting load map will be

      .. code-block:: py

         [[  -,     '1',     '2'],
           'A',  'E-11', 'G-12' ]]


.. parameter:: core_rotation_map
   :type: :term:`labeledgrid`
   :default: NA

   Specify the rotation of loaded assemblies in the core. Entries in this map are usually integers (giving the rotation
   in counter clockwise rotation in degrees) or empty :term:`placeholder` tokens for positions that should not be
   rotated.


.. parameter:: assembly_facility_load_map
   :type: :term:`labeledgrid`
   :default: NA

   Used to specify the loading for intra-assembly facilities. Entries in these map are usually strings (assembly names)
   or empty :term:`placeholder` tokens, but assembly instances are also allowed. String entries should point to rig
   names that are available in the inventory.

   A map for each facility is specified by calling this parameter with the facility name. For example:

   .. code-block:: py

      loading.assembly_facility_load_map['my_facility_name'] = \
         [[  -,     '1',     '2'],
           'A',       _,  'R-12' ]]


Facility movements
------------------

Under construction!

Saving the load configuration
-----------------------------

The system allows for multiple configurations to be saved. Actual (which means the loading was used in the plant) are
specified using `tags.actual`. General Test configurations are created using:

.. py:function:: tags.candidate(name, description=None)

   :param str name: Name used to identify the candidate loading.
   :param str description: Description of the loading.

When a tag is passed to `create_loading`, this will be the tag used when saving the configuration. The save tag can also
be specified using:

.. py:function:: loading.save(tag, time=None)

   :param tag: Tag used to save the core and facility loading.
   :param time: Time at which loading should be saved. If not specified, the top level :param:`time` parameter is used.

Importers
=========

These objects are used to customize the material meshes for assemblies added to the inventory. The system provides a
number of predefined importers (described below), as well as a framework for defining custom importers.

Fresh importer
--------------

This is the simplest importer, and is only used to customize fresh material meshes. An instance is created using,

.. code-block:: py

    importer = FreshImporter()

and has the following parameters which customize the material exposure mesh:

.. parameter:: bundle
   :type: :term:`list` or tag
   :default: all_together

   Specify how primitives should be bundled together for depletion purposes. By default, all primitives will be depleted
   together (that is, in one bundle). See :ref:`Customizing the depletion mesh` for more information.


.. parameter:: segments
   :type: :term:`list`
   :default: None

   Specify the radial sub-divisions for each bundle. Must be a list of length equal to the number of :param:`bundles`.
   If not specified, no radial subdivision will be performed. See :ref:`Customizing the depletion mesh`
   for more information.

.. parameter:: axial_layers
   :type: :term:`integer` or :term:`list`
   :default: None

   Specify the axial material exposure mesh. If an integer, the active height of the assembly will be subdivided into
   this many equal sizes. Otherwise, the specified list is used to deduce segment sizes (from bottom to top).
   See :ref:`Customizing the depletion mesh` for more information.

.. parameter:: burnable
   :type: :term:`bool`
   :default: True

   Flag all exposure materials as burnable or not.


   The above parameters only apply to the main (usually fuel) burnable bundle. To customize the depletion mesh for
   other bundles, use:

.. parameter:: burn_bundles
   :type: Custom
   :default: NA

   Customize the depletion mesh for other burn bundles. The mesh for any valid :term:`bundle_tag` can be customized.
   For example:

   .. code-block:: py

      importers.burn_bundles[assembly.burn_bundle.absorber].burnable = False

   will switch off depletion for the materials in the `absorber` bundle of the assembly. Any of the above parameters
   (e.g. :param:`bundles`, :param:`segments`, :param:`axial_segments`) can be set.


   .. attention::

      The depletion mesh customization only affects the material activation in the heterogeneous calculational line.
      Exposures for the homogeneous line were fixed during the **cOMPoSe** step.

Given axial exposures
---------------------

Under construction!

Given axial depletion
---------------------

Under construction!

From previous OSCAR versions
----------------------------

Under construction!

Command line invocation
=======================

The load script is executed on the command line using the following signature:

.. code-block:: console

   $ oscar5 MY_REACTOR.facility_loading [options] <mode> [mode options]

with the following root options:

.. option:: --log-level <str>

   Set the application log level. Options are WARNING, INFO or DEBUG. The default level is WARNING.

The available modes are:

====================================  ==================================================================================
Run Mode                               Description
====================================  ==================================================================================
:ref:`visualization <visualization>`    Visualize the underlying model.
execute_                                Write the facility state. This is the default mode.
====================================  ==================================================================================

.. _execute:

Execution mode
--------------

Saves all assembly batches to the inventory, and saves all specified loadings to the facility archive. This mode accepts
the following flags:

.. option:: --force

   Overwrites any existing assembly history files.

.. option:: --skip-nodal-inventory

   Do not write **MGRAC** history files.

    .. note::

        In order to write **MGRAC** hisitory files, the model must have a nodal configuration specified.