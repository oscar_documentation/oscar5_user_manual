Documentation Macros
********************

This section describes how to use macros when documenting models or generating report templates. Macros are special
tokens which the pre-processor unroll into valid text that can be interpreted by the documentation engine. They are
used in documentation files as follows:

.. code-block:: none

   #macro_name[*args, **kwargs]

where `*args, **kwargs` are the positional and keyword arguments respectively.

.. attention::

   Please take note of the following restrictions and limitations when calling macros

Headings
========

These macros are used to write headings at various levels.

Title
-----

The highest level of heading:

.. code-block:: none

   #title[My Title]

which expands to

.. code-block:: rst

   ########
   MY TITLE
   ########

Part
----

Second level heading:

.. code-block:: none

   #part[My Part]

which expands to

.. code-block:: rst

   #######
   My Part
   #######

Chapter
-------

Denote a specific chapter in the documentation:

.. code-block:: none

   #chapter[My Chapter]

which expands to

.. code-block:: rst

   **********
   My Chapter
   **********


Section
-------

Next level heading:


.. code-block:: none

   #section[My Section]

which expands to

.. code-block:: rst

   My Section
   ==========

Sub-section
-----------

Next level section.


.. code-block:: none

   #subsection[My Section]

which expands to

.. code-block:: rst

   My section
   ----------


Sub-sub-section
---------------

Lowest level section.


.. code-block:: none

   #subsubsection[My Section]

which expands to

.. code-block:: rst

   My section
   ^^^^^^^^^^


.. note::

   These macros are designed specifically to address the problem of variable or unknown heading lengths, that is, when
   your heading contains a variable token:

   .. code-block:: none

      #chapter[Detailed description of ${cycle_name} core characteristics]

.. attention::

   These levels are merely a guide, since the documentation system still automatically deduce heading levels for each
   individual source file.

Data Elements
=============

.. attention::

   Most of these macros have been replaced with te use of proper :ref:`directives <rapyds_directives>`, and support for
   them might be dropped in future versions.

Date
----

Extract and format a date.

.. code-block:: none

   #date[value, fmt=<str>]

Arguments
+++++++++

The first argument must be a valid :term:`datetime` object.

Keyword arguments
+++++++++++++++++

Add a format specifier:

  - *fmt=<str>*: specify how the date should be printed. Any sequence of python `strftime <http://strftime.org>`_ day,
    month and year format tokens is accepted. The default is `'%x'`, which will
    use the locale’s appropriate date representation.

Examples
++++++++

.. code-block:: none

    #date[${start_time}, fmt='%d %B %Y']

will expand to

.. code-block:: none

    12 September 2007


Time
----

Extract and format a time.

.. code-block:: none

   #time[value, fmt=<str>]

Arguments
+++++++++

The first argument must be a valid :term:`datetime` object.

Keyword arguments
+++++++++++++++++

Add a format specifier:

  - *fmt=<str>*: specify how the time should be printed. Any sequence of python
    `strftime <http://strftime.org>`_ hour, minutes and seconds format tokens are accepted. The default is `'%X'`, which
    will use the locale’s appropriate time representation.


Examples
++++++++

.. code-block:: none

    #time[${start_time}, fmt='%H:%M:%S']

will expand to

.. code-block:: none

    10:51:44


Dimensioned quantities
----------------------

Write a dimensioned quantity in specified units:

.. code-block:: rst

   #in_unit[value, unit, fmt=<str>]

Arguments
+++++++++

The first argument is the value to be formatted, the second the unit to which it should be converted.

.. attention::

   The dimension of the value and the unit must be the same.


Keyword arguments
+++++++++++++++++

An optional format specifier:

  - *fmt=<str>*: Describe how the scalar value once converted to the target unit should be printed.


Examples
++++++++

The following will cast the value to kg, and format to two significant digits, appending the unit:

.. code-block:: rst

    #in_unit[${BOC U-235 mass}, 'kg', fmt='{:.2f} kg']



Extract Elements
----------------

Retrieve and element from a nested set of arrays

.. code-block:: none

   #getitem[array, i, j, k, ...,
            unit=<str>,
            fmt=<str>]

Arguments
+++++++++

The first argument must be a multi-dimensional array, with the subsequent arguments a sequence of level indices
(strings or integers). This macro then returns:

.. code-block:: none

   array[i][j][k]...

.. note::

   The return value depends on the dimensionality of the array, and the number of level indices. For example, if the
   array is two dimensional, and only one index is supplied, a one dimensional array is returned. In this case,
   two level indices must be supplied to get a single value.

Keyword arguments
+++++++++++++++++

The following optional keyword arguments are supported:

  - *unit=<str>*: the unit in which a returned value should be cast,
  - *fmt=<str>*: format string used to write the output to a string.

Examples
++++++++

Get a value from a one dimensional array:


.. code-block:: none

   #getitem[${Days}, 0, unit='days']


To get the entry at position A1 in the first entry of a sequence containing :term:`labeledgrid` instances:

.. code-block:: none

   #getitem[${Core Masses}, 0, 'A1', unit='g', fmt='{:.1}']


Tables
------

Simple table generator.


.. code-block:: none

   #table[label0, data0, label1, data1, ...,
          title=<str>,
          unit0=<str>, unit1=<str>, ...,
          fmt0=<str>, fmt1=<str>, ...]

Arguments
+++++++++

Two values for each column in the table, with the first value in each pair the column label (a string), and the second
the column data (a :term:`list`). The latter is usually a variable token.

Keyword arguments
+++++++++++++++++

The following optional keyword arguments are supported:

   - *title=<str>*: which sets the title of the table.
   - *unit<i>=<str>*: unit in which values in column *i* should be reported.
   - *fmt<i>=<str>*: format string used to write entries in the column *i*.

.. note::

   If the formatting for a column is not specified, default string formatting will be used.

Examples
++++++++

The following will extract row data from the variables ``Bank Names`` and ``Bank Position``, cast the second
column to centimeters, and print values with one decimal place:

.. code-block:: none

   #table['Rod Number', ${Bank Names}, 'Rod Position (cm)', ${Bank Position}, unit1='cm', fmt1='{:.1f}']


Line Plots
----------

Simple line and scatter plots. Can plot multiple lines, on different :math:`y`-axis.

.. code-block:: none

   #plot[x=<list>, y=<list>, label=<str>, x_axis_label=<str>, x_axis_unit=<str>,
          y_axis_label=<str>, y_axis_unit=<str>, color=<str>, marker=<str>, line_style=<str>,
         x2=<list>, y2=<list>, label2=<str>, y2_axis_label=<str>, color2=<str>, marker2=<str>, line2_style=<str>,
         ...]

Arguments
+++++++++

Only keyword arguments are allowed.


Keyword arguments
+++++++++++++++++

The following keyword arguments are supported:

    - *x=<list>*: points on the :math:`x`-axis for the first (principle) line. Usually assigned a variable token which
      points to a :term:`list` of values.
    - *y=<list>*: points on the :math:`y`-axis for the first (principle) line. Usually assigned a variable token which
      points to a :term:`list` of values.
    - *label=<str>*: label of the first (principle) line. Labels are used to identify lines in the legend.
    - *x_axis_label=<str>*: label used for the :math:`x`-axis.
    - *x_axis_unit=<str>*: unit in which :math:`x` points should be scaled. If not specified, it will be deduced from
      the :math:`x` values.
    - *y_axis_label=<str>*: label used for the principle (left) :math:`y`-axis.
    - *y_axis_unit=<str>*: unit in which :math:`y` points should be scaled. If not specified, it will be deduced from
      the y values.
    - *color=<str>*: color of the first (principle) line. If not specified, one will be automatically assigned.
    - *marker=<str>*: point markers used on the first (principle) line. If not specified, one will be automatically
      assigned. Any valid `matplotlib marker <https://matplotlib.org/api/markers_api.html>`_ can be specified.
    - *line_style=<str>*: Style used to render the first principle line. The available options are listed
      `here <https://matplotlib.org/gallery/lines_bars_and_markers/line_styles_reference.html>`_. Use *line_style='None`*,
       if no line between point markers is desired. The default is a solid line.

Additional lines are added by specifying the above parameters, with an index appended, that is, *x<i>*, *y<i>*,
*label<i>*, *color<i>*, etc.

.. note::

   If *x<i>* is not specified, the line is assumed to have the same :math:`x` points as the principle line.

.. note::

   When adding secondary lines with dimensioned data different than the principle line, the system will add a second
   axis (on the right of the graph) automatically, with label equal to the line label. To have more control,
   specify *y<i>_axis_label*, and *y<i>_axis_label*.

Examples
++++++++

The following will plot two lines with different units:

.. code-block:: none

   #plot[x=${Days}, y=${Bank Positions}, x_axis_label='Cycle progression (days)',
        y_axis_label='Bank position (percent extracted)', x_axis_unit='day', y2=${Core U235 Mass},
        y2_axis_label='U-235 mass(g)', y2_axis_unit='g']



Maps
----

Show two dimensional discrete field data on a rectangular grid. The grid patches can display values of attached fields,
and color the background based on a specified field.

.. code-block:: none

   #rectangular_map[width=<float>, height=<float>, fontsize=<int>, show_grid=<bool>, position_labels=<int>,
                    label=<str>, data=<grid>, show_value=<bool>, colormap=<str>, unit=<str>, fmt=<str>,
                    color_by=<str>, show_scale=<bool>,
                    label2=<str>, data2=<grid>, show_value2=<bool>, colormap2=<str>,
                    ...]

Keyword arguments
+++++++++++++++++

The following keyword arguments are supported:

    - *width=<float>*: The width (:math:`x` size) of grid elements. Either a constant value (assumed to be in
      centimeters if a scalar), or a list of values if the pitch is not constant. Default value is 8 cm.
    - *height=<float>*: The height (:math:`y` size) of grid elements. Either a constant value (assumed to be in
      centimeters if a scalar), or a list of values if the pitch is not constant. Default value is 8 cm.
    - *fontsize=<int>*: Size (in pts) of labels printed in the grid patches. Default is 12 pts.
    - *show_grid=<bool>*: Draw the grid lines separating grid patches. Default is True.
    - *position_labels=<int>*: Were to put the row and column labels of fields. The options are:

          - `1`: Labels are placed on the axis, with column labels on the top of the graph, and row labels on the left.
          - `2`: Labels are placed in the top left corner of mesh patches.
          - `3`: No labels.

      The default is `2`.
    - *label=<str>*: Label identifying the first data set.
    - *data=<grid>*: First data set. Must point to a :term:`labeledgrid`. The first data set is used to determine the
      map size, and column and row labels.
    - *show_value=<bool>*: Flag indicating if a text representation of the data values should be shown in the grid patches.
      The default is True.
    - *unit=<str>*: Unit the data values should be cast to.
    - *fmt=<str>*: Format statement for data values. If not specified, the default string formatting will be used.
    - *colormap=<str>*: Color scale used to color the background patches for this data. Any
      `matplotlib color map <https://matplotlib.org/examples/color/colormaps_reference.html>`_, or custom scale. If
      not specified, and data contains numerical values, one is assigned automatically by the system.
    - *color_by=<str>*: Color the background mesh patch using the specified field. Must be a value used for *label*, or
      *lable<i>*. If this argument is not specified, no colors will be assigned to the mesh patches.
    - *show_scale=<bool>*: Flag indicating if a scaled color bar should be included on the right of the map.

Additional data fields are added by specifying the field parameters with an index appended, that is *label<i>=*,
*data<i>=*, etc.


Examples
++++++++

The following creates a map showing a field with assembly names, and masses:

.. code-block:: none

   #rectangular_map[label='Assemblies', data=${load_map}, label2='BOC U-235 mass(g)',
                   data2=${BOC U235 Mass Distribution}, unit2='g',
                   color_by='BOC U-235 mass(g)', show_scale=True, width='7.71', height='8.1']

Assembly Data
=============

Extract and print assembly data.

General table formatting keywords
---------------------------------

These keywords are common to all data that are produced in table form. It depends on the kind of restructured text
table that will be produced.

Basic table
+++++++++++

Produces a basic `table <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#tables>`__. The following
keywords defines what is included in the table, and how the text table will be produced:

    - *fmt=str*: Used to format all entries in the table. Either a function object returning a string, or a
      :term:`format` statement.
    - *max_width=120*: The maximum column with.
    - *min_width=10*: The minimum column width.
    - *include_column_headers=True*: Flag indicating that column headers should be included in the table.

.. note::

   The *max_width* and *min_width* parameters only affect the text form of the table produced, and not the final HTML
   or PDF version, as this is controlled by parameters within the documentation system itself.

List table
++++++++++

Produces a `list style table <https://docutils.sourceforge.io/docs/ref/rst/directives.html#list-table>`__. The following
keywords customizes the table contents and structure:

    - *title=<str>*: The table heading.
    - *align='center'*: How entries should be aligned in the final table (HTML or PDF). Options are 'left', 'center' or
      'right'.
    - *include_column_labels=True*: Flag indicating that column headers should be included in the table.

.. attention::

   If the `title` keyword is present when formatting tables, a list style table will always be produced.

Assembly composition
--------------------

Format the assembly fissionable material composition as a table of data:

.. code-block:: none

   #assembly_composition[asm, **kwargs]

Arguments
+++++++++

    - *asm*: An assembly instance.


Keyword arguments
+++++++++++++++++

Any table formatting arguments (`General table formatting keywords`_).


Assembly structure
------------------

Extract assembly bundle structure in table format.

.. code-block:: none

   #assembly_structure[asm, **kwargs]

Arguments
+++++++++

    - *asm*: An assembly instance.


Keyword arguments
+++++++++++++++++

Any table formatting arguments (`General table formatting keywords`_).

Assembly movable components
---------------------------

Extract assembly dynamic structure (e.g control rod movements) in table format.

.. code-block:: none

   #assembly_structure[asm, **kwargs]

Arguments
+++++++++

    - *asm*: An assembly instance.


Keyword arguments
+++++++++++++++++

Any table formatting arguments (`General table formatting keywords`_).

Assembly documentation
----------------------

Extract the full assembly documentation tree.

.. code-block:: none

   #assembly[asm, tables, **kwargs]

Arguments
+++++++++

    - *asm*: An assembly instance.
    - *tables*: List of data tables that should be included.


Keyword arguments
+++++++++++++++++

Any table formatting arguments (`General table formatting keywords`_).



Inventory Reporting
===================

Extracting and formatting data from assembly history files.

Inventory table
----------------

Create a table with data per assembly for the entire inventory:

.. code-block:: none

   #inventory_report[inventory, columns, at=<datetime>, start_date=<date_time>, flt=<predicate>, include_totals=<bool>, ...]

Arguments
+++++++++

    - *inventory*: Must be either the full path to the inventory or an inventory instance. The latter is automatically
      saved to the `${Inventory}` variable by many applications.
    - *columns*: This defines what is placed into the tables. It is a list of entries (one per column), with each entry
      a tuple of two to four values:

        1. The column title.

        2. What data that should be extracted from the inventory entry. The following options are supported:

            - 'name', the assembly name
            - 'type', the assembly type name
            - 'energy', the total energy delivered
            - 'exposure', which will extract the exposure value (energy  per mass)
            - 'heavy_metals', which will extract the total heavy metal mass
            - Any isotope or element name, in which cases total masses will be extracted.
            - A custom function, which accepts a `AssemblyHistory` instance and returns the value of interest.

        3. When or how data should be extracted from the inventory. The following options are supported:

            - None (or empty place holder), in which case data is extracted using the `at` parameter
            - A valid :term:`datetime`.
            - 'BOL', in which case the Beginning of Life data for the assembly is used.
            - The keywords "since [target]", then the  **absolute** difference from the specified target, which can be any
              of the above i.e. None (in which case *start_date* is used), a :term:`datetime` or 'BOL'.
            - The keywords "relative [target]", then the **relative** difference from the specified target, which can
              be any of the above i.e. None (in which case *start_date* is used), a :term:`datetime` or 'BOL'.

        4. How data should be formatted. This defaults to the standard python string formatting. The following options
           are supported:

             - A string entry, which is assumed to be a format statement.
             - Any function returning a string.

      Only the first two values are required, as the last two entries have default deduced from the global parameters.

      .. note::

         1. Differences (if the "since" or "relative" keywords are present) are computed as previous minus current. Thus
            make sure to change the sign in the format entry so the value is displayed correctly.
         2. Only assemblies with timestamps greater than or equal to *at* will be included in the report.

Keyword arguments
+++++++++++++++++

     - *at=<datetime>*: The principle point at which data will be extracted from the inventory.
     - *start_date=<date_time>*: The previous time point used when reporting changes.
     - *flt=<>*: Filter the inventory entries that appear in the report. Either a :term:`list` or :term:`labeledgrid` of
       assembly names, or a general :term:`predicate` accepting two arguments: the assembly name and :class:`AssemblyHistory`
       instance.
     - *include_totals=<bool>*: Flag indicating if a row containing totals should be included at the bottom of the
       table.

Examples
++++++++

.. note::

   The following assumes that you are working in a core follow application script, using the variables set by that
   application mode.

First, in the source script, set up the columns to include:

  1. The assembly names.
  2. Total energy delivered.
  3. Current U-235 mass.
  4. Total U-235 mass depleted.
  5. U-235 burnup as a percentage.

.. code-block:: py

   from core.units import format_unit
   from core import *

   columns = [('Assemblies', 'names'),
              ('Energy Delivered', 'energy', _, format_unit(units.MW * units.days, '{:.1f}'.format)),
              ('U235 Mass', 'U-235', _, format_unit('g', '{:.1f}'.format)),
              ('U-235 Burned', 'U-235', 'since bol', format_unit('g', '{:.1f}'.format)),
              ('U-235 Burnup', 'U-235', 'relative bol', lambda x : '{:.1f} %'.format(x * 100.0))]

   # register the variable
   parameters.doc_parameters().set('Columns', columns)

then, in the document source file

.. code-block:: none

   #inventory_report[${Inventory}, ${Columns}, at=${time_stamp}]

If the report should be restricted to assemblies loaded in the current cycle:

.. code-block:: none

   #inventory_report[${Inventory}, ${Columns}, at=${time_stamp}, flt=${load_map}]