Applications
============

This sections describes the various applications available in RAPYDS, and how to interact with them. Since all
applications are built using a common framework, their interface elements have a lot in common. These elements are
described in :ref:`General Application Guidelines`, and it is important to read this section before continuing to the
application specific documentation.

All the available application modes are listed in the following table:

.. list-table:: Application modules.
   :header-rows: 1
   :widths: 30 70
   :class: tight-table

   * - Application Module
     - Description
   * - :ref:`assembly_archiver <assembly_archiver>`
     - Save and interact with assembly models.
   * - :ref:`configuration <configuration>`
     - Save and interact with complete model configurations.
   * - :ref:`plotter <geometry_plotter>`
     - Produce basic 2D plots of slices through a model.
   * - :ref:`model_checker <model_checker>`
     - Check compatibility of a model for a particular target code (plugin).
   * - :ref:`critical_case <critical_case>`
     - Compute the equilibrium fission source and related responses.
   * - :ref:`cases <cases>`
     - Perform multiple equilibrium fission source calculation.
   * - :ref:`feedback_coefficients <feedback_coefficients>`
     - Calculate reactivity insertion/removals.
   * - :ref:`rod_calibration <rod_calibration>`
     - Compute detailed control rod (or bank) worth tables for a single rod (or bank).
   * - :ref:`multi_rod_calibration <rod_calibration>`
     - Compute detailed control rod (or bank) worth tables for a multiple rods (or banks).
   * - :ref:`exposure <Creation of Burnup Dependent Material Libraries>`
     - Creates a simple burnup dependent material library for a single assembly type.
   * - :ref:`docgen <docgen>`
     - The document generation system.
   * - :ref:`reload <reload_application>`
     - Perform predictive calculations for an upcoming cycle, or perform general design evaluations.



.. toctree::
   :maxdepth: 3
   :caption: Contents

   application_guidelines
   assembly_archiver_application_usermanual
   configuration_application_usermanual
   geometry_plotter_application_usermanual
   model_checker_application_usermanual
   critical_case_application_usermanual
   cases_application_usermanual
   feedback_coefficients_application_usermanual
   rod_calibration_application_usermanual
   multi_rod_calibration_application_usermanual
   facility_management
   exposure_application_usermanual
   core_follow_application_usermanual
   reload_application_usermanual
   odeht_usermanual
   docgen_usermanual

.. toctree::

   common_data_objects
   plugin_support
