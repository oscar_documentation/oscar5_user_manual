.. role:: py(code)
   :language: py

Building Core Configurations
****************************

With an assembly library in hand, the next step is to define how those separate components come together to form a
working reactor. This involves defining a core layout, loading irradiation facilities, configuring control structures,
and specifying the rest of the reactor systems.

The neutronics model is defined using the :class:`core.model_builder.ModelBuilder` interface, which is essentially
a large data object, exposing all the parameters required to construct a working model.

.. note::

   Fixed full core configurations are usually constructed within the in :ref:`configuration application <configuration>`
   input, which facilitates interaction with the model through the command line interface.

Using your assembly library
===========================

You can access your assembly archive by importing the :py:`assemblies` container from the local :py:`model` package:

.. code-block:: py

   from ..model import assemblies

All the assemblies in your assembly archive can be accessed through attributes of this container. The attribute used to
construct an assembly type has the same name as the name you specified in the build script. For example, to create an
instance of an assembly type called 'MY_REACTOR_assembly_type_1':

.. code-block:: py

   asm = assemblies.MY_REACTOR_assembly_type_1()

.. note::

   Spaces and :py:`-` characters used when naming your assembly type in the build script will be replaced with an ``_``
   character.

.. attention::

    The :py:`assemblies` structure only collects assembly archives. Thus, only assemblies with an existing archive will be
    available. To construct an assembly instance without an archive, import its build module instead:

    .. code-block:: py

        form ..model import my_assembly

        a1 = my_assembly.build()

    Note that certain features (e.g. visualization) will not work correctly without an assembly archive.

On assembly types, assembly names and instances
-----------------------------------------------

The :py:`assemblies` structure lists all **base** assembly types. The following will construct two **instances** of the
same **base** type:

.. code-block:: py

   asm = assemblies.MY_REACTOR_assembly_type_1

   i1 = asm()
   i2 = asm()

As far as the system is concerned, ``i1`` and ``i2`` are two **different** assemblies of the same **base** type. In
particular, they carry unique and separates states, so that modifying one will not affect the other. Thus, assembly
instances are not necessarily defined by a string tag. In the **rapyds** system, string tags are only required when
adding or retrieving assemblies from an inventory. Since this can, at times, be a bit confusing, it is recommended
that you also give assembly instances unique names (whenever possible):

.. code-block:: py

   asm = assemblies.MY_REACTOR_assembly_type_1

   i1 = asm(name='instance_1')
   i2 = asm(name='instance_2')


Modifying assembly instances
----------------------------

Once you have an assembly instance, it can be modified by loading facilities, re-orientating it using transformations, or
changing its state.

Loading facilities
++++++++++++++++++

Use the assembly's ``load_facility`` method to change the state of
:ref:`configured facilities <Specifying loadable facilities>`:

.. code-block:: py

   re = assemblies.MY_REACTOR_hollow_reflector()
   sample = assemblies.MY_REACTOR_some_irradiation_sample()

   re.load_facility('irradiation-position', sample)


.. note::

   The :attr:`model.assembly_facility_load_maps` parameter can be used to do bulk loading for the entire core. This is the
   recommended method if you want to load unique instances of assemblies into different positions. Intra-assembly
   control structures can also be configured using the :attr:`model.intra_assembly_control_map`, and
   :attr:`model.intra_assembly_control_facility` parameters.

Setting state
+++++++++++++

You can also modify the (material) state of any assembly instance using the ``set_state`` method:

.. code-block:: py

   import core.state

   i1.set_state(core.state.fuel_temperature(566 * units.K))

.. note::

   Changing the state for all assemblies in the core is achieved by modifying the :attr:`model.state` parameter. Channel
   specific states can be set with the :attr:`model.state_map` parameter.

Adding transformations
++++++++++++++++++++++

Change the orientation of your assembly by adding transformations. For example:

.. code-block:: py

   import core.geometry

   i1.add_transformation(core.geometry.reflection('y'))

.. note::

   The :attr:`model.transformation_map` parameter can be used to specify transformations for channels in the core map.


Building the model
==================

This section lists all the **model** parameters used to define your core configuration.

.. current_parameter_set:: model
   :synopsys: Set of parameters configuring the model.

Configuration meta data
-----------------------

.. parameter:: facility_description

   Description of the facility this model represents. This parameter simply collects the following sub-parameters:

   .. parameter:: site
      :type: :term:`string`
      :default: 'CORE'
      :module: model.facility_description

      Description of the facility this configuration belongs to. The following values are understood and handled by the
      system:

         - 'CORE'
         - 'POOL'
         - 'VAULT'

      .. note::

         Additional site names must be configured with the :ref:`facility manager <Facility Management>`.

   .. parameter:: unit
      :type: :term:`string`
      :default: 'Unknown'
      :module: model.facility_description

      Name of the particular unit at :py:attr:`model.facility_description.site` this model is based on.

   .. parameter:: name
      :type: :term:`string`
      :default: 'Unknown'
      :module: model.facility_description

      Full facility name.

   .. parameter:: reactor_type
      :type: :term:`string`
      :default: 'MTR'
      :module: model.facility_description

      Reactor type. Currently, only the following tags are recognized:

      +--------------+-----------------------------------------------------------------------------------------------+
      |   MTR        | Material Testing Reactor                                                                      |
      +--------------+-----------------------------------------------------------------------------------------------+
      |   PWR        | Pressurized Water Reactor                                                                     |
      +--------------+-----------------------------------------------------------------------------------------------+
      |   BWR        | Boiling Water Reactor                                                                         |
      +--------------+-----------------------------------------------------------------------------------------------+

      .. note::

         This is required by certain target codes to set default physics options.

   .. parameter:: design_power
      :type: :term:`power`
      :default: 1 MW
      :module: model.facility_description


       Design (nominal) power.

   .. parameter:: design_pressure
      :type: :term:`pressure`
      :default: 1 atmosphere
      :module: model.facility_description

      Nominal coolant temperature.

   .. parameter:: design_coolant_mass_flow
      :module: model.facility_description
      :default: 0 kg/s

      The average nominal coolant mass flow rate.

   .. parameter:: design_inlet_temperature
      :type: :term:`temperature`
      :module: model.facility_description
      :default: 20 degC

      The nominal inlet temperature.

   .. parameter:: design_coolant_flow_direction
      :type: 'UP' or 'DOWN'

      The coolant flow direction.

      .. attention::

         This parameter must be set in order for thermal feedback models to work correctly.


   Example:

   .. code-block:: py

      model.facility_description.site = 'CORE'
      model.facility_description.reactor_type = 'MTR'
      model.facility_description.design_power = 5 * units.MW


.. parameter:: name
   :type: :term:`string`

   Name used to identify this model configuration.


.. parameter:: description
   :type: :term:`string`

   Optional description of the model configuration.


Parameters related to core layout
---------------------------------

.. parameter:: layout
   :type: :term:`string`
   :default: rectangular

   Specify how maps should be interpreted. The valid options are collected in `model.layout_options`:

   .. table:: Layout options.
      :class: tight-table

      +---------------------------------------------+------------------------------------------------------------------+
      | Option                                      |  Result                                                          |
      +=============================================+==================================================================+
      | `model.layout_options.rectangular`          | Maps are interpreted as specifying a rectangular layout.         |
      +---------------------------------------------+------------------------------------------------------------------+
      | `model.layout_options.square`               | Maps are interpreted as specifying a square layout.              |
      +---------------------------------------------+------------------------------------------------------------------+
      | `model.layout_options.hexagonal_x`          | Maps are interpreted as specifying a                             |
      |                                             | :ref:`flat top hexagonal <flat_top>` layout.                     |
      +---------------------------------------------+------------------------------------------------------------------+
      | `model.layout_options.hexagonal_y`          | Maps are interpreted as specifying a                             |
      |                                             | :ref:`pointy top hexagonal <pointy_top>` layout.                 |
      +---------------------------------------------+------------------------------------------------------------------+

   .. note::

      This parameter only needs to be set if maps are specified as generic two dimensional maps (i.e. nested
      :term:`list`). Otherwise, the layout will be deduced form the input :term:`grid` type.

.. parameter:: core_map
   :type: :term:`labeledgrid` [:term:`component` | :term:`placeholder`]

   Defines the layout of components in the core, as well as the labels used to denote core positions. It explicitly
   loads static positions, and indicates where and how loadable assemblies should be placed.

   Grid entries must be either :term:`component` or :term:`placeholder` instances. Place holder tags are used to denote
   positions that will be filled from other maps. Use the :py:`_p` tag to denote positions that will be filled from the
   main :attr:`model.load_map`, :py:`_p1` for positions filled from :attr:`model.load_map_1`, :py:`_p2` for positions filled from
   :attr:`model.load_map_2`, etc. Any empty placeholder :py:`_` position will be filled using the :attr:`model.filler`
   component.

   Consider the following example:

   .. code-block:: py

      a1 = assemblies.my_assembly_type_1()
      a2 = assemblies.my_assembly_type_2()

      model.core_map = \
      [[_  , '1',  '2',  '3', '4'],
       ['A',  a1,   a2,  _p1,  a1],
       ['B',  a1,   _p,   _p,  a1],
       ['C',  a1,   _p,   _p,  a1],
       ['D',  a1,   a2,   a2,  a1]]

   The above will have the following effect:

    1. :attr:`model.column_labels` will be set to ``['1',  '2',  '3', '4']``

    2. :attr:`model.row_labels` will be set to ``['A',  'B',  'C', 'D']``

    3. All positions containing ``a1`` will be filled with the same **instance** of ``my_assembly_type_1``

    4. All positions containing ``a2`` will be filled with the same **instance** of ``my_assembly_type_2``

    5. Positions 'B2', 'B3', 'C2' and 'C3' will be filled using the main :attr:`model.load_map``

    6. Position 'A3' will be filled from :attr:`model.load_map_1`

   .. note::

      If :attr:`model.column_labels` and :attr:`model.row_labels` are set manually, the core map may be given as a :term:`grid`,
      without the column and row labels. This, together with the :attr:`model.symmetry` parameter, are more convenient
      mechanisms for specifying large maps.

.. parameter:: row_labels
   :type: :term:`list` [:term:`string` | :term:`integer`]
   :default: ``None``

   Specify the row labels used to identified rows in the :attr:`model.core_map`. This parameter is **not** required if
   :attr:`model.core_map` is specified as a :term:`labeledgrid`.

   .. note::

      The size of this list determines number of rows (:math:`y` dimension) of your core map.

.. parameter:: column_labels
   :type: :term:`list` [:term:`string` | :term:`integer`]
   :default: ``None``

   Specify the column labels used to identified columns in the :attr:`model.core_map`. This parameter is **not** required if
   :attr:`model.core_map` is specified as a :term:`labeledgrid`.

   .. note::

      The size of this list determines number of columns (:math:`x` dimension) of your core map.

.. parameter:: symmetry
   :type: :term:`list` [:term:`string`]
   :default: ``None``

   Specify unfolding (reflection) pattern that should be applied to all grids smaller than the total core map size. This
   parameter requires that :attr:`model.row_labels` and :attr:`model.column_labels` are set, so that the full core map size can be
   determined.

   Each entry in the list is either a :term:`affine_transformation` or one of the following reflection tags:

    - 'r', 'r+' : Reflect to the right ('+' to also reflects the first column).
    - 'l', 'l+': Reflect to the right ('+' to also reflects the last column).
    - 't', 't+': Reflect to the top ('+' to also reflects the first row).
    - 'b', 'b+': Reflect to the bottom ('+' to also reflects the last row).

   For example to unfold 1/4 symmetry (assuming the top left corner is specified), use ``['r','b']`` for odd number
   of rows and columns, and ``['r+','b+']`` for an even number.

   .. note::

      The :attr:`model.row_labels`, :attr:`model.column_labels` and :attr:`model.symmetry` parameters make it easier to specify large
      maps. However, unless it seriously hinders readability, stick to full labeled grids whenever possible.

.. parameter:: fill
   :type: :term:`component`

    Component that will be used to fill empty placeholder positions in the core map.

.. parameter:: transformation_map
   :type: :term:`labeledgrid` [:term:`affine_transformation` | :term:`integer` | :term:`placeholder`]

   Transformation that should be applied to positions in the core. Use the empty placeholder for positions that should
   not be transformed. Integer entries can be used for basic rotations (positive for counter clockwise rotation, and
   negative entries for clockwise rotation).

   For example:

   .. code-block:: py

      import core.geometry

      rf = core.geometry.reflection('y')

      model.transformation_map = \
       [[_  , '1',  '2',  '3', '4'],
       ['A',   _,   rf,   rf,   _],
       ['B',   _,   90,   90,   _],
       ['C',   _,    _,    _,   _],
       ['D', -90,    _,    _,   _]]

   will apply a reflection through the :math:`y` axis in positions 'A2' and 'A3', a counter clockwise rotations of 90
   degrees to positions 'B2' and 'B3', and a clockwise rotation of 90 degrees to position 'D1'. All other positions
   will remain unchanged.

   .. note::

      Transformations in this map will stack with any transformations already applied to the assembly.


Parameters related to core structure
------------------------------------

.. parameter:: core_pitches
   :type: :term:`tuple` [:term:`length`]

   Core pitch. If a single value is given, the core layout is assumed to be on a square lattice. If two values are
   given, the core is assumed to be on a constant pitch rectangular layout, with the first value the :math:`x` pitch
   (column width), and second value the :math:`y` pitch (row width).

   To specify a non constant pitched rectangular layout, pass a list of pitches in the first and second entry. In
   this case, the lists must have the same length as the number of columns and rows respectively.

   This value is required if the systems should automatically generate a core structure, that is, :attr:`model.reactor_core`
   is not specified.

   .. note::

      Even if :attr:`model.reactor_core` is specified, the core pitches are used to scale core map pictures, so it is
      recommended that you specify this parameter.

.. parameter:: reactor_core
   :type: :term:`component`

    Custom component defining the core configuration. Use a custom core if your core layout cannot be constructed from
    the auto core parameters (e.g. :attr:`model.core_pitches`, :attr:`model.merge_map`), or if it contains some static features
    (e.g guide boxes). A core model is a special :term:`component` that can interpret your :attr:`model.core_map`.

    If this parameter is not specified, the system will automatically create a core structure based on the
    :attr:`model.core_pitches` and :attr:`model.merge_map` parameters.

.. parameter:: merge_map
   :type: :term:`labeledgrid` [:term:`integer` | :term:`placeholder`]

   Use this map to specify which core positions should be merged into a single position. Integers are used to group
   positions together, and empty placeholder for positions that should not be merged.

   This structure is used if the core grid has "bulk" positions that can hold a single assembly filling multiple
   channels. For research reactors, a typical example is a large experimental facility.

   For example,

   .. code-block:: py

      model.merge_map = \
       [[_  , '1', '2', '3', '4'],
       ['A',   1,   1,    -,   _],
       ['B',   1,   1,    _,   _],
       ['C',   _,   _,    _,   _],
       ['D',   _,   _,    2,   2]]

   will merge 'A1', 'A2', 'B1' and 'B2' into a single position, so that a component of size 2 times :math:`x` pitch
   and 2 times :math:`y` pitch can be loaded. Similarly, 'D3' and 'D4' is merged into a single position, of size
   :math:`y` pitch by 2 :math:`x` pitches.

   .. note::

      When filling a merged position in the :attr:`model.core_map`, only the to left entry of the merged position is used.

   .. attention::

      You can only merge adjacent positions.

.. parameter:: pool
   :type: :term:`component`

   Pool or reflector component. Special component that specifies where the core is located, and defines the structures
   surrounding the core. If this parameter is not set, the system will simply create a boundary around the core, with
   nothing (vacuum) outside it.

   Boundary conditions on the pool boundary are set using :attr:`model.boundary_condition` parameter.

.. parameter:: boundary_condition
   :type: :term:`boundary_condition`
   :default: ``vacuum``

   Boundary condition that should be placed on the boundary of the :attr:`model.pool`. For this to have any effect, the
   pool component must have a boundary defined (that is, cells that are flagged as ``outside``).

   .. note::

      The auto pool generated when :attr:`model.pool` is not set always has the core boundary as its boundary.

Parameters related to control element configuration
---------------------------------------------------

.. parameter:: intra_assembly_control_map
   :type: :term:`labeledgrid` [:term:`component` | :term:`string`]

   Loads intra assembly control :term:`component` instances into assemblies in the core map. All target assemblies must have the
   :attr:`model.intra_assembly_control_facility` available. String entries in this map will be passed to the
   :attr:`model.inventory_manager`, which will load assemblies from the inventory. This feature is optional
   though, as you can always construct and load fresh elements.

   The following example will load fresh control assemblies in positions 'B2' and 'C3', but 'C2' is filled with an
   assembly from the inventory:

   .. code-block:: py

      ir = assemblies.my_control_element

      model.intra_assembly_control_map = \
       [[_  , '1', '2', '3', '4'],
       ['A',   _,    _,    -,   _],
       ['B',   _, ir(),    _,   _],
       ['C',   _,'B53', ir(),   _],
       ['D',   _,    _,    _,   _]]

   .. note::

      You must manually add burnable control elements to the inventory, since there is no automatic procedure for
      managing non-fuel burnable assemblies.

.. parameter:: intra_assembly_control_facility
   :type: :term:`string`
   :default: 'rod'

   Name of the facility which contain the control element(s).

.. parameter:: bank_map
   :type: :term:`labeledgrid` [:term:`string`]

   Group control positions in the :attr:`model.core_map` into banks. Bank names are arbitrary tags used to identify control
   structures. This parameters identifies banks by providing a core map, using empty :term:`placeholder` tags for non
   control positions. For example,

   .. code-block:: py

      model.state_map = \
       [[_  ,      '1',  '2',      '3', '4'],
       ['A',         _,    _,        -,   _],
       ['B',  'Bank 1',    _,        _,   _],
       ['C',  'Bank 2',    _, 'Bank 2',   _],
       ['D',         _,    _,        _,   _]]

   Instead of specifying a full map, the more flexible :attr:`model.banks` parameter can also be used.

   .. attention::

      If this model is used for core follow applications, the bank names specified here must coincide with the bank
      names retrieved from plant data.

.. parameter:: banks
   :type: :term:`dict`

   Groups control positions (or elements) into banks. Unlike the :attr:`model.bank_map` parameter, this is specified
   using a simple :term:`dict` object, with keys the bank names, and values the positions (or control elements for
   intra assembly control). For example, the following will have the same effect as the  :attr:`model.bank_map` example:

   .. code-block:: py

      model.banks = {'Bank 1': 'B1', 'Bank 2': ['C1', 'C3']}

.. py:function:: set_banks(*positions, **kwargs)

   Sets the current :attr:`model.banks` positions. The `positions` can either be a:

      1. Single :term:`travel` instance, in which case all :attr:`model.banks` will be set to this position. For example,
         to fully extract all the banks:

         >>> model.set_banks(control.fully_extracted())

      2. A :term:`dict` instance mapping :attr:`model.banks` names, to their :term:`travel` positions. For example

         >>> model.set_banks({'Bank 1': control.percent_extracted(50), 'Bank 2': control.percent_extracted(75)})

   Alternatively, bank positions can be specified as key word values:

   >>> model.set_banks(Bank_1=control.percent_extracted(50), Bank_2=control.percent_extracted(75))

Parameters related to core state
--------------------------------

.. parameter:: state
   :type: ``State``

   Sets the state for the entire core, including the :attr:`model.pool` component. Use the state tags in the
   :py:mod:`core.state` module. For example,

   .. code-block:: py

      import core.state

      model.state[core.state.fuel_temperature()] = 90 * units.degC

   will set the fuel temperature for all elements containing fuel materials.

.. parameter:: state_map
   :type: :term:`labeledgrid` [:term:`state`]

   Sets the state for specific positions (channels) in the core map. Each entry in this map must be a complete
   :class:`core.state.State` container, or something from which one can be constructed (e.g. a single or list of
   :term:`state`). For example,

   .. code-block:: py

      import core.state

      sp = core.state.State(core.state.fuel_temperature(60 * units.degC),
                            core.state.moderator_density(0.94 * units.g / units.cc))

      ft = core.state.fuel_temperature(90 * units.degC)

      model.state_map = \
       [[_  , '1', '2', '3', '4'],
       ['A',   _,   _,    -,   _],
       ['B',   _,  sp,    _,   _],
       ['C',   _,   _,   ft,   _],
       ['D',   _,   _,    _,   _]]

   will change the fuel and moderator state for position 'B2', and only the fuel temperature for position 'C3'.

Parameters related to inventory configuration
---------------------------------------------

.. parameter:: inventory_manager

   Object used to manage the loading and storing of assembly histories, which keeps track of an assembly's
   state during its lifetime. Assembly histories are uniquely defined by the assembly instance name. The
   `InventoryManager`'s main purpose is to create an assembly instance from such a unique name.

   .. attention::

      Although the :attr:`model.inventory_manager.history_importer` and :attr:`model.inventory_manager.load_map` is
      still supported, it's functionality is now supported by the new facility management tool.

   .. parameter:: model_path
      :type: :term:`directory`
      :module: model.inventory_manager

      Path to the assembly library. This usually points to the **model** package within your reactor package structure.

      This parameter is currently required, as there is no reliable mechanism for automatically deducing it.

   .. parameter:: inventory
      :type: :term:`directory`
      :module: model.inventory_manager

      Path to the directory containing assembly history data, or any object satisfying the
      :class:`inventory.inventory.Inventory` interface.

      .. note::

         When using target codes that manage their own inventory, this path should still point to the root, code
         independent, directory.

   .. parameter:: load_time
      :type: :term:`datetime`
      :module: model.inventory_manager

      Time point at which all assemblies should be loaded from the :attr:`model.inventory_manager.inventory`, or constructed using the
      :attr:`model.history_importer`. This parameter is **required** for all time bound input modules.

      .. attention::

         Since a core configuration is intended to be valid for a period of time, this parameter should not be set
         in the configuration module.

      .. note::

         This parameter is set automatically when setting :attr:`base_param.time_stamp`.

   .. parameter:: auto_decay
      :type: :term:`bool`
      :default: False
      :module: model.inventory_manager

      If this flag is set, all assemblies will be decayed from their last save point in
      :attr:`model.inventory_manager.inventory`, to :attr:`base_param.time_stamp`.

      .. note::

         The decay is performed in memory, and the :attr:`model.inventory_manager.inventory` is **not** modified.

   .. parameter:: history_importer
      :type: ``HistoryImporter``
      :module: model.inventory_manager

      Object used to construct assemblies if they are not found in the :attr:`model.inventory_manager.inventory`. This can be used to
      automatically import assembly states from external data.

      The process used to construct named assemblies from inventories are described below.

   .. parameter:: load_map
      :type: :term:`labeledgrid` [:term:`loader`]
      :module: model.inventory_manager

      Map used to create assembly instances in core positions, that is, a loadable type map. It is required for the
      automatic creation of fresh assemblies, or when a :attr:`model.history_importer` is specified.

      Example:

      .. code-block:: py

         a1 = model.assembly_type_1
         a2 = model.assembly_type_2

         model.inventory_manager.load_map = \
         [[_  , '1', '2',  '3', '4'],
          ['A',   _,   _,    -,   _],
          ['B',   _,  a1,   a2,   _],
          ['C',   _,  a2,   a1,   _],
          ['D',   _,   _,    _,   _]]

      .. attention::

         Entries in this map should be :term:`loader` objects, and **not** assembly instances. The
         :attr:`model.inventory_manager` has an ``add_loadable`` method that can also be used to create :term:`loader`
         instances with additional data.


Parameters related to core loading
----------------------------------

.. note::

   Load maps are usually case (or time) bound. Thus they are rarely specified in a *configuration* module.

.. parameter:: facility_loading
   :type: :term:`filepath`
   :default: None

   Path to the facility loading archive. Used to automatically deduce core loading based on the :attr:`model.time_stamp`
   parameter. See :ref:`Facility Management` for more details on using this mechanism to manage the core and other
   facility states.

   .. attention::

      This is the recommended method for stetting and managing loadable positions in the core.

.. parameter:: load_map
   :type: :term:`labeledgrid` [:term:`placeholder` | :term:`string`]

   Grid positions of loadable assemblies. Entries in this grid that are not :term:`placeholder` instances, are usually explicit
   names of assemblies tracked in the :attr:`model.inventory_manager.inventory`, or importable via the
   :attr:`model.inventory_manager.history_importer` if set.

   If auto loading is configured, a fresh assembly can be specified using a :term:`tuple`,
   with the first entry the new assembly name, and the second entry its heavy metal mass.

   Values in the :attr:`model.core_map` that should be filled from this primary load map should use the ``_p`` placeholder
   token.

   .. note::

      If specified, this parameter will overwrite the load map deduced from :attr:`model.load_map`.

.. parameter:: load_map_1
   :type: :term:`labeledgrid` [:term:`placeholder` | :term:`string`]

   Additional load map. Provides a convenient mechanism to separate different loadable types. For instance, use the
   primary :attr:`model.load_map` for fuel assemblies, and this map for reflector elements.

   Values in the :attr:`model.core_map` that should be filled from this load map should use the ``_p1`` placeholder
   token.


.. parameter:: load_map_2
   :type: :term:`labeledgrid` [:term:`placeholder` | :term:`string`]

   Yet another load map.

   Values in the :attr:`model.core_map` that should be filled from this load map should use the ``_p2`` placeholder
   token.

.. parameter:: assembly_facility_load_maps
   :type: :term:`labeledgrid` [:term:`placeholder` | :term:`string`]

   Parameter used to perform bulk loading of intra assembly facilities in the core. This parameter is actually
   a :term:`dict` object, with keys the facility names, and values load maps, describing what should be loaded where.

   Example:

   .. code-block:: py

      s1 = assemblies.my_irradiation_target(name='Sample#1')
      s2 = assemblies.my_irradiation_target(name='Sample#2')
      s3 = assemblies.my_irradiation_target(name='Sample#3')

      model.assembly_facility_load_maps['irradiation_channel`] = \
       [[_  , '1', '2',  '3', '4'],
        ['A',   _,  s1,   s2,   _],
        ['B',   _,   _,    _,   _],
        ['C',   _,   _,    _,   _],
        ['D',   _,  s3,    _,   _]]

   .. note::

      This parameter is only useful if you want to simultaneously load multiple positions. If only one position is
      loaded, it is more efficient to use `this <Loading facilities>`_ mechanism.

Process used to deduce assembly instances from strings
++++++++++++++++++++++++++++++++++++++++++++++++++++++

In the **rapyds** package, the core is only fully loaded when all positions in the :attr:`model.core_map` are assembly
instances. The following describes the process in which assembly instances are constructed from names (strings)
encountered in the various load maps:

  1. If :attr:`model.model.inventory_manager.inventory` is set, the system will try and load the assembly instance from
     it at the time point specified by :attr:`model.model.inventory_manager.load_time`. If this time point is later than
     the last time stamp in the inventory, it will be decayed to this point.
  2. Otherwise, the system will query the :attr:`model.model.inventory_manager.history_importer` object with the assembly
     name. See :ref:`configuring auto inventory management <Configuring the automatic creation of assembly histories>`,
     for more information.

The ``extract_load_grid`` method will go through the above procedure, and return a full core grid, filled entirely
with assembly instances. Thus,

.. code-block:: py

   lg = model.extract_load_grid()

will return a map with the same dimensions and labels as :attr:`model.core_map`, but with all positions assembly instances.

Parameters related to the nodal model
-------------------------------------

.. parameter:: tag
   :type: :term:`string`

    Short string tag used to identify this model as well as its nodal (homogenized) representation. If this parameter
    is set, it will be used automatically in the :ref:`cOMPoSe <compose>` application when importing this model.

    .. note::

       This parameter is automatically deduced when the :attr:`model.name` parameter is set. This will however not ensure
       that the tag is *unique*.

.. parameter:: nodal_representation

   Set the homogenized representation associated with this model. Can either be a valid stored nodal representation
   tag, or an instance retrieved from the assembly library archive. For example, if `NC` was the tag used to store
   a configuration, then

   .. code-block:: py

      model.nodal_representation = 'NC'

   and

   .. code-block:: py

      from ..model import assemblies

      model.nodal_representation = assemblies.nodal_configurations.NC()

   are equivalent.

   .. attention::

      For the latter method to work, the nodal configuration must already exist!

   .. note::

      If not specified, the system will try to automatically deduce the nodal configuration based on the :attr:`model.tag`
      parameter.

   The following flags can be set to change the behavior of the nodal model:

   .. parameter:: radial_re_homogenization
      :type: :term:`bool`
      :default: True
      :module: model.nodal_representation

      Switch the radial re-homogenization model (if available) on or off.

   .. parameter:: axial_re_homogenization
      :type: :term:`bool`
      :default: True
      :module: model.nodal_representation

      Switch the axial re-homogenization model (if available) on or off.

   .. parameter:: cross_section_shape_feedback
      :type: :term:`bool`
      :default: True
      :module: model.nodal_representation

      Flag indicating if the internal model to correct cross sections due to state changes in the node should be used.

   .. parameter:: side_depletion
      :type: 'macroscopic' or 'microscopic'
      :default: 'microscopic'
      :module: model.nodal_representation

      Which model to use when calculating side number densities for larger nodes.

   .. note::

      The use of these flags depends on the capabilities of the target nodal solver.