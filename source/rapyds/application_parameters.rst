.. role:: py(code)
   :language: py

Application Parameters
**********************

Input to any application is passed using a special object, which is referred to as the *parameter set*. These parameter
objects contain simple data records (keyword value pairs), as well as methods (function calls) which are used to set
more complicated input.

Creating an Application Parameter Set
=====================================

There are currently two ways to obtain a parameter set for a specific application, as discussed in the following
sections.

Using the global application parameters
---------------------------------------

Each application module (in the :py:`applications` package), contains an instance of its own set of input parameters, which
is immediately available upon a global import, as the symbol :py:`parameters`:

.. code-block:: py

   from applications.target_app import *

   parameters.project_name = 'my_name'

In this case, there is no need to manually construct the parameter set.

.. attention::

   Although this method is still fully supported and is perfectly safe in most instances, it can potentially cause
   problems when importing one or more applications of the same type into other script. This is because, since
   :py:`parameters` is global, it is overwritten by subsequent imports. Although rare, there are some applications
   (e.g docgen) that can combine input and output from multiple applications, in which case `Creating a unique parameter set`_
   should be used.

Creating a unique parameter set
-------------------------------

Instead of using the global :py:`parameters` directly, a unique copy can be created as follows:

.. code-block:: py

   import applications.target_app as app

   parameters = app.parameters()
   parameters.project_name = 'my_name'

.. note::

   When using this method, the copy need not be named :py:`parameters`, since it is just another variable. For example,

   .. code-block:: py

      app_param = app.parameters()

   is also perfectly legal. In order to distinguish between different application parameter set, this approach will
   be used throughout this manual.

.. _general_app_parameters:

General Parameters
==================

Although each application has a slightly different interface, they all share a common set of parameters, which
sets and customizes the underlying model, determines where input will be generated, set options that will be passed to the
target analysis code, and customize how the application should be launched. These are described further in the
following sections.

.. note::

   In the following :py:`app_param` denote an instance of the application's parameter set, instantiated through the
   mechanism described in `Creating a unique parameter set`_.

.. current_parameter_set:: app_param
   :synopsys: Common application parameters

.. _general_application_parameters_summary:
.. table:: General application parameters.
   :width: 90%
   :align: center
   :widths: 4 6
   :class: tight-table

   +----------------------------+---------------------------------------------------+
   | Category                   | Parameters                                        |
   +============================+===================================================+
   | Model setup                | - :attr:`app_param.model`                         |
   |                            | - :attr:`app_param.time_stamp`                    |
   +----------------------------+---------------------------------------------------+
   | Input generation           | - :attr:`app_param.config_file`                   |
   |                            | - :attr:`app_param.target_mode`                   |
   |                            | - :attr:`app_param.project_name`                  |
   |                            | - :attr:`app_param.working_directory`             |
   +----------------------------+---------------------------------------------------+
   | Execution                  | - :attr:`app_param.executable`                    |
   |                            | - :attr:`app_param.arguments`                     |
   |                            | - :attr:`app_param.host_directory`                |
   |                            | - :attr:`app_param.use_rsync`                     |
   |                            | - :attr:`app_param.threads`                       |
   |                            | - :attr:`app_param.nodes`                         |
   |                            | - :attr:`app_param.walltime`                      |
   |                            | - :attr:`app_param.queue`                         |
   +----------------------------+---------------------------------------------------+
   | Numerics                   | - :attr:`app_param.particles`                     |
   |                            | - :attr:`app_param.source_iteration`              |
   |                            | - :attr:`app_param.max_iteration`                 |
   |                            | - :attr:`app_param.max_feedback_iteration`        |
   |                            | - :attr:`app_param.convergence_criteria`          |
   |                            | - :attr:`app_param.error_tolerance`               |
   |                            | - :attr:`app_param.bank_search`                   |
   +----------------------------+---------------------------------------------------+
   | Physics                    | - :attr:`app_param.doppler_broadening`            |
   |                            | - :attr:`app_param.use_stochastic_mixing`         |
   |                            | - :attr:`app_param.cross_section_library`         |
   |                            | - :attr:`app_param.decay_data_library`            |
   |                            | - :attr:`app_param.fission_product_yields`        |
   |                            | - :attr:`app_param.thermal_hydraulic_feedback`    |
   |                            | - :attr:`app_param.thermal_hydraulic_options`     |
   +----------------------------+---------------------------------------------------+


.. parameter:: model
   :type: :py:class:`ModelBuilder`

   The model that your application will use. This is a large sub section which contains all data related to building
   and customizing the model. See :ref:`Building Core Configurations`.

   The recommended method is to start with a predefined model. For instance,

   .. code-block:: py

      from .configurations.my_configuration import model

      app_param.model = model

   This avoids cluttering the application script with model specific input. You can however modify the model,
   for instance

   .. code-block:: py

       parameters.model.set_banks(core.control.fully_extracted())

   will fully extract all the banks for this particular application.

   Since some applications will use a simplified or heavily customized model, by default the :attr:`app_param.model` is an
   empty :py:class:`ModelBuilder` instance, allowing you to construct the model in the script.
   See :ref:`Building Core Configurations`.

.. parameter:: time_stamp
   :type: :term:`datetime`

   The real model date and time which this input or application is attached to. This parameter is required when
   certain model elements (:attr:`model.load_map`, etc), should be deduced from the facility loading data base. See the
   :attr:`model.facility_manager` parameter, and the :ref:`Facility Management` application
   mode for additional information.

.. parameter:: config_file
   :type: :term:`filepath`

   Host configuration file. The configuration file configures target modes for the application's intended execution host
   (local or remote). Thus, it affects both input generation and application launch. If nothing is specified, the
   system configuration file is used, and the local host is assumed. See :ref:`Configuring Hosts` for details on
   customizing configuration files.

   This parameter is most frequently set on the command line, but when specifying it in the script make sure to pass the
   full path to the configuration file.

.. parameter:: target_mode
   :type: :term:`string`
   :default: 'SERPENT'

   Specify for which underlying code input should be generated. This must either be a build-in mode, or a mode
   configured in the :attr:`app_param.config_file`.

.. parameter:: project_name
   :type: :term:`string`

   Input project name. This parameter is used as a basis to name files generated by the system. It is highly
   recommended that you set this parameter to something that uniquely identifies the project.

   .. versionchanged:: 1.2

      If the value is not explicitly specified, it will default to to the input script name.

.. parameter:: input_file_name
   :type: :term:`string`

   Name of the input file the project will write to. Defaults to :attr:`app_param.project_name` with the extension
   *.i*.

.. parameter:: working_directory
   :type: :term:`directory`

   Sets the working directory for your application. In particular, all input, output and data files will be written to
   this directory. It is usually specified relative to the script:

   .. code-block:: py

      parameters.working_directory = core.utilities.path_relative_to(__file__, 'my_working_directory')

   .. note::

      A sub directory will be created under :attr:`app_param.working_directory` for each target code, so this parameter is
      :attr:`app_param.target_mode` independent.

   .. versionchanged:: 1.2

      If the value is not explicitly specified, it will default to a full caps version of the input script (module) name,
      in the same directory as the script.

.. parameter:: executable
   :type: :term:`string`

   Full path to target executable. Defaults to the value specified in :attr:`app_param.config_file`.

   .. attention::

      Since this parameter is specific to the execution host, it is better to use the values derived from your
      configuration file. Otherwise, your script will not be portable.

.. parameter:: arguments
   :type: :term:`string`

   Optional argument string that will be passed to the executable.

.. parameter:: host_directory
   :type: :term:`directory`

   Run directory on the remote machine. The default host directory is *<directory>/project_name/<timestamp>* where
   *directory* is the base remote directory is specified in the :ref:`configuration file <Host configuration>` ,
   and *timestamp* is the current date and time at launch.

   .. note::

      This parameter is only used when the execution host differs from the local host.

.. parameter:: use_rsync
   :type: :term:`bool`
   :default: False

   Use the :command:`rsync` protocol to copy files to and from a remote host. This can significantly speed up the
   transfer of files to a remote host.
   .. attention::

      This mode is not supported on Windows.

.. parameter:: threads
   :type: :term:`integer`
   :default: 1

   Number of threads (cores) application should use. This parameter applies to any machine with multiple cores.

.. parameter:: nodes
   :type: :term:`integer`
   :default: 1

   Number of compute nodes application should use. This parameter is only applicable to a machine or server that has MPI
   configured.

.. parameter:: walltime
   :type: float

   Maximum run time (in hours) allocated for the application. Only applicable when the target host is a cluster.

   .. note::

      If this parameter is not specified, the default value for the specified queue will be used. This value is set by
      the system administrator, and for large clusters can be quite restrictive. It is therefore recommended that, if the
      cluster implements a wall time cutoff, you manually specify this parameter.

.. parameter:: queue
   :type: :term:`string`

   Job queue application should be launched to. Only applicable when the target host uses a job management system.
   Defaults to the value specified in :attr:`app_param.config_file`.

.. parameter:: source_iteration
   :type: :term:`integer`
   :default: 50

   Number of source convergence cycles. The way in which this parameter is used depends on the target code, but is
   always related to convergence (at a fixed state) of the fission source. For instance, in Monte Carlo simulations,
   it is mapped to the inactive cycle input, while in MGRAC it affects the number of inner and outer iterations.

.. parameter:: max_iteration
   :type: :term:`integer`
   :default: 100

   General parameter specifying the maximum iterations.

.. parameter:: max_feedback_iteration
   :type: :term:`integer`
   :default: 75

   The maximum number of cross section update iterations, used when resolving temperature and other non-linear feedback
   effects.

.. parameter:: particles
   :type: :term:`integer`
   :default: 16000

   Number of source particles per iteration step.

.. parameter:: convergence_criteria
   :type: :term:`float`
   :default: 1.0E-5

   Iteration convergence criteria. Used to determine iteration stopping criteria were applicable.

.. parameter:: error_tolerance
   :type: :term:`float`
   :default: 1.0E-5

   General error tolerance parameter.

.. parameter:: bank_search
   :type: :term:`bool` or :term:`float`
   :default: False

   Set up bank critical searches. It can be switched on by setting it to True, or by passing the
   target :math:`k_{eff}`. Once switched on, the following sub-parameters can be set:

   .. parameter:: bank_search.target_keff
      :type: :term:`float`
      :default: 1.00

      The target effective multiplication factor (:math:`k_{eff}`).

   .. parameter:: bank_search.banks
      :type: :term:`list` [:term:`string`]

      List of bank names that can be searched. If not specified, all banks will be searched.

   .. parameter:: bank_search.continue_on_failure
      :type: :term:`bool`
      :default: False

      Flag indicating that the simulation should continue even when the search iteration fails to reach the
      :attr:`app_param.bank_search.target_keff`.

.. parameter:: doppler_broadening
   :type: :term:`bool`
   :default: True

   Use (automatic) Doppler resonance broadening if available.

.. parameter:: use_stochastic_mixing
   :type: :term:`bool`
   :default: False

   For target codes that does not support automatic :attr:`app_param.doppler_broadening`, or does not use libraries
   that support temperature interpolation, this technique can be used to better capture temperature effects in
   materials.

   Although its use can be customized per target mode, it is typically implemented as follows:

    1. For each isotope in the material composition, let :math:`T_l < T < T_u` denote the temperatures closest to
       the target temperature :math:`T` at which cross section libraries are available.

    2. Then, when writing the material composition, two entries are created for the isotope, one pointing to the
       library at :math:`T_l`, with density scaled by

       .. math::

          w_l = \frac{T_u - T}{T_u-T_l},

       and another pointing to the library at :math:`T_u`, with density scaled by :math:`w_u = 1 - w_l`.

    3. This will effectively model a linear interpolation between these two libraries.

   .. attention::

      If this flag is not switched on, the isotope library with temperature closest to the target temperature will be
      used. Thus, it is recommended that this mechanism is used if the temperature steps between subsequent libraries
      are large.

.. parameter:: cross_section_library
   :type: :term:`filepath`

   Path to cross section library or directory file. Defaults to the value specified in :attr:`app_param.config_file`.


   .. attention::

      Since this parameter is specific to the execution host, it is better to use the values derived from your
      configuration file. Otherwise, your script will not be portable.

.. parameter:: decay_data_library
   :type: :term:`filepath`

   Path to additional decay data if applicable. Defaults to the value specified in :attr:`app_param.config_file`.

   .. attention::

      Since this parameter is specific to the execution host, it is better to use the values derived from your
      configuration file. Otherwise, your script will not be portable.

.. parameter:: fission_product_yields
   :type: :term:`filepath`

   Path to additional fission product yield data if applicable. Defaults to the value
   specified in :attr:`app_param.config_file`.


   .. attention::

      Since this parameter is specific to the execution host, it is better to use the values derived from your
      configuration file. Otherwise, your script will not be portable.

.. parameter:: thermal_hydraulic_feedback
   :type: :term:`bool` or :term:`tuple` (:term:`state`)
   :default: False

   Switch thermal-hydraulic feedback on or off. Instead of passing `True`, which will switch on all feedback effects,
   individual effects can be switched on by passing a :term:`tuple` of :term:`state` tags. For example, to switch on only
   doppler feedback:

   .. code-block:: py

      app_param.thermal_hydraulic_feedback = state_param.fuel_temperature,

   and to switch on doppler and moderator temperature feedback,

   .. code-block:: py

      app_param.thermal_hydraulic_feedback = state_param.fuel_temperature, state_param.moderator_temperature

.. parameter:: thermal_hydraulic_options

   Extra options that are used when :attr:`app_param.thermal_hydraulic_feedback` is active.

   .. parameter:: calculate_hot_channel
      :type: :term:`bool`
      :default: False

      Also include a hot channel estimate in the calculation. The definition of a coolant channel, depends on the
      assembly design.

   .. parameter:: heat_transfer_correlation
      :type: :term:`string`

      When performing heat transfer calculations for feedback within the target code, use the selected correlation

   .. parameter:: safety_correlation
      :type: :term:`string`

      When estimating the distance to the relevant operating temperature limit within the target code, use the selected correlation

.. note::

    Target codes will only make use of these parameters if it makes sense to them, for instance, a nodal diffusion code
    will ignore the :attr:`app_param.particles` parameter.


Activating documentation input
------------------------------

The generation of a document associated with the current application is activated by calling:

.. py:function:: app_param.doc_parameters()

   This will setup, and return a :ref:`docgen parameter set <docgen_parameters>`. The




