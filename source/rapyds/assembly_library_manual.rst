Building Assembly Libraries
***************************

The assembly library forms the backbone of the unified modeling approach in **OSCAR-5**. This involves the specification
of:

 - All assemblies or components loaded into the main core grid.
 - Sub irradiation rigs or experiments that are loaded into defined component *facilities*.
 - The reactor pool or vessel.

An assembly library is constructed by writing a :ref:`build module <assembly_build_module>`  for each assembly type.
These build modules contain all the information necessary to construct the assembly/component, and must not depend on
any other assembly. Thus, the system allows assemblies to be constructed and maintained independently from one another,
possibly by separate people. In particular, each assembly can have its own data source, version and version history.
This introduces some modularity in the overall model quality control.

Since many structural materials are common to various components, the **materials** module can be used to ensure some
consistency between models. See :ref:`Specifying Materials` for details on how to specify and use materials.


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   assembly_build_module
   assembly_classes
