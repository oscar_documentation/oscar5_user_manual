Data Model
==========

The different components that constitute a complete **rapyds** reactor model are show in the following
`figure <rapyds_data_model>`_:

.. _rapyds_data_model:
.. figure:: images/data-model.png

   Schematic illustration of the data components used to define and manage a model in rapyds.

The following provides a brief summary of each component, with more detail in subsequent sections:

    1. Materials define the physical composition of structural component. A description of how materials are defined,
       as well as the :ref:`built-in material <Built-in materials>` available, can be found in :ref:`Specifying Materials`.

    2. Cells are the fundamental building blocks used in the geometric description of assemblies. They are build from a
       set of basic geometric :ref:`shapes <Primitive regions>`, using :ref:`boolean set <Regularizing set operations>`
       operations. See :ref:`The Constructive Solid Geometry (CSG) package`.

    3. The structure of assemblies or components are defined in the :ref:`assembly library <Building Assembly Libraries>`.
       There are also a number of pre-defined :ref:`assembly types <Choosing an Assembly Class>`, which automatically
       creates the cells for typical assembly designs.

    4. The complete model is defined in a :ref:`configuration module <Building Core Configurations>`. This defines the
       static layout of assemblies from the assembly library, as wel as where dynamically loaded assemblies should be placed.

       Each facility other than the reactor core (e.g. storage vault, spent fuel pool, etc), can also be modelled using
       a configuration module.

    5. Special history files stores the transmutable state (materials that change during irradiation) of an assembly,
       at various time points. These history files are managed via one (or more) inventories. These are usually attached
       to the reactor model (see :attr:`model.inventory_manager.inventory`), and is updated automatically by certain
       application modes (e.g :ref:`core_follow <core_follow>`.)

       The contents of an inventory can be explored via the command line using the
       :ref:`manger utility <Managing the Inventory>`. See also the :ref:`Assembly Inventories` section.

    6. Finally, the state of all facilities at any given time, is stored by the facility manager. In particular,
       it can track the core loading, as well as the movement of assemblies from one facility to another. It is updated
       using the :ref:`facility_management <facility_management>` application.

       The contents of the data base can be explored on the command line using the
       :ref:`manager utility <Managing the Facility Loading Data Base>`.


.. toctree::
   :maxdepth: 3
   :caption: Contents

   specifying_materials
   csg_package
   assembly_library_manual
   building_core_configurations
   inventory_and_history_data