"""
Guide Pin
=========

Guide pin definition.

"""

from csg.composites import PinCell
from core.fuel_assembly import PinCellStack
from core import *
from . import materials


def build(moderator=materials.borated_water(), facility='guide-tube'):

    # ==================================================================================================================
    # Empty guide tube above dashpot
    # Source: Figure 4

    guide = PinCell()
    guide.add_cylindrical_shell(radius=0.56134 * units.cm, facility=facility, material=moderator)
    guide.add_cylindrical_shell(radius=0.60198 * units.cm, material=materials.zircaloy())
    guide.fill_material = moderator

    # ==================================================================================================================
    # Empty guide tube at dashpot
    # Source: Figure 5

    dashpot = PinCell()
    dashpot.add_cylindrical_shell(radius=0.50419 * units.cm, facility=facility, material=moderator)
    dashpot.add_cylindrical_shell(radius=0.54610 * units.cm, material=materials.zircaloy())
    dashpot.fill_material = moderator

    # ==================================================================================================================
    # Pin cell for Inconel top spacer inner egg crates
    # Source: Figure 20
    inconel_spacer = PinCell()

    inconel_spacer.add_cylindrical_shell(radius=0.56134 * units.cm, facility=facility, material=moderator)
    inconel_spacer.add_cylindrical_shell(radius=0.60198 * units.cm, material=materials.zircaloy())
    inconel_spacer.add_square_shell(radius=0.62208 * units.cm, material=moderator)
    inconel_spacer.fill_material = materials.inconel()

    # ==================================================================================================================
    # Pin cell for Inconel bottom spacer inner egg crates
    # Source: Figure 20
    inconel_spacer_dp = PinCell()

    inconel_spacer_dp.add_cylindrical_shell(radius=0.50519 * units.cm, facility=facility, material=moderator)
    inconel_spacer_dp.add_cylindrical_shell(radius=0.54610 * units.cm, material=materials.zircaloy())
    inconel_spacer_dp.add_square_shell(radius=0.62208 * units.cm, material=moderator)
    inconel_spacer_dp.fill_material = materials.inconel()

    # ==================================================================================================================
    # Pin cell for zircalloy intermediate spacer inner egg crates
    # Source: Figure 21
    zirc_spacer = PinCell()

    zirc_spacer.add_cylindrical_shell(radius=0.56134 * units.cm, facility=facility, material=moderator)
    zirc_spacer.add_cylindrical_shell(radius=0.60198 * units.cm, material=materials.zircaloy())
    zirc_spacer.add_square_shell(radius=0.60978 * units.cm, material=moderator)
    zirc_spacer.fill_material = materials.zircaloy()

    # ==================================================================================================================
    # Empty pin cell filled with water
    empty = PinCell()
    empty.fill_material = moderator

    # ==================================================================================================================
    # Pin cell stack
    # Source: Figure 30

    stack = PinCellStack(name='BEAVRS-guide-pin')

    stack.add_layer(0.00000 * units.cm, 35.1600 * units.cm, empty)
    stack.add_layer(35.1600 * units.cm, 37.8790 * units.cm, dashpot)  # active region starts
    stack.add_layer(37.8790 * units.cm, 42.0700 * units.cm, inconel_spacer_dp)  # grid 1
    stack.add_layer(42.0700 * units.cm, 45.0790 * units.cm, dashpot)
    stack.add_layer(45.0790 * units.cm, 99.1640 * units.cm, guide)
    stack.add_layer(99.1640 * units.cm, 104.879 * units.cm, zirc_spacer)  # grid 2
    stack.add_layer(104.879 * units.cm, 151.361 * units.cm, guide)
    stack.add_layer(151.361 * units.cm, 157.076 * units.cm, zirc_spacer)  # grid 3
    stack.add_layer(157.076 * units.cm, 203.558 * units.cm, guide)
    stack.add_layer(203.558 * units.cm, 209.273 * units.cm, zirc_spacer)  # grid 4
    stack.add_layer(209.273 * units.cm, 255.755 * units.cm, guide)
    stack.add_layer(255.755 * units.cm, 261.470 * units.cm, zirc_spacer)  # grid 5
    stack.add_layer(261.470 * units.cm, 307.952 * units.cm, guide)
    stack.add_layer(307.952 * units.cm, 313.667 * units.cm, zirc_spacer)  # grid 6
    stack.add_layer(313.667 * units.cm, 360.149 * units.cm, guide)
    stack.add_layer(360.149 * units.cm, 365.864 * units.cm, zirc_spacer)  # grid 7
    stack.add_layer(365.864 * units.cm, 401.767 * units.cm, guide)
    stack.add_layer(401.767 * units.cm, 416.720 * units.cm, inconel_spacer)  # grid 8
    stack.add_layer(416.720 * units.cm, 426.617 * units.cm, guide)
    stack.add_layer(426.617 * units.cm, 455.444 * units.cm, empty)

    stack.top_fill_material = moderator
    stack.bottom_fill_material = moderator

    stack.pitch = 2.0 * 0.62992 * units.cm

    # add the guide tube facility and fill it with water
    stack.add_facility(facility, state=moderator, shared=False)

    return stack.build()


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build, world_box=(2.0 * 0.62992 * units.cm, 2.0 * 0.62992 * units.cm))