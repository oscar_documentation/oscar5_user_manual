
from core.fuel_assembly import LoadablePlateAssembly
from csg import primitives
from csg.primitives import *
from core.units import units

from . import materials
from material_library.moderators import LightWater


def build(*args, **kwargs):
    mp = LoadablePlateAssembly(name='IPEN-empty-irradiation-device', **kwargs)
    mp.description = 'Irradiation Device (Holder) without any plates'

    # ==================================================================================================================
    # set some QA parameters
    mp.__source__ = 'BRA_IPEN_B_2b_Irradiation Device'
    mp.__version__ = '0.1.0'
    mp.__author__ = 'lesego'

    # ==================================================================================================================
    # set plate bundle parameters
    mp.number_of_plates = 5
    mp.pitches = 2 * 2.5 * units.mm  # centre plate 1 to centre plate 2

    mp.grid_plate_pitch = 46.5 * units.mm

    mp.slot_insert_depth = 2.0 * units.mm
    mp.slot_insert_width = 2.5 * units.mm

    box_length = (33.85 - 2 * 3.175) * units.mm
    mp.box = [-27.75 * units.mm, 27.75 * units.mm, -0.5 * box_length, +0.5 * box_length]

    holder_bottom = (172.0 - 170) * units.mm
    mp.axial_region = [-87.0 * units.mm, 0.5 * 170.0 * units.mm]
    #    mp.axial_region = [-86 * units.mm, 86 * units.mm]

    mp.grid_material = materials.miniplate_clad()
    mp.moderator_material = LightWater(mass_density=0.99820 * units.g / units.cc)

    # Temporarily fill the bundle space (speeds up the fill universe algorithm)
    mp.create_bundle_place_holder()

    # Add the side plate of the irradiation plate holder

    side_plate_left_outer = Py(-16.925 * units.mm)
    side_plate_left_inner = Py(-13.750 * units.mm)

    side_plate_right_outer = Py(16.925 * units.mm)
    side_plate_right_inner = Py(13.750 * units.mm)

    side_plate_upper_bound = Px(31.750 * units.mm)
    side_plate_lower_bound = Px(-31.750 * units.mm)

    side_plate_top = Pz(85.0 * units.mm)
    side_plate_bottom = Pz(-87.0 * units.mm)

    holder_handler = (480.0 - 172.0) * units.mm
    handler_extension_top = Pz(holder_handler)

    side_plate_right = (~side_plate_right_inner & side_plate_right_outer & ~side_plate_bottom & handler_extension_top &
                        side_plate_upper_bound & ~side_plate_lower_bound)

    mp.add_cell(side_plate_right,
                material=materials.miniplate_clad(),
                description='side plate right')

    side_plate_left = (side_plate_left_inner & ~side_plate_left_outer & ~side_plate_bottom & side_plate_top &
                       side_plate_upper_bound & ~side_plate_lower_bound)

    cutout = primitives.Cylinder(radius=15 * units.mm,
                                 alignment=ALIGNMENT.Y_AXIS,
                                 center=(1.5875 * units.mm, 85.0 * units.mm))

    mp.add_cell(side_plate_left & ~cutout,
                material=materials.miniplate_clad(),
                description='side plate left')

    mp.complete_universe(material=LightWater(mass_density=0.99820 * units.g / units.cc))

    mp.construct_bundle()

    # mp.simplify_cells()

    return mp


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build)
