"""
Standard fuel assembly
======================

Standard fuel assembly with 484 grams U-235, and cadmium absorbers in the grid plates.

Assumptions and simplifications
-------------------------------

 1. To preserve a fixed water gap, the pitch (distance to fuel meat centers) for the outer plates were adjusted.
    Need to verify that this is the correct assumption.
 2. No additional handling structures were modeled on top of the assembly.

"""
from core.fuel_assembly import PlateAssemblyWithBurnableAbsorbers, WireBurnableAbsorber
from csg import *
from core.units import units
from core.place_holders import _
from material_library.moderators import LightWater
from . import materials


def build(*args, **kwargs):

    fa = PlateAssemblyWithBurnableAbsorbers(name='OPAL_standard_fuel_assembly', **kwargs)
    fa.description = 'Standard fuel assembly with burnable absorbers'

    # ==================================================================================================================
    # set some QA parameters
    fa.__source__ = 'OPAL_reactor_specification_V2.pdf, Table 3, Figures 3 & 4'
    fa.__version__ = '0.3.0'
    fa.__author__ = 'francois'

    # ==================================================================================================================
    # set plate bundle parameters
    fa.number_of_plates = 21
    # Note that, to preserve the water gap, the outer pitch from fuel plate to fuel plate should be larger
    fa.pitches = [2.45 * units.mm + 0.5 * 1.5 * units.mm + 0.5 * 1.35 * units.mm] + \
                 [2.45 * units.mm + 1.35 * units.mm] * 18 +\
                 [2.45 * units.mm + 0.5 * 1.5 * units.mm + 0.5 * 1.35 * units.mm]
    #                   exterior plate             inner plates                   exterior plate
    fa.plate_width = [1.5 * units.mm] + [1.35 * units.mm] * 19 + [1.5 * units.mm]
    fa.plate_length = 75.0 * units.mm
    fa.plate_height = [825 * units.mm] + [655 * units.mm] * 19 + [825 * units.mm]

    fa.meat_width = 0.61 * units.mm
    fa.meat_length = 65.0 * units.mm

    fa.meat_height = 615.0 * units.mm

    fa.grid_plate_pitch = 80.5 * units.mm - 2.0 * 5.0 * units.mm

    fa.slot_insert_depth = 2.5 * units.mm
    fa.slot_insert_width = [1.6 * units.mm] + [1.45 * units.mm] * 19 + [1.6 * units.mm]

    fa.box = [-40.25 * units.mm, 40.25 * units.mm, -40.25 * units.mm, 40.25 * units.mm]

    top = (1045 - 615 - 158 - 145) * units.mm
    fa.axial_region = [-0.5 * fa.meat_height[0] - 158 * units.mm, 0.5 * fa.meat_height[0] + top]

    fa.clad_material = materials.cladding()
    fa.grid_material = materials.al6061()
    fa.moderator_material = LightWater(mass_density=0.99160 * units.g / units.cc, temperature=(273.15 + 40.0) * units.K)

    # ==================================================================================================================
    # Set the BA parameters
    fa.ba_groove_width = 0.5 * units.mm
    fa.ba_groove_depth = 0.6 * units.mm

    ba = WireBurnableAbsorber()
    ba.length = 308 * units.mm
    ba.diameter = 0.493 * units.mm
    ba.axial_seating = (-0.5 * 615 + 123 + 0.5 * 308) * units.mm

    # Only odd numbered plates has a wire (on each side)
    fa.ba_loading = \
    [[_ , _ ],
     [ba, ba],
     [_ , _ ],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _],
     [ba, ba],
     [_,   _]]

    # Add the fuel bundle cells
    fa.construct_bundle()

    # ==================================================================================================================
    # End box section

    cell.begin_part('Adapter')

    axial = region_macros.axial_strip(lower=(-0.5 * 615 - 158 - 145) * units.mm,
                                      upper=(-0.5 * 615 - 158) * units.mm)

    adapter = region_macros.clipped_box(diameter=69 * units.mm, cut=8 * units.mm)

    fa.add_cell(adapter & ~primitives.Cylinder(radius=30 * units.mm) & axial,
                material=materials.al6061(),
                description='Assembly end box')

    # ==================================================================================================================
    # Initiate the fuel material
    fa.fuel_bundle().depletion_mesh.initial_material_distribution = materials.standard_fuel()

    # ==================================================================================================================
    # Initiate the BA material
    fa.ba_bundle().depletion_mesh.initial_material_distribution = materials.cadmium()

    # ==================================================================================================================
    # Mesh completion settings
    fa.set_background(material=fa.moderator_material)      # fill remaining space with water
    fa.mesh_completion_required = True                     # ensure that missing water cells are added during archiving

    return fa


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build)