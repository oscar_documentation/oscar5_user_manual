"""
Standard fuel assembly (3.4% enriched)
======================================

"""
from core.fuel_assembly import SquarePinAssembly
from csg import *
from . import materials


from . import assemblies


def add_spacers(target):
    """
    Add the grid spacers sleeves (outside the active pin cell mesh)

    """
    target.add_spacer(37.8790 * units.cm, 42.0700 * units.cm, material=materials.zircaloy())
    target.add_spacer(99.1640 * units.cm, 104.879 * units.cm, material=materials.zircaloy())
    target.add_spacer(151.361 * units.cm, 157.076 * units.cm, material=materials.zircaloy())
    target.add_spacer(203.558 * units.cm, 209.273 * units.cm, material=materials.zircaloy())
    target.add_spacer(255.755 * units.cm, 261.470 * units.cm, material=materials.zircaloy())
    target.add_spacer(307.952 * units.cm, 313.667 * units.cm, material=materials.zircaloy())
    target.add_spacer(360.149 * units.cm, 365.864 * units.cm, material=materials.zircaloy())
    target.add_spacer(401.767 * units.cm, 416.720 * units.cm, material=materials.zircaloy())


def build(moderator=materials.borated_water(), fuel=materials.fuel34(), *args, **kwargs):
    """

    moderator: Moderator material
    fuel: Fuel material

    return:
        Basic BEAVRS fuel assembly with empty control and instrument pins
    """

    fa = SquarePinAssembly(name='BEAVRS-fuel-assembly', size=(17, 17))

    fa.pin_cell_pitch = 2.0 * 0.62992 * units.cm
    fa.spacer_pitch = 2.0 * 10.73635 * units.cm
    # just to ensure the model has a finite bounding box
    fa.axial_region = region_macros.axial_strip(-0.1 * units.cm, 456 * units.cm)

    # pins
    fp = assemblies.BEAVRS_fuel_pin()
    gt = assemblies.BEAVRS_guide_pin()
    ip = assemblies.BEAVRS_instrument_guide()

    # Calls the helper routine to add all the external spacer grid positions
    #
    add_spacers(fa)

    # Material that should be used in the sleeve or spacer region outside the fuel pin lattice and spacer grids
    #
    fa.sleeve_material = moderator

    # qa =
    # [[fp, fp, fp, fp, fp, fp, fp, fp, fa],
    #  [fp, fp, fp, fp, fp, fp, fp, fp, fp],
    #  [fp, fp, fp, fp, fp, gt, fp, fp, gt],
    #  [fp, fp, fp, gt, fp, fp, fp, fp, fp],
    #  [fp, fp, fp, fp, fp, fp, fp, fp, fp],
    #  [fp, fp, gt, fp, fp, gt, fp, fp, gt],
    #  [fp, fp, fp, fp, fp, fp, fp, fp, fp],
    #  [fp, fp, fp, fp, fp, fp, fp, fp, fp],
    #  [fp, fp, gt, fp, fp, gt, fp, fp, ip]]
    #
    # fa.pin_layout = Grid(qa).unfold('r', 'b')

    fa.pin_layout = \
        [[fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, fp, fp, fp],
         [fp, fp, fp, gt, fp, fp, fp, fp, fp, fp, fp, fp, fp, gt, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, ip, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, gt, fp, fp, fp, fp, fp, fp, fp, fp, fp, gt, fp, fp, fp],
         [fp, fp, fp, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp]
         ]

    # Distribute a fixed material to all the pins (this can be changed later for assemblies with different
    # enrichment)
    fa.fuel_bundle().initial_material_distribution = fuel

    # construct
    fa.build()

    # add the cell outside the sleeve (to complete the universe

    fa.add_cell(~fa.sleeve_box(),
                material=moderator,
                description='Water around assembly (to complete universe)')

    fa.set_background(material=materials.borated_water())

    return fa


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build, world_box=(2.0 * 10.73635 * units.cm, 2.0 * 10.73635 * units.cm))
