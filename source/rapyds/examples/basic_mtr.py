"""
SAFARI-1 fuel with simplified geometry
======================================

Plate type fuel assembly with 19.75% enriched U3Si2-Al fuel.

Sources
-------
SAFARI-1 benchmark description, Section 4.2

Assumptions and Simplifications
-------------------------------
 - Top and bottom adapters are approximated as cylinders.
 - Geometry of slots in side plates are guessed.

Version Log
-----------
 - 1.0.0 : Initial model definition
 - 1.1.0 : Update to better use new features

"""

import core.fuel_assembly
from csg import *
from csg.primitives import *
import material_library.moderators
from . import materials


def build(*args, **kwargs):
    fa = core.fuel_assembly.PlateAssembly(name='SAFARI-1-LEU-fuel')
    fa.description = 'SAFARI-1 fuel with simplified geometry'

    # ==================================================================================================================
    # Set some QA parameters
    fa.__source__ = 'SAFARI-1 benchmark specifications'
    fa.__version__ = '1.0.0'
    fa.__author__ = 'S.A. Groenewald'

    # ==================================================================================================================
    # Set plate bundle parameters
    fa.number_of_plates = 19
    fa.pitches = 2.95 * units.mm + 1.275 * units.mm  # coolant gap plus plate width
    fa.plate_width = 1.275 * units.mm
    fa.plate_length = 66.8 * units.mm + 2 * 2.5 * units.mm # distance between side plates + 2*(insert depth)
    fa.plate_height = [682.9 * units.mm] + [625.48 * units.mm]*17 + [682.9 * units.mm]  # outer plates extend further

    fa.meat_width = 0.510 * units.mm
    fa.meat_length = 63.00 * units.mm
    fa.meat_height = 593.7 * units.mm

    fa.grid_plate_pitch = 66.8 * units.mm

    fa.slot_insert_depth = 2.5 * units.mm  # slot insert depth guessed
    fa.slot_insert_width = 1.275 * units.mm

    fa.box = [-0.5 * 75.9 * units.mm, 0.5 * 75.9 * units.mm, -0.5 * 80.15 * units.mm, 0.5 * 80.15 * units.mm]
    fa.axial_region = [-0.5 * 682.9 * units.mm, 0.5 * 682.9 * units.mm]

    fa.clad_material = materials.al_clad()
    fa.grid_material = materials.al_clad()
    fa.moderator_material = material_library.moderators.H2O(mass_density=0.992 * units.g / units.cc)  # from specs TABLE 5

    # ==================================================================================================================
    # Building end adapters
    # Note: Cylinders are used as a simplified representation of both top and bottom adapters

    top_adap = region_macros.axial_strip(lower=0.5 * 682.9 * units.mm,
                                         upper=0.5 * 923.9 * units.mm)

    bot_adap = region_macros.axial_strip(lower=-0.5 * 923.9 * units.mm,
                                         upper=-0.5 * 682.9 * units.mm)

    rad_out = Cylinder(radius=(0.5 * 73.0) * units.mm)  # half of diameter, from specs FIG 10
    rad_in = Cylinder(radius=(0.5 * 63.5) * units.mm)

    # Top adapter
    fa.add_cell(rad_out & ~rad_in & top_adap,
                material=materials.al_ag3ne(),
                description='Top adapter',
                part='TopAdapter')

    # Bottom adapter
    fa.add_cell(rad_out & ~rad_in & bot_adap,
                material=materials.al_ag3ne(),
                description='Bottom adapter',
                part='BottomAdapter')

    fa.axially_fill_channels = False

    # ==================================================================================================================
    # Fill model with water (recommended to avoid empty cells when exported to the external code)
    fa.complete_universe(material=fa.moderator_material)

    # ==================================================================================================================
    # Construct the fuel bundle (i.e. add all the fuel- and side plates, and moderator in between)
    # More efficient to construct bundle after filling universe with water
    fa.construct_bundle()

    # ==================================================================================================================
    # Initiate the fuel material
    fa.fuel_bundle().depletion_mesh.initial_material_distribution = \
        materials.fuel_leu(plate_volume=0.51 * units.mm * 63.00 * units.mm * 593.7 * units.mm)

    return fa


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build, world_box=(7.71 * units.cm, 8.1 * units.cm))

