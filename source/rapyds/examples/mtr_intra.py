"""
Control Fuel Assembly
=====================

Model of the special control (17 plate) fuel assembly with 93% enriched uranium.

Sources
-------
The IRR-1 facility specification document. In particular Table 5 and Figure 3.

Assumptions and Simplifications
-------------------------------

The upper section of the control assembly, containing the drive slot mechanisms, were not modelled.

Outstanding Issues
------------------

 1. The structure above the active fuel is not clear, and is currently simply modelled as water.

Version Log
-----------
 - 1.0.0 : Initial model definition

"""
from core import *
from csg import *
from material_library.moderators import LightWater

from . import materials
from .fuel_assembly import build_nozzle

upper_extent = 0.5 * 625.5 * units.mm + (1255 * units.mm - 625.5 * units.mm)


def guide_structure(ext=9.975 * units.mm, include_grid=False):

    cells = cell.Part()

    pitches = 2.1 * units.mm + 0.51 * units.mm + 2.0 * 0.38 * units.mm

    guide_box_center = 0.5 * 17 * pitches + 6.93 * units.mm

    lower_plate = (guide_box_center - 0.5 * 6.35 * units.mm - 1.27 * units.mm,
                   guide_box_center - 0.5 * 6.35 * units.mm)

    grid_plate_pitch = 76.1 * units.mm - 2.0 * 4.75 * units.mm

    axial = region_macros.axial_strip(-0.5 * 625.5 * units.mm,
                                      0.5 * 625.5 * units.mm + (1255 * units.mm - 625.5 * units.mm))

    cells.add_cell(axial &
                   region_macros.rectangle(-0.5 * grid_plate_pitch, 0.5 * grid_plate_pitch,
                                           lower_plate[0], lower_plate[1]),
                   material=materials.al6061(),
                   part='GuidePlates')

    upper_plate = (guide_box_center + 0.5 * 6.35 * units.mm,
                   guide_box_center + 0.5 * 6.35 * units.mm + 1.27 * units.mm)

    axial = region_macros.axial_strip(-0.5 * 604 * units.mm - 44 * units.mm,
                                      0.5 * 625.5 * units.mm + (1255 * units.mm - 625.5 * units.mm))

    cells.add_cell(axial &
                   region_macros.rectangle(-0.5 * grid_plate_pitch, 0.5 * grid_plate_pitch,
                                           upper_plate[0], upper_plate[1]),
                   material=materials.al6061(),
                   part='GuidePlates')

    # extended grid plates
    if include_grid:
        lower_bound = 0.0 * units.mm
    else:
        lower_bound = 0.5 * 17 * pitches + 0.5 * pitches

    cells.add_cell(axial & region_macros.rectangle(-0.5 * grid_plate_pitch - 4.75 * units.mm, -0.5 * grid_plate_pitch,
                                                   lower_bound, 0.5 * 80 * units.mm),
                   material=materials.al6061(),
                   part='Box')

    cells.add_cell(axial & region_macros.rectangle(0.5 * grid_plate_pitch, 0.5 * grid_plate_pitch + 4.75 * units.mm,
                                                   lower_bound, 0.5 * 80 * units.mm),
                   material=materials.al6061(),
                   part='Box')

    # add the guide channel
    axial = region_macros.axial_strip(-0.5 * 625.5 * units.mm - ext,
                                      0.5 * 625.5 * units.mm + (1255 * units.mm - 625.5 * units.mm))

    cells.add_cell(axial & region_macros.rectangle(-0.5 * grid_plate_pitch, 0.5 * grid_plate_pitch,
                                                   lower_plate[1], upper_plate[0]),
                   facility='control-guide')

    # reflect to get bottom channel

    reflected = cell.reflect(ALIGNMENT.Y_AXIS, *cells)

    cells.add_cells(*reflected)

    # add the adapter here

    clip = region_macros.rectangle(-0.5 * grid_plate_pitch, 0.5 * grid_plate_pitch,
                                   -upper_plate[0], upper_plate[0])

    nozzle = build_nozzle(2.0 * upper_plate[0],
                          ext, clip)

    cells.add_cells(*nozzle)

    return cells


def build(*args, **kwargs):

    asm = fuel_assembly.PlateAssembly(name='IRR-1-control-fuel-assembly')
    asm.description = 'Control Fuel Assembly'

    # ==================================================================================================================
    # Set some QA parameters
    asm.__source__ = 'IRR-1 REACTOR SPECIFICATION'
    asm.__version__ = '1.0.0'
    asm.__author__ = 'francois'

    # ==================================================================================================================
    # Set fuel bundle parameters
    asm.number_of_plates = 17

    asm.meat_height = 604 * units.mm
    asm.meat_width = 0.51 * units.mm
    asm.meat_length = 63 * units.mm

    asm.plate_length = 71 * units.mm
    asm.plate_width = 0.51 * units.mm + 2.0 * 0.38 * units.mm
    # Rest of side plate extensions modelled outside the bundle region

    bottom_external_plate = - 0.5 * 604 * units.mm - 44 * units.mm
    lower_external_plate_extension = - 0.5 * 625.5 * units.mm - bottom_external_plate
    upper_external_plate_extension = 709 * units.mm - 625.5 * units.mm - lower_external_plate_extension

    extension_est = 0.3 * lower_external_plate_extension  # Estimated distance between bottom of plates and nozzle insert
    asm.plate_height = 625.5 * units.mm

    asm.slot_insert_depth = 2.2 * units.mm
    asm.slot_insert_width = 1.3 * units.mm

    # Coolant gap plus two times plate width
    asm.pitches = 2.1 * units.mm + asm.plate_width[0]

    asm.grid_plate_pitch = 76.1 * units.mm - 2.0 * 4.75 * units.mm

    # outer assembly dimensions
    asm.box = [-0.5 * 76.1 * units.mm, 0.5 * 76.1 * units.mm,
               -0.5 * 17 * asm.pitches[0] - 0.5 * asm.pitches[0],
                0.5 * 17 * asm.pitches[0] + 0.5 * asm.pitches[0]]
    asm.axial_region = [- 0.5 * 625.5 * units.mm - extension_est, +0.5 * 625.5 * units.mm + extension_est]

    asm.clad_material = materials.al6061(material.tags.clad)
    asm.grid_material = materials.al6061(material.tags.structural)
    asm.moderator_material = LightWater(pressure=1.8 * units.bar, temperature=35 * units.degC)

    # use custom object to add side plate enforcing

    asm.create_bundle_place_holder()

    # ==================================================================================================================
    # Grid plate extensions and handling structure above assembly box

    axial = region_macros.axial_strip(+0.5 * 625.5 * units.mm + extension_est, upper_extent)

    asm.add_cell(axial & asm.box &
                 (primitives.Px(-0.5 * asm.grid_plate_pitch) |
                  ~primitives.Px(+0.5 * asm.grid_plate_pitch)),
                 material=asm.grid_material,
                 part='Box',
                 description='Grid upper extension')

    # ==================================================================================================================
    # Bottom grid extensions

    axial = region_macros.axial_strip(-0.5 * 604 * units.mm - 44 * units.mm,
                                      -0.5 * asm.plate_height[0] - extension_est)

    asm.add_cell(axial & asm.box &
                 (primitives.Px(-0.5 * asm.grid_plate_pitch) |
                  ~primitives.Px(+0.5 * asm.grid_plate_pitch)),
                 material=asm.grid_material,
                 part='Box',
                 description='Grid lower extension')

    # ==================================================================================================================
    # Guide structure

    asm.add_facility('control-guide', state=asm.moderator_material)

    asm.add_cells(*guide_structure(extension_est))

    asm.complete_universe(material=asm.moderator_material)

    asm.construct_bundle()

    asm.fuel_bundle().depletion_mesh.initial_material_distribution = materials.uranium_aluminum()

    return asm


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build)
