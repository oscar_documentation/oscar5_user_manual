"""
Fuel Pin
=========

Standard fuel pin definition.



"""

from csg.composites import PinCell
from core.fuel_assembly import PinCellStack
from core import *
from . import materials


def build(bundle=bundle_tags.fuel, moderator=materials.borated_water()):

    # ==================================================================================================================
    # Active region pin cell
    # Source: Figure 2
    active = PinCell()

    active.add_cylindrical_shell(radius=0.39218 * units.cm, bundle=bundle)
    active.add_cylindrical_shell(radius=0.40005 * units.cm, material=materials.helium())
    active.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    active.fill_material = moderator

    # ==================================================================================================================
    # Pin cell for Inconel top and bottom spacer inner egg crates
    # Source: Figure 20
    inconel_spacer = PinCell()

    inconel_spacer.add_cylindrical_shell(radius=0.39218 * units.cm, bundle=bundle)
    inconel_spacer.add_cylindrical_shell(radius=0.40005 * units.cm, material=materials.helium())
    inconel_spacer.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    inconel_spacer.add_square_shell(radius=0.62208 * units.cm, material=moderator)
    inconel_spacer.fill_material = materials.inconel()

    # ==================================================================================================================
    # Pin cell for zircalloy intermediate spacer inner egg crates
    # Source: Figure 21
    zirc_spacer = PinCell()

    zirc_spacer.add_cylindrical_shell(radius=0.39218 * units.cm, bundle=bundle)
    zirc_spacer.add_cylindrical_shell(radius=0.40005 * units.cm, material=materials.helium())
    zirc_spacer.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    zirc_spacer.add_square_shell(radius=0.60978 * units.cm, material=moderator)
    zirc_spacer.fill_material = materials.zircaloy()

    # ==================================================================================================================
    # Pin cell for upper plenum
    # Source: Figure 3
    upper_plenum = PinCell()

    upper_plenum.add_cylindrical_shell(radius=0.06459 * units.cm, material=materials.inconel())
    upper_plenum.add_cylindrical_shell(radius=0.40005 * units.cm, material=materials.helium())
    upper_plenum.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    upper_plenum.fill_material = moderator

    # ==================================================================================================================
    # Pin cell for upper plenum in inconel egg crate
    # Source: Figure 3
    upper_plenum_grid = PinCell()

    upper_plenum_grid.add_cylindrical_shell(radius=0.06459 * units.cm, material=materials.inconel())
    upper_plenum_grid.add_cylindrical_shell(radius=0.40005 * units.cm, material=materials.helium())
    upper_plenum_grid.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    upper_plenum_grid.add_square_shell(radius=0.62208 * units.cm, material=moderator)
    upper_plenum_grid.fill_material = materials.inconel()

    # ==================================================================================================================
    # Solid stainless steel pin
    steel_pin = PinCell()

    steel_pin.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    steel_pin.fill_material = moderator

    # ==================================================================================================================
    # Solid zircaloy pin
    zirc_pin = PinCell()
    zirc_pin.add_cylindrical_shell(radius=0.45720 * units.cm, material=materials.zircaloy())
    zirc_pin.fill_material = moderator

    # ==================================================================================================================
    # Empty pin cell filled with water
    empty = PinCell()
    empty.fill_material = moderator

    # ==================================================================================================================
    # Pin cell stack
    # Source: Figure 29

    stack = PinCellStack(name='BEAVRS-fuel-pin')
    stack.add_layer(0.00000 * units.cm, 20.0000 * units.cm, empty)
    stack.add_layer(20.0000 * units.cm, 35.1600 * units.cm, steel_pin)
    stack.add_layer(35.1600 * units.cm, 36.0070 * units.cm, zirc_pin)
    stack.add_layer(36.0070 * units.cm, 37.8790 * units.cm, active)         # active region starts
    stack.add_layer(37.8790 * units.cm, 42.0700 * units.cm, inconel_spacer) # grid 1
    stack.add_layer(42.0700 * units.cm, 99.1640 * units.cm, active)
    stack.add_layer(99.1640 * units.cm, 104.879 * units.cm, zirc_spacer)    # grid 2
    stack.add_layer(104.879 * units.cm, 151.361 * units.cm, active)
    stack.add_layer(151.361 * units.cm, 157.076 * units.cm, zirc_spacer)    # grid 3
    stack.add_layer(157.076 * units.cm, 203.558 * units.cm, active)
    stack.add_layer(203.558 * units.cm, 209.273 * units.cm, zirc_spacer)    # grid 4
    stack.add_layer(209.273 * units.cm, 255.755 * units.cm, active)
    stack.add_layer(255.755 * units.cm, 261.470 * units.cm, zirc_spacer)    # grid 5
    stack.add_layer(261.470 * units.cm, 307.952 * units.cm, active)
    stack.add_layer(307.952 * units.cm, 313.667 * units.cm, zirc_spacer)    # grid 6
    stack.add_layer(313.667 * units.cm, 360.149 * units.cm, active)
    stack.add_layer(360.149 * units.cm, 365.864 * units.cm, zirc_spacer)    # grid 7
    stack.add_layer(365.864 * units.cm, 401.767 * units.cm, active)         # active region ends
    stack.add_layer(401.767 * units.cm, 412.529 * units.cm, upper_plenum)
    stack.add_layer(412.529 * units.cm, 416.720 * units.cm, upper_plenum_grid) # grid 8
    stack.add_layer(416.720 * units.cm, 421.223 * units.cm, upper_plenum)
    stack.add_layer(421.223 * units.cm, 423.272 * units.cm, zirc_pin)
    stack.add_layer(423.272 * units.cm, 426.617 * units.cm, empty)
    stack.add_layer(426.617 * units.cm, 435.444 * units.cm, steel_pin)
    stack.add_layer(435.444 * units.cm, 455.444 * units.cm, empty)

    stack.pitch = 2.0 * 0.62992 * units.cm

    stack.top_fill_material = moderator
    stack.bottom_fill_material = moderator

    return stack.build()


if __name__ == '__main__':
    from applications.assembly_archiver import main
    main(source=build)
