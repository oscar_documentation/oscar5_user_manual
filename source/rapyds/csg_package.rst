
The Constructive Solid Geometry (CSG) package
*********************************************

The **csg** package is intended for the universal creation of geometry models based on regularizing set operations.
Solid geometry modeling is quite common in Monte Carlo neutronics and shielding codes, and the basic principles
should be familiar to anybody with experience in codes such as MCNP and SERPENT. The main difference in our
approach is that we emphasize solids over surfaces. More specifically, the geometry is encoded directly in the volumes,
and there is no need for tedious book keeping of interface surfaces.

.. note::

   From a practical stand point, this means that you do not need to have a defined pool of surfaces
   (or primitives), but can construct them as you need them, even if they are used by many cells. The system will
   automatically deduce shared surfaces. Thus, regions (or volumes) can be constructed completely independently from
   one another, allowing one to focus on a particular part of an assembly, without having to take the entire construction
   into consideration.

   However, if you are more comfortable with the traditional approach, you can still make use of a single pool of
   surfaces (or primitives).

The package has two basic layers: The first simply provides a convenient, code independent interface for
creating and storing **CSG** geometries. This layer does not perform any geometric operations, it merely stores the
construction tree.

The second layer uses a polyhedra package to perform the actual geometric operations. This layer is optional,
but is currently required for the geometry visualization pipeline, as well as numerous geometric algorithms (e.g. cell
simplifications, meshing, volume calculations etc).

This package creates basic volumes (bounded or unbounded regions in 3D space), which can later be used to construct
physical cells (the primary building blocks of components), by adding data describing its purpose (e.g fill it with a
material, or burnable bundle). Components of the **csg** package are typically used in the following order:

    1. Choose a set of `primitive regions <Primitive regions>`__ that best capture the region one wishes to construct.

    2. Use `transformations <Transformations>`__ (rotations and translations) to correctly orient the primitives.

    3. Combine the primitives using `set operations <Regularizing set operations>`__.


Primitive regions
=================

Primitive regions constitute the basic building blocks out of which more complicated regions are constructed.
Since these regions are bounded by simple (and often parametric) surfaces, they play the same role as surface
definitions in Monte Carlo codes.

All basic primitives are defined in the :mod:`csg.primitives` module, while some extensions are defined in the
:mod:`csg.region_macros` module.

.. note::

   All primitives have a ``show()`` method, which opens a basic three dimensional view of the region.

Planes
------

These are infinite, half space regions bounded by a plane. The following plane constructions are available:

.. py:function:: primitives.Px(position=p)

   Region below a plane perpendicular the :math:`x`-axis.

   :param position: Intersection point with the :math:`x`-axis.
   :type position: :term:`length`

   Parametric set definition:

   .. math::

      \{(x,y,z) : x \leq p\}

   Example:

   >>> p = primitives.Px(2 * units.cm)

.. py:function:: primitives.Py(position=p)

   Region below a plane perpendicular the :math:`y`-axis.

   :param position: Intersection point with the :math:`y`-axis.
   :type position: :term:`length`

   Parametric set definition:

   .. math::

      \{(x,y,z) : y \leq p\}

   Example:

   >>> p = primitives.Py(1 * units.cm)

.. py:function:: primitives.Pz(position=p)

   Region below a plane perpendicular the :math:`z`-axis.

   :param position: Intersection point with the :math:`z`-axis.
   :type position: :term:`length`

   Parametric set definition:

   .. math::

      \{(x,y,z) : z \leq p\}

   Example:

   >>> p = primitives.Pz(3 * units.cm)

.. py:function:: primitives.Plane(a=a, b=b, c=c, d=p)

   Region below an arbitrary plane.

   :param float a: :math:`x` component of plane normal.
   :param float b: :math:`y` component of plane normal.
   :param float c: :math:`z` component of plane normal.
   :param d: Position of plane.
   :type d: :term:`length`

   Parametric set definition:

   .. math::

      \{(x,y,z) : ax + by + cz \leq p\}

   Example:

   >>> p = primitives.Plane(a=1.0, b=1.0, c=0.0, d=-1.0 * units.cm)

.. py:function:: primitives.create_plane(p0, p1, p2)

   Create a plane through 3 (non co-linear) points.

   :param p0: First point.
   :type p0: :term:`point`
   :param p1: Second point.
   :type p1: :term:`point`
   :param p2: Third point.
   :type p2: :term:`point`

   Example:

   >>> p = primitives.create_plane((1.0 * units.cm, 0, 0), (0, 1 * units.cm, 0), (0, 0, 1 * units.cm))
   >>> print(p.a, p.b, p.c)
   1.0 1.0 1.0


Cylindrical regions
-------------------

These are regions that are bounded in one coordinate plane by circular shapes, and is infinite in the perpendicular
dimension.


.. py:function:: primitives.Cx(radius=r (OR diameter=d), center=(y0,z0))

   Region inside an infinite cylinder parallel to the :math:`x`-axis.

   :param radius: Radius of the cylinder.
   :type radius: :term:`length`
   :param diameter: Diameter of the cylinder (instead of `radius`, but **not** both).
   :type diameter: :term:`length`
   :param center: Center of the cylinder in the :math:`yz`-plane. Default value is :math:`(0,0)`.
   :type center: :term:`point`

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : (y-y_0)^2 + (z-z_0)^2 \leq r^2\right\}

   Examples:

   >>> p0 = primitives.Cx(radius=1.0 * units.cm) # centered at (0,0)
   >>> p1 = primitives.Cx(diameter=2.0 * units.cm, center=(-1.0 * units.cm, 0.0))

.. py:function:: primitives.Cy(radius=r (OR diameter=d), center=(x0,z0))

   Region inside an infinite cylinder parallel to the :math:`y`-axis.

   :param radius: Radius of the cylinder.
   :type radius: :term:`length`
   :param diameter: Diameter of the cylinder (instead of `radius`, but **not** both).
   :type diameter: :term:`length`
   :param center: Center of the cylinder in the :math:`xz`-plane. Default value is :math:`(0,0)`.
   :type center: :term:`point`

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : (x-x_0)^2 + (z-z_0)^2 \leq r^2\right\}

   Examples:

   >>> p0 = primitives.Cy(radius=1.0 * units.cm) # centered at (0,0)
   >>> p1 = primitives.Cy(diameter=2.0 * units.cm, center=(-1.0 * units.cm, 0.0))

.. py:function:: primitives.Cz(radius=r (OR diameter=d), center=(x0,y0))

   Region inside an infinite cylinder parallel to the :math:`z`-axis.

   :param radius: Radius of the cylinder.
   :type radius: :term:`length`
   :param diameter: Diameter of the cylinder (instead of `radius`, but **not** both).
   :type diameter: :term:`length`
   :param center: Center of the cylinder in the :math:`xy`-plane. Default value is :math:`(0,0)`.
   :type center: :term:`point`

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : (x-x_0)^2 + (y-y_0)^2 \leq r^2\right\}

   Examples:

   >>> p0 = primitives.Cz(radius=1.0 * units.cm) # centered at (0,0)
   >>> p1 = primitives.Cz(diameter=2.0 * units.cm, center=(-1.0 * units.cm, 0.0))

.. note::

   There is no arbitrary orientated cylinder primitive. Choose one of the above and add the appropriate
   `transformation <Transformations>`__.


.. py:function:: region_macros.pipe(diameter, width, center=(0, 0), alignment=primitives.ALIGNMENT.Z_AXIS)

   Cylindrical pipe, that is, region between two cylinders.

   :param diameter: Diameter of the **outer** (bounding) cylinder.
   :type diameter: :term:`length`
   :param width: Width of the pipe sleeve. The **inner** diameter is calculated as `diameter` less two times
      this value.
   :type width: :term:`length`
   :param center: Center of the pipe in the :math:`uv`-plane.
   :type center: :term:`point`
   :param alignment: Axial axis of the pipe.

   Parametric set definition:

   .. math::

      \left\{(u,v,a) : \left(\frac{d}{2} - w\right)^2 \leq (u-u_0)^2 + (v-v_0)^2 \leq \frac{d}{2}^2\right\},

   where :math:`d` is `diameter`, and :math:`w` is `width`.


   .. note::

      The pipe is unbounded in the axial direction.


.. py:function:: primitives.wedge(radius, arc_length, angle=0.0 * units.degrees, alignment=primitives.ALIGNMENT.Z_AXIS, center=(0, 0), no_cylindrical_bound=False)

   Region bounded by a circular segment in the :math:`uv`-plane.

   :param radius: Radius of the circle.
   :type radius: :term:`length`
   :param arc_length: Total length of the segment arc.
   :type arc_length: :term:`length`
   :param angle: Center angle of the arc.
   :param alignment: Axial axis of the wedge.
   :param center: Center of the circle in the :math:`uv`-plane.
   :type center: :term:`point`
   :param bool no_cylindrical_bound: Flag indicating if the circular bound should be included.


   In polar coordinates,

   .. math::

      u-u_0 = r \cos(\theta), v-v_0 = r \sin(\theta)

   the region is described by

   .. math::

     \left\{(r, \theta) : 0 \leq r \leq r_b,  |\theta - \theta_0| \leq \frac{l}{2 r_0}\right\},

   where :math:`r_0` is `radius`, :math:`\theta_0` is `angle`, abd :math:`l` is `arc_length`. The radial upper
   bound :math:`r_b` is `radius` if `no_cylindrical_bound` is False, :math:`\infty` otherwise.


Rectangular regions
-------------------

These are axially aligned regions that are bounded in one coordinate plane by a rectangle, and either infinite or
bounded in the perpendicular directions. The `alignment` parameter determines the axes
the primitive is parallel to (i.e. the infinite direction), and is set by choosing the appropriate value from the
`ALIGNMENT` container. It always defaults to `ALIGNMENT.Z_AXIS` if not specified.

In all cases :math:`(u,v)` is used to denote coordinates in the perpendicular plane:

    =======================  ===================================================
    Alignment                  Perpendicular Plane
    =======================  ===================================================
    `X_AXIS` (:math:`a=x`)   :math:`yz`-plane (:math:`u=y, v=z`)
    `Y_AXIS` (:math:`a=y`)   :math:`xz`-plane (:math:`u=x, v=z`)
    `Z_AXIS` (:math:`a=z`)   :math:`xy`-plane (:math:`u=x, v=y`)
    =======================  ===================================================

.. py:function:: primitives.SquareCylinder(radius=r, center=(u0,v0), alignment=ALIGNMENT.Z_AXIS)

   Region bounded by a square in the :math:`uv`-plane.

   :param radius: Half the length (or width) of the square.
   :type radius: :term:`length`
   :param center: Center of the square in the :math:`uv`-plane. Default is :math:`(0,0`).
   :type center: :term:`point`
   :param alignment: Axis parallel to infinite extension.

   Parametric set definition:

   .. math::

      \{(u,v,a) : |u-u_0| \leq r, |v-v_0| \leq r\}

   Examples:

   >>> p0 = primitives.SquareCylinder(radius=1.0 * units.cm, alignment=ALIGNMENT.X_AXIS)
   >>> p1 = primitives.SquareCylinder(radius=1.0 * units.cm, alignment=ALIGNMENT.Z_AXIS, center=(0.0, -1.0 * units.mm))

.. py:function:: primitives.RectangularCylinder(width=du, length=dv, center=(u0,v0), alignment=ALIGNMENT.Z_AXIS)

   Region bounded by a rectangle in the :math:`uv`-plane.

   :param width: Length of the square in the :math:`u` coordinate direction.
   :type width: :term:`length`
   :param length: Length of the square in the :math:`v` coordinate direction.
   :type length: :term:`length`
   :param center: Center of the square in the :math:`uv`-plane. Default is :math:`(0,0`).
   :type center: :term:`point`
   :param alignment: Axis parallel to infinite extension.

   Parametric set definition:

   .. math::

      \left\{(u,v,a) : |u-u_0| \leq \frac{du}{2}, |v-v_0| \leq \frac{dv}{2}\right\}

   Examples:

   >>> p0 = primitives.RectangularCylinder(width=1.0 * units.cm, length=2.0 * units.cm, alignment=ALIGNMENT.X_AXIS)

.. py:function:: primitives.Cube(radius=r, center=(x0,y0,z0))

   Region bounded by an axis aligned cube.

   :param radius: Half the length of any side.
   :type radius: :term:`length`
   :param center: Center of the cube. Default value in :math:`(0,0,0)`.
   :type center: :term:`point`

    Parametric set definition:

   .. math::

      \{(x,y,z) : |x-x_0| \leq r, |y-y_0| \leq r, |z-z_0| \leq r\}

   Examples:

   >>> p = primitives.Cube(radius=1.0 * units.cm, center=(0.0, 1.0 * units.mm, 0.0))

.. py:function:: primitives.Cuboid(x_min, x_max, y_min, y_max, z_min, z_max)

   Region bounded by a cuboid.

   :param x_min: Minimum :math:`x`-coordinate.
   :type x_min: :term:`length`
   :param x_max: Maximum :math:`x`-coordinate.
   :type x_max: :term:`length`
   :param y_min: Minimum :math:`y`-coordinate.
   :type y_min: :term:`length`
   :param y_max: Maximum :math:`y`-coordinate.
   :type y_max: :term:`length`
   :param z_min: Minimum :math:`z`-coordinate.
   :type z_min: :term:`length`
   :param z_max: Maximum :math:`z`-coordinate.
   :type z_max: :term:`length`

   Parametric set definition:

   .. math::

      \{(x,y,z) : x_{min} \leq x \leq x_{max}, y_{min} \leq y \leq y_{max}, z_{min} \leq z \leq z_{max}\}

   Examples:

   >>> p = primitives.Cuboid(-1.0 * units.cm, 0.0 * units.cm, 0.0 * units.cm, 1.0 * units.cm, -1.0 * units.cm, 1.0 * units.cm)

Conic regions
-------------

.. py:function:: primitives.Cone(scaling=t, focus=a0, center=(u0,v0), alignment=ALIGNMENT.Z_AXIS, side=0)

   Basic single or two sided cone.

   :param float scaling: Rate at which the cone decreases.
   :param focus: Cone focus point in the axial coordinate direction.
   :type focus: :term:`length`
   :param center: Center of cone in the :math:`uv`-plane. Default value in :math:`(0,0)`.
   :type center: :term:`point`
   :param alignment: Axis parallel to cone.
   :param int side: Flag indicating which side(s) of the cone should be included. Use 0 for both sides, -1 for
       the side below the `focus`, and +1 for the side above the `focus`. The default value is 0.


   Parametric set definition:

   .. math::

       \left\{(u, v, a): \sqrt{(u-u0)^2 + (v-v0)^2} \leq \sqrt{t} (a-a0)\right\}

.. py:function:: primitives.conic_section(large_circle_radius, small_circle_radius, height, center=(0,0,0), alignment=ALIGNMENT.Z_AXIS)

   Conic section between two circles.

   :param large_circle_radius: Radius of the larger (lower) circle.
   :type large_circle_radius: :term:`length`
   :param small_circle_radius: Radius of the small (upper) circle.
   :type small_circle_radius: :term:`length`
   :param height: Total height of the section.
   :type height: :term:`length`
   :param center: Center of mass of the section.
   :type center: :term:`point`
   :param alignment: Axis parallel to the height of the section.

   Example:

   >>> p = primitives.conic_section(2.0 * units.cm, 1.5 * units.cm, 2.0 * units.cm)
   >>> p.show()

   .. image:: images/conic-section.png
      :align: center
      :width: 250 px

.. py:function:: primitives.rectangular_cone(base_rectangle_width, base_rectangle_length, upper_rectangle_width, upper_rectangle_length, height, center=(0,0,0), alignment=ALIGNMENT.Z_AXIS)

   Pyramid like region connecting two rectangles.

   :param base_rectangle_width: Length along the :math:`u` coordinate of the base (lower) rectangle.
   :type base_rectangle_width: :term:`length`
   :param base_rectangle_length: Length along the :math:`v` coordinate of the base (lower) rectangle.
   :type base_rectangle_length: :term:`length`
   :param upper_rectangle_width: Length along the :math:`u` coordinate of the upper rectangle.
   :type upper_rectangle_width: :term:`length`
   :param upper_rectangle_length: Length along the :math:`v` coordinate of the upper rectangle.
   :type upper_rectangle_length: :term:`length`
   :param height: Distance between lower and upper rectangle.
   :type height: :term:`length`
   :param center: Center of mass of the entire region.
   :type center: :term:`point`
   :param alighnment: Axis parallel to the height of the pyramid.

   Examples:

   >>> p = primitives.rectangular_cone(5 * units.cm, 4 * units.cm, 3 * units.cm, 2 * units.cm, height=3 * units.cm)
   >>> p.show()

   .. image:: images/rectangular-cone.png
      :align: center
      :width: 250 px


Spherical regions
-----------------

.. py:function:: primitives.Sphere(radius=r, center=(x0,y0,z0))

   Three dimensional spherical region.

   :param radius: Radius of the sphere.
   :type radius: :term:`length`
   :param center: Center of the sphere. Default is :math:`(0,0,0)`.
   :type center: :term:`point`


   Parametric set definition:

   .. math::

      \left\{(x,y,z) : (x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2 \leq r^2\right\}

.. py:function:: primitives.Ellipsoid(small_radius, large_radius, center=(0, 0, 0), orientation=ALIGNMENT.Z_AXIS)

   Three dimensional region bounded by an ellipsoid.

   :param small_radius: Small radius of the ellipsoid (short axis).
   :type small_radius: :term:`length`
   :param large_radius: Large radius of the ellipsoid (long axis).
   :type large_radius: :term:`length`
   :param center: Center of the ellipsoid.
   :type center: :term:`point`
   :param orientation: Direction of the long axis, that is, where the scaling should take place. See the description
            below.

   The `orientation` parameter can have any of the following values:

        - A coordinate axis (i.e `ALIGNMENT.X_AXIS`, `ALIGNMENT.Y_AXIS` or `ALIGNMENT.Z_AXIS`) in which case the long
          axis will be aligned with this axis.
        - A :term:`string` specifying a coordinate plane ('xy', 'xz' or 'yz'). In this case, the short axis will
          align with the coordinate axis perpendicular to the specified plane.
        - An arbitrary :term:`point`, in which case the long axis will be aligned with this vector.


   .. note::

      Currently, an ellipsoid is not an independent primitive, but merely a transformed (scaled) version of
      :func:`primitives.Sphere`.


Polyhedral regions
------------------

.. py:function:: primitives.Hexagon(diameter=None, radius=None, inner_radius=None, orientation=ALIGNMENT.X_AXIS, center=(0, 0), alignment=ALIGNMENT.Z_AXIS)

   Create a region bounded by a hexagonal prism.

   :param diameter: Outer diameter of the hexagon (that is, all vertices are on this circle).
   :type diameter: :term:`length`
   :param outer_radius: Outer radius of the hexagon (that is, all vertices are on this circle).
   :type outer_radius: :term:`length`
   :param inner_radius: Inner radius of the hexagon (that is, all edges are tangent to this circle)
   :type inner_radius: :term:`length`
   :param orientation: On what axis a vertex will be placed. If `ALIGNMENT.X_AXIS`, a vertex will be placed
        on the :math:`u`-axis creating a :ref:`flat top <flat_top>` hexagon. Otherwise, if  `ALIGNMENT.Y_AXIS`, a
        vertex will be placed on the :math:`v`-axis creating a :ref:`pointy top <pointy_top>` hexagon.
   :param alignment: Axial alignment of the hexagonal region.
   :param center: Center of the region in the :math:`uv`-plane.

   .. note::

      Only **one** of the parameters `diameter`, `radius` or `inner_radius` should be specified.



.. py:function:: region_macros.extruded(points, height=1.0 * units.cm, direction=primitives.ALIGNMENT.Z_AXIS, include_top=True, include_bottom=True)

   Create a three dimensional region by extruding a set of co-planar point enclosing a **convex** polygon.

   :param points: List of co-planar points.
   :type points: :term:`list` (:term:`point`)
   :param height: Height of the extrusion.
   :type height: :term:`length`
   :param direction: Direction along which the points should be extruded. Either `ALIGNMENT.X_AXIS`, `ALIGNMENT.Y_AXIS`,
       or `ALIGNMENT.Z_AXIS` for extrusion in a coordinate direction, or an arbitrary :term:`point`.
   :param bool include_top: Flag indicating if the extrusion should be bounded from above. If `False`, the region will
       be infinite in the positive `alignment` direction.
   :param bool include_bottom: Flag indicating if the extrusion should be bounded from above. If `False`, the region will
       be infinite in the negative `alignment` direction.

   .. attention::

      1. The points must be ordered *clockwise* with respect to `direction` (left hand rule).
      2. The last point in the list will be connected to the first point to create a closed cycle.
      3. For the extrusion to be perpendicular, all `points` must be in the plane perpendicular to `direction`.
      4. If `direction` is a :term:`point`, the `height` parameter is ignored, and the height of the extrusion is
         equal to the magnitude of the `direction`.

   Example:

   >>> points = [(-1 * units.cm, 0), (1 * units.cm, 0), (0.5 * units.cm, 1 * units.cm), (-0.5 * units.cm, 1 * units.cm)]
   >>> points.reverse()     # correct orientation
   >>> p = region_macros.extruded(points, height=2* units.cm)
   >>> p.show()

   .. image:: images/extruded.png
      :align: center
      :width: 250 px

Strips
------

Basic semi-infinite regions bounded by axially aligned planes.

.. py:function:: region_macros.axial_strip(lower=None, upper=None)

   Infinite region bounded by two axial (:math:`z`-axes) planes.

   :param lower: Lower axial bound. If None, region will not be bounded from below.
   :type lower: :term:`length` or None
   :param upper: Upper axial bound. If None, region will not be bounded from above.
   :type upper: :term:`length` or None

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : l < z < u \right\}

   where :math:`l` is `lower` or :math:`-\infty` if `lower` is None, and :math:`u` is `upper` or :math:`+\infty` if
   `upper` is None.


.. py:function:: region_macros.x_strip(xmin=None, xmax=None)

   Infinite region bounded by two :math:`x`-axes planes.

   :param xmin: Lower bound. If None, region will not be bounded from below.
   :type xmin: :term:`length` or None
   :param xmax: Upper bound. If None, region will not be bounded from above.
   :type xmax: :term:`length` or None

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : l < x < u \right\}

   where :math:`l` is `xmin` or :math:`-\infty` if `xmin` is None, and :math:`u` is `xmax` or :math:`+\infty` if
   `xmax` is None.


.. py:function:: region_macros.y_strip(ymin=None, ymax=None)

   Infinite region bounded by two :math:`y`-axes planes.

   :param ymin: Lower bound. If None, region will not be bounded from below.
   :type ymin: :term:`length` or None
   :param ymax: Upper bound. If None, region will not be bounded from above.
   :type ymax: :term:`length` or None

   Parametric set definition:

   .. math::

      \left\{(x,y,z) : l < y < u \right\}

   where :math:`l` is `ymin` or :math:`-\infty` if `ymin` is None, and :math:`u` is `ymax` or :math:`+\infty` if
   `ymax` is None.

.. py:function:: region_macros.rectangle(xmin=None, xmax=None, ymin=None, ymax=None)

   Rectangular region in the radial :math:`xy`-plane. Unbounded in the axial direction.

   :param xmin: Minimum :math:`x`-coordinate. If None, the region will be unbounded on the left.
   :type xmin: :term:`length` or None
   :param xmax: Maximum :math:`x`-coordinate. If None, the region will be unbounded on the right.
   :type xmax: :term:`length` or None
   :param ymin: Minimum :math:`y`-coordinate. If None, the region will be unbounded on the bottom.
   :type ymin: :term:`length` or None
   :param ymax: Maximum :math:`y`-coordinate. If None, the region will be unbounded on the top.
   :type ymax: :term:`length` or None


   Parametric set definition:

   .. math::

      \left\{(x,y,z) : x_{min} < x < x_{max}, y_{min} < y < y_{max} \right\}

   where :math:`x_{min}`, :math:`y_{min}` is :math:`-\infty` if `xmin`, `ymin` is None respectively, and
   :math:`x_{max}`, :math:`y_{max}` is :math:`+\infty` if `xmax`, `ymax` is None respectively.


Other radial regions
--------------------

These regions are bounded by specified shapes in the :math:`xy` plane, but are infinite in the axial direction.

.. py:function:: region_macros.clipped_box(diameter, cut)

   Square in the :math:`xy`-plane with clipped corners.

   :param diameter: Total width of the square.
   :type diameter: :term:`length`
   :param cut: Piece that should be clipped from each side, so that the remaining edge has length
           `diameter` less two times `cut`.
   :type cut: :term:`length`

   .. note::

      The square is centered at :math:`x=0, y=0`.

   Example:

   >>> p = region_macros.clipped_box(10 * units.cm, 2.0 * units.cm)
   >>> p.show()

   .. image:: images/clipped-box.png
      :align: center
      :width: 250 px

.. py:function:: region_macros.square_cross(radius, width, center=(0, 0))

   Square cruciform region.

   :param radius: Distance from `center` to cross end.
   :type radius: :term:`length`
   :param width: Width of the cross pieces.
   :type width: :term:`length`
   :param center:  Center of the cross in the :math:`xy`-plane.
   :type center: :term:`point`

   Example:

   >>> p = region_macros.square_cross(5 * units.cm, 2.0 * units.cm)
   >>> p.show()

   .. image:: images/square-cross.png
      :align: center
      :width: 250 px

Trivial regions
---------------

.. py:function:: primitives.Space()

   Region representing the entire space.

.. py:function:: primitive.Empty()

   Region representing an empty space (null set).


Transformations
===============

Since most of the available primitives are aligned to the coordinate axis, transformations (rotations and
translations) must be added in order to capture the correct region. This is achieved with the
``add_transformation`` method, common to all regions (primitive or not). The parameter passed to this method
must be a valid affine transformation.

In it's most general from, and affine transformation is a :math:`4\times4` matrix, with the first :math:`3\times 3`
entries :math:`R_{ij}` specifying the rotation matrix, and the translation :math:`b_j` specified in column 4:

.. math::

   \begin{bmatrix}
    R_{11} & R_{12} & R_{13} & b_1 \\
    R_{21} & R_{22} & R_{23} & b_2 \\
    R_{31} & R_{32} & R_{33} & b_3 \\
    0 & 0 & 0 & 1 \\
   \end{bmatrix}

Affine transformations transform points :math:`r=(x,y,z)` to :math:`r'=(x',y',z')` as follows:

.. math::

    r' = R r + b.

Note that translations are applied **after** the rotation. The package allows the user full freedom in specifying
the affine transformation matrix, but the :mod:`core.geometry` module contains a number of convenience
functions for creating the most commonly used ones.

Basic translations
------------------


.. py:function:: geometry.translation(x=0.0, y=0.0, z=0.0)

   Creates a rotation free translation matrix.

   :param x: The :math:`x`-coordinate of the translation vector.
   :type x: :term:`length`
   :param y: The :math:`y`-coordinate of the translation vector.
   :type y: :term:`length`
   :param z: The :math:`z`-coordinate of the translation vector.
   :type z: :term:`length`

   Example:

   >>> p = primitives.Cx(radius=1.0 * units.cm)
   >>> p.add_transformation(geometry.translation(1.0 * units.cm, 2.0 * units.cm, 1.0 * units.cm))

Rotation around an arbitrary vector
-----------------------------------

.. py:function:: geometry.rotation_around_vector(angle, vector=(0, 0, 1))

   Create a pure rotation transformation, which rotates all points by a `angle` around `vector`.

   :param angle: Angle of rotation (in degrees or radians).
   :param vector: Either a three coordinate vector, or an axis (`ALIGNMENT.X_AXIS`, `ALIGNMENT.Y_AXIS` or
      `ALIGNMENT.Z_AXIS`).

   Example:

   >>> p = primitives.Cx(radius=1.0 * units.cm)
   >>> p.add_transformation(geometry.rotation_around_vector(33.0 * units.degrees, ALIGNMENT.Z_AXIS))


   .. note::

      The rotation is anchored at the origin :math:`x=0, y=0, z=0`.

   .. attention::

      It is important to use ``units.degrees`` when specifying angles in degrees. Otherwise the system will
      assume it is in radians!

Euler rotations
---------------

.. py:function:: geometry.euler_rotations(roll=0.0, pitch=0.0, yaw=0.0)

   Creates a rotation based on euler operations.

   :param roll: Roll angle (rotation around :math:`x`-axis).
   :param pith: Pitch angle (rotation around :math:`y`-axis).
   :param yaw: Yaw angle (rotation around :math:`z`-axis).


   Example:

   >>> p = primitives.Cx(radius=1.0 * units.cm)
   >>> p.add_transformation(geometry.euler_rotations(pitch=10.0 * units.degrees, yaw=45.0 * units.degrees))


Combining affine transformations
--------------------------------

Affine transformations are automatically combined when multiple transformations are added to a region. For
instance, the following will rotate and then translate a region ``p``:

.. code-block:: py

    p.add_transformation(geometry.rotation_around_vector(33.0 * units.degrees, [1,0,0]))
    p.add_transformation(geometry.translation(1.0 * units.cm, 2.0 * units.cm, 1.0 * units.cm))


.. attention::

   Affine transformations are, in general, non-commutative. Thus, the order in which transformations are added
   is important!


Regularizing set operations
===========================

Regularizing set operations (complement, intersection and union) are used to construct more complicated
regions out of primitives. The **CSG** package allows two notation styles when specifying set operations:

    - Set operation style: ``~`` for complement, ``&``  for intersections and ``|`` for unions.

    - MCNP style: ``+`` for complement, space for intersections and ``:`` for unions.

.. note::

   The MCNP style notation is provided mostly for people with years of experience with MCNP input decks. However, we
   recommend adapting the set operation notation, as it is more expressive, and the entire validation example set
   was constructed using this notation.

Complement
----------

The complement of a region denotes everything **not** in the interior of the region.

.. code-block:: py

    inside = primitives.Cylinder(radius=1.0 * units.cm)
    outside = ~inside

Or, equivalently, in the MCNP notation:

.. code-block:: py

    inside = primitives.Cylinder(radius=1.0 * units.cm)
    outside = +inside

Intersection
------------

The intersection between two regions ``r1`` and ``r2`` denotes everything that is in ``r1`` **and** ``r2``. For example,
the following describe the region inside a cube and outside a cylinder:

.. code-block:: py

   r1 = primitives.Cube(radius=2.0 * units.cm)
   r2 = primitives.Cylinder(radius=1.0 * units.cm)
   region = r1 & (~r2)      #reads: inside cube AND NOT inside cylinder
   region.show()

Alternatively, in the MCNP style notation, intersections is not specified by a symbol:

.. code-block:: py

    r1 = primitives.Cube(radius=2.0 * units.cm)
    r2 = primitives.Cylinder(radius=1.0 * units.cm)
    region = -r1 +r2        # reads: inside cube AND outside cylinder
    region.show()

.. image:: images/intersection.png
   :align: center
   :width: 250 px

.. attention::

   Although the ``+`` symbol is optional in MCNP cell cards, it cannot be omitted here!

Union
-----

The union of two regions ``r1`` and ``r2`` denotes everything that is in ``r1`` **or** ``r2``. The following example
illustrates how region *outside* a rectangle can be constructed as the union of half plane regions:

.. code-block:: py

   outside_rect = primitives.Px(-1.0 * units.cm) | ~primitives.Px(1.0 * units.cm)  \
                  | primitives.Py(-1.0 * units.cm) | ~primitives.Py(1.0 * units.cm)


Equivalently, in MCNP style notation:

.. code-block:: py

   outside_rect = -primitives.Px(-1.0 * units.cm) : +primitives.Px(1.0 * units.cm) \
                   : -primitives.Py(-1.0 * units.cm) : +primitives.Py(1.0 * units.cm)

Set operations on composite regions
-----------------------------------

The above set operations are not restricted to primitives, and can be applied to any region. For example:

>>> from csg import *
>>> r1 = primitives.Cube(radius=2.0 * units.cm) & (~primitives.Cylinder(radius=1.0 * units.cm))
>>> co = (-1.5 * units.cm, 0.0 * units.cm, 0.0 * units.cm)
>>> r2 = primitives.Cube(radius=2.0 * units.cm, center=co) & ~primitives.Cylinder(radius=1.0 * units.cm, center=co)
>>> r3 = r1 & ~r2
>>> r3.show()

.. image:: images/set-on-region.png
   :align: center
   :width: 250 px

Additional region functions
---------------------------

The following are some useful functions when working with regions:

.. py:function:: region.intersects(r1, r2)

   Check if two regions intersects.

   :param r1: First region (either a primitive or composite).
   :param r2: Second region (either a primitive or composite).
   :returns: True if the intersection of `r1` and `r2` has non-empty interior, False otherwise.

.. py:function:: region.volume(r)

   Computes the volume of th region.

   :param r: Region to calculate volume of. Either a primitive or region constructed through set operations.
   :returns: The volume of the region (a :term:`volume` quantity).

   .. note::

      Infinite regions will be clipped to the universe box, so the returned volume will be finite. Thus, a meaningful
      value can only be expected for bounded regions.

.. py:function:: region.decompose(r)

   Decompose a region into disjoint pieces. This function can be used to create simpler regions from a region
   constructed through a complicated sequence of set operations.

   :param r: Region to decompose.
   :returns: A list of mutually disjoint regions.


Repeated structures
===================

The **csg** package has no predefined set of repeated structures. However, the entire python machinery is at your
disposal. Using loops that update transformations, one can recreate any lattice type structure (and much more!)

The following example builds a complicated region by repeatedly adding rotated cylinders:

.. code-block:: py

   from csg import *

   axial = ~primitives.Pz(-2.0 * units.cm) & primitives.Pz(2.0 * units.cm)

   prim = primitives.Cylinder(radius=0.75 * units.cm, center=(0.0 * units.cm, 1.0 * units.cm)) & axial

   # Initiate with a copy of the first cylinder
   region = prim.clone()

   for i in range(0, 7):
       prim.add_transformation(geometry.rotation_around_vector(45 * units.degrees, [0, 0, 1]))
       region |= prim

   region.show()

.. image:: images/repeated.png
   :align: center
   :width: 250 px

Working with cells
==================

Cells form the building blocks of all assemblies. They are essentially regions (bounded or unbounded) in three
dimensional space, with additional data connecting them to the physical structure of an assembly.

.. py:function:: cell.Cell(structure, material=None, facility=None, bundle=None, description=None, part=None, outside=False)

   Create a physical cell.

   :param structure: Any region (either a primitive, or composite region constructed using set operations).
   :param material: Material that fills the region.
   :type material: :term:`material`
   :param str facility: Mark this cell as containing the specified facility. This means that the contents of this cell
       is controlled by the assembly.
   :param bundle: Indicate that the cell contains a burn bundle. This means that the contents of the cell is controlled
       by the specified burnable material manager.
   :type bundle: :term:`bundle_tag`
   :param str description: Short description of what the cell is modeling.
   :param str part: Assembly part this cell belongs to. Parts are used to group cells into real physical parts, and
       is required when using the CAD link when documenting models.
   :param bool outside: Flag indicating if the cell falls outside the region of interest. That is, cell with
       `outside` True is considered to lie outside the boundary of the problem domain.


   .. note::

      All these keyword parameters (**except** structure) are also attributes of the cell container itself, and can be
      assigned after construction, e.g

      >>> c = cell.Cell(my_region)
      >>> c.material = my_material

The following method is used to retrieve the underlying structure of the cell:

.. py:method:: cell.region()

   :returns: The underlying structure of the cell. Either a primitive or composite (made from CSG operations) region.

This method can be used to emulate the MCNP *#* operator. For example, assuming ``c1``  is an existing cell,

>>> r = ~c1.region()

gives the region not in ``c1``.

Cell operations
---------------

.. py:function:: cell.reflect(axis, *cells)

   Reflect a number of cells through a coordinate plane.

   :param int axis: Reflection axis. Use

            - `ALIGNMENT.X_AXES` for reflection through the :math:`yz`-plane,
            - `ALIGNMENT.Y_AXES` for reflection through the :math:`xz`-plane, and
            - `ALIGNMENT.Z_AXES` for reflection through the :math:`xy`-plane.

   :param cells: List of cells to reflect.

   :returns: A new list of reflected cells (equal to the number of input `cells`).

   This function is very useful when modeling symmetric components, and effectively reduces the number of cells that
   need to be defined by half.

   Example:

   >>> from csg import *
   >>> r = region_macros.rectangle(0.5 * units.mm, 1.5 * units.mm, -1.0 * units.mm, 1.0 * units.mm)
   >>> c1 = cell.Cell(r)
   >>> rc = cell.reflect(ALIGNMENT.X_AXIS, c1)
   >>> (c1 | rc[0]).show()

Composites
==========

The :mod:`csg.composites` and :mod:`csg.adapters` contain a number of structures which can create sets of cells. These,
can be viewed as a pre-defined collection of parts which can be used in your models. Since the composites are usually
defined by a large number parameters, they are used by first instantiating the data container, setting its parameters,
then calling the ``build()`` method to construct the cells.


.. py:class:: composites.RoundedSquare

   Square region with rounded (circular) corners.

   .. parameter:: alignment
      :type: :term:`integer`
      :default: ALIGNMENT.Z_AXIS

      Coordinate axis along which the :param:`height` of the cells extend. Either `ALIGNMENT.X_AXIS`,
      `ALIGNMENT.Y_AXIS` or `ALIGNMENT.Z_AXIS`.

   .. parameter:: center
      :type: :term:`point`
      :default: :math:`(0,0,0)`

      Center of the part.

   .. parameter:: height
      :type: :term:`length`
      :default: Required

      Height of the part along the axes defined by :param:`alignment`.

   .. parameter:: square_radius
      :type: :term:`length`
      :default: Required

      Total radius of the square (that is, half the width).

   .. parameter:: corner_radius
      :type: :term:`length`
      :default: Required

      Radius of curvature of the corners.

   .. py:method:::: build(material=None, part=None, **kwargs)

      Create the cells that form the part.

      :param material: Material that will fill all constructed cells.
      :type material: :term:`material`
      :param str part: Name of the part all cells belong to.
      :param kwargs: Any additional keyword value pairs accepted by the :func:`cell.Cell` construction.


   Example:

   >>> from csg import *
   >>> rs = composites.RoundedSquare()
   >>> rs.square_radius = 2.0 * units.cm
   >>> rs.corner_radius = 0.5 * units.cm
   >>> rs.height = 5.0 * units.cm
   >>> cells = rs.build()
   >>> cells.show()

   .. image:: images/rounded-square.png
      :align: center
      :width: 250 px

.. py:class:: composites.RoundedSquareTube

   Strip between two square regions with rounded (circular) corners. Accepts all the parameters defined in
   :class:`composites.RoundedSquare` as well as:

   .. parameter:: thickness
      :type: :term:`length`
      :default: Required

      Width of the tubular region.

   .. note::

      The parameters :param:`square_radius`, :param:`corner_radius` refers to the dimensions of the **outer** square.

   Example:

   >>> rs = composites.RoundedSquareTube()
   >>> rs.square_radius = 2.0 * units.cm
   >>> rs.corner_radius = 0.5 * units.cm
   >>> rs.height = 5.0 * units.cm
   >>> rs.thickness = 1.0 * units.mm
   >>> cells = rs.build()
   >>> cells.show()

   .. image:: images/rounded-square-tube.png
      :align: center
      :width: 250 px

.. py:class:: composites.RoundedSquareCutOut

   Region between a square and a square with rounded corners. Accepts all the parameters defined in
   :class:`composites.RoundedSquare` as well as:

   .. parameter:: outside_square_radius
      :type: :term:`length`
      :default: Required

      Half the width of the outer (boundary) square region.

   .. note::

      In this case, the parameters :param:`square_radius`, :param:`corner_radius` refers to the dimensions of the
      **inner** square.

   Example:

   >>> rs = composites.RoundedSquareCutOut()
   >>> rs.square_radius = 2.0 * units.cm
   >>> rs.corner_radius = 0.5 * units.cm
   >>> rs.height = 5.0 * units.cm
   >>> rs.outside_square_radius = 2.0 * units.cm + 1.0 * units.mm
   >>> cells = rs.build()
   >>> cells.show()

   .. image:: images/rounded-square-cut-out.png
      :align: center
      :width: 250 px

.. _pin_cell:

.. py:class:: composites.PinCell

   Two dimensional pin cell definition. This composites adds consecutive rings, creating a nested set of tubular
   cells.

   The composite accepts the following parameter:

   .. parameter:: fill_material
      :type: :term:`material`
      :default: Required

      Material that fills space outside the last ring.

   Consecutive rings are added using the following three methods:

   .. py:method:: add_cylindrical_shell(radius, material=None, bundle=None, facility=None, part=None):

      Adds a cylindrical layer.

      :param radius: Radius of the layer.
      :type radius: :term:`length`
      :param material: Material that fills the region between the cylinder with `radius` and the previously added layer.
      :type material: :term:`material`
      :param str facility: Facility that the region between the cylinder with `radius` and the previously added layer contain.
      :param bundle: Indicate that the current cell contains a burn bundle.
      :type bundle: :term:`bundle_tag`
      :param str part: Assembly part the current cell belongs to.

      .. note::

         If this is the first layer added, the region will be a complete cylinder.

   .. py:method:: add_square_shell(radius, material=None, bundle=None, facility=None, part=None):

      Adds a square layer.

      :param radius: Radius of the layer.
      :type radius: :term:`length`
      :param material: Material that fills the region between the square with `radius` and the previously added layer.
      :type material: :term:`material`
      :param str facility: Facility that the region between the square with `radius` and the previously added layer contain.
      :param bundle: Indicate that the current cell contains a burn bundle.
      :type bundle: :term:`bundle_tag`
      :param str part: Assembly part the current cell belongs to.

      .. note::

         If this is the first layer added, the region will be a complete square.

   .. py:method:: add_generic_shell(region, material=None, bundle=None, facility=None, part=None):

      Adds an arbitrarily shaped shell.

      :param region: Outer boundary of the shell.
      :type region: :term:`region`
      :param material: Material that fills the region between the boundary `region` and the previously added layer.
      :type material: :term:`material`
      :param str facility: Facility that the region between the boundary `region` and the previously added layer contain.
      :param bundle: Indicate that the current cell contains a burn bundle.
      :type bundle: :term:`bundle_tag`
      :param str part: Assembly part the current cell belongs to.

   .. attention::

      The order in which layers are added is important, as the first layer will be used to construct the inner cell,
      the second layer adds the tubular cell between it and the previous cell etc. In particular, the `radius` parameter
      of subsequent calls must be **increasing**.

   .. note::

      The object also allows a short hand method using the ``|`` operator. See the examples below.

   .. py:method:: build(center=(0,0), axial=primitives.Space(), radial=primitives.Space())

      Create all the cells defined by this composite.

      :param center: Center (in the :math:`xy`-plane) at which cells should be constructed.
      :type center: :term:`point`
      :param axial: Region that should be used to clip cells axially.
      :type axial: :term:`region`
      :param radial: Region that should be used to clip cells radially. This clip is **only** applied to the region
            **outside** the last layer.

      :returns: A list of cells.

   Examples:

   >>> from csg import *
   >>> from core import *
   >>> pc = composites.PinCell()
   >>> pc.add_cylindrical_shell(0.8 * units.cm, bundle=bundel_tags.fuel, part='Fuel')
   >>> from material_library.gasses import Helium
   >>> pc.add_cylindrical_shell(0.9 * units.cm, material=Helium())
   >>> from material_library.structural import Al6061
   >>> pc.add_cylindrical_shell(1.2 * units.cm, material=Al6061(), part='Cladding')
   >>> from material_library.moderators import BoratedWater
   >>> pc.fill_material = BoratedWater()

   Instead of using explicit function calls to add shells, the following equivalent short hand notation can also be used:

   >>> pc = PinCell() | (0.8 * units.cm, bundle_tags.fuel) | (0.9 * units.cm, Helium()) | (1.2 * units.cm, Al6061())

   To add a crate like structure, add the following to the above:

   >>> pc.add_square_shell(3 * units.cm, material=LightWater())
   >>> pc.fill_material = Al6061()


Adapters
========


.. py:class:: adapters.CylindricalAdapter

   Models a sequence of axially aligned, stacked cylinders, with conic sections connecting them.

    .. parameter:: alignment
      :type: :term:`integer`
      :default: ALIGNMENT.Z_AXIS

      Coordinate axis along which the cylinders extend. Either `ALIGNMENT.X_AXIS`,
      `ALIGNMENT.Y_AXIS` or `ALIGNMENT.Z_AXIS`.

    .. parameter:: center
       :type: :term:`point`
       :default: :math:`(0,0,0)`

       Center of the part.

    .. parameter:: structural_material
       :type: :term:`material`
       :default: Required

       Material that should be used to fill structural cells.

    .. parameter:: fill_material
       :type: :term:`material`
       :default: None

       Material that fills all other cells.

    .. parameter:: holes
       :type: :term:`list` (:term:`region`)
       :default: None

       Optional holes that should be punched into the structure of the adapter. This is either a single list
       of regions, whose interiors describe the hole (or cavity) structure that should be removed from **all**
       cylindrical segment, or a list equal to the number of cylindrical sections, giving the hole structures
       for each segment.

       .. attention::

          These regions are used exactly as specified, and are **not** transformed to any particular segment
          position.

       .. note::

          Holes are filled with the :param:`fill_material`.

    .. parameter:: cylinder_diameters
       :type: :term:`list` (:term:`length`)
       :default: Required

        List giving the outer diameter of each cylinder in the stack. The length of this list is also used
        to determine the total number of cylinders in the stack.

    .. parameter:: wall_thickness
       :type: :term:`list` (:term:`length`)
       :default: Required

        List giving the thickness of each cylinder in the stack, that is, the inner diameter is the outer diameter,
        minus two times this value.

        Must have the same number of entries as :param:`cylinder_diameters`.

    .. parameter:: ranges
       :type: :term:`list` (:term:`tuple`)
       :default: Required

       List giving the axial extent and positioning of each cylindrical segment in the stack. Entries are a tuple
       of two values giving the upper and lower bound respectively.

       Must have the same number of entries as :param:`cylinder_diameters`.

       .. attention::

          The intervals must be mutually disjoint, and either increasing or decreasing.

    .. py:method:: build(include_fill=True)

       Creates all the cells making up the structure of the adapter.

       :param bool include_fill: Add all non-structural cells. These cells fill be filled with material
                                 :param:`fill_material`.

       :returns: A list of cells that can be added to your component.

       Each segment will be connected with a conic structure, whose outer boundary connects the outer diameters of the
       two cylinders, and whose inner region connects the inner diameters. If there is no distance between subsequent
       sections, this connection piece will be missing.

       .. note::

          If `fill_material` is True, the method will also add all outside cells, effectively filling the entire
          strip between the minimum and maximum value in :param:`ranges`.

    Example:

    >>> from csg import *
    >>> from material_library.structural import Al6061
    >>> adp = adapters.CylindricalAdapter()
    >>> adp.cylinder_diameters = [2.0 * units.cm, 1.5 * units.cm]
    >>> adp.wall_thickness = [2.0 * units.mm, 1.0 * units.mm]
    >>> adp.ranges = [(0.0 * units.cm, -1.0 * units.cm), (-1.5 * units.cm, -3.0 * units.cm)]
    >>> p = adp.build()
    >>> p.show()

    .. image:: images/cylindrical-adapter.png
      :align: center
      :width: 250 px


    To punch a 4 mm hole through the top cylinder add:

    >>> h = primitives.Cx(radius=2.0 * units.mm, center=(0, -0.5 * units.cm))
    >>> adp.holes = [h, None]
    >>> p = adp.build()
    >>> p.show()

    .. image:: images/cylindrical-adapter-with-hole.png
      :align: center
      :width: 250 px


.. py:class:: adapters.RectangleToCircleAdapter

   Part that connects a rectangular shaped base to a cylinder, with an (optional) cylindrical nozzle piece at the
   end.

    .. parameter:: alignment
      :type: :term:`integer`
      :default: ALIGNMENT.Z_AXIS

      Coordinate axis along which the adapter extend. Either `ALIGNMENT.X_AXIS`,
      `ALIGNMENT.Y_AXIS` or `ALIGNMENT.Z_AXIS`.

    .. parameter:: center
       :type: :term:`point`
       :default: :math:`(0,0,0)`

       Center of the part.

       .. note::

          The axial coordinate is taken as the center of the main part, that is, the rectangular box **without** the
          nozzle.

    .. parameter:: height
       :type: :term:`length`
       :default: Required

       Total height of the box adapter. This **excludes** the nozzle piece.

    .. parameter:: structural_material
       :type: :term:`material`
       :default: Required

       Material that should be used to fill structural cells.

    .. parameter:: fill_material
       :type: :term:`material`
       :default: None

       Material that fills all other cells.

    .. parameter:: holes
       :type: :term:`list` (:term:`region`)
       :default: None

       Optional holes that should be punched into the structure of the adapter. This is  a single list
       of regions, whose interiors describe the hole (or cavity) structure that should be removed from the main
       adapter part.

       .. note::

          Holes are filled with the :param:`fill_material`.

    .. parameter:: outer_rectangle_width
       :type: :term:`length`
       :default: Required

       Total width (:math:`u` dimension) of the adapter's rectangular base.

    .. parameter:: outer_rectangle_length
       :type: :term:`length`
       :default: Required

       Total length (:math:`v` dimension) of the adapter's rectangular base.

    .. parameter:: inner_rectangle_width
       :type: :term:`length`
       :default: Required

       Width (:math:`u` dimension) of the adapter's rectangular base's inner region. Must be less than
       :param:`outer_rectangle_width`, so that the side wall thickness is the half the difference between these two
       values.

    .. parameter:: inner_rectangle_length
       :type: :term:`length`
       :default: Required

       Length (:math:`v` dimension) of the adapter's rectangular base's inner region. Must be less than
       :param:`outer_rectangle_length`, so that the side wall thickness is the half the difference between these two
       values.

    .. parameter:: target_outer_radius
       :type: :term:`length`
       :default: Required

       Outer radius of the target cylindrical structure.

    .. parameter:: target_inner_radius
       :type: :term:`length`
       :default: Required

       Inner radius of the target cylindrical structure.

    .. parameter:: nozzle_height
       :type: :term:`length`
       :default: 0.0

       Optional height of the cylindrical nozzle that will be added to the target cylindrical part of the
       adapter. If larger than 0.0, a cylindrical tube of this height with outer radius :param:`target_outer_radius`
       and inner radius :param:`target_inner_radius` will be added to this part.

    .. parameter:: nozzle_holes
       :type: :term:`list` (:term:`region`)
       :default: None

       List of holes that should be cut from the nozzle piece.

    .. py:method:: build(include_fill=True)

       Creates all the cells making up the structure of the adapter.

       :param bool include_fill: Add all non-structural cells. These cells fill be filled with material
                                 :param:`fill_material`.

       :returns: A list of cells that can be added to your component.

       .. note::

          If `fill_material` is True, the method will also add all outside cells, effectively filling the entire
          strip between `center.a - 0.5 * height` and `center.a + 0.5 * height + nozzle_height`.


    Examples:

    >>> from csg import *
    >>> adp = adapters.RectangleToCircleAdapter()
    >>> adp.outer_rectangle_width = 0.5 * (73.2 + 73.05) * units.mm
    >>> adp.outer_rectangle_length = 0.5 * (74.50 + 74.35) * units.mm
    >>> adp.inner_rectangle_width = 63.5 * units.mm
    >>> adp.inner_rectangle_length = 66 * units.mm
    >>> adp.target_outer_radius = 0.5 * 70.55 * units.mm
    >>> adp.target_inner_radius = 0.5 * 63.5 * units.mm
    >>> adp.height = 63.5 * units.mm + 19.25 * units.mm
    >>> from material_library.structural import Al6061
    >>> from material_library.moderators import H2O
    >>> adp.structural_material = Al6061()
    >>> adp.fill_material = H2O()
    >>> p = adp.build()
    >>> p.show()

    .. image:: images/square-to-cylinder.png
      :align: center
      :width: 250 px

    To add 5 cm of nozzle tubing:

    >>> adp.nozzle_height = 5 * units.cm
    >>> p = adp.build()
    >>> p.show()

    .. image:: images/square-to-cylinder-with-nozzle.png
      :align: center
      :width: 250 px