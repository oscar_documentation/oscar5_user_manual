.. _poison_curve:

Calculate Xenon Reactivity Curves
*********************************

.. _poison_curve_typical_use_cases:

Typical Use Cases
=================

.. _poison_curve_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager critical_case MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'projects'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --configuration <str>

   The configuration module that contains the model used in this script.

.. option:: --mco <str>

   Name (or path) of the model configuration archive that should be used in this script.

   .. attention::

      This option can not be specified when :option:`--configuration` is also specified.

.. option:: --time-stamp <str>

   Time point at which facility state should be recovered (see :attr:`app_param.time_stamp`). Must be a
   valid :term:`datetime`.

A typical input module has the following form:

.. code-block:: py

   import applications.critical_case as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   ccp = app.parameters()

   # Set some base application parameters
   cpp.time_stamp = '01-01-2020 00:00:00'

   # Set and modify the model
   cpp.model = model
   cpp.model.set_banks(control.percent_extracted(50))
   # etc

   # Application specific
   cpp.power = 1 * units.MW
   # etc

   if __name__ == '__main__':
    app.main(parameters=cpp)


.. _poison_curve_parameters:

Additional Input Parameters
===========================

In the following `ccp` denotes a `critical_case` parameter set, created as follows:

.. code-block:: py

   import applications.critical_cases as app

   cpp = app.parameters()

.. current_parameter_set:: cpp
   :synopsys: applications.critical_case.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`, as well as the following:

.. parameter:: power
   :type: :term:`power`
   :default: 1 W

   The (neutronic) power used to normalize flux levels.

.. _poison_curve_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`.

.. _poison_curve_typical_cli:

Typical command line usage
--------------------------

.. _poison_curve_output_tokens:

Output Tokens
=============

.. _poison_curve_examples:

Examples
========
