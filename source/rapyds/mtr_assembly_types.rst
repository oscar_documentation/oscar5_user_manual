.. role:: py(code)
    :language: python

Plate Type Fuel Assemblies
==========================

Collect all assemblies with plate type fuel. This includes typical irradiation targets using this fuel design.

Target plate holder
-------------------

Models an assembly used to hold a variable number of target plates. It only
constructs the grid plate structure, but also provides methods to load target plates into empty plate slots. The
assembly type is called :class:`LoadablePlateAssembly`, in the :mod:`core.fuel_assembly` module:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.LoadablePlateAssembly(name='MY_REACTOR_assembly_base_name')

       asm.number_of_plates = 5
       asm.pitches = 15 * units.mm
       # etc

.. current_parameter_set:: target_holder
   :synopsys: core.fuel_assembly.LoadablePlateAssembly

The macro has the following parameters:

.. parameter:: number_of_plates
   :type: :term:`integer`
   :default: Required

   The total number of plate slots.

.. parameter:: pitches
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

    The distance between consecutive plate centers. Either a single value, in which case the pitch is assumed
    to be constant, or a list of values, giving the distance between plates from the bottom or left side. In the
    latter case, list must have exactly :attr:`target_holder.number_of_plates` - 1 entries.

.. parameter:: grid_plate_pitch
   :type: :term:`length`
   :default: Required

    The inner distance between grid (side) plates, along the length of the target plates.

    .. note::

       Grid plates are the structure supporting the plates. They are also frequently referred to as side plates.

.. parameter:: orientation
   :type: :term:`orientation`
   :default: horizontal

    The plate orientation. If :class:`horizontal`, plates will be placed parallel to the :math:`x`-axis, while if
    :class:`vertical`, plates will be parallel to the :math:`y`-axis.

.. parameter:: center
   :type: :term:`point`
   :default: 0,0,0

    The center of the plate structure. This parameter is used to position the plates and plate slots. For example,
    the center of the first plate is,

    >>> center[j] - 0.5 * sum(pitches)

    where ``j`` is 1 if :attr:`target_holder.orientation` is ``horizontal`` and 0 if :attr:`target_holder.orientation`
    is ``vertical``.

    The :math:`z`-coordinate of this parameter also determines the axial center of the plates.


.. parameter:: slot_insert_depth
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Depth of the plate slot inserts in the grid plate. Either a single value, if all slots have the same depth, or
   a list of length :attr:`target_holder.number_of_plates`.

.. parameter:: slot_insert_width
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Width of the plate slot inserts in the grid plate. Either a single value, if all slots have the same width, or
   a list of length :attr:`target_holder.number_of_plates`.

.. parameter:: box
   :type: :term:`list` (:term:`length`) or :term:`region`

   Radial bounding region. When the grid is clipped with a simple rectangular region, this parameter can be specified
   as a list of four values, giving the :math:`x` and :math:`y` limits respectively. For example:

   >>> asm.box = [-4.05 * units.cm, 4.05 * units.cm, -4.05 * units.cm, 4.05 * units.cm]

   will clip all grid cells with an :math:`8.1 \times 8.1` square centered at the origin. Alternatively, any
   :term:`region` can be specified.

   .. attention::

      The :attr:`target_holder.box` is independent of :attr:`target_holder.center`. That is, the :math:`xy` center
      of this region is **not** necessarily the center of the assembly.

.. parameter:: axial_region
   :type: :term:`list` (:term:`length`) or :term:`region`

   Axial bounding region, used to clip grid cells. When the grid is bounded by a simple axial strip, this parameter
   can be specified as a list of two values, giving the :math:`z` bounds. For example,

   >>> asm.axial_region = [-40 * units.cm, 35 * units.cm]

   will clip all cells to the region :math:`-40 <= z <= 35`. Alternatively, any :term:`region` can be specified.

   .. attention::

      The :attr:`target_holder.axial_region` is independent of :attr:`target_holder.center`. That is, the :math:`z`
      center is **not** necessarily the center of this region.

.. parameter:: coolant_channels
   :type: :term:`list` (str)
   :default: None

   Specify which coolant channels should be added by the macro. This is a list of string entries (or ``None``), equal to
   the number of coolant channels (:attr:`target_holder.number_of_plates` - 1). The string value will be used to name the facility
   which fills the channel, while ``None`` is used to indicate a channel that should not be added. For example, if there
   are 4 plates,

   .. code-block:: py

      asm.coolant_channels = ['channel-0', None, 'channel-1']

   will add the channel between plate 0 and 1, and plate 2 and 3, but skip the central channel (between the plates 1
   and 2).

   By default, all channels are added with names :py:`"coolant-channel-i"`.

   .. note::

      Skipping channels are required when the space between plates will contain additional structure (e.g. control rods).
      Otherwise, intersecting cells will be present.

.. parameter:: grid_material
   :type: :term:`material`
   :default: Required

   Material used to fill grid plate (side plate) cells.

.. parameter:: moderator_material
   :type: :term:`material`
   :default: Required

   Material that fills the gap between plate slots and regions between grid slots and fueled plates. In the case of
   an empty plate holder, it is also used to fill the cells that can later be filled with target plates.

The following table summarizes the cells added by this assembly macro:

+--------------------------------------------------------------+------------------------------------------------------+
| Cells Created                                                | Material                                             |
+==============================================================+======================================================+
| Grid plates                                                  | :attr:`target_holder.grid_plate_material`            |
+--------------------------------------------------------------+------------------------------------------------------+
| Dummy plate cells                                            | :attr:`target_holder.moderator_material`             |
+--------------------------------------------------------------+------------------------------------------------------+
| Coolant channels                                             | :attr:`target_holder.moderator_material`             |
+--------------------------------------------------------------+------------------------------------------------------+
| Remaining cells in :attr:`target_holder.box` and             | :attr:`target_holder.moderator_material`             |
| :attr:`target_holder.axial_region`                           |                                                      |
+--------------------------------------------------------------+------------------------------------------------------+

.. attention::

   The macro fills the entire region defined by :attr:`target_holder.box` and :attr:`target_holder.axial_region`.
   Thus, any additional cells added to the assembly should not intersect this region.

The following loadable facilities are added:

+-------------------------------+---------------------------------------------------------+------------------------------------------+
| Facilities Added              |  Description                                            | Default State                            |
+===============================+=========================================================+==========================================+
| *water-channel-i* [#f1]_      | Coolant channel between plates :math:`i` and :math:`i+1`| :attr:`target_holder.moderator_material` |
|                               | Can be used to load detectors between fuel plates.      |                                          |
+-------------------------------+---------------------------------------------------------+------------------------------------------+
| *plate-slot-i*                | Slot for target plate :math:`i`.                        | :attr:`target_holder.moderator_material` |
+-------------------------------+---------------------------------------------------------+------------------------------------------+

Targets can also be loaded using the ``[ ]`` method. For example, to load a target into slot 0:

.. code-block:: py

   asm[0] = assemblies.MY_REACTOR_target_plate(name='TG1')

Target plate
------------

Single plate assembly, which can be loaded into a `Target plate holder`_. The object :class:`TargetPlate` is
defined in the :mod:`core.fuel_assembly` module:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.TargetPlate(name='MY_REACTOR_target_plate_type')


       asm.meat_width = 0.45 * units.mm
       # etc

       asm.construct_bundle()

.. current_parameter_set:: target_plate
   :synopsys: core.fuel_assembly.TargetPlate

Apart from the basic parameters described in :ref:`Generic assembly`, the following is used the define the plate structure:

.. parameter:: meat_width
   :type: :term:`length`
   :default: Required

   Width of the active fueled region.

   .. note::

      Since the orientation is fixed, with the plate extending along the :math:`x`-axis, this is the :math:`y` dimension
      of the meat region.

.. parameter:: meat_length
   :type: :term:`length`
   :default: Required

   Length of the active fueled region.

   .. note::

      Since the orientation is fixed, with the plate extending along the :math:`x`-axis, this is the :math:`x` dimension
      of the meat region.

.. parameter:: meat_height
   :type: :term:`length`
   :default: Required

   Axial (:math:`z`-dimension) of the active region.

.. parameter:: plate_width
   :type: :term:`length`
   :default: Required

   Width of the total plate. This is equal to :attr:`target_plate.meat_width` plus two times the cladding thickness.

   .. note::

      Since the orientation is fixed, with the plate extending along the :math:`x`-axis, this is the :math:`y` dimension
      of the plate.

.. parameter:: plate_length
   :type: :term:`length`
   :default: Required

   Length of the total plate.

   .. note::

      Since the orientation is fixed, with the plate extending along the :math:`x`-axis, this is the :math:`x` dimension
      of the plate.

.. parameter:: clad_material
   :type: :term:`material`
   :default: Required

   Material used in the plate cladding.

.. parameter:: fuel_material
   :type: :term:`material`
   :default: Required

   Active material used in the meat region.

.. parameter:: moderator_material
   :type: :term:`material`
   :default: None

   Material used to surround the plate.

   .. note::

      If not specified, there will be no cells added outside the plate, and it is the user's responsibility to add the
      missing cells.


Plate fuel assembly
-------------------

Models standard plate type (MTR) fuel assemblies. The :class:`PlateAssembly` class extends the `Target plate holder`_
macro by adding all the fuel plates. The macro is also defined in the :mod:`core.fuel_assembly` module:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.PlateAssembly(name='MY_REACTOR_assembly_base_name')

       asm.number_of_plates = 5
       asm.pitches = 15 * units.mm
       # etc

       asm.construct_bundle()

It has all the parameters listed in `Target plate holder`_, as well as the following:

.. current_parameter_set:: plate_asm
   :synopsys: core.fuel_assembly.PlateAssembly

.. parameter:: meat_width
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Width of the active fueled region. Either a single value if all plates have the same active width, or a list
   of values giving the width per plate. In the latter case, the list must have length equal to
   :attr:`target_holder.number_of_plates`.

   .. note::

      The width is the dimension perpendicular to the :attr:`target_holder.orientation`. Thus, if the orientation is horizontal,
      the width is the :math:`y` dimension of the plate, and the :math:`x` dimension if the orientation is vertical.

.. parameter:: meat_length
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Length of the active fueled region. Either a single value if all plates have the same active length, or a list
   of values giving the width per plate. In the latter case, the list must have length equal to
   :attr:`target_holder.number_of_plates`.

   .. note::

      The length is the dimension parallel to the :attr:`target_holder.orientation`. Thus, if the orientation is horizontal,
      the length is the :math:`x` dimension of the plate, and the :math:`y` dimension if the orientation is vertical.

.. parameter:: meat_height
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Height (:math:`z` dimension) of the active fueled region. Either a single value if all plates have the same active
   height, or a list of values giving the height per plate. In the latter case, the list must have length equal to
   :attr:`target_holder.number_of_plates`.

   .. note::

      The maximum :attr:`plate_asm.meat_height` determines the assembly's active height.

.. parameter:: plate_width
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Total width of the fuel plate. This includes the meat width, and cladding on **both** sides. Either a single value if
   all plates have the same width, or a list of values giving the width per plate.

   .. attention::

      The plate width must be less than or equal to the :attr:`target_holder.slot_insert_width`.

.. parameter:: plate_length
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Total length of the fuel plate. This includes the meat length, and cladding on **both** sides. Either a single value
   if all plates have the same length, or a list of values giving the length per plate.

   .. attention::

      The plate length must be less than or equal to :attr:`target_holder.grid_plate_pitch` + 2 :math:`\times`
      :attr:`target_holder.slot_insert_depth`.

.. parameter:: plate_height
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Total height of the fuel plate. This includes the meat height, and cladding on **both** sides. Either a single value
   if all plates have the same height, or a list of values giving the height per plate.

.. parameter:: clad_material
   :type: :term:`material`
   :default: Required

    Material used for plate fuel cladding.

.. parameter:: axially_fill_channels
   :type: :term:`bool`
   :default: True

   Flag indicating if the cells above the plates should be added. These cell will fill everything defined by
   :attr:`target_holder.axial_region`.

   .. attention::

      This should be set to False if there are structural components (e.g. adapters), that will intersect with this
      region.

These parameters are illustrated below in `radial <plate_assembly_radial>`_ and `axial <plate_assembly_axial>`_
schematics.

.. _plate_assembly_radial:

.. figure:: images/plate_fuel_assembly.svg
   :align: center

   Radial schematic of :class:`PlateAssembly` structure.

.. _plate_assembly_axial:

.. figure:: images/plate_fuel_assembly_axial.svg
   :align: center

   Axial schematic of :class:`PlateAssembly` structure. In the above, the outside plate's :attr:`plate_asm.plate_height`
   is longer than interior plates. This is typical for boxed assemblies.

The following table summarizes the cells added by this assembly macro:

+--------------------------------------------------------------+------------------------------------------------------+
| Cells Created                                                | Material                                             |
+==============================================================+======================================================+
| Grid (side) plates                                           | :attr:`target_holder.grid_plate_material`            |
+--------------------------------------------------------------+------------------------------------------------------+
| Fuel plate cladding                                          | :attr:`plate_asm.clad_material`                      |
+--------------------------------------------------------------+------------------------------------------------------+
| Active fuel meat                                             | Contains ``fuel_bundle`` depletion mesh              |
+--------------------------------------------------------------+------------------------------------------------------+
| Coolant channels [#f1]_                                      | :attr:`target_holder.moderator_material`             |
+--------------------------------------------------------------+------------------------------------------------------+
| Remaining cells in :attr:`target_holder.box` and             | :attr:`target_holder.moderator_material`             |
| :attr:`target_holder.axial_region`                           |                                                      |
+--------------------------------------------------------------+------------------------------------------------------+

.. attention::

   If :attr:`plate_asm.axially_fill_channels` is True, the macro fills the entire region defined by
   :attr:`target_holder.box` and :attr:`target_holder.axial_region`. Thus, any additional cells added to the assembly
   should not intersect this region.

   Otherwise, if :attr:`plate_asm.axially_fill_channels` is False, this will only create the grid (side plate), plate, and
   water channels [#f1]_ cells.

The following loadable facilities are added:

+-------------------------------+---------------------------------------------------------+-----------------------------------------+
| Facilities Added              |  Description                                            | Default State                           |
+===============================+=========================================================+=========================================+
| *water-channel-i* [#f1]_      | Coolant channel between plates :math:`i` and :math:`i+1`| :attr:`target_holder.moderator_material`|
|                               | Can be used to load detectors between fuel plates.      |                                         |
+-------------------------------+---------------------------------------------------------+-----------------------------------------+

.. [#f1] This depends on whether the :attr:`target_holder.coolant_channel` parameter was modified.

.. attention::

   Remember to set the initial material for the depletion mesh in the build module:

   .. code-block:: py

      asm.fuel_bundle().depletion_mesh.initial_material_distribution = materials.my_fuel()

   See `Depletion meshes for plate type assemblies`_ for more details.

.. _mtr_follower:

Follower type plate assembly
----------------------------

Models standard plate type (MTR) fuel follower assembly. The :class:`FollowerPlateAssembly` macro is also defined in the
:mod:`core.fuel_assembly` module:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.FollowerPlateAssembly(name='MY_REACTOR_assembly_base_name')

       # plate bundle parameters
       asm.number_of_plates = 5
       asm.pitches = 15 * units.mm
       # etc

       # control parameters
       asm.base_position = control.fully_extracted()
       asm.travel_direction = control.up()
       asm.total_travel_distance = 74.79 * units.cm

This macro simply combines the `Plate fuel assembly`_ and :ref:`Control assembly` types. The macro behaves exactly the
same way as `Plate fuel assembly`_ (adds the same cells, etc.).

.. attention::

   This macro does not add **any** structure related to the absorber part of the assembly. These cells should be added
   manually.

.. _mtr_with_ba:

Plate assembly with burnable absorbers
--------------------------------------

Models standard plate type (MTR) fuel follower assembly, with burnable absorbers embedded in the side plates. The
:class:`PlateAssemblyWithBurnableAbsorbers` macro is also defined in the :mod:`core.fuel_bundle` module:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.PlateAssemblyWithBurnableAbsorbers(name='MY_REACTOR_assembly_base_name')

       # plate bundle parameters
       asm.number_of_plates = 5
       asm.pitches = 15 * units.mm
       # etc

       # new slot parameters
       asm.ba_groove_width = 2 * units.mm
       asm.ba_groove_depth = 2 * units.mm
       # etc

It adds the following parameters to the `Plate fuel assembly`_  macro:

.. current_parameter_set:: plate_asm_with_ba
   :synopsys: core.fuel_assembly.PlateAssemblyWithBurnableAbsorber

.. parameter:: ba_groove_width
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Width of the slot in the grid plate that will house the absorber. Either a single value, if the grooves all have
   the same width, or a list of values.

.. parameter:: ba_groove_depth
   :type: :term:`length` or :term:`list` (:term:`length`)
   :default: Required

   Depth of the slot in the grid plate that will house the absorber. Either a single value, if the grooves all have
   the same depth, or a list of values.

.. parameter:: slot_fill
   :type: string or :term:`material`
   :default: "moderator_material"

   Material that should be used to fill the groove slots outside the absorber material. It can be any :term:`material`
   instance, or a string referring to one of the materials already used in the macro, that is:

    - *grid_material*,
    - *clad_material*, or
    - *moderator_material*.

.. parameter:: ba_loading
   :type: :term:`list`
   :default: Required

   List describing which plate positions have additional absorber slots, and what the structure of the burnable absorbers
   are. This takes the form of a list, with :attr:`target_holder.number_of_plates` entries. Each entry is again a list
   (or :term:`tuple`) of two entries, giving the absorber structure that should be loaded into the left and right plate
   respectively. For example, assuming there are 21 plates, the following will load wires into every second
   plate slot:

   .. code-block:: py

      ba = fuel_assembly.WireBurnableAbsorber()
      ba.length = 308 * units.mm
      ba.diameter = 0.493 * units.mm
      ba.axial_seating = (-0.5 * 615 + 123 + 0.5 * 308) * units.mm


      fa.ba_loading = \
      [[_ , _ ],
       [ba, ba],
       [_ , _ ],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _],
       [ba, ba],
       [_,   _]]

   In the above, the :class:`WireBurnableAbsorber` is a predefined absorber structure. It has the following parameters:

   .. current_parameter_set:: wire_ba
      :synopsys: core.fuel_assembly.WireBurnableAbsorber

   .. parameter:: diameter
      :type: :term:`length`
      :default: Required

      The wire diameter.

   .. parameter:: length
      :type: :term:`length`
      :default: Required

      The axial length (:math:`z` dimension) of the wire.

   .. parameter:: axial_seating
      :type: :term:`length`
      :default: 0.0

      Axial center of wire (relative to the fuel assembly).

   The :mod:`fuel_assembly` module also contains a :class:`CladdedBurnableWireAbsorber` definition, which models a
   cladded wire. In addition to the :class:`WireBurnableAbsorber` parameters, it is defined by:

   .. current_parameter_set:: cladded_wire_ba
      :synopsys: core.fuel_assembly.CladdedBurnableWireAbsorber

   .. parameter:: clad_thickness
      :type: :term:`length`
      :default: Required

      Radial thickness of the clad surrounding the wire. The total diameter of the wire plus clad is therefore
      :attr:`wire_ba.diameter` plus two times :attr:`cladded_wire_ba.clad_thickness`.


   .. parameter:: clad_axial_thickness
      :type: :term:`length` or :term:`list` (:term:`length`)
      :default: 0.0

      Axial length of wire. Either a single value, if the bottom and top cladding is the same, or two values, giving
      the bottom and top value respectively.

   .. parameter:: clad_material
      :type: :term:`material`
      :default: Required

      Material used to clad the wire.

The structure defined by this macro is illustrated below:

.. _plate_assembly_with_wires:

.. figure:: images/plate-assembly-with-wires.svg
   :align: center

   Radial schematic of :class:`PlateAssemblyWithBurnableAbsorbers` structure. Only the new parameters are illustrated. All
   other parameters are as shown in Fig `plate_assembly_radial`_. Here, only every second plate has a slot for a
   burnable absorber.

Curved plate assembly
---------------------

Under construction!


Depletion meshes for plate type assemblies
------------------------------------------

All assembly types adds a ``fuel_bundle``, which manages the distribution of burnable materials among the fuel plates.
Each plate is counted as a separate fueled region (or :ref:`primitive <What are fueled primitives?>`), indexed from 0
to :attr:`target_holder.number_of_plates` - 1. Grouping the plates into :ref:`bundles <Grouping primitives into bundles>` is
accomplished by assigning a group index to each plate. For example, if there are 19 plates,

.. code-block:: py

   asm.burn_bundle[bundle_tags.fuel].depletion_mesh.bundles = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]

will produce 19 bundles with each plate depleted separately. Setting,

.. code-block:: py

    asm.burn_bundle[bundle_tags.fuel].depletion_mesh.bundles = [0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 0, 0, 0]

will create 3 different bundles, with

     - the first bundle containing the first three, and last three plates,
     - the second bundle contains plates 4, 5, 6 and 14, 15 and 16,
     - the third bundle contains all the remaining central plates.


The :ref:`radial segments <Radial meshing>`
parameter of the fuel bundle's depletion mesh divides the plates along its length. Currently, no subdivision in the
width of the plate is performed.

The :ref:`axial mesh <Axial meshing>` simply divides the plates along their :attr:`plate_asm.meat_height`.

The default depletion mesh bundles all plates together, with no radial and axial sub-division. This can be customized
later, depending on the application, by :ref:`Customizing the Depletion Mesh`.

When burnable absorber wires are present (e.g `PlateAssemblyWithBurnableAbsorber <mtr_with_ba>`_), the depletion mesh
for these structures can also be customized. In this case, the wires are the basic
:ref:`primitives <What are fueled primitives?>`, and they are :ref:`bundled <Grouping primitives into bundles>` by
specifying a two dimensional grid, with an index for each wire. Each row in the grid will have exactly two entries,
one for each side plate.

For example, suppose there are 5 plates with a wire embedded on each side. Then,

.. code-block:: py

   asm.burn_bundle[bundle_tags.ba].depletion_mesh.bundles = \
     [[0, 1],
      [2, 3],
      [4, 5],
      [6, 7],
      [8, 9]]

will burn all wires seperately (total of 10 bundles), while

.. code-block:: py

   asm.burn_bundle[bundle_tags.ba].depletion_mesh.bundles = \
     [[0, 1],
      [0, 1],
      [0, 1],
      [0, 1],
      [0, 1]]

will group all wires on each side together (only two bundles).

In this case, the :ref:`radial segments <Radial meshing>`
parameter of the fuel bundle's depletion mesh divides the wires into radial rings. For example, if there are two bundles,

.. code-block:: py

   asm.burn_bundle[bundle_tags.ba].depletion_mesh.segments = \
    [[0.5, 0.5], [0.4, 0.3, 0.3]]

will divide the first bundle wires into two rings with the same area, and the second bundle wires into three rings, with
the inner most (central) zone having 40 percent of the total area, and the next two rings 30 percent each.

Finally, as with the plate region, the :ref:`axial mesh <Axial meshing>` divides the wires along their length.

Examples
--------

This section gives complete build script examples for some of the assembly types described in this chapter.

Target plate holder assembly build script
+++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/plate_holder.py

Basic plate type assembly build script
++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/basic_mtr.py

Plate assembly with burnable absorbers build script
+++++++++++++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/ba_mtr.py

Plate assembly with intra assembly control
++++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/mtr_intra.py




