.. role:: py(code)
   :language: py


Application Output
******************

.. _app_result_file:

Result files
============

All applications store their output in application specific binary result archives, with extension `.res`, and name
usually the :param:`parameters.project_name`. These output
files are usually stored within the :param:`parameters.working_directory` under the :param:`parameters.target_code`
sub directory. They are simply serialized forms of the applications :py:`core.result.ResultsSummary` instance.

The :py:`utilities.load_file` utility can be used to open and explore these result files, for example:

>>> from core import *
>>> res = utilities.load_file('FULL_PATH_TO_ROOT/WORKING_DIRECTORY/TARGET_CODE/project_name.res')

Since these objects are considered part of the low level interface, they are only fully described in the developers
manual, but some online help is available when using

>>> help(res)

where :py:`res` is the result object loaded earlier. Result :ref:`tokens <Using output tokens>`, which is described next,
provides a cleaner interface to the application's results archive.

.. _app_output_tokens:

Using output tokens
===================

These are special, application specific objects that:

    1. Customize how results are extracted from the result archive, and provide a more user friendly interface to these
       results.

    2. Is used as a general placeholder for the results, in that it can be used as a variable (in any expression), even
       before the result is known.

    3. Set the required input variables and flags required by the application in order for the result to be available.

The last two properties play an important role in applications which dynamically generate sub applications based on
output requirements (e.g :ref:`reload <reload_application>`), and the document generation utility :ref:`docgen <docgen>`.

Using a token to extract results
--------------------------------

Each token has a :py:`get` method, which can be used to retrieve the requested output from the result archive. For
example,

.. code-block:: py

   import applications.critical_case as app
   from core import *

   res = utilities.load_file('FULL_PATH_TO_ROOT/WORKING_DIRECTORY/TARGET_CODE/project_name.res')
   # Extract k_eff using the multiplication_factor token
   k_eff = app.multiplication_factor.get(res)
   # Extract power fraction map

Changing the underlying application parameters
----------------------------------------------

For application that dynamically generate sub-applications based on requested output tokens (such as
:ref:`reload <reload_application>`), the behavior of the generated application can be customized by passing some
additional parameters during the token construction:

  1. The `parameters` keyword, set to a :term:`dict` object, mapping standard
     :ref:`application parameter names <general_application_parameters_summary>` to their requested values. For example,

     .. code-block:: py

        import applications.critical_case as app

        tkn = app.multiplication_factor(parameters={'target_mode': 'SERPENT', 'particles': 16000})

     will set the :attr:`app_param.target_mode` and :attr:`app_param.particles` parameters of the
     generated :ref:`critical_case <critical_case>` application.

     Parameter names can be nested, so that any sub-parameter can be set as well. For example,

     .. code-block:: py

        import applications.critical_case as app
        from core import *

        tkn = app.multiplication_factor(parameters={'target_mode': 'SERPENT', 'particles': 16000, 'model.state': state_param.fuel(50 * units.degC)})

     will also modify the :attr:`model.state`.

  2. The `setup` keyword, which can be used to pass a custom function (or function object) to perform more complex
     modifications of the generated application. The function passed must accept a single argument, which is the
     parameter set of the target application. For example, the same results as above can be achieved using

     .. code-block:: py

        import applications.critical_case as app
        from core import *

        def func(param):

            param.target_mode = 'SERPENT'
            param.particles = 16000
            param.model.state[state_param.fuel] = 50 * units.degC

        tkn = app.multiplication_factor(setup=func)







