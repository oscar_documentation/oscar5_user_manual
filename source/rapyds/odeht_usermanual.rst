De-Homogenization Tool (ODEHT) User Manual
******************************************

Introduction
============

The de-homogenization tool is primarily used to transfer homogenized material compositions to heterogeneous component
definitions used in Monte Carlo codes such as MCNP. This requires correcting the density form a nodal value to the
correct volume of fuel primitives (e.g plates and pins), as well as compensating for isotopes not explicitly tracked by
the core simulator. The former requires an existing heterogeneous description of relevant assemblies, while the latter
requires a detailed, burn-up dependent isotopic inventory.

Requirements
============

Assembly models
---------------

For each fueled (or loadable) assembly tracked by the core simulator, a heterogeneous specification is required. The
specification should, at a minimum, describe the structure and (initial) material composition of burnable components.
Details on defining assembly and fueled bundles can be found in the component construction manual.


Exposure data base
------------------

To estimate detailed isotopic inventories at various burnup states, a simplified burnup calculation must be performed
for all relevant fueled assembly models. These calculations are usually performed in in simplified
core environments, and, since only average material compositions are extracted, in two dimensions. The
:ref:`exposure application<Creation of burnup dependent material libraries>` is designed specifically for this task,
and will store the material library directly in the assembly archive, so that no additional data passing is required.

.. note::

    The exposure data base only contains a single material composition for each burnable bundle type. Thus, even if a
    detailed spatial burnup shape is generated, it will be averaged into a single material.

Defining the basic core configuration
=====================================

The defining core configurations manual gives a detailed description on building full core models. For the purpose
of the number density transfer application, configuring the automatic assembly history import functionality is the
most important, and we review it here.

To facilitate the  automatic construction of assemblies instances not present in the system (code independent)
inventory, the following data is required:

    - Definition and configuration of loadable assembly types.
    - Location of loadable assembly types in the core.
    - Define a history importer, which is responsible for setting the material distribution of a named assembly. That
      is, the history importer maps input burnup data to the burnable structures within the assembly.

First, create a module within the ``configuration`` package of your reactor using the ``create_module`` utility:

.. code-block:: console

    $oscar5 MY_REACTOR.create_module configuration base \
    --description 'Base configuration for ODEHT application'

Will create a ``base.py`` module in the ``configuration`` directory.

.. note::

    You do no need to define a configuration specifically for the transfer application. Any existing configuration with
    a valid history importer can be used.

Specify loadable assembly types
-------------------------------

Loadable assembly types are specified by registering them with the ``inventory_manager`` component of the ``model``:

.. code-block:: py

    from ..model import assemblies
    f1 = model.inventory_manager.add_loadable_assembly(assemblies.MY_REACTOR_fuel_type_1)
    f2 = model.inventory_manager.add_loadable_assembly(assemblies.MY_REACTOR_fuel_type_2)

Here *MY_REACTOR_fuel_type_1* and *MY_REACTOR_fuel_type_2* are assemblies types.

Static compositions are used to freeze certain isotopes at specified densities, or simply add them to the imported
mixtures if they are not tracked by the core simulator. A typical example are the non burnable components
of a fuel alloy. For instance, supposing that our fuel assemblies contains an Uranium Silicide fuel alloy, then

.. code-block:: py

    from ..model import materials
    leu = materials.fresh_leu()

    f1.static_fuel_alloy_composition = {
        'Si-Nat': leu['Si-Nat'],
        'Al-27' : leu['Al-27']
    }

    f2.static_fuel_alloy_composition = {
        'Si-Nat': leu['Si-Nat'],
        'Al-27' : leu['Al-27']
    }

will ensure that these isotopes are imported at their initial (fresh) densities. You can add any isotope, even if it
does not appear in the original material specification. The format is simply a python :term:`dict` object with isotope
names as keys and number densities as values. For instance, to add a fixed amount of B-10:

.. code-block:: py

    f1.static_fuel_alloy_composition['B-10'] = 1.0E-6 * units.number_density

Specify loadable assembly type map
----------------------------------

This defines were in the core the different assembly types will be loaded. It is captured by the
``inventory_manager.load_map`` variable which accepts any :term:`labeledgrid`. For instance:

.. code-block:: py

    model.inventory_manager.load_map = \
    [[_  , 'A', 'B', 'C', 'D'],
     ['1',   _,  f1,  f1,   _],
     ['2',  f1,  f2,  f2,  f1],
     ['3',   _,  f1,  f1,   _]]

.. attention::

   The ``model.inventory_manager.load_map`` should have the same size and labels as ``model.load_map`` and
   ``model.core_map``.

Specify a history importer
--------------------------

The :py:mod:`inventory.inventory_management` module contains a number of pre-defined history importers, and
also provides a framework to implement custom importers. In addition, importers from current and past OSCAR versions are
available in the :py:mod:`extensions.oscar_history_importers` module.

For instance, to import from OSCAR-4 MGRAC history files:

.. code-block:: py

    from extensions.oscar_history_importers import OSCAR4HistoryImporter

    model.inventory_manager.history_importer = OSCAR4HistoryImporter()

An overview of the available parameters for each importer is given :ref:`below<Configuring the history importer>`. Some
importer parameters are suitable to set within the base configuration, while other (dynamic) parameters should
be set within the input module itself.

Complete the core configuration by specifying a core map
--------------------------------------------------------

For the system to produce a valid model, a core map is required. In an odeht specific configuration, only loadable
assemblies are important, and all other positions can be kept empty:

.. code-block:: py

    model.core_map = \
    [[_  , 'A', 'B', 'C', 'D'],
     ['1',   _,  _p,  _p,   _],
     ['2',  _p,  _p,  _p,  _p],
     ['3',   _,  _p,  _p,   _]]


The ``_p`` place holder token is used to indicate that these positions are filled from the ``model.load_map``.

Finally, set the core pitches, for instance:

.. code-block:: py

    model.core_pitches = (7.71 * units.cm, 8.1 * units.cm)


Configuring the history importer
================================

Common OSCAR history importer parameters
----------------------------------------

The following parameters are common to all history importers:

.. parameter:: residual_isotope_treatment
   :type: :ref:`residual_options`
   :default: ``replace_with_exposure``

   How to treat residual isotopes not explicitly tracked by the core simulator. These residual isotopes essentially
   replace the lumped (or structural) material from the homogenized mixture.

.. parameter:: exposure_interpolation
   :type: :ref:`interpolation_options`
   :default: ``exposure``

   How to interpolate from exposure data base.

.. parameter:: ba_isotope
   :type: :term:`isotope`
   :default: ``None``

   Burnable absorber isotope explicitly tracked by the core simulator.

.. parameter:: estimate_ba_composition
   :type: :term:`bool`
   :default: False

   Flag indicating if burnable absorber isotopic composition should be estimated from exposure archive. Since the
   simulator only tracks a single isotope, this feature can be used to estimate the full isotopic composition of
   the burnable absorbers. Burnable absorber compositions are always fetched from the archive by matching
   :param:`ba_isotope` number densities. Thus, irrespective if this flag is on or off, the concentration of
   :param:`ba_isotope` will be the same. If ``False`` only :param:`ba_isotope` number densities will be
   exported.

.. parameter:: estimate_ba_segments
   :type: :term:`bool`
   :default: False

   Flag indicating if the absorber wire or rod depletion shape should be estimated from the exposure archive. If this
   option is used, a single wire depletion shape (estimated from the exposure archive using the current
   :param:`ba_isotope` content)  will be distributed to all the burnable absorbers. The system ensures that the total
   absorber content remains the same.

   In order to use this feature, radial segments for the wires or pins must be specified in the exposure calculation.

   .. note::

      The system only stores a single in wire depletion shape, which is averaged over all the wires in the exposure
      calculation. This is because the intra wire depletion shape can be reasonably expressed as a function of its
      overall depletion rate, but the distribution in burnup between wires are more a function of the assembly's
      environment.

.. parameter:: cool_down_time
   :type: :term:`time`
   :default: ``None``

   Cool down, or decay, period that should be applied to imported material compositions. If ``None``, no material
   decays will be performed.

.. parameter:: cool_down_only_residual
   :type: :term:`bool`
   :default: False

   Flag indicating if only missing (residual) isotopes should be decayed. This parameter has no effect if
   :param:`cool_down_time` is ``None``.



Treating residual isotopes
++++++++++++++++++++++++++

The options for treating residual isotopes are:

.. _residual_options:

.. table:: Residual Options

    +---------------------------------------------------+-------------------------------------------------------------------+
    |Option                                             |Description                                                        |
    +===================================================+===================================================================+
    |``residual_isotope_options.equivalent_boron``      |Lump missing isotopes into an equivalent B-10 number               |
    |                                                   |density (calculated to preserve total absorption)                  |
    +---------------------------------------------------+-------------------------------------------------------------------+
    |``residual_isotope_options.no_correction``         |Ignore missing isotopes                                            |
    +---------------------------------------------------+-------------------------------------------------------------------+
    |``residual_isotope_options.replace_with_exposure`` |Missing isotopes estimated from an infinite assembly               |
    |                                                   |depletion calculation                                              |
    +---------------------------------------------------+-------------------------------------------------------------------+
    |Any :term:`isotope`                                |Will use this isotope as lump instead of B-10                      |
    +---------------------------------------------------+-------------------------------------------------------------------+

To better understand how these options are used, we briefly describe how final material compositions are constructed.
Let :math:`I_E` denote the list of isotopes tracked by the core simulator (available in the
history or exposure file), :math:`I_S` the :ref:`static compositions<Specify loadable assembly types>` in the fuel
alloy, and :math:`I_R` the residual fission products and higher actinides not explicitly accounted for.

At each fuel position and axial layer the application encounters in the homogenized mixture, the
following procedure is used to recover number densities:

 - For each isotope :math:`i \in I_E` the number density :math:`N_i` from the source file is multiplied by the appropriate
   de-homogenization factor, which scales it from a node averaged density, to a plate averaged density.
 - For each isotope :math:`i \in I_R`, the number densities :math:`N_i` are recovered from the exposure data base at the
   position and layer specific exposure. Simple linear interpolation is used for exposures falling between calculated
   points. The :param:`exposure_interpolation` determines how interpolation is performed.
 - Static isotopes in the alloy :math:`I_S` are used exactly as specified.

If the ``residual_isotope_options.no_correction`` option is selected, the process stops, and no further adjustments are
made to the imported mixture.

If the ``residual_isotope_options.replace_with_exposure`` is selected, all isotopes in :math:`I_R` are added to the
mixture.

When ``residual_isotope_options.equivalent_boron`` is selected, an equivalent B-10 number density is calculated as
follows:

Along with the number densities, the exposure files also contain calculated microscopic absorption rates :math:`R_i`
for all isotopes in :math:`I_E`, :math:`I_S`, as well as the total macroscopic absorption :math:`\Sigma_a` rate over
all the fuel bundle materials. In addition, the microscopic reaction rate for B-10 :math:`R_{B_{10}}` is also stored at
each exposure step.

An equivalent B-10 number density is calculated so that the total absorption rate is preserved:

.. math::

    N_{B_{10}} = \frac{\Sigma_a- \sum_{i\in I_E \cup I_S}N_i R_i}{R_{B_{10}}}


.. note::

   The B-10 reaction rate is burnup dependent, and not a single static cross section.

.. attention::

    For this to work correctly, the list of microscopic isotopes specified in your
    :ref:`exposure application<Exposure application input module>` should include all isotopes in :math:`I_E`,
    :math:`I_S` as well as B-10.


Finally, if :param:`residual_isotope_treatment` is set to a specific isotope, that isotope will be used to compute an
equivalent number density instead of B-10. The only requirement is that the specified isotope was included in the
:ref:`exposure application<Exposure application input module>` calculation.

Before choosing which option to select, consider the following:

    - The explicit treatment will typically lead to large material input cards.
    - Using B-10 as a lumping isotope might cause a slight shift in your spectrum, as it is a thermal absorber, while
      many of the isotopes it intends to replace are not.
    - If the imported mixtures are used as a starting point for a depletion calculation, the explicit treatment is
      recommended, as B-10 tends to burn away very quickly, causing long term reactivity trends as the correct poisons
      burn in.

Exposure interpolation
++++++++++++++++++++++

.. _interpolation_options:

.. table:: Interpolation Options

    +--------------------------------------------+----------------------------------------------------------------------+
    |Option                                      |Description                                                           |
    +============================================+======================================================================+
    |``exposure_interpolation_options.exposure`` |Interpolate on exposure (power delivered per fissionable mass) value. |
    +--------------------------------------------+----------------------------------------------------------------------+
    |``exposure_interpolation_options.U235``     |Interpolate by matching U-235 number density.                         |
    +--------------------------------------------+----------------------------------------------------------------------+

.. note::

   If the `exposure_interpolation_options.exposure` is selected, the system will compare the U-235 number density from
   the interpolated mixture with the number density from the homogenized mixture, and log a warning if they differ
   significantly.

Using cool down times
+++++++++++++++++++++

The material compositions in the exposure data base are always at 'hot' full power conditions. Thus, if you import from
the database, you might end up with an excess of short lived isotopes. To remedy this, the :param:`cool_down_time`
and :param:`cool_down_only_residual` parameters can be used. How they are used depends on the time point
at which materials are extracted form data files, and at which time point they should be imported to the
heterogeneous model. In order to illustrate this, consider the following cases

    1. Importing data at EOC conditions and BOC (for next cycle) conditions are required: Set
       :param:`cool_down_time` to the down time between cycles, and :param:`cool_down_only_residual` to ``False``,
       so that both de-homogenized isotopes :math:`I_E` and residual :math:`I_R` isotopes are decayed.
    2. Importing data at BOC conditions and BOC (for next cycle) conditions are required: In
       this case, set :param:`cool_down_time` to the down time between cycles, and
       :obj:`cool_down_only_residual` to ``True``, so that only residual :math:`I_R` isotopes are decayed.
    3. Importing data from the middle of a cycle and hot conditions are required: In this case,
       switch of cool down completely, by setting :param:`cool_down_time` to ``None``.

.. attention::

    The cool down mechanism should only be used with the ``residual_isotope_options.replace_with_exposure`` option, as the
    other options uses the total macroscopic absorption rate :math:`\Sigma_a`, which cannot be decayed, and is still at
    hot conditions.

Importing from MGRAC history files
----------------------------------

In addition to the :ref:`common parameters<Common OSCAR history importer parameters>` the follow parameters should be
set when importing from OSCAR-4 history files:


.. parameter:: inventory_path
   :type: :term:`directory`
   :default: None

   Full path to MGRAC history files.

.. parameter:: base_path
   :type: :term:`dict`
   :default: None

   Dictionary linking base names to base files. There should be an entry for each base type appearing in the relevant
   history files. The system used base definition file to deduce axial meshing.

.. parameter:: library
   :type: :term:`filepath`
   :default: None

   Full path to LINX library. The library file is used to determine the explicit microscopic isotope list :math:`I_E`.

.. note::

    If your MGRAC model was constructed using the cOMPoSe system, all the parameters will be derived
    from the data stored with the homogenized model, and there is no need to specify anything here.

All these parameters are frequently case independent, and can therefore be specified in the base configuration. For
example:

.. code-block:: py

    from extensions.oscar_history_importers import OSCAR4HistoryImporter

    model.inventory_manager.history_importer = OSCAR4HistoryImporter()

    # Full path to inventory
    model.inventory_manager.history_importer.inventory_path = '$HOME/my_model/corana_3D/hist'

    # Specify base file location for all relevant base types
    model.inventory_manager.history_importer.base_path = {
       'LEUFFF': '$HOME/my_model/corana_3D/assdef/LEU-FE---FFF.BASE',
       'LEUCR ': '$HOME/my_model/corana_3D/assdef/LEU-CR------.BASE'
        }

    # Path to LNX file (needed to deduce microscopic isotopes)
    model.inventory_manager.history_importer.library = '$HOME/my_model/library/test.LNX'

    # BA isotope tracked by core simulator (if relevant)
    model.inventory_manager.history_importer.ba_isotope = 'Cd-113'

.. attention::

    Remember that if you are working in a docker container, environmental variables refers to the container environment,
    and directories must point to mounted volumes in the container.

Importing from OSCAR-3 exposure files
-------------------------------------

The OSCAR-3 importer accepts only one additional parameter:

.. parameter:: filename
   :type: :term:`filepath`
   :default: Required

    Full path to OSCAR-3 exposure file.

Since this file is case dependent, it must be specified in the :ref:`input module<The ODEHT input module>`. You can use
the configuration module to initiate the importer

.. code-block:: py

    from extensions.oscar_history_importers import OSCAR3HistoryImporter

    model.inventory_manager.history_importer = OSCAR3HistoryImporter()

.. attention::

    Extracting burnable absorber compositions is not currently supported.

Importing from OASYS PREOS data files
-------------------------------------

The PREOS based importer has the following additional parameters:


.. parameter:: filename
   :type: :term:`filepath`
   :default: Required

   Full path to PREOS data (.dat) file.

.. parameter:: homogenized_area
   :type: :term:`area`
   :default: Required

   Homogenized area (node size).

.. parameter:: homogenized_active_height
   :type: :term:`length`
   :default: Required

   Axial height of homogenized active region.

The :param:`homogenized_area`, and :param:`homogenized_active_height` parameters are common, and can be
defined in the configuration module, while the :param:`filename parameter` is case dependent, and defined in
the :ref:`input module<The ODEHT input module>`.

Adding this importer in the configuration file typically contains the following statements:

.. code-block:: py

    from extensions.oscar_history_importers import PREOSHistoryImporter

    model.inventory_manager.history_importer = PREOSHistoryImporter()

    model.inventory_manager.history_importer.homogenized_area = 7.71 * units.cm * 8.1 * units.cm
    model.inventory_manager.history_importer.homogenized_active_height = 60.0 * units.cm

.. attention::

    Since the burnable absorber composition is currently not dumped by OASYS, extracting burnable absorber compositions
    is not supported.

The ODEHT input module
======================

The final step in using the de-homogenization tool is to prepare a input module for each case. Use the ``create_module``
to create a blank script in the *odeht* package directory:

.. code-block:: console

   $oscar5 MY_REACTOR.create_module odeht my_case --configuration base \
                    --description 'Import number densities for my_case'


Where *base* should be replaced with whatever configuration file you prepared
:ref:`Defining the basic core configuration<earlier>`. This will create a *my_case.py* module in the *odeht* package,
and set some basic parameters.

Additional application parameters
---------------------------------

The ODEHT application has only one parameter:

.. parameter:: template_file
   :type: :term:`filepath`
   :default: 'templates/dehomogenize.tmpl'

   Sets which template file should be used to format the mixtures you import. The system should have generated a basic
   example in the *odeht/templates* directory, which can be used as a basis to create custom templates.
   See `Creating template files`_.

Example:

.. code-block:: py

   parameters.template_file = core.utilities.path_relative_to(__file__, 'templates/dehomogenize.tmpl')


Setting the load map
--------------------

Complete the model by specifying assembly instances for all loadable positions in the core map. These
assembly names must be recognizable by the history importer, e.g. for OSCAR-4 they are valid history file names
(without the *.HIST* extension).

Example:

.. code-block:: py

   parameters.model.load_map = \
    [[_  ,  'A',  'B',  'C',  'D'],
     ['1',    _, 'F1', 'F2',    _],
     ['2', 'F3', 'C1', 'C3', 'F4'],
     ['3',    _, 'F5', 'F7',    _]]

Customizing the importer behavior
---------------------------------

As described :ref:`here<Using cool down times>`, you can set the cool down time to simulate BOC conditions if required:

.. code-block:: py

    importer.cool_down_time = 3 * units.days
    importer.cool_down_only_residual = True

When an importer requires a specific time stamp, be sure to set the load time parameter of the inventory manager:

.. code-block:: py

   parameters.model.inventory_manager.load_time = '20070425 070028'

.. note::

   It is good practice to always set this parameter for time bound applications.

For importers requiring a specific data file, add it here:

.. code-block:: py

   importer.filename = '/path/to/data_file'

Finally, you can customize the :ref:`residual isotopes treatment<Treating residual isotopes>`. For example,

.. code-block:: py

   importer.residual_isotope_treatment = residual_isotope_options.equivalent_boron

Note that all of these parameters can also be set from the command line when the application is launched.

Customizing libraries and mixture output
----------------------------------------

Each isotope in the imported mixture must eventually be translated into something your intended target system can
understand. For instance, in MCNP, U-235 translates to something like ``92235.30c``. You can achieve this manually,
by calling

.. code-block:: cheetah

    $str('%s.03c' % $i.zaid())

in your :ref:`template<Creating template files>`, or use the existing system mechanism for deducing isotope formatting.
he latter involves :ref:`configuring the target mode for your intended host<Configuring Hosts>`, and
setting the :param:`target_mode `and :param:`config_file parameters`. For example, assuming mode MCNP is configured in
*localhost.cfg*:

.. code-block:: py

   parameters.target_mode = 'MCNP'
   parameters.config_file = core.utilities.path_relative_to(__file__, '../../localhost.cfg')

Alternatively, these parameters can also be specified on the command line. The added advantage of this approach is that
state changes will be processed correctly. For example, changing the fuel temperature

.. code-block:: py

   import core.state

   model.state[core.state.fuel_temperature] = 90 * units.degC

will be processed correctly when formatting the isotope.

.. note::

   Even though the odeht application will not run any code, you can still specify a remote host, and the library data
   on that host will be used.

Running the application
=======================

The actual extraction of material compositions is achieved by running the :ref:`input module<The ODEHT input module>` on
the command line. The application accepts the following set of command line flags, which sets or overwrites parameters
defined in the input module:

.. option:: -h, --help

   Display command line options and application modes.

.. option:: --config-file <str>

   Changes the :param:`config_file` parameter, which :ref:`configures the target host<Configuring Hosts>`.
   The argument should be a valid :term:`filepath`.

.. option:: --target-mode <str>

   Sets the :param:`target_mode`. The argument must be a valid mode defined in the custom configuration file (specified
   with the :option:`--config-file` option), or a built-in mode.

.. option:: --cross-section-library <str>

   Customize which library (directory) file will be used. The argument should be a valid :term:`filepath` on the target
   host.

.. option:: --cross-section-library <str>

   Customize the library filter. The argument should be a valid :term:`regex` expression.

.. note::

   These command line options should only be used to check the effects of changing parameters, and not to set any
   data that is expected to be persistent. Persistent data should be done in the input module.

The application has the following sub-modes:


+-------------------------------------+----------------------------------------------------------------------+
|Run Mode                             |Description                                                           |
+=====================================+======================================================================+
|:ref:`archive <archive>`             |Compress entire :param:`working_directory` into a single archive file |
+-------------------------------------+----------------------------------------------------------------------+
|:ref:`clean <clean>`                 |Clears files and directories from the :param:`working_directory`      |
+-------------------------------------+----------------------------------------------------------------------+
|:ref:`visualization <visualization>` |Visualize the underlying model                                        |
+-------------------------------------+----------------------------------------------------------------------+
|execute_                             |Extract number densities and format them using the                    |
|                                     |:param:`template_file`                                                |
+-------------------------------------+----------------------------------------------------------------------+

.. _execute:

Execution mode
--------------

The execution mode accepts a number of flags, which customize the history importer behaviour:

.. program:: execute

.. option:: --residual-isotope-treatment <option>

   Set how :ref:`residual isotopes are treated<Treating residual isotopes>`. Valid options are:

     - 'no_correction' for ignoring residuals,
     - 'equivalent_boron' to compute an equivalent boron concentration, or
     - 'replace_with_exposure' include an explicit list of residual isotopes.

.. option:: --exposure-interpolation <option>

   Set how interpolation in the exposure data base is performed. Options are:

      - 'exposure' to interpolate based on power delivered, or
      - 'U235' to interpolate using U-235 number density.

.. option:: --estimate-ba-composition

   Flag indicating if burnable absorber compositions should also be estimated from the exposure data base.

.. option:: --load-time <str>

   Customize the time at which history files will be processed. Must be a valid :term:`datetime`.

.. option:: --filename <str>

   Customize the data file used.

   .. note::

       This option is only available for exporters with the  :parm:`filename` parameter.

.. option:: --input-file-name <str>

   Customize name of the text file containing formatted material distributions produced by the application. The default
   is ``parameters.project_name.i``.

   .. note::

      The option name is inherited from the standard application execution mode, were a real input file is produced. In
      this case, the produced file is the only output, but will still probably be used as input somewhere down the line.

These options can be viewed from the command line by passing the *--help* flag:

.. code-block:: console

    $oscar5 MY_REACTOR.odeht.my_case execute --help

Example usage
--------------

Basic use (without flags):

.. code-block:: console

    $oscar5 MY_REACTOR.odeht.my_case execute

which will create a file named ``parameters.project_name.i`` in the working directory. The following
will create a new file 'my_case.ba' using the ``equivalent_boron`` option:

.. code-block:: console

    $oscar5 MY_REACTOR.odeht.my_case execute --fission-product-treatment equivalent_boron --input-file-name my_case.ba

Finally, you can add the use of a configuration file and target mode to customize isotope formatting:

.. code-block:: console

    $oscar5 MY_REACTOR.odeht.my_case --config-file localhost.cfg --target-mode MCMP execute --input-file-name my_case.ba2


Creating template files
=======================


The template file, which describes how extracted material distributions are formatted, is build using the rapyds
:ref:`template system<The Template System>`. A basic example template is available in the *MY_REACTOR/odeht/templates*
directory, which you can use as a base for developing your own template. Instead of trying to cover all aspects of
template syntax, and the relevant API elements, we simply highlight a few elements in some basic examples.

Basic example
-------------

.. code-block:: cheetah

    #from applications.odeht import Dehomogenization
    #import core.assembly
    #extends Dehomogenization
    #def __init__(*args,**kwargs)
    #pass
    #end def
    ## --------------------------------------------------------------------------------------------------------------------
    ## This example will simply print material distributions for loaded assemblies in the following form:
    ##                  position     name        axial layer     isotope     number density
    ## --------------------------------------------------------------------------------------------------------------------
    #set $load_grid=$parameters.model.extract_load_grid()                              ## Load assemblies
    #set $nr,$nc = $load_grid.shape()                                                  ## Shape of the core grid
    #for $r in $range($nr)                                                             ## Loop over rows
        #for $c in $range($nc)                                                         ## Loop over columns
            #set $asm = $load_grid[$r][$c]                                             ## Assembly at position in grid
            #if not $asm.fuel_bundles.has_bundle($core.assembly.fuel_bundle())         ## Skip assemblies without fuel
                #continue
            #end if
            #set $dm = $asm.fuel_bundles[$core.assembly.fuel_bundle()].depletion_mesh  ## Extract the depletion mesh
            #set $pos = $load_grid.get_label($r, $c)                                   ## Position label
            #for $ai in range(0, $dm.num_axial_layers())                               ## Loop over axial layers
                #set $mat = $dm.get_material(axial_layer=$ai, bundle_index=0)          ## Material at burn layer
                #for $iso, $p in $mat.isotopes.iteritems()                             ## Loop over all isotopes
                ## Print line
    $pos    $asm.name    $ai    $iso    $p
                #end for                                                               ## End loop over isotopes
            #end for                                                                   ## End loop over axial layers
        #end for                                                                       ## End loop over columns
    #end for

Some highlights from the above:

 - Most of the template simply sets up all the nested loops to get to material compositions.
 - All directives start with ``#``, and comments start with ``##``. Any other line will be printed as text.
 - Variables are assigned and referenced using ``$`` character. Use the ``#set`` directive to initially assign
   variables.
 - All the magic happens in ``parameters.model.extract_load_grid()``, which will build assemblies using the
   history importer (and inventory if specified) to get material distributions.
 - Actual material compositions are stored in the ``depletion_mesh`` attribute of the burnable bundles, e.g.

   .. code-block:: py

      dm = asm.fuel_bundles[core.assembly.fuel_bundle()].depletion_mesh
      ba = asm.fuel_bundles[core.assembly.ba_bundle()].depletion_mesh

   will retrieve the fuel and burnable absorber bundles respectively.
 - The ``get_material`` method is used to retrieve a specific ``material`` instance.

Adding isotope formatting
-------------------------

The following example shows how to introduce isotope formatting:

.. code-block:: cheetah

     #from applications.odeht import Dehomogenization
     #import logging
     #import core.assembly
     #extends Dehomogenization
     #def __init__(*args,**kwargs)
     #pass
     #end def
     ## --------------------------------------------------------------------------------------------------------------------
     ## This example will simply print material distributions for loaded assemblies in the following form:
     ##                  position     name        axial layer     isotope id    number density
     ## --------------------------------------------------------------------------------------------------------------------
     #set $logger=$logging.getLogger('dehom.tmpl')                                      ## Create a logger
     #set $load_grid=$parameters.model.extract_load_grid()                              ## Load assemblies
     #set $nr,$nc = $load_grid.shape()                                                  ## Shape of the core grid
     #for $r in $range($nr)                                                             ## Loop over rows
        #for $c in $range($nc)                                                          ## Loop over columns
            #set $asm = $load_grid[$r][$c]                                              ## Assembly at position in grid
            #if not $asm.fuel_bundles.has_bundle($core.assembly.fuel_bundle())          ## Skip assemblies without fuel
                #continue
            #end if
            #set $dm = $asm.fuel_bundles[$core.assembly.fuel_bundle()].depletion_mesh   ## Extract the depletion mesh
            #set $pos = $load_grid.get_label($r, $c)                                    ## Position label
            #for $ai in range(0, $dm.num_axial_layers())                                ## Loop over axial layers
                #set $mat = $dm.get_material(axial_layer=$ai, bundle_index=0)           ## Material at burn layer
                #set $fmt = $mat.format($self)                                          ## Format material based on mode
                #for $iso, $p in $fmt.isotopes.iteritems()                              ## Loop over all isotopes
                #try
                    #set $tg = $fmt.library.identifier($iso, $fmt.temperature)          ## Try to retrieve id at temp
                    ## Print line
     $pos    $asm.name    $ai    $tg    $p
                #excpet
                    #silent $logger.warn('Isotope %s not found in library!', $iso)
                #end try
                #end for                                                                ## End loop over isotopes
            #end for                                                                    ## End loop over axial layers
        #end for                                                                        ## End loop over columns
     #end for

What's new:

  - Introduced a logger to warn user if an isotope is not found in the library.
  - Instead of working with the material itself, we work with a formatted version:

    .. code-block:: py

       fmt = mat.format(self)

    This will ensure that the attached library is the one specified in the configuration file for the target code.
  - Instead of simply printing the isotope name, and isotope identifier deduced form the library is used via the
    ``identifier`` method.

Creating a full input deck
--------------------------

Finally, the following example illustrates how to incorporate a complete MCNP input deck into your template:

.. code-block:: cheetah

     #from applications.odeht import Dehomogenization
     #import core.assembly
     #extends Dehomogenization
     #def __init__(*args,**kwargs)
     #pass
     #end def
     ## --------------------------------------------------------------------------------------------------------------------
     ## This example show how to write a complete input deck
     ## --------------------------------------------------------------------------------------------------------------------
     #set $load_grid=$parameters.model.extract_load_grid()                              ## Load assemblies
     c standard input formatting for all non-fueled elements (no markup)
     c .
     c .
     c
     #set asm = $load_grid['B3']
     #set $dm = $asm.fuel_bundles[$core.assembly.fuel_bundle()].depletion_mesh
     c FUEL --------- Fuel Assembly at B3 ($asm.name) - Universe 101 ----------------
     c Plate ( axial zones form top to bottom)
     ## The following will format the material, and register it as a material resource, with id 1101.
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1101)
     1101 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4     50 -1101        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1102)
     1102 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1101 -1102        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1103)
     1103 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1102 -1103        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1104)
     1104 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1103 -1104        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1105)
     1105 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1104 -1105        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1106)
     1106 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1105 -1106        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1107)
     1107 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1106 -1107        U=301  imp:n=1 $
     #set $fmt = $parameters.project.add_resource($dm.get_material(axial_layer=0), 1108)
     1108 $fmt.id  $str('%.5E' % $fmt.density().magnitude)    1    -2     3    -4   1107   -51        U=301  imp:n=1 $
     c
     c Rest of fuel assembly (no markup)
     1125  110 -2.68000         1    -2     3    -4    51              U=301  imp:n=1 $ Non active fuel plate (Top)
     1126  110 -2.68000         1    -2     3    -4         -50        U=301  imp:n=1 $ Non active fuel plate (Bottom)
     c
     1127  110 -2.68000    (         27   -28) #( 1    -2     3    -4) U=301  imp:n=1 $ Clad
     1128  120 -0.99300       -27                                      U=301  imp:n=1 $ Moderator (S)
     1129  120 -0.99300              28                                U=301  imp:n=1 $ Moderator (N)
     1130    0                 35   -36    31   -32     LAT=1 FILL= 0:0 -10:10 0:0
                                                           301   20r  U=201  imp:n=1 $ Array of 21 Plates
     1140  120 -0.99300        25   -26    33   -34    52   -53
                                                           FILL=201   U=101  imp:n=1 $ Array of 19 Plates
     1141  110 -2.68000        29   -25    33   -34    54   -55        U=101  imp:n=1 $ Side Plate (W)
     1142  110 -2.68000        26   -30    33   -34    54   -55        U=101  imp:n=1 $ Side Plate (E)
     1143  120 -0.99300       -29                      54   -55        U=101  imp:n=1 $ Moderator (W) - To complete universe
     1144  120 -0.99300              30                54   -55        U=101  imp:n=1 $ Moderator (E) - To complete universe
     1145  120 -0.99300        29   -30   -33          54   -55        U=101  imp:n=1 $ Moderator (S) - To complete universe
     1146  120 -0.99300        29   -30          34    54   -55        U=101  imp:n=1 $ Moderator (N) - To complete universe
     1147  120 -0.99300        25   -26    33   -34    53   -55        U=101  imp:n=1 $
     1148  120 -0.99300        25   -26    33   -34    54   -52        U=101  imp:n=1 $
     c
     1151  120 -0.99300                               -60              U=101  imp:n=1 $ Water below Core Grid
     1153  120 -0.99300                         -71    55   -57        U=101  imp:n=1 $ Region of conic coupling (Top)
     1154  110 -2.68000                    71   -70    55   -57        U=101  imp:n=1 $ Region of conic coupling (Top)
     1155  120 -0.99300                          70    55   -57        U=101  imp:n=1 $ Region of conic coupling (Top)
     1157  120 -0.99300                         -73    57   -59        U=101  imp:n=1 $ Region of cylindrical coupling (Top)
     1158  110 -2.68000                    73   -72    57   -59        U=101  imp:n=1 $ Region of cylindrical coupling (Top)
     1159  120 -0.99300                          72    57   -59        U=101  imp:n=1 $ Region of cylindrical coupling (Top)
     c
     1160  120 -0.99300                         -75    56   -54        U=101  imp:n=1 $ Region of conic coupling (Bottom)
     1161  110 -2.68000                    75   -74    56   -54        U=101  imp:n=1 $ Region of conic coupling (Bottom)
     1162  120 -0.99300                          74    56   -54        U=101  imp:n=1 $ Region of conic coupling (Bottom)
     1163  120 -0.99300                         -73    60   -56        U=101  imp:n=1 $ Region of cylindrical coupling
     1164  110 -2.68000                    73   -72    58   -56        U=101  imp:n=1 $
     1165  120 -0.99300                    73   -72    60   -58        U=101  imp:n=1 $
     1166  120 -0.99300                   -206   72    60   -56        U=101  imp:n=1 $
     1167  110 -2.68000                    206   72    60   -56        U=101  imp:n=1 $ Grid-plate
     1168  120 -0.99300                                      59        U=101  imp:n=1 $ Water above fuel element
     c Next fuel assembly (etc)
     c Rest of cell cards
     c .
     c .

     c Surface cards
     1     px    -3.15                                                                $ Meat (W)
     2     px     3.15                                                                $ Meat (E)
     3     py    -0.0255                                                              $ Meat (S)
     4     py     0.0255                                                              $ Meat (N)
     c .
     c . etc

     c Other material cards
     c
     m03     1001.70c        6.6667E-02                                               $ H-1
             8016.70c        3.3333E-02                                               $ O-16
     mt03   lwtr.10t                                                                  $ T=300K
     c .
     c . etc

     ## Note, since materials already have ids assigned, the order in which they are printed out is irrelevant, and
     ## we simply loop over all the registered material templates.
     c Burnable materials
     #for $fmt in $parameters.project.resources.materials.templates
     $str(fmt)
     #end for