Customizing the depletion mesh
==============================

Instead of fixing the location and distribution of exposure meshes during assembly construction, depletion meshes are
used to customize how materials are activated for a given application. A depletion mesh is automatically added when
a burn bundle is specified, and is usually defined by the assembly type. One can also add certain custom bundles to
any assembly. See :ref:`Specifying burn bundles`.

All depletion meshes are defined by first grouping the discrete fueled primitives into bundles, then subdividing them
into smaller radial and axial segments.

.. note::

   When using an homogenized model, the exposure mesh is defined during model construction, and modifying the depletion
   mesh has no effect.


.. attention::

   When using Monte Carlo codes, the depletion mesh does **not** affect the flux calculation. The average reaction rates
   calculated for the burnable material will be correct. The only approximation occurs in how the different elements of
   the assembly undergo transmutation.


What are fueled primitives?
---------------------------

These are usually defined by the assembly type and/or burn bundle. For example:

    - :ref:`Plate fuel assembly` have individual plates as fueled primitives,
    - Pin assemblies have pins as primitives,
    - The overlay mesh bundle has individual cells as primitives

Since assemblies can have more than one burn bundle, it can also have more than one type of primitive. For example,
in the :ref:`Plate assembly with burnable absorbers`, the `fuel_bundle` has plates as primitives, while the `ba_bundle`
has wires as primitives.

.. note::

   The term *fueled* primitive is a bit misleading, as it actually refers to any structure with transmutable material,
   and not only fissionable parts of an assembly.

Grouping primitives into bundles
--------------------------------

The first step in defining the depletion mesh, is to set how individual primitives should be grouped into separate
bundles. All primitives in a bundle will be activated with a single set of average reaction rates, and they share a
single burnable material.

.. parameter:: bundles
   :type: :term:`list`
   :default: `None`


   Primitives are grouped by assigning an integer in :math:`[0, B-1]` to each primitive. Here :math:`B` is the total
   number of bundles. How these integers are assigned, depends on the bundle type. See for instance
   :ref:`Depletion meshes for plate type assemblies`, :ref:`Depletion meshes for pin type assemblies`.


The `core.depletion_mesh` contains some tags for specifying simple bundle grouping. For example,

.. code-block:: py

   asm.burn_bundle[bundle_tags.fuel].depletion_mesh.bundles = grouping_tags.all_separate

will place each primitive in its own bundle, and

.. code-block:: py

   asm.burn_bundle[bundle_tags.fuel].depletion_mesh.bundles = grouping_tags.all_together

will place all primitives in a single bundle.

How primitives are indexed depends on the burn bundle. Plates are always indexed using arrays, pins using two
dimensional grids, etc. More information on how to specify bundles for a particular assembly type can be found
in the assemblies :ref:`documentation <Choosing an assembly class>`.

.. attention::

   Since further subdivision is performed per bundle, it is important that each bundle contains primitives of the same
   type and size.

Radial meshing
--------------

Each bundle can be further subdivided into radial segments.

.. parameter:: segments
   :type: :term:`list`
   :default: `None`

   Defines how each bundle should be segmented radially. This is specified with a list of fractions for each bundle.
   For example, if there are three bundles,

   .. code-block:: py

       asm.burn_bundle[bundle_tags.fuel].depletion_mesh.segments = [[0.25, 0.5, 0.25]] * 3

   will divide each primitive in each bundle into three radial segments, of size 25%, 50% and 25% of the total radial
   area. The shape of the radial segments depend on the primitive:

    - Plates are segmented into pieces along the **length** of the plate.
    - Pins, wires, and annular regions are segmented into radial rings.

   If left at its default value, no radial subdivision will be performed.

.. attention::

   The number of entries must be equal to the number of bundles.

Axial meshing
-------------

Subdivide each bundle into axial segments.

.. parameter:: axial_layers
   :type: :term:`integer` or :term:`list`
   :default: `None`

   Defines the axial depletion mesh. If a single integer :math:`n`, each bundle will be subdivided into :math:`n`
   equal pieces. Otherwise, a list of fraction per bundle must be specified, similar to the notation used in
   :param:`segments`:

   .. code-block:: py

      asm.burn_bundle[bundle_tags.fuel].depletion_mesh.axial_layers = [[0.25, 0.25, 0.25, 0.25]] * 3

The resulting material mesh
---------------------------

The final number of burnable materials generated is:

.. math::

   \Sigma_{b=1}^B S_b * A_b

where :math:`S_b` and :math:`A_b` are the number of radial and axial segments for bundle :math:`b`.

.. attention::

   The amount of storage used by assembly inventories scales linearly with the number of depletion meshes, and it will
   also take significantly more time to write input files, and read data from output files. Many target codes will also
   have a limit on the number of burnable materials it or your computing hardware can support. When using Monte Carlo
   codes, there is also the possibility that estimated reaction rates will not be well converged on very detailed
   depletion meshes.

   With the currently available tools, it is recommended to only use detailed (per primitive) depletion meshes for
   assembly level applications such as exposure and lattice calculations. When moving to core calculations, a courser
   mesh (usually only axial segmentation and a limited number of radial subdivisions) should be used.