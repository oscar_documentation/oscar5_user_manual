.. _docgen:

Document Generation Utility
***************************

This application is used to drive the documentation system, which allows data available in *rapyds* to be inserted
directly in high quality documents. The system relies on the `Sphinx <https://www.sphinx-doc.org>`__ engine, which in
turn is built using the `docutils <https://docutils.sourceforge.io>`__ framework.

.. _docgen_typical_use_cases:

Typical Use Cases
=================

You would typically use this functionality in the following situations:

   - Writing a model description report, which references various components of the underlying model data.

   - Writing a validation report, which typically includes results from multiple application runs.

   - Combining output from multiple application runs into a single analysis report.

.. note::

   When results form only a single application is used, the built-in documentation mode for that application
   can be used, and there is no need to create a separate input script. See :ref:`Activating documentation input` and
   the corresponding :ref:`doc` mode.

.. _docgen_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager docgen MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'docs'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --template <str>

   Path to template structure which is used to generate the documentation input skeleton.

A typical input module has the following form:

.. code-block:: py

   import applications.docgen as app
   from core import *
   # import some result token(s)
   from applications.critical_case import multiplication_factor

   # Create parameter set
   doc_param = app.parameters()

   # Base documentation skeleton
   doc_param.documentation_templates = '/full/path/to/template'

   # Set to which files the pre processor should be applied
   doc_param.template_pre_processor_filter = 'conf.py'

   # Extract results and bind to variables
   keff = doc_param.extract(multiplication_factor, '/full/path/to/results')
   doc_param.register('My_keff', keff)
   # etc

   if __name__ == '__main__':
    app.main(parameters=cpp)

The input module only `binds variables <docgen_binding_variables>`_ to be used in the document, and provides an
interface for generating and viewing the result. Most of the actual document contents, is specified within the
usual `Sphinx <https://www.sphinx-doc.org>`__ structure, which typically has the following form:

.. _documentation_source_tree:

.. code-block:: console

   WORKING_DIRECTORY
         + - build
         + - source
           + - images
           + - static
             - index.rst
             - conf.py
             - .results.pkl
         - Makefile | make.bat

Here,

   - *WORKING_DIRECTORY*, is the root directory as set using :attr:`doc_param.working_directory`.

   - The *build* directory is were the generated document(s) will be written too.

   - All inputs are contained within the *source* directory. It usually has two (possibly empty) subdirectories:
     *images*, which contain images that should be included in the document, and *static*, which contains style elements.

   - A master `reStructuredText <https://docutils.sourceforge.io/rst.html>`__ source file (*index.rst* in this case,
     but can be customized using :attr:`doc_param.master_document`). This file describes the document's content, and
     can include other `reStructuredText <https://docutils.sourceforge.io/rst.html>`__ source files.

   - The *conf.py* is a `Sphinx configuration <https://www.sphinx-doc.org/en/master/usage/configuration.html>`__ file,
     which contains numerous customization options, such as setting the HTML theme, configuring the LaTeX (PDF) output,
     etc.

   - A *.results.pkl* file, which is a binary archive collecting all the `bound variables <docgen_binding_variables>`_.
     It is generated automatically (and updated) during the `pre-processing <docgen_pre>`_ phase.

   - The *Makefile* (*make.bat* on Windows), is the Sphinx build script, which describes all the generator targets
     (*html*, *latexpdf*, etc.)


.. _docgen_parameters:

Input Parameters
================

In the following `doc_param` denotes a `docgen` parameter set, created as follows:

.. code-block:: py

   import applications.docgen as app

   doc_param = app.parameters()

.. current_parameter_set:: doc_param
   :synopsys: applications.docgen.parameters

This application supports all the following parameters:

.. parameter:: project_name
   :type: :term:`string`

   This parameter is used in certain input templates to name input and output files. If not specified, it
   will be assigned the input module name.

.. parameter:: working_directory
   :type: :term:`directory`

   Specify where the documentation tree should be created. It is usually specified relative to the script:

   .. code-block:: py

      parameters.working_directory = core.utilities.path_relative_to(__file__, 'my_working_directory')

.. parameter:: documentation_template
   :type: :term:`directory`

   A base `document source tree <document_source_tree>`_ which is used to initiate the source tree associated with
   the current document.

   There are two built-in templates:

      1. A basic one:

         .. code-block:: py

            doc_param.documentation_template = '%AUTODOC/templates/basic'

      2. An advance one containing custom elements to produce a specific report format:

         .. code-block:: py

            doc_param.documentation_template = '%AUTODOC/templates/report'

   Both of these can also be used as a starting point to produce your own template.

.. parameter:: master_document
   :type: :term:`string`
   :default: index

   Name of the document source file that acts as the entry point for the document. It contains either the entire
   document source, or an `toctree <https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html>`__
   directive included other source files.

.. parameter:: template_pre_processor_filter
   :type: :term:`regex`

   Filter that determines to which files the pre-processor should be applied. For example, when only
   :ref:`directives <rapyds_sphinx_extensions>` are used in the *rst* files, then the pre-processor should only be
   applied to the `conf.py` file:

   .. code-block:: py

      doc_param.template_pre_processor_filter = 'conf.py'



.. _docgen_binding_variables:

Binding variables to be used in the document
--------------------------------------------

.. py:function:: extract(output_token, result=None)

   Convenience method used to extract output from a result set.

   :param output_token: :ref:`Token <app_output_tokens>` indicating what should be extracted.
   :param result: Target :ref:`application result <app_result_file>`. If not specified, `output_token` will be extracted
      from the attached application.
   :returns: A reference to the output value.

   There are a number of ways to assign the `result`:

      1. By passing the absolute path to the result (*.res*) file, e.g:

         .. code-block:: py

            v = doc_param.extract(token, '/path/to/MY_REACTOR/WORKING_DIRECTORY/TARGET_MODE/project_name.res')

      2. Importing the target input module and extracting the results:

         .. code-block:: py

            from . import target_module  # assuming app sits in same package

            v = doc_param.extract(token, target_module.parameters.create_app(target_mode='TARGET_MODE').get_results())

   .. note::

      The returned value can be combined with other output value references (or static values) in arithmetic expressions
      to produce new values.

   .. attention::

      This only serves to extract the result, and does **not** bind it to document variable set. Use the :func:`set` or
      :func:`register` for this purpose.

.. py:function:: set(name, value)

   Binds a variable for use in the document.

   :param str name: The variable name, which will be used to reference `value` in the document.
   :param value: The variable value, which can be any Python data type, a result token (as returned by
       :func:`doc_param.extract`), or combination of such tokens.

   The variable value con be referenced using the `${name}` convention in the document itself. See
   :ref:`Variable tokens`.

.. py:function:: register(name, expression, description=None, ranges=None, uncertainty=None, fmt=None, results=None)

   Similar to :attr:`doc_param.set`, but with the possibility to pass more information.

   :param str name: The variable name, which will be used to reference `expression` in the document.
   :param expression: The variable value, which can be a raw variable token (as passed as input to
     :func:`doc_param.extract`), a result token (as returned by :func:`doc_param.extract`), or combination of such tokens.
   :param str description: Optional description of the variable.
   :param ranges: The range (upper and lower bound) in which the value should fall.
   :param uncertainty: The typical uncertainty associated with the parameter value.
   :param fmt: How the value should be formatted in the document.
   :param results: Similar to the `result` variable in :func:`doc_param.extract`.

   The variable value con be referenced using the `${name}` convention in the document itself. See :ref:`Variable tokens`.


.. _docgen_cli:

Command Line Usage
==================

This application has two sub-modes:

 +-----------------------------+---------------------------------------------------------+
 | `pre <docgen_pre>`_         | Updates the `source tree <document_source_tree>`_ and   |
 |                             | bound variable set.                                     |
 +-----------------------------+---------------------------------------------------------+
 | `execute <docgen_execute>`_ | Run the documentation generation and view the generated |
 |                             | document(s).                                            |
 +-----------------------------+---------------------------------------------------------+


.. _docgen_pre:

pre
---

This mode accomplishes the following:

   1. Create the `source tree <document_source_tree>`_ from the :attr:`doc_param.documentation_template`
      if it does not exist.
   2. Runs the pre-processor on the files specified in :attr:`doc_param.template_pre_processor_filter`.
   3. Updates the bound variable set, and writes it to a *.results.pkl* object in the
      `source tree <document_source_tree>`_.

.. _docgen_execute:

execute
-------

This modes calls the document generator engine `Sphinx <https://www.sphinx-doc.org>`__, which produces the final
document(s). It accepts the following flags:

.. option:: --force

   Force running the pre-processor, and rebinding of variables. This is required if anything in the
   :attr:`doc_param.documentation_template` changed, or if the value of any bound variable changed.

.. option:: --skip-html

   Don't generate HTML output.

.. option:: --skip-pdf

   Don't generate LaTeX output.

.. option:: --show

   View the generated output. On most platforms, it will be opened in a browser window.

.. _docgen_typical_cli:

Typical command line usage
--------------------------

.. code-block:: console


   oscar5 MY_REACTOR.docs.my_module execute --force --show


.. _docgen_examples:

Examples
========

.. _docgen_extra_resources:

Extra Resources
===============

.. toctree::
   :maxdepth: 2

   sphinx_and_rst
   sphinx_extensions
   autodoc_macros

