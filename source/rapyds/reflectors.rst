Reflectors
==========

.. py:function:: material_library.reflectors.Beryllium(mass_density=1.85 * units.g / units.cc)

    Pure beryllium.

   :param mass_density: The beryllium mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.reflectors import Beryllium
   >>> brl = Beryllium(mass_density=3.01 * units.g / units.cc)
   >>> print(brl)
   Material     : Beryllium
   Type         : reflector
   Mass density : 3.0100 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Be-9      100.00    %         2.01135e-01
   Total     100.00              2.01135e-01


.. py:function:: material_library.reflectors.BerylliumOxide(mass_density=3.01 * units.g / units.cc)

   Beryllium oxide composite.

   :param mass_density: The beryllium mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.reflectors import BerylliumOxide
   >>> brlo= BerylliumOxide(mass_density=3.01 * units.g / units.cc)
   >>> print(brlo)
   Material     : Beryllium Oxide
   Type         : reflector
   Mass density : 3.0100 g/cc
   Isotope   Wt                   Nd (1/b/cm)
   Be-9      36.04     %         7.24860e-02
   O-16      63.96     %         7.24860e-02
   Total     100.00              1.44972e-01



.. attention::

   These beryllium mixtures consist solely of Be-9, with no impurities. All mined beryllium will contain trace values
   of numerous other metals, which varies significantly between different grades and manufactures.

.. py:function:: material_library.reflectors.Graphite(mass_density=1.7 * units.g / units.cc)

   Pure graphite using natural carbon.

   :param mass_density: The graphite mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.reflectors import Graphite
   >>> grt= Graphite(mass_density=1.7 * units.g / units.cc)
   >>> print(grt)
   Material     : Graphite
   Type         : reflector
   Mass density : 1.7000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     100.00    %         8.52355e-02
   Total     100.00              8.52355e-02



.. py:function:: material_library.reflectors.HeavyWater(mass_density=1.107 * units.g / units.cc, enrichment=100.0)

   Heavy water.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`
   :param float enrichment: Percentage of hydrogen atoms that are deuterium.

   Example:

   >>> from core import *
   >>> from material_library.reflectors import HeavyWater
   >>> hwt = HeavyWater(mass_density=1.107 * units.g / units.cc, enrichment=100.0)
   >>> print(hwt)
   Material     : Heavy Water
   Type         : reflector
   Mass density : 1.1070 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-2       20.12     %         6.65881e-02
   O-16      79.88     %         3.32941e-02
   Total     100.00              9.98822e-02


.. note::

   All predefined reflector materials have the appropriate thermal composition attached to them.