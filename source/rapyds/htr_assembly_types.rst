.. role:: py(code)
    :language: python

High Temperature Reactor (HTR) Designs
======================================

This chapter collects all macros and functions used to model typical HTR fuel and core designs. Common to all
components is the notion of a dispersed particle distribution, which plays the role of a fueled primitive (just like
plates are for :ref:`MTR <Plate Type Fuel Assemblies>`, and pins for :ref:`LWR <Pin Type Fuel Assemblies>`).

.. note::

   It is important to note that the entire dispersed distribution is usually considered a single primitive, and not
   the individual coated particles, or pebbles. This design principle is followed because the number of individual
   fueled components (on their own), are usually orders more than what is encountered in other designs.


Generating Dispersed Particle Distributions
-------------------------------------------

A dispersed particle distribution is defined by a number of particle (usually spheres) centers, sizes and identifiers defining
the particle type. The pre processor has a number of options for creating or importing distributed particle distributions.
This covers both the generation of coated particles inside fuel compacts, and pebbles in the core. These are described in the
following sections.

All tools are contained in the :mod:`core.disperse` module.

.. note::

   All particle distribution routines make use of the discrete element package `YADE <https://yade-dem.org/doc/index.html>`__.
   See `Installing YADE`_ for instructions on how to obtain the tool.

Importing from external sources
+++++++++++++++++++++++++++++++

There are a number of external tools that can generate both random or regular particle distributions. One example is using
the disperser routine in `Serpent <http://serpent.vtt.fi/mediawiki/index.php/Installing_and_running_Serpent#Particle_disperser_routine>`__.
Output from these tools can be placed in a simple text file which is imported using the following function:

.. py:function:: read_ascii_spheres(filename, u=units.cm, predicate=None, id_map=None)

   Read dispersed spherical particle distribution from a text file. The file is assumed to have the following format:

        - Each particle entry is separated by a new line break.
        - Each line contains at least the 5 entries:

           - The :math:`x`, :math:`y`, :math:`z` coordinates of the sphere center (floating point values)
           - The radius of the sphere (floating point value)
           - Particle identifier (integer or string).

   :param filename: Full path to target file name.
   :type filename: :term:`filepath`
   :param u: The units particle center coordinates are given in.
   :param predicate: Used to filter particles. Must be a function object accepting five arguments (the particle center
     :math:`x`, :math:`y`, :math:`z`, size and id), returning True if the particle should be included, or False otherwise.
   :param dict id_map: Used to change string ids present in the file to integer ids.

   .. attention::

      If the ids in `filename` are not integers, the `id_map` parameter is required.

Regular lattice distributions
+++++++++++++++++++++++++++++

Currently, the :mod:`core.disperse` can produce
`hexagonal close-packed <https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres>`__ distribution using

.. py:function:: hcp(radius, shape, spacing=0.0 * units.cm, tag=0)

   Create a hexagonal close-packed (HCP) particle distribution of equal shaped spheres.

   :param radius: Radius of the spheres that will be packed.
   :type radius: :term:`length`
   :param shape: The boundary of the region to which the lattice should be clipped.
   :type shape: :term:`region`
   :param spacing: The distance between neighboring spheres. If 0, spheres will touch.
   :type spacing: :term:`length`
   :param tag: How ids should be assigned to generated spheres. If a fixed integer value, all particles will have the
    same id, otherwise this parameter must be a function accepting the particle position (:math:`x`, :math:`y`,
    :math:`z`) coordinates as input, and returning an integer.

   .. attention::

      The `region` must be bounded!

   For example, the following will create a close packing of spheres with radius 1 mm in a cylindrical region:

   >>> from csg import *
   >>> from core import disperse
   >>> spheres = disperse.hzp(1 * units.mm, Cylinder(radius=1 * units.cm, lower_bound=-10 * units.cm, upper_bound=10 * units.cm))

   .. note::

      This will create a explicit distribution of spheres, contained within the bounding `region`. Instead of creating
      such an explicit list, which is not supported by all attached target codes, the :class:`HexagonalClosePacked`,
      option can be used. Since it models the distribution as standard hexagonal lattice, it is supported by more
      attached solvers.


Closed packed spheres are also modelled using the :class:`HexagonalClosePacked` or :class:`FaceCenteredCubic` objects.

Random dense packing
++++++++++++++++++++

A random dense packing (with spheres potentially touching), is produced by trying to fit as many randomly positioned
spheres as possible into a specified region.

.. py:function:: rdp(radius, shape, tag=0)

   Create a Random Dense Packing of equal sized spheres, clipped to a specified region.

   :param radius: Radius of the spheres that will be packed.
   :type radius: :term:`length`
   :param shape: The boundary of the region to which the lattice should be clipped.
   :type shape: :term:`region`
   :param tag: How ids should be assigned to generated spheres. If a fixed integer value, all particles will have the
    same id, otherwise this parameter must be a function accepting the particle position (:math:`x`, :math:`y`,
    :math:`z`) coordinates as input, and returning an integer.


.. note::

   Currently, there is no way to specify the target packing fraction or number of particles. If this is required,
   rather use a different tool `Serpent disperser routine <http://serpent.vtt.fi/mediawiki/index.php/Installing_and_running_Serpent#Particle_disperser_routine>`__.

Gravity deposition
++++++++++++++++++

These distributions are generated using an explicit simulation of dropping particles into a container, and modelling
their interaction under the relevant physical forces. The discrete element solver `YADE <https://yade-dem.org/doc/index.html>`__
can be used to perform such simulations.

Examples of simulation scripts can be found in the relevant HTR benchmark sets.


Coated Particles
----------------

The concept of a generic coated particle is represented by a :class:`CoatedParticle` instance,  which creates an
arbitrary number of spherical shells, with the inner most sphere the kernel (or core) region. It is located in the
:mod:`fuel_assembly` module, and apart from the :ref:`standard component parameters <Generic assembly>`:

.. current_parameter_set:: cp
   :synopsys: core.fuel_assembly.CoatedParticle

.. parameter:: kernel_radius
   :type: :term:`length`
   :default: Required

   The radius of the innermost region.

.. parameter:: kernel_contents
   :type: :term:`material` or :term:`bundle_tag`
   :default: None

   The contents of the innermost region.

   .. note::

      If the coated particle will form part of a dispersed distribution, the kernel contents can also be specified
      when initiating the material of that distribution. Thus, it is not necessary to specify it when defining the
      coated particle.

Once the inner kernel size is specified, additional shells are added using:

.. py:function:: cp.add_shell(radius, contents)

   Add a new layer to the coated particle.

   :param radius: Radius of the new layer. Must be larger than the previously added layer.
   :type radius: :term:`length`
   :param contents: Contents of the new layer.
   :type contents: :term:`material`

   .. attention::

      The :param:`kernel_radius` must be set prior to making this call.

For example, the following will create a simple particle with a single coating:

>>> from core import *
>>> from material_library.structural import SiliconCarbide
>>> cp = fuel_assembly.CoatedParticle(name='CP')
>>> cp.kernel_radius = 0.1 * units.mm
>>> cp.add_shell(0.15 * units.mm, SiliconCarbide())

The :mod:`fuel_assembly` also contains a macro for creating typical `TRISO <https://en.wikipedia.org/wiki/Nuclear_fuel#TRISO_fuel>`__
fuel particles:

.. py:function:: fuel_assembly.TRISO(fuel_radius=0.2125 * units.mm, buffer_radius=0.3125 * units.mm, inner_pyc=0.3475 * units.mm, sic_radius=0.3825 * units.mm, outer_pyc=0.4225 * units.mm, buffer=PorousGraphite(), pyc=PyroliticGraphite(), sic=SiliconCarbide(), **kwargs)

   Standard TRI-structural ISO-tropic fuel particle design.

   :param fuel_radius: Radius of the inner fuel kernel.
   :type fuel_radius: :term:`length`
   :param buffer_radius: Radius of the buffer zone.
   :type buffer_radius: :term:`length`
   :param inner_pyc: Radius of the inner pyrolitic carbon layer.
   :type inner_pyc: :term:`length`
   :param sic_radius: Radius of the silicon carbide layer.
   :type sic_radius: :term:`length`
   :param outer_pyc: Radius of the outer pyrolitic carbon layer. This is also the total size of the particle.
   :type outer_pyc: :term:`length`
   :param buffer: Material used in the buffer zone.
   :type buffer: :term:`material`
   :param pyc: Material used in the pyrolitic carbon layers.
   :type pyc: :term:`material`
   :param sic: Material used in the  silicon carbide layer.
   :type sic: :term:`material`


Pebble Bed Designs
------------------


Pebbles
+++++++

The :class:`Pebble` class in the :mod:`fuel_assembly` can be used to model typical pebble designs. Apart from the
:ref:`standard component parameters <Generic assembly>`, it is defined by setting the following:

.. current_parameter_set:: pebble
   :synopsys: core.fuel_assembly.Pebble

.. parameter:: radius
   :type: :term:`length`
   :default: 3 cm

   Total (outer) radius of the pebble.

.. parameter:: reflector_thickness
   :type: :term:`length`
   :default: 5 mm

   Thickness of the outer reflector (fuel free) layer.

.. parameter:: reflector_material
   :type: :term:`material`
   :default: Graphite

   Material used in the reflector zone.

.. parameter:: matrix
   :type: :term:`material`
   :default: Graphite

   Material used in the coated particle compact (fueled region), that is, the material filling the region outside
   the coated particles.

.. parameter:: coolant
   :type: :term:`material`
   :default: Helium

   Material used to fill space outside the pebble.

.. note::

   The default value used for :param:`reflector_material` and :param:`matrix`, is pure graphite (at 1.74 g/cc)
   without any impurities. Any reactivity estimation will be quite sensitive to the material used here, so be sure
   to replace the defaults with the correct composition.

Once all of the parameters are set, the actual fueled region is defined using:

.. py:function:: pebble.pack(distribution, fuel_zone=None, coated_particle=TRISO(), fuel=None, packing_fraction=None, bundle=fuel)

   Add the dispersed fuel particles to the fueled region of the pebble.

   :param distribution: Source describing the particle distribution. Can be produced by any of the mechanisms described
      in `Generating Dispersed Particle Distributions`_:

      .. table::
         :class: tight-table

         +--------------------------------------------------+----------------------------------------------------------+
         | Distribution                                     | Description                                              |
         +==================================================+==========================================================+
         | :term:`filepath`                                 | Text file in a format supported by                       |
         |                                                  | :func:`read_ascii_spheres`                               |
         +--------------------------------------------------+----------------------------------------------------------+
         | `HexagonalClosePacked()`                         | HCP sphere distribution, modelled as a lattice.          |
         +--------------------------------------------------+----------------------------------------------------------+
         | `FaceCenteredCubic()`                            | FCC sphere distribution, modelled as a lattice.          |
         +--------------------------------------------------+----------------------------------------------------------+

   :param fuel_zone: Sub region bounding the active fuel region. If not specified, the full inner spherical region
      (with radius :param:`radius` - :param:`reflector_thickness`) will be used.
   :type fuel_zone: :term:`region`
   :param coated_particle: The particle(s) that should be used in the dispersion. If a single value, this particle will
       be used in all locations. Alternatively, a :term:`dict` object with keys the ids appearing in the `distribution`
       and values the particle types.
   :type coated_particle: :term:`component` or :term:`dict`
   :param fuel: The material that should fill the kernel region of the `coated_particle`. This parameter is optional
        here, as it can also be specified when initiating the burnable material(s).
   :param packing_fraction: The packing fraction of a close packed sphere distribution. This is *only* used if
       `distribution` is :class:`HexagonalClosePacked` or :class:`FaceCenteredCubic`.
   :type packing_fraction: :term:`float`

   .. note::

      The pre generated `distribution` must be clipped to the `fuel_zone`. In particular, if using the default zone,
      the dispersed distribution should be generated in a sphere with radius :param:`radius` - :param:`reflector_thickness`.

   .. attention::

      This call will create all the cells defining the pebble structure, thus **all** parameters must be specified prior
      to this call.

   .. note::

      The reflector region is now defined as the region inside the outer sphere, but inside the `fuel_region`.


For example, the following will create a standard pebble using the shipped kernel distribution (with packing fraction):


.. note::

   Dummy pebbles, or pebbles that do not contain any fuel compacts, should not be generated using the `Pebble` macro.
   Instead they are defined as :ref:`generic assemblies <Generic assembly>`. For example, the following will create
   a solid graphite pebble:

   >>> from core import *
   >>> from csg import *
   >>> from material_library.reflectors import Graphite
   >>> from material_library.gasses import Helium
   >>> dp = assembly.Assembly(name='Dummy-pebble')
   >>> dp.add_cell(primitives.Sphere(radius=3 * units.cm), material=Graphite())
   >>> dp.add_cell(~primitives.Sphere(radius=3 * units.cm), material=Helium())


Pebble bed core
+++++++++++++++

The core component is modelled using the :class:`PebbleBed` class available in the :mod:`reactor_core` module. It is
used in the build script as follows:

.. code-block:: py

   from core import *
   from csg import *
   from core.reactor_core import PebbleBed
   from material_library.gasses import Helium

   def build():

       bed = PebbleBed(name='MY_HTR-pebble-bed')
       bed.coolant = Helium()

The :class:`PebbleBed` object is a :ref:`standard assembly <Generic assembly>`, with all the general parameters, and
also uses the following parameters and methods:

.. parameter:: coolant
   :type: :term:`material`
   :default: Helium

   The coolant used in the core. This is the material that surrounds the pebbles.

Similar to the coated particle distribution in pebbles, the distribution of pebbles in the core is set using the `pack`
method:

.. py:function:: pebble_bed.pack(distribution, pebbles, cavity=None)

   Set the distribution of pebbles in the core.

   :param distribution: Source describing the particle distribution. Can be produced by any of the mechanisms described
      in `Generating Dispersed Particle Distributions`_. If this is a :term:`filepath`, the file must be in the format
      readable by :func:`read_ascii_spheres`.
   :param pebbles: The pebbles(s) that should be used in the dispersion. If a single value, this pebble will
       be used in all locations. Alternatively, if more than one pebble type is present, a :term:`dict` object with
       keys the ids appearing in the `distribution` and values the particle types.
   :param cavity: Region in which the `distribution` should be placed. This parameter is optional here, as it can be
       specified later using the :func:`pebble_bed.add_fueled_cell` method.

   Although any distribution can be used, ones generated using :ref:`gravity sphere drop simulation <Gravity deposition>`
   are the most appropriate in this case.

   Additional cells containing the pebble `distribution` are specified using :func:`pebble_bed.add_fueled_cell`.

.. py:function:: pebble_bed.add_fueled_cell(region)

   Add additional cells which are filled with the pebble distribution (specified using :func:`pebble_ped.pack`.

   :param region: Boundary of the cell.
   :type region: :term:`region`



Prismatic Designs
-----------------

A typical prismatic element is modelled using the following steps:

   1. Create one or more `fuel compacts <Cylindrical compacts>`_.

   2. Stack the compacts or pellets to form one or more fuel (or absorber) `pins <Pellet stacks>`_.

   3. Distribute the pins, and specify the coolant holes, for one or more `elements <Prismatic element>`_.

   4. Stack the elements to form a complete `assembly <Prismatic assembly>`_.

Cylindrical compacts
++++++++++++++++++++

The :class:`CylindricalCompact` class in the :mod:`fuel_assembly` can be used to model typical cylindrical compact
(pellet) designs. Apart from the :ref:`standard component parameters <Generic assembly>`, it is defined by setting
the following:

.. current_parameter_set:: pellet
   :synopsys: core.fuel_assembly.CylindricalCompact

.. parameter:: radius
   :type: :term:`length`
   :default: 3 cm

   Total (outer) radius of the cylindrical compact.

.. parameter:: matrix
   :type: :term:`material`
   :default: Graphite

   Material used in the coated particle compact (fueled region), that is, the material filling the region outside
   the coated particles.

.. parameter:: coolant
   :type: :term:`material`
   :default: Helium

   Material used to fill space outside the pebble.

.. note::

   The default value used for :param:`matrix`, is pure graphite (at 1.74 g/cc)
   without any impurities. Any reactivity estimation will be quite sensitive to the material used here, so be sure
   to replace the defaults with the correct composition.

Once all of the parameters are set, the actual fueled region is defined using the same approach as :func:`pebble.pack`.


Pellet stacks
+++++++++++++

A stack of cylindrical compacts are modelled using :class:`CompactPin`. Compacts (pellets), as well as plugs or spacers,
are added using the following interface:

.. current_parameter_set:: compact_pin
   :synopsys: core.fuel_assembly.CompactPin

.. py:function:: add_layer(lower=None, upper=None, size=None, target=None)

   Adds a layer to the compact stack.

   :param lower: The axial lower bound of the layer. Only needed for the first layer, as subsequent layers will assume
      the previous layer's upper bound.
   :type lower: :term:`length`
   :param upper: Upper bound of the axial layer.
   :type upper: :term:`length`
   :param size: Size of the layer. Can be specified instead of `upper`.
   :type size: :term:`length`
   :param target: Contents of the layer. Usually a cylindrical compact, plug or spacer element.
   :type target: :term:`component`

   .. note::

      1. Layers **must** be added from top to bottom.

      2. If `lower` is specified, a check is performed to see that it is the same as `upper` of the previous
         layer (if present).

The following example will create a simple pin with 5 pellets, and a graphite plug on the top and bottom:

.. code-block:: py

   from core.fuel_assembly import CompactPin, CylindricalCompact
   from core.disperse import HexagonalClosePacked
   from core.assembly import Assembly
   from csg.primitives import Cylinder
   from material_library.reflectors import Graphite
   from material_library.gasses import Helium

   # create the plug
   plug = Assembly(name='Fuel_pin_plug')
   plug.add_cell(Cylinder(radius=hole, lower_bound=0 * units.cm, upper_bound=2 * units.cm),
                 material=Graphite())
   plug.add_cell(~Cylinder(radius=hole, lower_bound=0 * units.cm, upper_bound=2 * units.cm),
                 material=Helium())

   # create the compact
   p = (total_height - 4 * units.cm)/5.0
   compact = CylindricalCompact(name='Test')
   compact.radius = 0.98 * hole
   compact.height = p
   compact.pack(HexagonalClosePacked(), packing_fraction=0.1)

   pin = CompactPin(name='Fuel-Pin')

   pin.add_layer(-0.5 * total_height, size=2.0 * units.cm, target=plug)

   for i in range(5):
       pin.add_layer(size=p, target=compact)

   pin.add_layer(size=2.0 * units.cm, target=plug)


Prismatic element
+++++++++++++++++

A single prismatic block is modelled using :class:`PrismaticElement` available in the :module:`fuel_assembly` module.
Apart from the :ref:`standard component parameters <Generic assembly>`, it is defined by setting
the following:

.. current_parameter_set:: prismatic_element
   :synopsys: core.fuel_assembly.PrismaticElement

.. parameter:: size
   :type: :term:`length`
   :default: 36 cm

   The inner diameter of the hexagonal boundary of the element.

.. parameter:: height
   :type: :term:`length`
   :default: 79.3 cm

   The height (axial extent) of the element.

.. parameter:: orientation
   :default: X_AXIS

   The orientation of the bounding hexagonal shape.

.. parameter:: grid_cell
   :type: :term:`region`

   Set a custom bounding region of the underlying hexagonal pin layout. This should be used if the element has cavities,
   such as a central hole, or a control rod position.

   If not specified, it defaults to the radial bound (a hexagon with diameter :attr:`prismatic_element.size` and
   orientation :attr:`prismatic_element.orientation`), extruded from 0 to :attr:`prismatic_element.height`.

.. parameter:: cell_pitch
   :type: :term:`length`
   :default: 1.8796 cm

   The pitch of the underlying hexagonal pin lattice.

   .. note::

      Equal to the inner diameter of the hexagonal elements in the tesselation.

.. parameter:: lattice_orientation
   :default: Y_AXIS

   The :ref:`orientation <hexagonal_grid_orientation>` of the underlying :term:`hexagonal grid`.

.. parameter:: fuel_hole_diameter
   :type: :term:`length`
   :default: 1.27 cm

   Diameter of the hole containing fuel. Can be used in :func:`prismatic_element.pin_cell`.

.. parameter:: large_coolant_hole_diameter
   :type: :term:`length`
   :default: 1.588 cm

   The diameter of a large coolant hole. Used by :func:`prismatic_element.large_coolant_cell`.

.. parameter:: small_coolant_hole_diameter
   :type: :term:`length`
   :default: 1.27 cm

   The diameter of a small coolant hole. Used by :func:`prismatic_element.small_coolant_cell`.

.. parameter:: coolant
   :type: :term:`material`
   :default: Helium

   The material that fills coolant holes, as well as the space outside the :attr:`prismatic_element.grid_cell`.

.. parameter:: moderator
   :type: :term:`material`
   :default: Graphite

   The material that fills the interior of :attr:`prismatic_element.grid_cell`, that is, around all pins, and the filler
   region around the hexagonal lattice.

.. parameter:: layout
   :type: :term:`hexagonal grid`

   Set the layout of pins within the hexagonal lattice. The pins are usually those produced by the functions,
   :func:`prismatic_element.pin_cell`, :func:`prismatic_element.solid_cell`, :func:`prismatic_element.large_coolant_cell`,
   or :func:`prismatic_element.small_coolant_cell`. Layout is specified using a :term:`hexagonal grid`, in either
   :ref:`axial coordinates <flat_top>` or using a :term:`grid`. An empty grid is created using
   :func:`prismatic_element.new_grid`.

   For example:

   .. code-block:: py

      elem = PrismaticElement(name='test-prismatic-block')
      elem.height = 70 * units.cm
      elem.size = 5 * elem.cell_pitch

      # set the layout
      grid = elem.new_grid()
      grid.orientation = 'y'

      fc = elem.pin_cell(elem.fuel_hole_diameter, pin)
      hl = elem.large_coolant_cell()

      # fill the grid
      grid[0] = -2, [fc, hl, fc, hl, fc]
      grid[1] = -2, [fc, hl, hl, fc]
      grid[2] = -2, [fc, fc, fc]

      grid.fold()        # symmetric distribution

      elem.layout = grid
      elem.show()


   .. attention::

      Any new pin type created (using :func:`prismatic_element.pin_cell`), and added to the layout, will be activated
      separately during burnup calculations. See `depletion_mesh_prismatic`_.

.. py:function:: pin_cell(diameter, pin, name=None)

   Create a hexagonal pin cell.

   :param diameter: The hole within the graphite body that will contain the pin.
   :type diameter: :term:`length`
   :param pin: Pin that will be placed in the hole. Either a `compact stack <Pellet stacks>`_, or
         :ref:`cylindrical stack <pin_cell_stack>`.
   :param name: Name of the pin cell. If not specified, one will be created based on the name of `pin`.
   :type name: :term:`string`

   Returns an element that can be used when specifying :attr:`prismatic_element.layout`.

.. py:function:: solid_cell(name=None)

   Creates and returns a simple solid pin cell, that can be used when specifying :attr:`prismatic_element.layout`.

.. py:function:: large_coolant_cell()

   Returns a hexagonal pin, with a :attr:`prismatic_element.large_coolant_hole_diameter` size hole filled with
   :attr:`prismatic_element.coolant`.

.. py:function:: small_coolant_cell()

   Returns a hexagonal pin, with a :attr:`prismatic_element.small_coolant_hole_diameter` size hole filled with
   :attr:`prismatic_element.coolant`.


Prismatic assembly
++++++++++++++++++

Models a complete prismatic


.. _depletion_mesh_prismatic:

Customizing the depletion mesh for prismatic elements and assemblies
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


Custom Designs
--------------


Installing YADE
---------------

If you are working on Linux, follow the `YADE installation instructions <https://yade-dem.org/doc/installation.html>`__.

.. note::

   It is our intention to also distribute YADE as a **conda** package once the compilation issues have been sorted out.

.. _htr_examples:
Examples
--------

This section gives complete build script examples for some of the assembly types described in this chapter.

Pebble bed core build script
++++++++++++++++++++++++++++

.. literalinclude:: examples/cor.py


Coated particles build script
+++++++++++++++++++++++++++++

.. literalinclude:: examples/coatd.py


Pebble build script
+++++++++++++++++++

.. literalinclude:: examples/pble.py
