.. _sphinx_and_rst:

Sphinx and reStructuredText
***************************

This section contains more information on the document generation engine `Sphinx <https://www.sphinx-doc.org>`__
and `reStructuredText <https://docutils.sourceforge.io/rst.html>`__ markup language.

Structure of a reStructureText File
===================================

A typical `reStructuredText <https://docutils.sourceforge.io/rst.html>`__ source file contains a mixture of basic text,
light markup elements (headings, tables, lists, etc.), as well as special inline and block markup elements, called *roles*
and *directives* respectively. This is illustrated in the following `figure <typical_rst>`_:

.. _typical_rst:
.. figure:: images/rst-cc.jpeg

   Typical structure of a reStructure.


More information on the various markup elements can be found from the following sources:

.. list-table:: Links to reStructuredText documentation
   :class: tight-table

   * - Basic syntax and elements such as font style, lists and tables.
     - `docutils <https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html>`__
       or the `rst primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#rst-directives>`__
   * - Advanced docutils directives.
     - `docutils directives <https://docutils.sourceforge.io/docs/ref/rst/directives.html>`__
   * - Additional roles and directives provided by Sphinx.
     - `sphinx directives <https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html>`__
   * - Additional roles and directives available in *rapyds*.
     - :ref:`rapyds directives <rapyds_sphinx_extensions>`
