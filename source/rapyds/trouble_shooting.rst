Trouble Shooting
****************

This section lists how to resolve few known system and operating system specific problems.

Installation
------------



Runtime errors
--------------

Exceptions on interpreter shutdown
++++++++++++++++++++++++++++++++++

On some installations you may occasionally encounter a random exception in the terminal after the application terminates.
They are due to problems in the shutdown and cleanup of the python interpreter itself, and **not** due to a system error.
These problems are annoying, but harmless, since they do not affect the application outcome.

If possible, try to upgrade the python interpreter. Otherwise, let us know the application mode for which this persist,
and we can try to manually improve the interpreter shutdown process.

.. note::

   This occurs most often in Docker containers running on Windows.

Fortran file io errors
++++++++++++++++++++++

When working on Windows, either natively or through a docker container, you may (seemingly randomly) encounter a
Fortran 28 error. This is usually happens when the operating system opens a file, while the Fortran executable tries
to close it. One known solution is to disable file indexing for your working directories. On Windows, this is handled
by the **Indexing Options** utility, available in the **Control Panel**.


