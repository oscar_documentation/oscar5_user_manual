.. role:: py(code)
   :language: py

The Application Input Module
****************************


Application input is primarily specified in a basic python script (also called a module). As with any executable python
script, it typically contains the following:

    1. A pre-amble on the top of the file with import statements.

    2. A body where application parameters are set.

    3. A closing section, detailing how the script will be executed when executed on the command line.

Import statements
-----------------

The first module that should be imported is the one that implements the target application's input parameters and
execution framework. There are two different ways to achieve this, as described in `Global application import`_ and
`Local application import`_ respectively.

Global application import
+++++++++++++++++++++++++

In this case, all symbols from the application module :py:`app_type` is imported:

.. code-block:: py

   from application.app_type import *

where :py:`app_type` should be replaced with the required application type. The symbols which are made available with this
statement differs slightly between applications, but always includes at least the following:

=================  ====================================================================================
Symbol              Description
=================  ====================================================================================
:py:`parameters`    Set of input parameters for the application.
:py:`main`          The application driver routine.
=================  ====================================================================================

.. note::

   The drawback of this method is that a global :py:`parameters` set is used, which can cause problems when the same
   application type is imported more than one in another script. This is discussed further in
   :ref:`Creating an Application Parameter Set`.

Local application import
++++++++++++++++++++++++

In this case, only the application module is imported:

.. code-block:: py

   import application.app_type as app

where :py:`app_type` should be replaced with the required application type. Symbols can now be accessed from the
:py:`app` alias, for example :py:`app.parameters` and :py:`app.main`. See :ref:`Creating an Application Parameter Set`
for more information on how this is used to create a local (unique) parameter set for the application.

System imports
++++++++++++++

Since the application input is likely to use other system resources, these should be imported next:

.. code-block:: py

   from core import *

The modules and variables made available through this statement, is described in :ref:`System interface basics`. You can
also import any standard python module, or other external package. For example,

.. code-block:: py

    import math

will make the entire python `math <https://docs.python.org/2/library/math.html>`__ available.

Relative imports
++++++++++++++++

The application might also need to import data or methods from other applications, or other modules within the larger
local package structure. Importing an existing model is the most common occurrence. Assuming the larger package
has the following structure:

.. code-block:: none

    ROOT
     |-my_config.cfg
     MY_REACTOR
      |-model
      |-configurations
      |  |-my_configuration.py
      |-projects
         |-my_app.py

To import the model form *my_configurations.py* in *my_app.py*, add

.. code-block:: py

    from ..configurations.my_configuration import model

.. note::

   When using relative imports the script can only be executed in package mode, which is automatically used when
   invoking the :command:`oscar5` wrapper script.

Input module body
-----------------

The body of the application

Making the script executable
----------------------------

The final part of any application script contains a the statement that will determine how the application is executed
in a terminal prompt. It depends on how the application was imported:

When using the `Global application import`_ option, it is simply:

.. code-block:: py

   if __name__ == '__main__':
      main()

While, using the `Local application import`_ option requires:

.. code-block:: py

   if __name__ == '__main__':
      app.main(parameters=my_parameters)

where :py:`my_parameters` should be replaced with whatever you named your local parameter set
(see :ref:`Creating a unique parameter set`).

.. note::

   The :py:`if` statement wrapping the :py:`main` method will prevent application launch when the script is imported.

