Choosing an Assembly Class
==========================

Assembly classes are used to store assembly meta and geometric structure data. All assembly classes are derived from
an :ref:`Generic assembly`, but differ in the amount of additional meta data, and pre-defined structure they
provide.

The following groups assembly types by their typical use and design base:

.. toctree::
   :maxdepth: 4

   generic_assembly_types
   mtr_assembly_types
   lwr_assembly_types
   htr_assembly_types




