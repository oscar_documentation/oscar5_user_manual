General Application Guidelines
******************************

This chapter introduces the basic application interface, in particular, how input scripts are structured, and how the
scripts are executed on the command line. Since all applications are constructed using the same underlying framework,
they all share a similar interface. Here, we will outline these common features, and leave the particulars to the
application specific user guides.


.. toctree::

   application_input_module
   application_parameters
   application_cli
   application_output