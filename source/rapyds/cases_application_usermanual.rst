.. _cases:

Multiple Steady State Neutron Flux Calculations
***********************************************

This application mode will create and run multiple :ref:`steady state <critical_case>` calculations. It's primary intended
use is to study the effect of perturbing a base case (e.g. swapping assemblies, changing loaded rigs or sample states),
but it can be used for many other purposes as well.

.. note::

   Although this application mode can also be used to study reactivity worth of control elements, and estimate feedback
   effects, there are other application modes specifically for this purpose.

.. _cases_typical_use_cases:

Typical Use Cases
=================

.. _cases_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager cases MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'projects'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --configuration <str>

   The configuration module that contains the model used in this script.

.. option:: --mco <str>

   Name (or path) of the model configuration archive that should be used in this script.

   .. attention::

      This option can not be specified when :option:`--configuration` is also specified.

.. option:: --time-stamp <str>

   Time point at which facility state should be recovered (see :attr:`app_param.time_stamp`). Must be a
   valid :term:`datetime`.

A typical input module has the following form:

.. code-block:: py

   import applications.cases as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   cases = app.parameters()

   # Set some base application parameters
   cases.time_stamp = '01-01-2020 00:00:00'

   # Set and modify the model
   cases.model = model
   cases.model.set_banks(control.percent_extracted(50))
   # etc

   # Application specific
   c1 = cases.add_case('new_bank')
   c1.model.set_banks(control.percent_extracted(75))
   # etc

   if __name__ == '__main__':
    app.main(parameters=cases)


.. _cases_parameters:

Additional Input Parameters
===========================

In the following `cases` denotes a `applications.cases` parameter set, created as follows:

.. code-block:: py

   import applications.cases as app

   cases = app.parameters()

.. current_parameter_set:: cases
   :synopsys: applications.cases.parameters

This application supports all the :ref:`critical case parameters <critical_case_parameters>`. The parameters set here
will determine how the base calculation is performed, and also set the default parameters for additional cases.

.. py:function:: add_case(name, description=None)

   Adds a new case to be calculated.

   :param name: The case name.
   :type name: :term:`string`
   :param description: Optional description of the case.
   :type name: :term:`string`

   This will return a :ref:`critical case parameter <critical_case_parameters>` set, which is used to modify the case
   application data.


.. _cases_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`. Apart from the standard modes, each case added with
:func:`cases.add_case` will be available as a sub-mode, allowing one to run them individually.

.. _cases_typical_cli:

Typical command line usage
--------------------------

.. _cases_output_tokens:

Output Tokens
=============

.. py:currentmodule:: critical_case

.. py:function:: case_results(param, order=None, **kwargs)

   Extract a parameter from each case.

   :param str param: The

.. _cases_examples:

Examples
========
