.. _multi_rod_calibration:

Rod Calibration Calculations for Multiple Rods
**********************************************

Application mode used to calculate differential and integral control rod worth as a function of insertion depth for
multiple rods. It simply collects a number :ref:`rod_calibration` application cases into a single input script.

.. _multi_rod_calibration_typical_use_cases:

Typical Use Cases
=================

This application mode is used to:

   1. Simulate multiple rod worth experiments, using experimentally measured data.

   2. Calculate the integral worth of control rods as a function of their position.

.. _multi_rod_calibration_creating_input_module:

Creating an Input Module
========================

An empty input module can be created using the :command:`manager` utility:

.. code-block:: console

   oscar5 MY_REACTOR.manager multi_rod_calibration MODULE_NAME [options]

This command the following optional arguments:

.. option:: --package <str>

   The subpackage to which this module should be added. The default value is 'projects'.

.. option:: --description <str>

   A short description of the input module.

.. option:: --configuration <str>

   The configuration module that contains the model used in this script.

.. option:: --mco <str>

   Name (or path) of the model configuration archive that should be used in this script.

   .. attention::

      This option can not be specified when :option:`--configuration` is also specified.

.. option:: --time-stamp <str>

   Time point at which facility state should be recovered (see :attr:`app_param.time_stamp`). Must be a
   valid :term:`datetime`.

A typical input module has the following form:

.. code-block:: py

   import applications.multi_rod_calibration as app
   from ..configurations.my_conf import model
   from core import *

   # Create parameter set
   rods_cal = app.parameters()

   # Set some base application parameters
   rods_cal.time_stamp = '01-01-2020 00:00:00'

   # Set and modify the model
   rods_cal.model = model
   # etc

   # Add a rod
   r1 = rods_cal.add_rod('R1', description='First rod')

   r1.rod_steps.add_step(critical_banks={'r1': control.percentage_extracted(50),
                                         'r2': control.percentage_extracted(45)},
                         perturbed_banks={'r1': control.percentage_extracted(55)})
   # etc

   # Add next rod
   r2 = rods_cal.add_rod('R2', description='Second rod')

   # etc

   if __name__ == '__main__':
    app.main(parameters=rods_cal)


.. _multi_rod_calibration_parameters:

Additional Input Parameters
===========================

In the following `rods_cal` denotes a `multi_rod_calibrations` parameter set, created as follows:

.. code-block:: py

   import applications.multi_rod_calibration as app

   rods_cal = app.parameters()

.. current_parameter_set:: rods_cal
   :synopsys: applications.multi_rod_calibration.parameters

This application supports all the :ref:`general parameters <general_app_parameters>`. These parameters are propagated to
the individual rod (or bank) calculations, which are added using:

.. py:function:: add_rod(tag, description=None)

   Adds a new rod (or bank) for which reactivity worth calculations should be performed.

   :param str tag: How this case will be identified. It is recommended that the name of the target rod or bank be used.
   :param str description: Optional description of the case.

   :returns: An :ref:`rod_cal <rod_calibration_parameters>` parameter set, which is used to further specify input for
             this case.


.. _multi_rod_calibration_cli:

Command Line Usage
==================

This application supports all the standard application modes and options as described in
:ref:`General Application Command Line Interface (CLI)`. In addition to the standard modes, the
:ref:`rod_calibration <rod_calibration_cli>` application for each rod added with :func:`rods_cal.add_rod` is available
as a sub mode.

.. _multi_rod_calibration_typical_cli:

Typical command line usage
--------------------------

Runs the calibration simulations for all rods:

.. code-block:: console

   oscar5 MY_REACTOR.calibration.my_rods --target-mode MY_TARGET --config-file MY_CONFIG run --force

View the results for a specific rod

.. code-block:: console

   oscar5 MY_REACTOR.calibration.my_rods R1 --target-mode MY_TARGET post --show


.. _multi_rod_calibration_output_tokens:

Output Tokens
=============

.. py:currentmodule:: multi_rod_calibration

This application provides the following output variables:

.. py:function:: differential_curve(rod, **kwargs)

   Retrieve the calculated differential reactivity values.

   :param str rod: The target rod.
   :param kwargs: Any keyword argument accepted by :func:`rod_calibration.differential_curve`.

   This token (via its :py:`get` method), returns two lists, the first being the rod movement scale, and the second the
   reactivities (see :func:`rod_calibration.differential_curve`).


.. py:function:: s_curve(rod, **kwargs)

   Retrieve the calculated cumulative reactivity over the specified rod movement.

   :param str rod: The target rod.
   :param kwargs: Any keyword argument accepted by :func:`rod_calibration.s_curve`.

   This token (via its :py:`get` method), returns two lists, the first being the rod movement scale, and the second the
   integral worth (see :func:`rod_calibration.s_curve`).

.. py:function:: total_worth(rod, **kwargs)

   Retrieve the calculated total worth.

   :param str rod: The target rod.

.. py:function:: measured_differential_curve(rod, **kwargs)

   Same as :func:`multi_rod_calibration.differential_curve`, but returns the reference reactivities.

.. py:function:: measured_s_curve(rod, **kwargs)

   Same as :func:`multi_rod_calibration.s_curve`, but returns the reference reactivities.

.. py:function:: rod_critical_multiplication_factors(rod, **kwargs)

   Retrieve the effective multiplication factor (:math:`k_{eff}`) at the critical set points, for the specified rod.

   :param str rod: The target rod.
   :param kwargs: Any keyword argument accepted by :func:`rod_calibration.critical_multiplication_factors`.

   This token (via its :py:`get` method), returns two lists, the first being the positions of the
   `rod` at each critical set point, and the second the :math:`k_{eff}` values.


.. py:function:: critical_multiplication_factors(*rods, **kwargs)

   Retrieve the effective multiplication factor (:math:`k_{eff}`) at the critical set points, for *multiple* rod.

   :param rods: Sequence of target rod names. If not specified, results for all rods will be extracted.

   This token (via its :py:`get` method), returns two lists, the first is simply a range of indices which over all
   critical positions, and the second the :math:`k_{eff}` values.


.. _rod_calibration_examples:

Examples
========
