Cladding
========

.. py:function:: material_library.cladding.PorousGraphite(mass_density=0.9700 * units.g / units.cc)

   Low density carbon typically used as a buffer layer in coated particles.

   :param mass_density: The graphite mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.cladding import PorousGraphite
   >>> pgr = PorousGraphite()
   >>> print(pgr)
   Material     : PorousGraphite
   Type         : clad
   Mass density : 0.9700 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     100.00    %         4.86344e-02
   Total     100.00              4.86344e-02

   .. note::

      Nominal (default) mass density taken from
     `NEA Benchmark of the Modular High-Temperature Gas-Cooled Reactor-350 MW Core Design <http://www.oecd.org/officialdocuments/publicdisplaydocumentpdf/?cote=NEA/NSC/R(2017)4&docLanguage=En>`__


.. py:function:: material_library.cladding.PyroliticGraphite(mass_density=1.9 * units.g / units.cc)

   Coating layer used in TRISO particle manufacturing.

   :param mass_density: The mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.cladding import PyroliticGraphite
   >>> pyg = PyroliticGraphite()
   >>> print(pyg)
   Material     : PyC
   Type         : clad
   Mass density : 1.9000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     100.00    %         9.52632e-02
   Total     100.00              9.52632e-02

.. py:function:: material_library.cladding.Zircaloy2(mass_density=6.55 * units.g / units.cc, impurities=True)

   Basic reactor grade Zirconium alloy.

   Taken form
    `standards specification <https://www.atimetals.com/Products/Documents/datasheets/zirconium/alloy/Zr_nuke_waste_disposal_v1.pdf>`__

   :param mass_density: The mass density of the alloy
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.cladding import Zircaloy2
   >>> zirc = Zircaloy2()
   >>> print(zirc)
   Material     : Zircaloy-2
   Type         : clad
   Mass density : 6.5500 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Cr-Nat    0.10      %         7.58615e-05
   Fe-Nat    0.15      %         1.05950e-04
   Ni-Nat    500.0     ppm       3.36026e-05
   Zr-Nat    98.20     %         4.24614e-02
   Sn-Nat    1.50      %         4.98418e-04
   Total     100.00              4.31753e-02


.. py:function:: material_library.cladding.Zircaloy4(mass_density=6.55 * units.g / units.cc, impurities=True)

    Basic reactor grade Zirconium alloy.

    Taken from
    `standards specification <https://www.atimetals.com/Products/Documents/datasheets/zirconium/alloy/Zr_nuke_waste_disposal_v1.pdf>`__

   :param mass_density: The mass density of the alloy
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.cladding import Zircaloy4
   >>> zirc = Zircaloy4()
   >>> print(zirc)
   Material     : Zircaloy-4
   Type         : clad
   Mass density : 6.5500 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Cr-Nat    0.10      %         7.58615e-05
   Fe-Nat    0.21      %         1.48329e-04
   Zr-Nat    98.19     %         4.24571e-02
   Sn-Nat    1.50      %         4.98418e-04
   Total     100.00              4.31797e-02




