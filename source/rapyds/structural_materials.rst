Structural Materials
====================

.. py:function:: material_library.structural.Al6061()

   Aluminum series 6061 based on published nominal composition.

   Example:

   >>> from core import *
   >>> from material_library.structural import Al6061
   >>> al = Al6061()
   >>> print(al)
   Material     : Al-6061
   Type         : structural
   Mass density : 2.7000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Mg-Nat    0.99      %         6.65538e-04
   Al-27     96.70     %         5.82723e-02
   Si-Nat    0.60      %         3.45562e-04
   Ti-Nat    0.15      %         5.06813e-05
   Cr-Nat    0.19      %         6.06660e-05
   Mn-55     0.15      %         4.41581e-05
   Fe-Nat    0.70      %         2.02763e-04
   Cu-Nat    0.27      %         7.00072e-05
   Zn-Nat    0.25      %         6.18508e-05
   Total     100.00              5.97735e-02


.. py:function:: material_library.structural.Al1050()

   Generic Aluminum series 1050 based on published nominal composition.

   Example :

   >>> from core import *
   >>> from material_library.structural import Al1050
   >>> al = Al1050()
   >>> print(al)
   Material     : Al-1050
   Type         : structural
   Mass density : 2.7000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Mg-Nat    250.0     ppm       1.67254e-05
   Al-Nat    99.54     %         5.99824e-02
   Si-Nat    0.13      %         7.23660e-05
   Ti-Nat    150.0     ppm       5.09530e-06
   V-Nat     250.0     ppm       7.97963e-06
   Mn-Nat    250.0     ppm       7.39914e-06
   Fe-Nat    0.20      %         5.82318e-05
   Cu-Nat    250.0     ppm       6.39685e-06
   Zn-Nat    250.0     ppm       6.21741e-06
   Total     100.00              6.01628e-02

    .. note::

     Composition taken from `generic material specification <https://www.makeitfrom.com/material-properties/1050-A91050-Aluminum>`__,
     with all impurities at their **maximum** specification.

.. py:function:: material_library.structural.Al1200()

   Generic 1000 series aluminum with high degree of purity.

   Example:

   >>> from core import *
   >>> from material_library.structural import Al1200
   >>> al = Al1200()
   >>> print(al)
   Material     : Al-1200
   Type         : structural
   Mass density : 2.7000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   Mg-Nat    0.15      %         1.00353e-04
   Al-Nat    99.03     %         5.96751e-02
   Si-Nat    0.30      %         1.73679e-04
   Ti-Nat    250.0     ppm       8.49217e-06
   Cr-Nat    500.0     ppm       1.56356e-05
   Mn-Nat    0.15      %         4.43949e-05
   Fe-Nat    0.20      %         5.82318e-05
   Cu-Nat    500.0     ppm       1.27937e-05
   Zn-Nat    500.0     ppm       1.24348e-05
   Total     100.00              6.01011e-02

    .. note::

     Composition taken from `matweb <http://www.matweb.com/search/datasheet.aspx?matguid=7bc8905c329a44e5bcf44a818589d53f&ckck=1>`__,
     with all impurities at their **maximum** specification.

.. py:function:: material_library.structural.StainlessSteel304L(mass_density=8.03 * units.g / units.cc)

   Stainless steel 304L based on published nominal conditions.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from material_library.structural import StainlessSteel304L
   >>> stnl = StainlessSteel304L(mass_density=8.03 * units.g / units.cc)
   >>> print(stnl)
   Material     : SS-304L
   Type         : structural
   Mass density : 8.0300 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     300.0     ppm       1.20784e-04
   N-14      1000.0    ppm       3.45337e-04
   Si-Nat    0.75      %         1.29133e-03
   P-31      450.0     ppm       7.02563e-05
   S-Nat     300.0     ppm       4.52365e-05
   Cr-Nat    19.00     %         1.76705e-02
   Mn-55     2.00      %         1.76045e-03
   Fe-Nat    69.04     %         5.97881e-02
   Ni-Nat    9.00      %         7.41515e-03
   Total     100.00              8.85071e-02

  .. note::

     This is a generic composition taken from `this data sheet <http://www.aksteel.com/pdf/markets_products/stainless/austenitic/304_304l_data_sheet.pdf>`__


.. py:function:: material_library.structural.StainlessSteel316L(mass_density=7.99 * units.g / units.cc)

   Stainless steel 316L based on published nominal conditions.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.structural import StainlessSteel316L
   >>> stls = StainlessSteel316L()
   >>> print (stls)
   Material     : SS-316L
   Type         : structural
   Mass density : 7.9900 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     300.0     ppm       1.20182e-04
   N-14      0.10      %         3.43617e-04
   Si-Nat    0.75      %         1.28490e-03
   P-31      450.0     ppm       6.99063e-05
   S-Nat     300.0     ppm       4.50111e-05
   Cr-Nat    17.00     %         1.57317e-02
   Mn-55     2.00      %         1.75168e-03
   Fe-Nat    65.55     %         5.64746e-02
   Ni-Nat    12.00     %         9.83761e-03
   Mo-Nat    2.50      %         1.25370e-03
   Total     100.00              8.69129e-02

   .. note::

     This is a generic composition taken from `this data sheet <http://www.aksteel.com/pdf/markets_products/stainless/austenitic/316_316l_data_sheet.pdf>`__



.. py:function:: material_library.structural.Concrete(mass_density=2.30 * units.g / units.cc)

   US-NRC standardized concrete.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.structural import Concrete
   >>> cncrt = Concrete()
   >>> print(cncrt)
   Material     : Concrete
   Type         : structural
   Mass density : 2.3000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-1       1.00      %         1.37434e-02
   O-16      53.20     %         4.60690e-02
   Na-Nat    2.90      %         1.74720e-03
   Al-27     3.40      %         1.74538e-03
   Si-Nat    33.70     %         1.66195e-02
   Ca-Nat    4.40      %         1.52064e-03
   Fe-Nat    1.40      %         3.47234e-04
   Total     100.00              8.17923e-02



.. py:function:: material_library.structural.GenericWood(mass_density=40 * units.lb / units.ft / units.ft / units.ft)

   Generic wood composition.

   :param mass_density: The total mass density or one of the tags given in the table below.
   :type mass_density: :term:`density` or :term:`string`

   =================  =============================
    Wood Type                 Density (g /cc)
   =================  =============================
    Southern pine      0.64
    White pine         0.43
    Yellow pine        0.71
    Ash                0.55
    White ash          0.67
    Balsa              0.125
    Cedar              0.67
    Cherry             0.43
    Fir                0.51
    Elm                0.56
    Hickory            0.77
    Mahogany           0.7
    Maple              0.68
    White Maple        0.53
    Oak                0.67
    White oak          0.77
    Poplar             0.43
    Redwood            0.42
    Spruce             0.45
    Walnut             0.59
    Course sawdust     0.29
    Fine sawdust       0.40
    Plywood            0.58
   =================  =============================

   Example:

   >>> from core import *
   >>> from material_library.structural import GenericWood
   >>> wood = GenericWood()
   >>> print(wood)
   Material     : Generic wood
   Type         : structural
   Mass density : 0.6407 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-Nat     5.96      %         2.28309e-02
   C-Nat     49.70     %         1.59671e-02
   N-Nat     0.50      %         1.36911e-04
   O-Nat     42.74     %         1.03088e-02
   Mg-Nat    0.20      %         3.15624e-05
   S-Nat     0.50      %         5.97984e-05
   K-Nat     0.20      %         1.96196e-05
   Ca-Nat    0.20      %         1.91400e-05
   Total     100.00              4.93738e-02

   .. note::

      Taken from `PNNL-15870 Rev.1 <https://www.pnnl.gov/main/publications/external/technical_reports/PNNL-15870Rev1.pdf>`__.

.. py:function:: material_library.structural.PlateGlass(mass_density=2.4 * units.g / units.cc)

   Generic plate glass.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.structural import PlateGlass
   >>> glass = PlateGlass()
   >>> print(glass)
   Material     : Glass(Plate)
   Type         : structural
   Mass density : 2.4000 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   O-Nat     45.98     %         4.15370e-02
   Na-Nat    9.64      %         6.06302e-03
   Si-Nat    33.66     %         1.73191e-02
   Ca-Nat    10.72     %         3.86608e-03
   Total     100.00              6.87853e-02

   .. note::

      Taken from `PNNL-15870 Rev.1 <https://www.pnnl.gov/main/publications/external/technical_reports/PNNL-15870Rev1.pdf>`__.

.. py:function:: material_library.structural.SiliconCarbide(mass_density=3.21 * units.g / units.cc)

   Silicon Carbide.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.structural import SiliconCarbide
   >>> sil = SiliconCarbide()
   >>> print(sil)
   Material     : SiC
   Type         : clad
   Mass density : 3.2100 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   C-Nat     29.95     %         4.82108e-02
   Si-Nat    70.05     %         4.82108e-02
   Total     100.00              9.64215e-02


.. py:function:: material_library.structural.TitaniumAlloy(mass_density=4.43 * units.g / units.cc)

   Titanium Alloy.

   :param mass_density: The total mass density.
   :type mass_density: :term:`density`

   Example:

   >>> from core import *
   >>> from material_library.structural import TitaniumAlloy
   >>> tit = TitaniumAlloy()
   >>> print(tit)
   Material     : TitaniumAlloy
   Type         : structural
   Mass density : 4.4300 g/cc
   Isotope   Wt                  Nd (1/b/cm)
   H-1       110.0     ppm       2.91180e-04
   C-Nat     570.0     ppm       1.26605e-04
   N-Nat     210.0     ppm       3.99966e-05
   O-Nat     0.14      %         2.35114e-04
   Al-Nat    6.12      %         6.05117e-03
   Ti-Nat    89.37     %         4.98076e-02
   V-Nat     4.00      %         2.09480e-03
   Fe-Nat    0.28      %         1.35194e-04
   Total     100.00              5.87817e-02

   .. note::

      Taken from `PNNL-15870 Rev.1 <https://www.pnnl.gov/main/publications/external/technical_reports/PNNL-15870Rev1.pdf>`__.