.. _irradiation_history:
Irradiation histories
=====================

This is the general object used to define and store the definition of burnup steps as well as the state of the model
during irradiation. The top level `IrradiationHistory` is simply a sequence of `IrradiationStep` and `DecayStep`
entries, with a number of convenience function to add steps and extract data.

An irradiation  history instance has the following parameters:

.. current_parameter_set:: irr
   :synopsys: core.irradiation.IrradiationHistory

.. parameter:: start_time
   :type: :term:`datetime`
   :default: None

   Calender date and time when the irradiation starts. All applications will set this automatically from it's
   `time_stamp` parameter.


.. parameter:: step_type
   :type: Tag
   :default: `delta`

   Indicate if the duration of irradiation steps are specified as absolute or cumulative values. For the latter case,
   each duration indicates the *total* time since the start of the irradiation history.

   Use the tag `delta` to indicate that each step will specify its duration since the previous step, and `cumulative`
   if step sizes are cumulative.


.. parameter:: xenon_mode
   :type: Tag
   :default: `calculated`

   How xenon build up should be treated. There are two options:

   - `calculated`: The xenon build up should be explicitly calculated, or *burned in*. This usually requires the use
     of small irradiation steps to account for the rapid change in material compositions.

   - `equilibrium`: Set the xenon at its equilibrium value, assuming a constant power (see below).


   .. attention::

      Currently the `equilibrium` option will triggers the relevant option in a target code, and performs no independent
      equilibrium xenon estimation. It should therefore only be used with target codes that supports this option.

.. parameter:: time_stepping_algorithm
   :type: Tag
   :default: `predictor_corrector`

   How the change in material properties during an irradiation step should be estimated. The following options are
   supported:

    - `predictor_corrector`: Perform a predictive flux calculation with material properties fixed at their beginning
      of step values, deplete materials using the flux to estimate reaction rates, then repeat the process with
      materials at the new state. The final material composition is estimated as a linear combination of the two
      calculations. Thus, each depletion step requires two flux, and two depletion calculations.

    - `backwards`: This assumes that the change in material compositions won't affect the flux distribution. Thus a
      single flux calculation is used to deplete the materials for the step.


    .. note::

       The algorithm used applies to all steps, and can not be changed for individual steps.


    .. attention::

       Currently, for multi-step depletion calculations, the `predictor_corrector` option is only available for target
       codes that supports it.

.. parameter:: base_power
   :type: :term:`power`
   :default: Custom

   The base (or default) power for the irradiation history. If not specified, the model's design power will be assumed.

.. parameter:: rig_loader
   :type: Custom
   :default: None

   Object used to deduce the rig loading during the irradiation history. It should only be specified if you plan to
   load, unload or move intra assembly components during the irradiation.

   The loader's function is to interpret rig schedules from the plant data, and return real model elements. Setting up
   rig loading for you model is described here.

Irradiation steps are added to the irradiation history instance using the following two methods:

.. py:method:: irradiation_history.add_decay_step(duration, **kwargs)

   Adds a pure decay (zero power) step.

   :param duration: Total duration of the step, either relative to the previous step, or relative to the beginning
                    of the irradiation history if :param:`step_type` is `cumulative`.
   :type duration: :term:`time`.
   :param **kwargs: Any additional parameters defining a :ref:`general step <General parameters defining an irradiation step>`.
   :returns: A `DecayStep` instance.

   For example, to add a decay step of two days and save the assembly material compositions at the end of the step:

   .. code-block:: py

      irradiation_history.add_decay_step(2 * units.days, save=True)


.. py:method:: irradiation_history.add_step(duration, **kwargs)

   Adds an active irradiation step.

   :param duration: Total duration of the step, either relative to the previous step, or relative to the beginning
                    of the irradiation history if :param:`step_type` is `cumulative`.
   :type duration: :term:`time`.
   :param **kwargs: Any additional parameters defining a :ref:`basic <General parameters defining an irradiation step>`,
   or :ref:`active <Additional parameters for depletion steps>` or active step.
   :returns: An `IrradiationStep` instance.

   For example, to add an irradiation step of two days, with average tep power of 2 mega watt:

   .. code-block:: py

      irradiation_history.add_step(2 * units.days, power=2 * units.MW)

   .. note::

     When required parameters, e.g :param:`power` or :param:`average_bank` is not specified, it will be inherited from
     the previous step added to the irradiation history.


Alternatively, instead of specifying parameters during step addition, they can be set in the returned object:

.. code-block:: py

   step = irradiation_history.add_step(2 * units.days)
   step.power = 2 * units.MW
   step.average_banks = control.percentage_inserted(54.0)


General parameters defining an irradiation step
-----------------------------------------------

.. parameter:: duration
   :type: :term:`time`
   :default: Required

   The total duration of the step. This is the delta step time, or the step time since start of the irradiation if
   :param:`step_type` was set to `cumulative`

.. parameter:: save
   :type: bool
   :default: False

   Flag indicating if the material compositions at the end of the step should be saved to the inventory.

   .. note::

      For calculations that modify an inventory, only the material states at the beginning and end of the irradiation
      is saved by default. Use this flag if you want additional snap shot states in the irradiation period to be saved.

   .. attention::

      This parameter is never inherited from previous steps, and must be explicitly switched on for the desired steps.

.. parameter:: critical_power
   :type: :term:`power`
   :default: None

   Optional flag that indicates the real power (as opposed to the average :param:`power`), at the **end** of the step.
   It is used for end of step flux calculations. If these calculations are not going to be performed, this parameter
   need not be specified.

.. parameter:: critical_banks
   :type: :term:`dict` or :term:`travel`
   :default: None

   Optional flag that indicates the real bank position(s) (as opposed to :param:`average_banks`), at the **end** of the
   step. Either a single :term:`travel` instance, if all banks have the same position, or a :term:`dict` instance
   mapping bank names to positions.

   It used for end of step flux or criticality calculations. If these calculations are not going to be performed,
   this parameter need not be specified.

.. parameter:: step_rigs
   :type: Custom
   :default: None

   State of rigs (or inter assembly loadable components), that will be used **during** the irradiation step (that is, to
   activate and decay materials). Must be a value that can be interpreted by :param:`rig_loader`.

.. parameter:: critical_rigs
   :type: Custom
   :default: None

   State of rigs (or inter assembly loadable components), at the **end** of the irradiation step. Must be a value
   that can be interpreted by :param:`rig_loader`.

   It used for end of step flux or criticality calculations. If these calculations are not going to be performed,
   this parameter need not be specified.


.. note::

   Parameters related to bank positions (e.g :param:`critical_banks`, :param:`average_banks`), are optional, and only
   makes sense when the underlying model has banks defined.

Additional parameters for depletion steps
-----------------------------------------

Apart from the :ref:`base <General parameters defining an irradiation step>` parameters, the following must be set
for active depletion steps:

.. parameter:: power
   :type: :term:`power`
   :default: Required

   The *average* core neutronic power used during the irradiation step. This is the value that will be used to scale
   fluxes when calculating reaction rates.

.. parameter:: average_banks
   :type: :term:`dict` or :term:`travel`
   :default: None

   Position of the banks used when calculating fluxes for the depletion calculation. This is usually taken to be the
   average bank position for the :param:`duration` of the step. Either a single :term:`travel` instance, if all banks
   have the same position, or a :term:`dict` instance mapping bank names to positions.

Useful methods
--------------

The following lists a number of queries that can be performed on the irradiation history instance:

.. py:method:: irradiation_history.duration()

   Get the total duration of the entire irradiation history.

   :returns: A quantity with :term:`time` dimensions.

.. py:method:: irradiation_history.total_power_delivered()

   The total power delivered for the entire irradiation history.

   :returns: A quantity with :term:`power` times :term:`time` dimensions.

.. py:method:: irradiation_history.step_frame(step_index)

   Get the start and end time of a step.

   :param int step_index: The step number.
   :returns: Two :term:`datetime` values, with the first value the step start time, and the second the step end time.