.. role:: py(code)
    :language: python

Pin Type Fuel Assemblies
========================

Collects all macros used to construct pin type fuel assemblies, as used in LWR (PWR, and BWR) reactor designs.

Unlike the typical :ref:`MTR <Plate fuel assembly>` macros, which creates all fueled components during construction,
pin type assemblies are constructed in two phases:

    1. First define all pins (fuel pins, guide tubes, etc), usually in separate build scripts.
    2. Then provide the pin layout in another build script defining the actual assembly.

This strategy was adapted to avoid having an extremely large number of very similar cells in the assembly.


Pins
----

.. attention::

   Although pins are constructed separately, their activation during irradiation calculations are still controlled by
   the parent assembly. This is why burnable materials are not assigned directly to the pins, but rather
   :term:`bundle_tag` instances.

Pin type assemblies are constructed by axially stacking two dimensional (radial) cell definitions. The latter is
constructed by the :class:`PinCell` macro, while the former is accomplished using a :class:`PinCellStack`. Both
are available form the :mod:`core.fuel_assembly` module.

Defining radial structure
+++++++++++++++++++++++++

Radial pin layers are constructed using the :ref:`PinCell <pin_cell>` structure. A detailed description of its general
use can be found :ref:`here <pin_cell>`. The :mod:`core.fuel_assembly` also provides the following convenience function
to create a typical fuel pin radial layout:

.. py:function:: fuel_assembly.fuel_pin(diameter, gap, clad, bundle=assembly.fuel_bundle(), gas=Helium(), cladding=Zircaloy4(), fill=BoratedWater())

   Create the radial structure of a typical fuel pin, consisting of a meat region, a gap and cladding.

   :param diameter: The diameter of the active region.
   :type diameter: :term:`length`
   :param gap: The gap size.
   :type gap: :term:`length`
   :param clad: The cladding thickness.
   :type clad: :term:`length`
   :param bundle: Contents of the active region.
   :type bundle: :term:`bundle_tag`
   :param gas: Material filling the gap between fuel and cladding.
   :type gas: :term:`material`
   :param cladding: Cladding material.
   :type cladding: :term:`material`
   :param fill: Material surrounding the pin.
   :type fill: :term:`material`

   :returns: A :ref:`PinCell <pin_cell>`.

   .. note::

      This macro is not general enough to treat more sophisticated cases, e.g integrated burnable absorber layers, or
      inclusion of crate like structures. In these cases the :ref:`PinCell <pin_cell>` construction should be used
      instead.



.. _pin_cell_stack:
Defining axial structure
++++++++++++++++++++++++

Different radial layers are stacked to form a complete pin using a :class:`PinCellStack`. It is contained in the
:mod:`core.fuel_assembly` module and is used in your build module as follows:

.. code-block:: py

   from core import *

   def build(*args, **kwargs):

       asm = fuel_assembly.PinCellStack(name='MY_REACTOR_some_awesome_pin')


Apart from all the standard :ref:`assembly parameters <Generic assembly>`, it also defines the following:

.. current_parameter_set:: pin_cell_stack
   :synopsys: core.fuel_assembly.PinCellStack

.. parameter:: top_fill_material
   :type: :term:`material`
   :default: `DryAir`

   The material that will be used to fill the space above the last layer added.

.. parameter:: bottom_fill_material
   :type: :term:`material`
   :default: `DryAir`

   The material that will be used to fill the space below the last layer added.

.. parameter:: pitch
   :type: :term:`length` or :term:`region`
   :default: None

   This parameter defines the lattice (cookie cutter) cell which this pin will fill. It is either

        - A single :term:`length` value, for a square pitch.
        - Two :term:`length` values, the :math:`x` and :math:`y` dimensions of a rectangular pitch.
        - Any CSG :term:`region`. For example a hexagon for hexagonal pin layouts.

   .. note::

      This parameter is optional, and will only be used to clip unbounded radial regions for visualization purposes.

The actual pin is constructed by adding consecutive layers using the following method:

.. py:function:: add_layer(lower_bound, upper_bound, data)

   Adds a new radial layer.

   :param lower_bound: The minimum axial (:math:`z`) value of the layer.
   :type lower_bound: :term:`length`
   :param upper_bound: The maximum axial (:math:`z`) value of the layer. Must be strictly larger than `lower_bound`.
   :type lower_bound: :term:`length`
   :param data: The radial structure of the layer. Usually a :ref:`PinCell <pin_cell>` instance, but can be any
        :term:`region`.

   .. attention::

      Currently, the layers must be added in an increasing fashion from bottom to top.

   .. note::

      Even though any :term:`region` can be added, make sure to use :ref:`PinCell <pin_cell>` for at least the active
      region, otherwise the burnable material zones will not be set up correctly.


Finally, the pin structure is created using the :py:`pin_cell_stack.build` method:

.. py:function:: build()

   Creates all the radial cells, and stacks them axially. Returns the resulting :ref:`Assembly <Generic assembly>`.

Example:

>>> from core import *
>>> from core.fuel_assembly import PinCell, PinCellStack, Helium, ZircAlloy4
>>> fuel = PinCell() | (0.39218 * units.cm, bundle_tags.fuel) | (0.40005 * units.cm, Helium()) | (0.45720 * units.cm, Zircalloy4())
>>> solid = PinCell() | (0.45720 * units.cm, Zircalloy4())
>>> stack = PinCellStack(name='REACTOR-fuel-pin')
>>> stack.add_layer(20.0000 * units.cm, 35.1600 * units.cm, solid)
>>> stack.add_layer(35.1600 * units.cm, 300 * units.cm, fuel)
>>> stack.add_layer(300 * units.cm, 315.16 * units.cm, solid)
>>> stack.pitch = 2.0 * 0.62992 * units.cm
>>> pin = stack.build()
>>> pin.show()

Pins with different burnable materials
++++++++++++++++++++++++++++++++++++++

Many pin type assemblies contain pins with different fuel material compositions. To accommodate this, additional
:term:`bundle_tag` should be used. New tags of the same type are created by passing an index. For example,

>>> from core import *
>>> from core.fuel_assembly import PinCell, PinCellStack, Helium, ZircAlloy4
>>> std = PinCell() | (0.39218 * units.cm, bundle_tags.fuel) | (0.40005 * units.cm, Helium()) | (0.45720 * units.cm, Zircalloy4())
>>> rpr = PinCell() | (0.39218 * units.cm, bundle_tags.fuel(1)) | (0.40005 * units.cm, Helium()) | (0.45720 * units.cm, Zircalloy4())

will assign the `fuel` tag with index 0 (the default) to the first pin, and the `fuel` tag with index 1 to the second
pin. These now represent two different burnable structures, which are managed independently. In particular, initial
material distributions must be assigned for each one. See  and `Depletion meshes for pin type assemblies`_ for more
information.

Axially heterogeneous materials
+++++++++++++++++++++++++++++++

Although axially heterogeneous fuel materials (e.g. breeder zones) can also be treated using the above
:term:`bundle_tag` mechanism (tags are assigned per radial region after all), this can become cumbersome. It is
recommended to used and explicit initial (axial) material mesh instead. See `Depletion meshes for pin type assemblies`_
for an example.

Guide tubes
+++++++++++

Guide tubes, used for control structures, irradiation targets or instruments, are added just like normal fuel pins (that
is, by :ref:`stacking <Defining axial structure>` different :ref:`radial <Defining radial structure>` zones). Instead of
assigning a :term:`bundle_tag` to the interior region, a :ref:`facility <Specifying loadable facilities>` name is
assigned. For example,

>>> from core import *
>>> from core.fuel_assembly import PinCell, ZircAlloy4
>>> gt = PinCell() | (0.56134 * units.cm, 'guide-tube') | (0.60198 * units.cm, ZircAlloy4())

This facility name is then used later to load targets, like control control fingers or absorbers.


Removable absorbers
+++++++++++++++++++

Non integrated absorbers, that is, absorbers which can be added and removed from the assembly, typically in guide tubes,
are modelled using a separate component. It can be constructed as a :ref:`pin <Pins>`, or any other assembly.

.. attention::

   Only the actual absorber structure should be modelled, and not the entire guide tube with the absorber. This is
   because the absorber will be loaded to the appropriately marked facility in the guide tube.

Absorbers are loaded using the :func:`pin_assembly.load_burnable_absorbers` method, or during core configuration.

.. note::

   If removable absorber activation should be tracked, the absorber rods should be made burnable and added to the
   inventory before loading into the assembly.


Control fingers
+++++++++++++++

The control rods entering intra assembly guides are modelled as separate :ref:`control <generic_control_type>`
assemblies. The one difference is that a single instance can be used, as it would be automatically duplicated to
produce all the fingers.

Pin Bundles
-----------

.. current_parameter_set:: pin_assembly
   :synopsys: core.fuel_assembly.RectangularPinAssembly

These are the assembly types that collects the pins in a particular layout, and adds some additional structure. They
are contained in the :mod:`fuel_bundle` module.

Although the assembly types differ in pin layout and assembly shape, they all allow for the inclusion of cavities in
the pin lattice. A cavity is any structure which breaks the regular lattice, and is typically used for additional
control mechanisms like rods or burnable absorbers. They are added using the following call:

.. py:function:: add_cavity(region)

   Register a the boundary of a hole in the pin lattice.

   :param region: The boundary of the hole.
   :type region: :term:`region`

   The pins specified in the :attr:`pin_assembly.pin_layout` fill the bounding region outside these cavities.

   .. attention::

      Only the **boundary** of the cavity should be specified here. All cells filling the cavity are specified using the
      normal :ref:`cell addition <assemblies_add_cells>` methods.

As with other assembly macros, after all parameters are specified, cells are constructed by calling :py:`construct_bundle()`.

Square and rectangular pin bundles
++++++++++++++++++++++++++++++++++

Assemblies with pins arranged in a rectangular mesh. There are two types in :mod:`core.fuel_assembly`:

    1. :class:`SquarePinBundle` (or :class:`RectangularPinBundle`), which defines only the cells containing the pin
       lattice.
    2. :class:`SquarePinAssembly` (or :class:`RectangularPinAssembly`), which also defines additional sleeve or
       spacer cells outside the pin lattice.

Since both are :class:`Assemblies`, all the standard :ref:`assembly parameters <Generic assembly>` are available. In
addition, the following parameters are exposed:

.. parameter:: size
   :type: :term:`tuple`
   :default: Required

   The number of rows and columns of the pin lattice (grid). For example, :py:`asm.size = 17, 18` means there are
   17 rows (stacked along the :math:`y`-axis) and 18 columns in the pin lattice.

.. parameter:: pin_cell_pitch
   :type: :term:`length` or :term:`tuple` (:term:`length`)
   :default: Required

   The pitch, or distance between neighboring pins in the lattice. Either a single :term:`length` value if the lattice
   is square, or a two :term:`length` values if the lattice is rectangular. In the latter case the :math:`x` distance
   (neighboring columns) is given first, and the :math:`y` distance (between neighboring rows) is the second value.

   .. note::

      Only fixed pitch, regular lattices are allowed.

.. parameter:: filler
   :type: :term:`material` or :term:`component`
   :default: None

   The material or component that should be used in the empty positions (denoted by the :term:`placeholder` :py:`_`) in
   the layout grid. This parameter is only required if there are empty positions in the grid, which typically arise if
   cavities (as specified using :func:`pin_assembly.add_cavity`) are present.

.. parameter:: pin_layout
   :type: :term:`grid`
   :default: Required

   The layout of pins in the rectangular lattice. This is specified as a two dimensional grid of size equal to
   :attr:`pin_assembly.size`, containing :ref:`pin <Pins>` or empty :term:`placeholder` tags.

   .. code-block:: py

      from core import *
      from . import assemblies

      fp = assemblies.MY_REACTOR_fuel_pin()
      gt = assemblies.MY_REACTOR_guide_pin()

      fa.pin_layout = \
        [[fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, fp, fp, fp],
         [fp, fp, fp, gt, fp, fp, fp, fp, fp, fp, fp, fp, fp, gt, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, gt, fp, fp, fp, fp, fp, fp, fp, fp, fp, gt, fp, fp, fp],
         [fp, fp, fp, fp, fp, gt, fp, fp, gt, fp, fp, gt, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp],
         [fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp, fp]
         ]

   Only the symmetric portion of the pin layout may be specified. For example, the above can be simplified by only
   specifying the upper left corner (1/4 of assembly), and then unfolding:

   .. code-block:: py


      fa.pin_layout = \
          [[fp, fp, fp, fp, fp, fp, fp, fp, fa],
           [fp, fp, fp, fp, fp, fp, fp, fp, fp],
           [fp, fp, fp, fp, fp, gt, fp, fp, gt],
           [fp, fp, fp, gt, fp, fp, fp, fp, fp],
           [fp, fp, fp, fp, fp, fp, fp, fp, fp],
           [fp, fp, gt, fp, fp, gt, fp, fp, gt],
           [fp, fp, fp, fp, fp, fp, fp, fp, fp],
           [fp, fp, fp, fp, fp, fp, fp, fp, fp],
           [fp, fp, gt, fp, fp, gt, fp, fp, ip]]

      fa.pin_layout = fa.pin_layout.unfold('r', 'b')

   .. attention::

      All rows and column entries must be specified. If there are cavities (added using :func:`pin_assembly.add_cavity`) in the lattice, use the
      empty :term:`placeholder` :py:`_` in the positions that will be clipped.


.. parameter:: axial_region
   :type: :term:`region`
   :default: None

   Specify the axial bound of the region containing the pin cell lattice. The radial bound is deduced
   automatically from :attr:`pin_assembly.size` and :attr:`pin_assembly.pin_cell_pitch`.

   If not specified, the assembly will not be bounded axially (which is usually not a problem).

.. parameter:: background_material
   :type: :term:`material`

   The material that should be used to fill the region outside the pin lattice.

   .. note::

      Do **not** specify this parameter if you plan to add custom cells (e.g an assembly box) outside the pin lattice!

   .. attention::

      If this parameter is not specified, no cells will be added around the lattice. Thus, in order complete the
      assembly construction, these cells must be added automatically, or one of the
      :ref:`mesh completion <Auto generating background or missing cells>` mechanisms must be used.


The :class:`SquarePinAssembly` (or :class:`RectangularPinAssembly`) extends the basic :class:`SquarePinBundle` by adding
spacer regions outside the pin lattice. In order to accommodate this, the following additional parameters are required:

.. parameter:: spacer_pitch
   :type: :term:`length` or :term:`tuple` [:term:`length`]

   This is the total pitch of assembly including the sleeve region. It must be larger that the size of the pin lattice,
   but less than or equal to the actual core pitch (distance between neighboring assemblies in the reactor core).

   Either a single :term:`length` value if the pitch is square, or a :term:`tuple` of two :term:`length` values for a
   rectangular pitch.

   .. note::

      This parameter is optional, and if it is **not** specified, the assembly will be equivalent to a
      :class:`SquarePinBundle`.

.. parameter:: sleeve_material
   :type: :term:`material`
   :default: None

   Material that fills the axial gap between spacer sleeve layers. Will be initiated to :attr:`pin_assembly.background_material` if
   this parameter was set.

The spacer sleeves (which usually coincides with crate structures within the pin lattice) are from bottom to top using
the following:

.. py:function:: add_spacer(lower, upper, material)

   :param lower: The lower axial bound of the sleeve region.
   :type lower: :term:`length`
   :param upper: The upper axial bound of the sleeve region.
   :type lower: :term:`length`
   :param material: The material which should fill the sleeve region between `lower` and `upper`.

   The spacers must be added in strictly increasing order. The regions between the spacers are filled with
   :attr:`pin_assembly.sleeve_material`.

.. _pin_bundle_spacers:

.. figure:: images/pin-sleeve-il.png
   :align: center

   Schematic showing were the sleeve spacers are present in the final assembly.




Hexagonal pin bundles
+++++++++++++++++++++

Under construction!


Loading burnable absorbers
--------------------------

All pin bundle type assemblies also allow burnable absorbers to be loaded using the following method:

.. py:function:: load_burnable_absorbers(layout, facility_name=None)

   Loads absorbers into specified positions.

   :param layout: A grid giving the absorber instances to load. The grid should have the same dimensions as the
         assembly :attr:`pin_assembly.size`. Use empty :term:`placeholder` tokens :py:`_` to denote positions that can not be loaded
         or should be skipped. E
   :type layout: :term:`grid` [:term:`component`]
   :param str facility_name: The facility to target. If not specified, the first facility added to a :ref:`pin <Guide tubes>`
         will be assumed.

Removable absorber patterns can also be specified and loaded during :ref:`core configuration <Building Core Configurations>`,
using the :attr:`model.removable_absorber_load_map` parameter.

.. note::

   By its very definition, removable absorber loading is usually not defined in the assembly build script.



Depletion meshes for pin type assemblies
----------------------------------------

The principal fuel (or burnable) primitive in these assemblies are `Pins`_, and :ref:`bundles <Grouping primitives into bundles>`
represent groups of pins. Pins are bundled by assigning them an index in the pin grid. For example, if the
assembly has a :math:`5\times 5` pin grid, then:

.. code-block:: py

   asm.burn_bundles[bundle_tags.fuel].depletion_mesh.bundle =\
    [[0, 0, 0, 0, 0],
     [0, 1, 1, 1, 0],
     [0, 1, 1, 1, 0],
     [0, 1, 1, 1, 0],
     [0, 0, 0, 0, 0]]

will group (and therefore activate) all outside pins together, while the interior pins belong to another group. Use
the empty :term:`placeholder` token :py:`_` to indicate positions that are not burnable pins (e.g instrument or
control positions), or are managed by another bundle.

Grouping the pins in other fuel bundles or burnable absorber bundles, works exactly the same way.

.. attention::

   If you have multiple bundles remember to initiate their materials. For example,

   .. code-block::

      asm.fuel_bundles[bundle_tags.fuel] = materials.base_fuel()
      asm.fuel_bundles[bundle_tags.fuel(1)] = materials.second_fuel_type()

      asm.burn_bundles[bundle_tags.ba] = materials.ba_material()
      #etc

The :ref:`radial segments <Radial meshing>` parameter of the fuel bundle's depletion mesh divides the pins into radial
rings. For example, in the above example case:

.. code-block:: py

   asm.burn_bundles[bundle_tags.fuel].depletion_mesh.segments = \
        [[0.5, 0.35, 0.15], [0.5, 0.5]]

will subdivide the outer pins (bundle 0) into three rings, with the inner most region containing 50% of the total area,
the second 35% and the final ring 15%. The inner pins (bundle 1), is subdivided into two equal sized rings.

Finally, as with other primitives, the :ref:`axial mesh <Axial meshing>` divides the pins along their active
height. If the pin contains different initial materials along its axial height (e.g. breeder zones), it is specified
by manually setting the :ref:`axial mesh <Axial meshing>`, and the initial material distribution. For example,

.. code-block:: py

   asm.burn_bundles[bundle_tags.fuel].depletion_mesh.axial_layers =\
    [[0.25, 0.25, 0.25, 0.25]] * 2

   asm.burn_bundles[bundle_tags.fuel] =\
    [materials.breeding(), materials.fuel(), materials.fuel(), materials.breeding()]

will divide the pins into 4 axial zones, with different initial material compositions.

.. note::

   The materials will be assigned to all the pin bundles for that bundle tag. Pins with different axial structure should
   be handled by assigning them a different `bundle tag <Pins with different burnable materials>`_.

.. attention::

   Currently, if the axial structure and initial material distribution is defined here, it cannot be refined later. Thus
   make sure you use a fine enough mesh for depletion purposes. This restriction will be lifted in future versions.

.. _lwr_examples:

Examples
--------

Basic fuel pin
++++++++++++++

.. literalinclude:: examples/pin_pwr.py


Guide pin
+++++++++

.. literalinclude:: examples/guide_pin_pwr.py

Pin assembly
++++++++++++

.. literalinclude:: examples/pin_asm.py
