Additional Resources
********************

Introduction
============

The **OSCAR-5** system is distributed with various pracitcal examples and tutorials to help guide the user in the
optimal use of the system. This section of the user guide contains a list of all additional resources at the user's
disposal.

Tutorials
=========

The set of tutorials aim to guide the user through the major steps involved in preparing and executing reactor
calculations with **OSCAR-5**. Basic model building and applications such as reload and core-follow calculations are
explained in a step by step manner.

The tutorial set includes:

 1. Your first OSCAR-5 run: Single assembly reactor
 2. Building a reactor model with OSCAR-5, Part 1: Mini core heterogeneous model
 3. Building a reactor model with OSCAR-5, Part 2: Mini core homogeneous model
 4. Running corefollow and related applications with OSCAR-5: Full core applications

.. _additional_video_tutorials:
Video Tutorials
===============

Video tutorials covering more advanced features of **OSCAR-5**:


 1. Working with the FreeCAD extension, Part I

 .. raw:: html

    <video controls src="_static/videos/freecad-link-tutorial-1-cmp.mov"></video>

 2. Working with the FreeCAD extension, Part II

 .. raw:: html

     <video controls src="_static/videos/freecad-link-tutorial-2.mp4"></video>

 3. Working with the FreeCAD extension, Part II

 .. raw:: html

     <video controls src="_static/videos/freecad-link-tutorial-3.mp4"></video>

 4. Introduction to the OSCAR-5 GUI

.. _additional_presentations:
Presentations
=============

There are a number of presentations associated with the **OSCAR-5** tutorials:

 1. OSCAR-5 Theory
 2. Underlying codes theory
 3. How to run the tutorials
 4. Tut 1 presentation
 5. Tut 2 part 1 presentation
 6. Tut 2 part 2 presentation
 7. Tut 3 presentation

Additional presentations, not related to the tutorial set:

 1. :download:`Quick overview of the model documentation mechanism <../presentations/OSCAR-5-document-models.pdf>`

Benchmark examples
==================

A number of benchmark examples are included in the **OSCAR-5** package. These benchmarks serve as an excellent validation
platform as well as practical examples of how to construct reactor models and create applications for various target
codes with the **OSCAR-5** reactor analysis platform.

The overhead document for these benchmark models can be found in the :ref:`Benchmark set` document, where each
benchmark model is briefly discussed and the notable features illustrated with regards to **OSCAR-5** functionality
as well as reactor modelling in general.

The benchmark set includes models for:

 1. ETRR-2, Egypt Test and Research Reactor
 2. IRR1, Israeli Research Reactor
 3. MNR, McMaster Nuclear Reactor
 4. OPAL, Open-Pool Australian Lightwater reactor
 5. SAFARI-1, South Africa Fundamental Atomic Research Installation reactor