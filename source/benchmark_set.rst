Benchmark set
*************

Introduction
============

A number of benchmark examples are included in the **OSCAR-5** package. These benchmarks serve as an excellent
validation platform as well as practical examples of how to construct reactor models and create applications for
various target codes with the **OSCAR-5** reactor analysis platform. This document briefly outlines each benchmark
model and describes the notable features illustrated in each model with regard to **OSCAR-5** functionality as well as reactor modelling in general.

The benchmark set includes models for:

 1. ETRR-2, Egypt Test and Research Reactor
 2. IRR1, Israeli Research Reactor
 3. MNR, McMaster Nuclear Reactor
 4. OPAL, Open-Pool Australian Lightwater reactor
 5. SAFARI-1, South Africa Fundamental Atomic Research Installation reactor

ETRR-2
======

The Egypt Test and Research Reactor, number 2, at the Nuclear Research Centre in Egypt.

ETRR-2 benchmark source
-----------------------

Part of the IAEA CRP 1496: Innovative Methods in Research Reactor Analysis – Benchmarking Against Experimental Data.

Also part of the followup IAEA CRP T12029: Benchmark of Computational Tools against Experimental Data on Fuel Burnup
and Material Activation for Utilization, Operation and Safety Analysis of Research Reactors.

ETRR-2 facility overview
------------------------

ETRR-2 is a 22 MW, open pool research reactor with light water as coolant and moderator and with beryllium as
reflector. This reactor is an array of fuel assemblies, inter-assembly absorber plates inside guide-boxes, a double
walled chimney and irradiation boxes. The grid pithc is 8.1cm x 8.1cm and the ex-core grid pitch is 7.87cm x 7.87cm.

There is a total of 29 fuel assemblies with plate type fuel of 19.7% enriched :math:`U_3O_8`. The 6 control blades are
intra-assembly type with Ag-In-Cd absorber material.

ETRR-2 experimental description
-------------------------------

Two categories of experimental data is available in this benchmark.

 1) Control rod calibrations

   a) Rod calibrations done on several commissioning cores with fresh fuel

 2) Fuel burnup

   a) Burnup over four cycles (with limited plant data available)
   b) Discharge burnup of three fuel assemblies measured using gamma spectroscopy

ETRR-2 model description
------------------------

"*Provide a summary of the OSCAR-5 model description and link to the auto doc for ETRR-2.*"

Notable features of ETRR-2 benchmark
------------------------------------

There are several notable features for the ETRR-2 benchmark. These can be split into notable features of the
ETRR-2 reactor, notable features of the benchmark and notable features used in **OSCAR-5** to model this reactor and
benchmark calculations.

 1) Reactor features

   a) Intra-assembly control rods
   b) Customizable core and ex-core configuration

 2) Benchmark features

   a) Experiments done on commissioning cores with fresh fuel
   b) Burnup measurements of three fuel assemblies

 3) **OSCAR-5** features

   a) Customizing core and pool configurations for different cycles
   b) Using the application drivers for rod calibration


IRR1
====

The Israeli Research Reactor, number 1, at the Soreq Nuclear Research Centre in Israel.

"*This part of the documentation is still under construction.*"

MNR
===

The McMaster Nuclear Reactor, at the McMaster University in Canada.

MNR benchmark source
--------------------

Part of the IAEA CRP 1496: Innovative Methods in Research Reactor Analysis – Benchmarking Against Experimental Data.

MNR facitily overview
---------------------

The MNR reactor is an open pool MTR type research reactor operating at 3 MW thermal power. The configurable grid
consists of 9 x 6 positions and is loaded with 28 fuel assemblies, 6 intra-assembly control rods and graphite, lead
and beryllium reflectors with in-core irradiation positions. There are 6 ex-core beam tubes as well as further ex-core
irradiation facilities. The reactor is light water moderated and graphite reflected.

Fuel assemblies consist of LEU (19.75% enriched) :math:`U_3Si_2-Al` material as well as one burnt HEU assembly. The 6
shim control rods have Ag-In-Cd absorber and the 1 regulating rod is made of stainless steel.

MNR experimental description
----------------------------

Four categories of experimental data is available in this benchmark.

 1) Core change measurements

   a) Rod worth measurements.
   b) Reactivity change measurements (fuel movements and additions, irradiation vessel and capsule loading).
   c) Radial flux wire mapping (in all fuel assemblies).
   d) Burnup (21.9 MWd).

 2) Axial flux wire experiment

   a) Low power flux wire measurements in sample irradiation positions.

 3) Simulated void experiment

   a) Critical rod measurements during aluminium plate insertion into coolant channels of fuel assemblies.

 4) Pool temperature experiment

   a) Critical rod measurements during heating of primary system.

MNR model description
---------------------

"*Provide a summary of the OSCAR-5 model description and link to the auto doc for MNR.*"

Notable features of MNR benchmark
---------------------------------

There are several notable features for the MNR benchmark. These can be split into notable features of the MNR reactor,
notable features of the benchmark and notable features used in **OSCAR-5** to model this reactor and benchmark calculations.

 1) Reactor features

   a) Intra-assembly control rods
   b) Large amounts of graphite and lead
   c) High neutron leakage at two core sides
   d) Curved fuel, graphite and beryllium assemblies

 2) Benchmark features

   a) Extensive set of neutronic experimental data

 3) **OSCAR-5** features

   a) Modelling intra-assembly control rods
   b) Modelling curved fuel and other assemblies
   c) Importing an inventory from a previous OSCAR-4 model
   d) Using advanced cOMPoSe features for albedo boundary conditions and lattice coloursets

OPAL
====

The Open-Pool Australian Lightwater reactor, at the Australian Nuclear Science and Technology Organisation (ANSTO) in
Australia.

OPAL benchmark source
---------------------

Part of the IAEA CRP 1496: Innovative Methods in Research Reactor Analysis – Benchmarking Against Experimental Data.

Also part of the followup IAEA CRP T12029: Benchmark of Computational Tools against Experimental Data on Fuel Burnup
and Material Activation for Utilization, Operation and Safety Analysis of Research Reactors.

OPAL facitily overview
----------------------

The OPAL reactor is a 20 MW open-pool type research reactor. The reactor is cooled and moderated with light water and
reflected with heavy water. The compact core consists of 16 plate-type fuel assemblies arranged in a 4x4 grid, with
four control blades and a central cruciform control blade inserted in between the assemblies. The heavy water reflector
vessel surrounding the core is populated with beam tubes, irradiation facilities and a cold neutron source.

The fuel assemblies have 19.8% enriched :math:`U_3Si_2-Al` material with burnable cadmium wires in the side plates of
two of the three fuel types (three types with different uranium loading). The control blades have hafnium absorber
material.

OPAL experimental description
-----------------------------

Two categories of experimental data is available for this benchmark.

 1) Control rod calibration experiments

   a) Commissioning experiments on all rods

 2) Operational data

   a) Core-follow data for seven cycles, from commissioning core

 3) Material activation experiment

   a) Gold foil experiments

OPAL model description
----------------------

"*Provide a summary of the OSCAR-5 model and link to the auto doc for MNR.*"

Notable features of OPAL benchmark
----------------------------------

There are several notable features for the OPAL benchmark. These can be split into notable features of the
OPAL reactor, notable features of the benchmark and notable features used in **OSCAR-5** to model this reactor and
benchmark calculations.

 1) Reactor features

   a) Compact core with high leakage
   b) Heavy water reflector
   c) Cold neutron source

 2) Benchmark features

   a) Control rod calibration data for a commissioning core
   b) Core-follow plant data over an extensive burnup period

 3) **OSCAR-5** features

   a) Modelling intra-assembly control blades
   b) Modelling integrated burnable absorbers in fuel assemblies
   c) Using the application drivers for rod calibration and core-follow calculations
   d) Using advanced cOMPoSe features for fuel assembly segmentation and albedo boundary conditions
   e) Modelling multi-cycle operation

SAFARI-1
========

The South Africa Fundamental Atomic Research Installation reactor, number 1, at the South African Nuclear Energy
Corporation (Necsa) in South Africa.

SAFARI-1 benchmark source
-------------------------

Part of the IAEA CRP T12029: Benchmark of Computational Tools against Experimental Data on Fuel Burnup and Material
Activation for Utilization, Operation and Safety Analysis of Research Reactors.

SAFARI-1 facitily overview
--------------------------

The SAFARI-1 reactor is a tank-in-pool type MTR reactor operating at 20 MW thermal. The configurable grid consists of
9 x 8 positions, hosting 26 fuel assemblies and 6 fuel follower-type control assemblies. Other in-core positions house
beryllium, lead and aluminium reflectors and irradiation positions. Ex-core there are irradiation facilities and 7
beam tubes.

Fuel assemblies are LEU (19.75% enriched) with U 3 Si 2 -Al material. The control assemblies are follower-type with
fuel assemblies attached to the bottom of each absorber section. The absorber sections consist of a cadmium box inside
an aluminium frame.

SAFARI-1 experimental description
---------------------------------

Two categories of experimental data is available for this benchmark.

 1) Multi-cycle operational data

   a) Operational history for 1 year, plant data in fine scale and averaged
   b) Low power flux wire experiments at start of each (in all fuel and fuel follower assemblies)
   c) Control rod calibration experiments for five cycles

 2) Beryllium poison concentration estimates

   a) Approximate fluence and spectra history over the lifetime of beryllium elements
   b) Beryllium poison related spectral measurements at start of operational data

SAFARI-1 model description
--------------------------

"*Provide a summary of the OSCAR-5 model and link to the auto doc for SAFARI.*"

Notable features of SAFARI-1 benchmark
--------------------------------------

There are several notable features for the SAFARI-1 benchmark. These can be split into notable features of the
SAFARI-1 reactor, notable features of the benchmark and notable features used in **OSCAR-5** to model this reactor and
benchmark calculations.

 1) Reactor features

   a) Follower control rods
   b) High leakage at pool side of core
   c) Highly heterogeneous core configuration

 2) Benchmark features

   a) Core-follow plant data over an extensive burnup period
   b) Flux wire and control rod data over multiple cycles
   c) Fresh beryllium loaded at start of operational time for benchmark

 3) **OSCAR-5** features

   a) Modelling follower-type control rods
   b) Importing an inventory for burnable materials
   c) Importing a reactor model from MCNP
   d) Using the application drivers for rod calibration, flux wire mapping and core-follow calculations
   e) Modeling complicated rig movements
   f) Modelling beryllium poison
   g) Modelling multi-cycle operation
   h) Calculating thermal-hydraulic feedback