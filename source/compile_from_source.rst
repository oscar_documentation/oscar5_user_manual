Compiling from Source
*********************

.. attention::

   This section is now critically out of date!

This section contains instructions for compiling the OSCAR-5 distribution from source. Currently only Linux-based
systems are supported.


System dependencies
-------------------

The following sections lists the major system dependencies required to compile the source distribution:

Complete C++/C build environment
++++++++++++++++++++++++++++++++

A valid :command:`C++` compiler and associated :command:`std` library must be available. Compilation has been tested
on the following compilers:

   1. GNU :command:`g++ v4.8.5`
   2. Apple :command:`LLVM v8.10`
   3. GNU :command:`g++ v7.x.x`


==================   ==========================   ===============================================
Linux Distribution   Package Name(s)              Command
==================   ==========================   ===============================================
Debian (Ubuntu)      ``build-essential``          ``sudo apt-get install build-essential``
CentOS               ``'Development Tools'``      ``sudo yum groupinstall 'Development Tools'``
==================   ==========================   ===============================================


Python and python development package
+++++++++++++++++++++++++++++++++++++

Since :command:`rapyds` makes use of :command:`C++` python bindings, the python development library is required.

==================   ================================  =================================================
Linux Distribution   Package Name(s)                   Command
==================   ================================  =================================================
Debian (Ubuntu)      ``python2.7``                     ``sudo apt-get install python2.7``
     \               ``python2.7-dev``                 ``sudo apt-get install python2.7-dev``
CentOS               ``python27-python-devel.x86_64``  ``sudo yum install python27-python-devel.x86_64``
==================   ================================  =================================================

Boost
+++++

All extensions require the `Boost <https://boost.org>` :command:`C++` library. Compilation has only been tested using
version 1.58, but any version larger than this should work. This version is distributed with the source under the
**third_party** directory.

.. attention::

   The **boost.serialization** package is not guaranteed to be backwards compatible. Thus, if you want to share
   your archived data between different users, make sure that the same version of **boost** is use.

CMake
+++++

The system uses the cross platform utility `cmake <https://cmake.org/>`_ as its main build configuration tool. Any
version larger than 2.8.11 can be used.

==================   ==========================  ==========================================================
Linux Distribution   Package Name(s)             Command
==================   ==========================  ==========================================================
Debian (Ubuntu)      ``cmake``                   ``sudo apt-get install cmake``
CentOS               ``cmake``                   ``sudo yum install cmake``
==================   ==========================  ==========================================================

High precision libraries
++++++++++++++++++++++++

The `Computational Geometry Algorithms Library (CGAL) <https://www.cgal.org/>`_, which is used extensively for geometry
processing, requires the high precision libraries `GMP <http://gmplib.org/>`_ and `MPFR <http://www.mpfr.org/>`_.

==================   ================================  =================================================
Linux Distribution   Package Name(s)                   Command
==================   ================================  =================================================
Debian (Ubuntu)      ``libgmp-dev``, ``libmpfr-dev``   ``sudo apt-get install libgmp-dev libmpfr-dev``
CentOS               ``gmp-devel``, ``mpfr-devel``     ``sudo yum install gmp-devel mpfr-devel``
==================   ================================  =================================================

Miscellaneous
+++++++++++++

The python package used to communicate with remote servers requires the :command:`ffi` development libraries.

==================   ==========================  ==========================================================
Linux Distribution   Package Name(s)             Command
==================   ==========================  ==========================================================
Debian (Ubuntu)      ``libffi-dev``              ``sudo apt-get install libffi-dev``
CentOS               ``libffi-devel``            ``sudo yum install -y libffi libffi-devel``
==================   ==========================  ==========================================================


Additional python dependencies
------------------------------

The following sections list python specific dependencies (extensions) that is better to install system wide.

Matplotlib
++++++++++

The extensive python plotting framework `matplotlib <https://matplotlib.org>`_ is used to create static and interactive
plots. Although it can be installed via a python distribution tool (like :command:`pip`), you can prevent a lot of
compilation headaches by using the package available in your distribution.

==================   ==========================  ==========================================================
Linux Distribution   Package Name(s)             Command
==================   ==========================  ==========================================================
Debian (Ubuntu)      ``python-matplotlib``       ``sudo apt-get install python-matplotlib``
CentOS               ``python-matplotlib``       ``sudo yum install python-matplotlib``
==================   ==========================  ==========================================================

VTK python bindings
+++++++++++++++++++

Required for three dimensional visualization using the `VTK <http://vtk.org>`_ pipeline.

==================   ==========================  ==========================================================
Linux Distribution   Package Name(s)             Command
==================   ==========================  ==========================================================
Debian (Ubuntu)      ``python-vtk``              ``sudo apt-get install python-vtk``
CentOS               ``vtk-python``              ``sudo yum install vtk-python``
==================   ==========================  ==========================================================


QT4 python bindings
+++++++++++++++++++

Required for various user interface elements.

==================   ==========================  ==========================================================
Linux Distribution   Package Name(s)             Command
==================   ==========================  ==========================================================
Debian (Ubuntu)      ``python-qt4``              ``sudo apt-get install python-qt4``
CentOS               ``PyQt4``                   ``sudo yum install PyQt4``
==================   ==========================  ==========================================================

Building external dependencies
------------------------------

The **third_party** directory contains a number of third party packages that you should compile if they are not
already available on the system.

Boost
+++++

In a terminal:

.. code-block:: console

    cd OSDEV_ROOT/third_party/
    tar -xvf boost_1_58_0.tar.bz2
    cd boost_1_58_0
    ./bootstrap.sh
    ./b2

If you are able (and willing) to install this boost version system wide:

.. code-block:: console

    sudo ./b2 install

otherwise, you can point :command:`cmake` to this local **boost** when compiling the extensions.

CGAL
++++

In a terminal:

.. code-block:: console

   cd osdev/third_party/
   tar -xvf CGAL-4.6.3.tar.bz2
   cd CGAL-4.6.3
   mkdir build
   cd build
   cmake ..
   make
   sudo make install

.. note::

   The bundled CGAL version includes a modification that allows thread safe reference counting of objects, which
   enables multi-threaded builds of polyhedral structures. If you want to use your own source distribution, you can
   apply the patch located in the **patches** directory. See the **README** file for instructions.


Building and installing the extensions
--------------------------------------

Finally, the extension libraries can be compiled and installed:

.. code-block:: console

   cd OSDEV_ROOT
   mkdir build
   cd build
   cmake ..
   make
   make install

Pointing to local boost
+++++++++++++++++++++++

If you want to use a local boost installation, pass its installation root to the :command:`cmake` command using the
``DBOOST_ROOT`` flag. For instance,

.. code-block:: console

   cd OSDEV_ROOT
   mkdir build
   cd build
   cmake .. -DBoost_NO_SYSTEM_PATHS=TRUE  -DBOOST_ROOT:PATHNAME=OSDEV_ROOT/third_party/boost_1_58_0
   make
   make install

Setup on local host
-------------------

Setup python package management
+++++++++++++++++++++++++++++++

If not already installed, you will need the python package management utility :command:`pip`.

==================   ==========================   ===========================================
Linux Distribution   Package Name(s)              Command
==================   ==========================   ===========================================
Debian (Ubuntu)      ``python-pip``               ``sudo apt-get install python-pip``
CentOS               ``python-pip``               ``yum -y install python-pip``
==================   ==========================   ===========================================

Configuring a virtual environment
+++++++++++++++++++++++++++++++++

When using the pre-processing system :command:`rapyds` locally, it is recommended to use a python virtual
environment. This avoids creating conflicts with your existing system packages.

.. note::

    A python virtual environment is a fully function python system, including an interpreter and installed packages,
    which is isolated from the system wide python environment. Thus, packages installed in a virtual environment will
    only be available in that environment, and won't interfere with the default system.


Get python virtual environment utilities
++++++++++++++++++++++++++++++++++++++++

Activate python virtual environment using :command:`pip`:

.. code-block:: console

   $ sudo pip install virtualenv

The `virtualenvwrapper <http://virtualenvwrapper.readthedocs.io/en/latest/>`_ greatly simplifies the creation and
use of virtual environments. Install it with :command:`pip`:

.. code-block:: console

   $ sudo pip install virtualenvwrapper


Creating a virtual environment
++++++++++++++++++++++++++++++

Create virtual environment called *rapyds*:

.. code-block:: console

   $ mkvirtualenv rapyds --system-site-packages

Complete system configuration
+++++++++++++++++++++++++++++

To complete the installation run :command:`setup.py` located in the installation root directory. If using a
virtual environment, activate it first:

.. code-block:: console

   $ workon rapyds
   $(rapyds) python setup.py --first-time

otherwise:

.. code-block:: console

   $ python setup.py --system-wide