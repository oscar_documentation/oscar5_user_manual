.. _docker : https://docker.com/community-edition

Using a Docker Container
========================

You will need a working docker installation. See https://docker.com/community-edition for instructions on
how to obtain and configure docker for your operating system.

.. note::

   Since some features (like hardware acceleration) is generally not available in a Docker container, this installation
   method should only be used on operating systems which lacks a native release, or where the installation of
   third party software (other than Docker) is restricted. 

Linux
-----

Let :command:`OSCAR5_ROOT` denote the root directory containing the OSCAR-5 distribution on your system. For the purpose
of this manual, we will assume that it is :command:`$HOME/oscar5`. Inside this directory should be a number of
subdirectories, as well as a :command:`Dockerfile`.

Step 1: Build a base image
++++++++++++++++++++++++++

Enter the installation directory, and create an image by invoking :command:`docker build`:

.. code-block:: console

   $ cd $HOME/oscar5
   $ docker build -t rel/oscar:1.0 .

This will create an image called ``rel/oscar:1.0``. You can change the image tag to anything you want, as long as you
stick to the docker image naming rules.

.. note::

   Depending on how docker was configured, calls to docker might require super user privileges.

Step 2: Create a container
++++++++++++++++++++++++++

Next, we create a container based on the image you built in :ref:`Step 1<Step 1: Build a base image>`. You must define
mount points for all directories you want to have access to while working in the container. At a minimum,
:command:`OSCAR5_ROOT` must be mounted, but you should also mount your as well as any data directories the system will
read data from.

If you haven't done so already create a application root directory, which will contain all your reactor models. For
example,

.. code-block:: console

   $ mkdir $HOME/my_apps

Now create a container called *oscar5* using the following command:

.. code-block:: console

   $ docker run --name oscar5 -it  \
      -v $HOME/oscar5:/oscar5      \
      -v $HOME/my_apps:/my_apps \
      -v /path/to/data:/data   \
      -e DISPLAY=$DISPLAY  -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
      rel/oscar:1.0 /bin/bash

.. note::

    Shared mounts (volumes) are created with the :command:`-v` switch, which has the format
    ``[local directory]:[container directory]``. You can add as many mount points as you want.

The second to last line sets up display sharing between the host and the container, while the last line lists the
base image (you should change the name to whatever you called your base image in :ref:`Step 1<Step 1: Build a base image>`),
and defines the entry point. Since we set the entry point to :command:`/bin/bash` you will now be in an interactive
shell in the container. To complete the container configuration, simply run the installation script:

.. code-block:: console

   # python /oscar5/install.py --system-wide

To exit the container,

.. code-block:: console

   # exit

and to enter the container again:

.. code-block:: console

   $ docker exec -it oscar5 /bin/bash

.. note::

   If the container was stopped or the host was shut down, you need to start the container first ``docker start oscar5``.

.. attention::

   Before continuing, either exit or re-enter the container, or setup your environment (for this session only) by typing

   .. code-block:: console

      # source /oscar5/.bashrc

Configuring a display server for Windows
----------------------------------------

An X-server is required to view and use GUI components from within the docker container. We recommend using
`xming <http://www.straightrunning.com/XmingNotes/>`__, but there are some other alternatives. It can be downloaded
`here <https://sourceforge.net/projects/xming/>`__.

After running the installer, launch XLaunch to configure the server. Most of the setting can be left at their default
settings, but make sure that the No Access Control box is ticked on the third screen:

.. image:: images/xming-setup-1.png

On the last screen, be sure to save the configuration:

.. image:: images/xming-setup-2.png

Then, when launching Xming, be sure to select your saved configuration:

.. image:: images/xming-launch.png


.. warning::

   Some aspects of three dimensional visualization through the docker container on Windows are currently not working
   correctly. In particular, the in GUI VTK viewer (launched with the **--interactive** switch) does not always work.
   Hardware acceleration is also not available. We are working on resolving these issues, but for now basic result
   viewers seem to work.

Windows 7 & 8
-------------

.. attention::

   Since Windows 10 has native virtualization support, it is easier to use docker containers on this version. The Docker Toolbox
   solution described in this section uses a (light weight) virtual machine in the background, and will be less
   efficient. Moreover, Docker Toolbox is a legacy support tool that will likely be discontinued at some point. Thus, if
   possible, upgrade to Windows 10, but in the meantime this solution seems to work just fine.

Place the **oscar5** distribution somewhere in ``C:\Users``. For the remainder of the instructions, we will assume
that you placed **oscar5** in your local user directory ``C:\Users\<username>``, that is:

.. code-block:: console

   C:\Users\<username>\oscar5

.. note::

   By default, Toolbox only has access to the ``C:\Users`` directory and mounts it into the Virtual Machine at
   ``/c/Users``. Thus all working directories you later want to mount to the container must be in ``C:\Users``.

Step 1: Install docker
++++++++++++++++++++++

Download, and follow the
`Docker Toolbox installation instructions <https://docs.docker.com/toolbox/toolbox_install_windows/>`_. Since the
toolbox will also install a virtual machine, administrative rights are required.


Step 2: Build a base image
++++++++++++++++++++++++++

Open the Docker QuickStart Terminal, by running the link located in your Start menu or on the Desktop:

.. image:: images/docker-icons.png

Once in the terminal window prompt, change to the **oscar5** root directory:

.. code-block:: console

   $ cd  /c/Users/username/oscar5

Then create an image by invoking :command:`docker build`:

.. code-block:: console

   $ docker build -t rel/oscar:1.0 .

This will create an image called ``rel/oscar:1.0``. You can change the image tag to anything you want, as long as you
stick to the docker image naming rules.

Step 3: Create a container
++++++++++++++++++++++++++

Next, we create a container named ``oscar5`` based on the image you built in the previous step. You must define
mount points for all directories you want to have access to while working in the container. At a minimum,
the root **oscar5** directory must be mounted, but you should also mount your root :command:`app` directory, as well as
any data directories the system will need to access. For illustration, we will assume that your root application
directory (containing your reactor models) is ``C:\Users\<username>\my_apps``, and external data (from previous versions
of **OSCAR** for instance) is located at ``C:\Users\<username>\data``.

Now create a container called ``oscar5`` using the following command:

.. code-block:: console

   $ docker run --name oscar5 -it  \
      -v /c/Users/<username>/oscar5:/oscar5      \
      -v /c/Users/<username>/oscar5/my_apps:/my_apps \
      -v /c/Users/<username>/data:/data   \
      -e DISPLAY=<hostip>:0 \
      rel/oscar:1.0 /bin/bash

where ``<hostip>`` should be replaced with your local machines ip (which can be found using ``$ ipconfig``).

.. note::

    Shared mounts (volumes) are created with the :command:`-v` switch, which has the format
    ``[local directory]:[container directory]``. You can add as many mount points as you want.

The second to last line sets up display sharing between the host and the container, while the last line lists the
base image (you should change the name to whatever you called your base image in the previous step),
and defines the entry point. Since we set the entry point to :command:`/bin/bash` you will now be in an interactive
shell in the container. To complete the container configuration, run the install script:

.. code-block:: console

   # python /oscar5/install.py --system-wide


.. attention::

   Before continuing, either exit and re-enter the container, or setup your environment (for this session only) by typing

   .. code-block:: console

      # source /oscar5/.bashrc

Finally, to exit the container,

.. code-block:: console

   # exit

Starting an existing container
++++++++++++++++++++++++++++++

Open the Docker QuickStart Terminal. Start the **oscar5** container:

.. code-block:: console

   $ docker start oscar5

To enter the **oscar5** container, type the following into the terminal:

.. code-block:: console

   $ docker exec -it oscar5 /bin/bash

.. note::

   If you want to use GUI elements, make sure that the `Xming server <Configuring a display server for Windows>`__ is
   up and running. Also, if your machine IP changed, pass it to the ``DISPLAY`` variable when entering the container:

   .. code-block:: console

      $ docker exec -it -e DISPLAY=<hostip>:0 oscar5 /bin/bash

   where ``<hostip>`` should be replaced with your local IP.

Windows 10
----------

Place the **oscar5** distribution somewhere on your local drive. For the remainder of the instructions, we will assume
that you placed **oscar5** in your local user directory ``C:\Users\<username>``, that is:

.. code-block:: console

   C:\Users\<username>\oscar5

Step 1: Install and start docker
++++++++++++++++++++++++++++++++

Download, and follow the `Docker for Windows installation instructions <https://docs.docker.com/docker-for-windows/install/>`_.

Start the docker app by launching Docker for Windows in the Start menu. If everything goes well, you should see
the whale in the status bar:

.. image:: images/whale-icon-systray.png

Step 2: Build a base image
++++++++++++++++++++++++++

Open a terminal window (Command Prompt or PowerShell, but not PowerShell ISE). Go to the root **oscar5** directory:

.. code-block:: console

   > cd C:/Users/<username>/oscar5

Then create an image by invoking :command:`docker build`:

.. code-block:: console

   > docker build -t rel/oscar:1.0 .

This will create an image called ``rel/oscar:1.0``. You can change the image tag to anything you want, as long as you
stick to the docker image naming rules.

Step 3: Create a container
++++++++++++++++++++++++++

Next, we create a container named ``oscar5`` based on the image you built in the previous step. You must define
mount points for all directories you want to have access to while working in the container. At a minimum,
the root **oscar5** directory must be mounted, but you should also mount your root :command:`app` directory, as well as
any data directories the system will need to access. For illustration, we will assume that your root application
directory (containing your reactor models) is ``C:\Users\<username>\my_apps``, and external data (from previous versions
of **OSCAR** for instance) is located at ``C:\Users\<username>\data``.

Now create a container called ``oscar5`` using the following command:

.. code-block:: console

   > docker run --name oscar5 -it  ^
      -v c:/Users/<username>/oscar5:/oscar5 ^
      -v c:/Users/<username>/oscar5/my_apps:/my_apps ^
      -v c:/Users/<username>/data:/data  ^
      -e DISPLAY=<hostip>:0 ^
      rel/oscar:1.0 /bin/bash

where ``<hostip>`` should be replaced with your local machines ip (which can be found using ``> ipconfig``).

.. note::

    Shared mounts (volumes) are created with the :command:`-v` switch, which has the format
    ``[local directory]:[container directory]``. You can add as many mount points as you want.

.. note::

   Docker for Windows might launch a popup asking permission to share the drives. Confirm by clicking Share it.

The second to last line sets up display sharing between the host and the container, while the last line lists the
base image (you should change the name to whatever you called your base image in the previous step),
and defines the entry point. Since we set the entry point to :command:`/bin/bash` you will now be in an interactive
shell in the container. To complete the container configuration, run the install script:

.. code-block:: console

   # python /oscar5/install.py --system-wide

.. attention::

   Before continuing, either exit and re-enter the container, or setup your environment (for this session only) by typing

   .. code-block:: console

      # source /oscar5/.bashrc

Finally, to exit the container,

.. code-block:: console

   # exit

Starting an existing container
++++++++++++++++++++++++++++++

Open a terminal window (Command Prompt or PowerShell, but not PowerShell ISE). Start the **oscar5** container:

.. code-block:: console

   > docker start oscar5

To enter the **oscar5** container, type the following into the terminal:

.. code-block:: console

   > docker exec -it oscar5 /bin/bash

.. note::

   If you want to use GUI elements, make sure that the `Xming server <Configuring a display server for Windows>`__ is
   up and running. Also, if your machine IP changed, pass it to the ``DISPLAY`` variable when entering the container:

   .. code-block:: console

      > docker exec -it -e DISPLAY=<hostip>:0 oscar5 /bin/bash

   where ``<hostip>`` should be replaced with your local IP.

Docker cheat sheet
==================

To list all available containers:

.. code-block:: console

   $ docker ps -a

To stop a running container:

.. code-block:: console

   $ docker stop oscar5

To start the container (if stopped):

.. code-block:: console

   $ docker start oscar5

Open a terminal in the container:

.. code-block:: console

   $ docker exec -it oscar5 /bin/bash

