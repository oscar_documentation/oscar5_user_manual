Installation
************

This manual describes how to install and configure the **OSCAR-5** system.

.. attention::

   The **OSCAR-5** pre and post processing framework is intended to act as a light weight **client**, installed on a
   user's local workstation. It has built in features that enables communication with target codes residing on
   other machines. Thus it is not advisable to install the entire system in a shared environment.

Getting Access to Git Repositories
==================================

All of the components of the **OSCAR-5** system are hosted on private `GitLab <https://gitlab.com>`__ repositories.
Although not required, joining a project on `GitLab <https://gitlab.com>`__ will give you access to:

    1. An issue tracking system, where you can report bugs, make suggestions, and view other user's as well
       as developer's posts.
    2. Latest bug fixes and system improvements.
    3. Bleeding edge features by switching to the developer branch.
    4. The ability to contribute to the system if you are a developer partner.

To join a project, please create a user account on `GitLab <https://gitlab.com>`__, then email us your account name
so that we can add you to the relevant groups. See :ref:`Working with Git`, for a quick introduction.

Installation Manuals
====================

.. toctree::
   :maxdepth: 3

   conda_installation_manual
   docker_installation_manual
   compile_from_source






