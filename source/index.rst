.. OSCAR-5 documentation master file, created by
   sphinx-quickstart on Sat Mar  3 10:54:36 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OSCAR-5's documentation!
===================================

.. attention::

   This manual is a work in progress! Large layout and content changes are likely to occur regularly. Expect
   (and forgive) typos and other errors. Complete API documentation will be added in the near future.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   system_overview
   external_interfaces
   installation_manual
   working_with_git
   rapyds/getting_started
   rapyds/configuration_file_manual
   rapyds/data_model
   rapyds/manager_utility
   rapyds/application_usermanuals
   compose/compose_usermanual

.. toctree::
   :maxdepth: 2
   :caption: Appendix:

   rapyds/extra_units
   rapyds/variable_types
   additional_resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
