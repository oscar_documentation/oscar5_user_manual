Adding two dimensional axial cuts
*********************************

Currently, homogenization calculations are performed on two dimensional slices, or cuts, through the three dimensional
heterogeneous model. These cuts are used as building blocks for the homogeneous three dimensional model.


Things to consider when choosing cuts
=====================================

The specification of a cut includes a height, that is, homogenization is not performed on a true
plane cut, but rather an axial section. A two dimensional calculation is emulated by placing reflective boundary
conditions on the top and bottom planes, but it will only be truly two dimensional if there are no axial
changes within the section. Since nodal equivalence currently only covers the radial plane, with axial leakage
assumed to be zero, it is best to try and limit the amount of axial heterogeneity within your sections. This
only applies to cuts containing active (fissionable) material, as reflector regions are treated differently.
However, especially in research reactors, there are frequently a lot of axial changes even in the active section
of the core (irradiation rigs, beam tubes etc), making it impractical, or even impossible to fully capture
everything in axially homogeneous slices. In this case, one has to use broader considerations than pure structural
variation when placing cuts. The most important factor is to separate regions were large variations in average cross
sections are expected, e.g. moving from a highly scattering to a highly absorbing region.

The system also allows you to refine your model by adding (or removing) cuts if the final error checking yields
unacceptable results.

.. note::

   The system will always correctly compute the *average* behavior over the region, you will just loose the axial
   shape, and true two dimensional equivalence will no longer be achieved.

Defining cuts that include fissionable materials
================================================

Cuts are defined after all the :ref:`Homogenization Parameters` have been set. Standard cuts, which includes source
materials, are added using the following function:

.. py:function:: parameters.add_cut(label, position=0.0 * units.cm, width=1.0 * units.cm, rng=None, description=None)

   Defines a section on which homogenization will be performed.

   :param str label: Name used to identify the section. It will be used to identify the cut on the command line. It
        is recommended to use a unique tag no longer than 12 characters.
   :param position: Center axial position of the section.
   :type position: :term:`length`
   :param width: Axial size of the section.
   :type width: :term:`length`
   :param rng: Minimum and maximum axial position of the section.
   :type rng: :term:`tuple` (:term:`length`)
   :param str description: String giving more information about the cut (e.g. where it is located, why it was chosen,
       etc)
   :returns: A container with all the parameters configuring the section.

   .. attention::

      The `label` is also used when naming mixtures, so it is important that it is unique.

   .. attention::

      The `rng` parameter only makes sense if neither `position` nor `width` is given.


   Typical examples are:

   .. code-block:: py

      cut = parameters.add_cut('MY-CUT', 0.0 * units.cm, 10.0 * units.cm, description='My first axial cut')

   or equivalently:

   .. code-block:: py

      cut = parameters.add_cut('MY-CUT', rng=[-5 * units.cm, 5 * units.cm], description='My first axial cut')

   .. attention::

      The axial position coordinates are absolute, i.e. not relevant to the core axial center line!

   Equivalence and replacement tests are available for these cuts.


The returned variable `cut` contains a copy of all the homogenization parameters, and can be used to modify
the model, or any other generator parameter for that cut. These are described `below <The cut container interface>`_.

Defining axial reflector cuts
=============================

For axial sections that do not contain source (fissionable) material, the following method is used to define the
homogenization calculation.

.. py:function:: parameters.add_axial_reflector_cut(label, position=0.0 * units.cm, width=1.0 * units.cm, rng=None, description=None)

   Defines a section, without source material, on which homogenization will be performed.

   :param str label: Name used to identify the section. It will be used to identify the cut on the command line. It
        is recommended to use a unique tag no longer than 12 characters.
   :param position: Center axial position of the section.
   :type position: :term:`length`
   :param width: Axial size of the section.
   :type width: :term:`length`
   :param rng: Minimum and maximum axial position of the section.
   :type rng: :term:`tuple` (:term:`length`)
   :param str description: String giving more information about the cut (e.g. where it is located, why it was chosen,
       etc)
   :returns: A container with all the parameters configuring the section.

   This method is typically used for regions above or below the active core. For example,

   .. code-block:: py

      h = parameters.generator.model.active_height()
      baf = parameters.add_axial_reflector_cut('BAFFLE', rng=(0.5 * h, 0.5 * h + 6 * units.cm),
                                               description='Baffle region above active fuel')

   will homogenize a 6 cm region above the active core. Equivalence and replacement tests are **not** available for these
   cuts.


.. note::

   Unlike normal active cuts, which tries to mimic two dimensional sections, reflector cuts uses the entire core as a
   driver region, and contains no axial boundary conditions. Thus, the amount of axial heterogeneity within the section
   is **not** an important factor.

The cut container interface
===========================

Each cut reference returned by `parameters.add_cut` and `parameters.add_axial_reflector_cut` has the following methods
to access and modify data:

.. py:function:: cut.get_parameters()

   Returns the **generator** parameter set for the cut. This can be used to modify application parameters, like
   :param:`particles` or :param:`max_iteration`, from the defaults defined in the :param:`generator` section. These
   modifications only affect to the current cut.

   Example:

   .. code-block:: py

      parm = cut.get_parameters()
      parm.particles = 16000

.. py:function:: cut.model()

   Returns a reference to the underlying heterogeneous model. This can be used to modify the model's state, change
   bank positions etc.


   Example:

   .. code-block:: py

      cut.model().set_banks(control.fully_inserted())

   .. attention::

      Any modifications made to the model via this call only applies to the current cut.


.. py:function:: cut.__getitem__(label)

   Returns a reference to a particular mixture in the :param:`homogenization_grid` for this cut.

   :param label: Either a row index or a position label in the homogenization grid.
   :type label: int or str
   :returns: A reference to the mixture at this position. Can be used when defining
        :ref:`replacement cases <Adding re-configured test cases>`, and material meshes.

   For example, suppose the homogenization grid's first row and column label is 'A' and '1' respectively,
   then

   .. code-block:: py

      mix = cut[0][0]

   or

   .. code-block:: py

      mix = cut['A1']

   will return the mixture that is in top left corner of the grid.


