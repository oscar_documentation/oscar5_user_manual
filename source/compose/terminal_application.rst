Running a cOMPoSe application module from the terminal
******************************************************

As is evident from the amount of information in the input file, running a **cOMPoSe** application is an involved
process, with numerous steps and sub-processes. The basic signature is,

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module <cut or mode> [options [mode [options] ...]

where **cut** is any `cut tag specified in the module <Define two dimensional axial cuts>`_ and **mode** is any of the
top level application modes listed in the following table:

========================================  ==============================================================================
 Mode                                     Description
========================================  ==============================================================================
:ref:`archive <archive>`                  Compress all input and output into a single archive file.
:ref:`clean <clean>`                      Clears all input and output from the working directory (use with caution!)
`visualization <tl_viz>`_                 Show a three dimensional representation of the model and the overlay
                                          homogenization mesh.
`generator <recursivegenerator>`__        Launches generator_ calculations for all the cuts.
`update <recursiveupdate>`__              Updates generator_ results for all the cuts.
`library <recursivelibrary>`__            Runs library_ mode for specified cuts.
`equivalence <recursiveequivalence>`__    Runs equivalence_ mode for specified cuts.
deploy_                                   Collects all homogenized mixtures in a single library, and exports the nodal
                                          configuration.
test_                                     Runs defined 3D test cases.
========================================  ==============================================================================

All available modes can be viewed on the command line by passing the **--help** flag:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module --help

Apart from the final deploy_ and test_ phases, most of the application interaction will be on a specific cut, which is
covered next.

Homogenizing a two dimensional cut
==================================

For each cut, the application will:
  1. Homogenize each channel (or node) in the radial mesh using the specified transport code,
  2. Generate nodal parameters from the transport code output, and
  3. Perform tests to check the performance of these parameters.

This is accomplished by a number of application sub modes, summarized in the following table:

====================  ==================================================================================================
 Mode                 Description
====================  ==================================================================================================
generator_            Perform homogenization by running the transport code.
library_              Create nodal parameters for all nodes (channels).
equivalence_          Check if equivalence to the reference transport solution is preserved.
reconf_               Check to what extend equivalence is broken when certain nodes are replaced with mixtures from
                      different environments.
====================  ==================================================================================================

.. _generator:

Generator mode
--------------

This mode controls homogenization of the current two dimensional cut. It is a standard **rapyds**
application, and supports the full interface described :ref:`here <Running applications from the command line>`. The
following lists the typical usage sequence for standard and axial reflector cuts.

First, start the homogenization procedure by launching (for example) on the remote host configured in **host.cfg**
using 20 threads:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module MY-CUT generator --config-file host.cfg execute --threads 20

You can monitor the (remote) running application by passing the **--status** flag to the execution mode:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module MY-CUT generator --config-file host.cfg execute --status

Results can be viewed at any time using **post** mode:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module MY-CUT generator --config-file host.cfg post --show

The **--show** flag will open a result viewer, which summarizes convergence errors in the homogenization application.
The displayed error map shows a grid of the same size as the overlay :param:`homogenization_grid`. Values in the nodes
indicate the level of balance achieved in this node. These values should be close to one. Nodes are colored according
to the maximum cross section relative error in that node.

Each segment in the mesh has two nested circles. The inner most circle denotes the relative error on boundary flux
estimation, while the outer circle depicts errors on boundary current estimation. The outer circle is divided in
two fragments, depicting in and outgoing current respectively.

The drop down list in the top right corner of the viewer allows one to switch between spectrum weighted and group
specific errors. In the latter case, the balance number indicates within group balance, while in the former case,
the number indicates total balance.

.. note::

   You can run the post processor and view errors as soon as the target code has produced some output, that is, you do
   not need to wait for the run to finish. For long calculations running on a remote host, this is a useful mechanism
   to monitor convergence.


Burnup and state dependent homogenization
+++++++++++++++++++++++++++++++++++++++++


Since `lattice cuts <Adding burnup and state dependent cuts>`_, have a lot more generator mode options, the command line
interaction is covered :ref:`in a separate section <lattice_command_line>`.

.. _library:

Library mode
------------

Triggers the creation of HED files, POLX input and also runs POLX for each channel in the :param:`homogenization_grid`.
This mode accepts the following flags:

.. option:: --correctbalance

   Perform group wise balance correction for all nodes. This is an experimental feature, which ensures that balance
   is achieved in all energy groups. It is mostly applicable when Monte Carlo codes, which does not solve a local
   balance equation in each node, are used. In this case, the system will re-sample all parameters, from their current
   uncertainty distribution, with the constraint that balance is achieved in all nodes and energy groups.

   .. warning::

      The flag will perform constraint sampling of **all** the nodal parameters, which is a very large system
      for full core problems, requiring ample computer memory and time.


.. option:: --errormap

   Show a map of segments where the calculation of equivalence parameters failed. This usually occurs on boundaries were
   the equivalent nodal diffusion solution produced negative fluxes. Equivalence parameters on these boundaries are
   switched of.

.. option:: --force

   Force re-generation of all library files. If this flag is not specified, only out of date library files will be
   re-generated.

Typical usage:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module MY-CUT library --errormap


.. _equivalence:

Equivalence mode
----------------

Packs the homogenized mixtures of a specified two dimensional cut into an equivalent two dimensional **MGRAC** model.
It also run **MGRAC** and compares results with the generator output. Since it will also run library_ mode if library
files are not found, it accepts all the library_ mode flags.

.. option:: --errormap

   In this mode, the flag displays a basic interactive window with maps of errors relative to the generator results.
   The drop down box in the upper right corner can be used to switch between power and group flux errors.

Typical usage:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module MY-CUT equivalence --errormap

Equivalence errors are usually caused by the following:

  1. Lack of group wise balance in the generator results. This can be remedied by running the generator longer, or
     passing the :command:`--correctbalance` flag.

  2. Failure to produce equivalence parameters in library_ mode. This is the by far the biggest contributor to
     equivalence errors, and is not straight forward to remedy, as it usually indicates regions were diffusion theory
     fails. If errors occur in thin nodes, switching to a flat leakage model might help.

.. attention::

    You can not run an equivalence calculation for axial reflector cuts.

.. _reconf:

Reconfiguration mode
--------------------

Similar to equivalence_ mode, except that certain positions have reconfigured mixtures from other environments. Usage
on the command line is as follows

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module <cut> reconf <case> [options]

where, ``<cut>`` should be replaced with the desired cut name, and ``<case>`` any one of the re-configured cases
defined in the `input module <Adding re-configured test cases>`_. The available options are:

.. option:: --errormap

   In this mode, the flag displays a basic interactive window with maps of errors relative to the generator results.
   The drop down box in the upper right corner can be used to switch between power and group flux errors.

.. _tl_viz:

Top level visualization
=======================

The top level visualization will show the entire model, and the nodalization mesh. It accepts the following command
line arguments:

.. program:: visualization

.. option:: --cuts <list>

   Comma separated list of cut names to include in the visualization. Defaults to 'all', so that all cut meshes will
   be displayed.


.. _recursivegenerator:

Recursive generator mode
========================

Recursively launch all generator_ calculations for all the cuts defined in the script. This mode has the following
signature:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module generator [options]

where *options* are any :ref:`standard application arguments <Setting or modifying parameters using command line arguments>`
or :ref:`execution mode arguments <execute>`.

.. attention::

   This mode will currently not work with any burnup dependent lattice cuts defined in the input module.


.. _recursiveupdate:

Recursive update mode
=====================

Recursively updates all generator_ results for the cuts defined in the script. This mode has the following
signature:

.. code-block:: console

   $ oscar5 MY_REACTOR.compose.my_module update [options]

where *options* are any :ref:`standard application arguments <Setting or modifying parameters using command line arguments>`
or :ref:`post mode arguments <post>`. This mode is particularly useful when a number of calculations were launched on
a remote server.


.. _recursivelibrary:

Recursive library mode
======================

Recursively run library_ mode for all specified cuts. Accepts the following arguments:

.. option:: --cuts <list>

   Comma separated list of cut names. Defaults to 'all'.

.. option:: --force

   Flag indicating if existing libraries should be overwritten. If omitted, only non-existing or out of date libraries
   will be regenerated.

.. _recursiveequivalence:

Recursive equivalence mode
==========================

Recursively run equivalence_ mode for all specified cuts. Accepts the following arguments:

.. option:: --cuts <list>

   Comma separated list of cut names. Defaults to 'all'.

.. _deploy:

Deployment mode
===============

Combine all the homogenized mixtures into a single library by running LYNX, and saves the nodal configuration to your
**model** package.


This mode does not currently have any additional flags.

.. _test:

Test mode
=========

Under construction!