Homogenization Parameters
*************************

The **cOMPoSe** `Configuration` class contain all the data defining how and what the script will do. Create an
instance using its default constructor:

.. _parameter_pack:
.. code-block:: py

    parameters = Configuration()


Since **cOMPoSe** is a standard **rapyds** application, the `Configuration` parameter pack accepts all
:ref:`general <General project parameters>` and  :ref:`execution <General application execution parameters>`
parameters. The most important of these are :param:`working_directory`, and :param:`config_file`, which defines
what version of the nodal package should be targeted. For example:

.. code-block:: py

   parameters.working_directory = core.utilities.path_relative_to(__file__, 'EXPERIMENTS')
   parameters.config_file = '%COMPOSE/OSCAR4-v1.0.cfg'

The following lists all the additional parameters, unique to the `Configuration` container:

.. parameter:: generator
   :type: section
   :default: NA

   Parameters passed directly to the application used in homogenization calculations. This section exposes all
   :ref:`general <General project parameters>`, :ref:`numerical <General physics and numerical parameters>`, and
   :ref:`application <General application execution parameters>` parameters.

   The most important parameter is ``model``, which defines the underlying heterogeneous model.

   .. attention::

      Don't set ``generator.working_directory``, as it is determined by the top level driver application.

   .. parameter:: model
      :type: ``ModelBuilder``
      :default: NA

       The underlying heterogeneous model. If you imported an existing model from **configurations**, you can simply
       pass it to this parameter:

       .. code-block:: py

          from ..configurations.my_configuration import model

          parameters.generator.model = model

       Otherwise, you must construct the model in the script. See :ref:`Building Core Configurations`.

   .. attention::

      Setting these parameters prior to defining any cuts will ensure that they propagate correctly to the cut's
      generator mode. All parameters can be overwritten or specified on the command line.

.. parameter:: homogenization_grid
   :type: :term:`labeledgrid` [:term:`integer` | :term:`boundary_condition`]
   :default: Required

   Defines the size and labels of the overlay (radial) homogenization grid. Homogenization will be performed for
   each position, or channel, in the grid. Use core labels wherever your grid coincides with core positions. Otherwise,
   try to stick with row and column labels of two characters or less.

   Entries in the grid are used to define channel types, which typically coincides with assembly types. For instance,
   in the following map, all ex-core reflector regions are flagged as 0, all in-core fuel positions are flagged as 1,
   etc.

   .. code-block:: py

        parameters.homogenization_grid = \
        [[0   , 'E3', 'E2', 'E1',   1,   2,   3,   4,   5,   6,   7,   8,   9, 'W1', 'W2', 'W3'],
         ['N3',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         ['N2',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         ['N1',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         [ 'A',    0,    0,    0,   4,   4,   4,   4,   4,   4,   4,   4,   4,    0,    0,    0],
         [ 'B',    0,    0,    0,   4,   4,   1,   1,   1,   3,   1,   3,   4,    0,    0,    0],
         [ 'C',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'D',    0,    0,    0,   4,   4,   1,   1,   1,   4,   1,   3,   4,    0,    0,    0],
         [ 'E',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'F',    0,    0,    0,   4,   4,   1,   1,   1,   4,   1,   3,   4,    0,    0,    0],
         [ 'G',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'H',    0,    0,    0,   4,   4,   1,   1,   1,   1,   1,   4,   4,    0,    0,    0],
         ['S1',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         ['S2',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         ['S3',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0]]

   .. attention::

      The homogenization grid defines the **entire** nodal model, and not just its core region. Thus, you should include
      reflector regions and relevant ex-core structures.

   Tags are very useful later in the script when doing replacements and constructing the full nodal model, but they do
   not affect the homogenization procedure itself. Boundary condition tags can be used to flag channels as outside
   the problem domain. For example:

   .. code-block:: py

        from core.boundary_conditions import vacuum
        vc = vacuum()

        parameters.homogenization_grid = \
        [[0   , 'E3', 'E2', 'E1',   1,   2,   3,   4,   5,   6,   7,   8,   9, 'W1', 'W2', 'W3'],
         ['N3',   vc,   vc,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,   vc,   vc],
         ['N2',   vc,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,   vc],
         ['N1',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         [ 'A',    0,    0,    0,   4,   4,   4,   4,   4,   4,   4,   4,   4,    0,    0,    0],
         [ 'B',    0,    0,    0,   4,   4,   1,   1,   1,   3,   1,   3,   4,    0,    0,    0],
         [ 'C',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'D',    0,    0,    0,   4,   4,   1,   1,   1,   4,   1,   3,   4,    0,    0,    0],
         [ 'E',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'F',    0,    0,    0,   4,   4,   1,   1,   1,   4,   1,   3,   4,    0,    0,    0],
         [ 'G',    0,    0,    0,   4,   4,   3,   1,   2,   1,   2,   1,   4,    0,    0,    0],
         [ 'H',    0,    0,    0,   4,   4,   1,   1,   1,   1,   1,   4,   4,    0,    0,    0],
         ['S1',    0,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0],
         ['S2',   vc,    0,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,   vc],
         ['S3',   vc,   vc,    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,   vc,   vc]]


   This is frequently used to model PWR and PWR circular cores.

   The overlay grid need not cover the entire :param:`core_map`. You can use it to edit any *colorset* region from the
   model, isolated with the appropriate boundary conditions (see below). For example, the following will restrict
   homogenization to a 3x3 window in the bottom right of the full mesh:

   .. code-block:: py

      parameters.homogenization_grid = \
      [[0  ,  2, 3, 4],
       ['G',  4, 3, 1],
       ['H',  4, 1, 1],
       ['S1', 0, 0, 0]]

   .. note::

      Although you have a lot of freedom in defining the overlay mesh, it must in the end be used to generate a valid
      nodal model, so keep the following restrictions in mind:

        1. A channel, or mesh, cannot contain more that one loadable assembly.

        2. A channel cannot contain more that one control element.

.. parameter:: homogenized_grid_pitches
   :type: :term:`list` [:term:`length`]
   :default: Required

   Size of the overlay mesh channels. The first entry is a list of length equal to the number of columns, defining the
   :math:`x` (column) sizes, and the second a list of length equal to the number of rows, defining the :math:`y` (row)
   sizes.

   For example:

   .. code-block:: py

      parameters.homogenization_grid_pitches = \
      [[7 * units.cm]*2 + [6 * units.cm]  + [7.71 * units.cm]*9 + [6 * units.cm] + [7 * units.cm]*2,
       [7 * units.cm] * 2 + [6 * units.cm] + [8.10 * units.cm]*8 + [6 * units.cm] + [7 * units.cm]*2]

.. parameter:: homogenization_grid_center
   :type: :term:`point`
   :default: ``(0,0,0)``

   Define the center of the overlay grid, in the underlying heterogeneous model coordinates. The parameter essentially
   anchors the grid to the underlying model. Typically, you would use parameters provided by the heterogeneous model,
   for instance,

   .. code-block:: py

      parameters.homogenization_grid_center = parameters.generator.model.core_center()

   will ensure that the mesh is centered on the core center line.

   .. note::

      The visualization_ mode can be used to check the positioning and size of your overlay mesh relative to the
      underlying model.

.. parameter:: boundary_conditions
   :type: section
   :default: NA

   Radial boundary conditions applied to boundaries of the overlay grid

    .. parameter:: north
       :type: :term:`boundary_condition`
       :default: ``vacuum``

       Boundary condition applied to the maximum line in the grid perpendicular to the :math:`y` axis, that is, above
       the first row.

    .. parameter:: south
       :type: :term:`boundary_condition`
       :default: ``vacuum``

       Boundary condition applied to the minimum line in the grid perpendicular to the :math:`y` axis, that is, below
       the bottom row.

    .. parameter:: east
       :type: :term:`boundary_condition`
       :default: ``vacuum``

       Boundary condition applied to the minimum line in the grid perpendicular to the :math:`x` axis, that is, left
       of the first column.

    .. parameter:: west
       :type: :term:`boundary_condition`
       :default: ``vacuum``

       Boundary condition applied to the maximum line in the grid perpendicular to the :math:`x` axis, that is, right
       of the last column.

.. parameter:: homogenization_group_structure
   :type: :term:`list` [:term:`energy`]
   :default: Required

   The coarse energy group structure. Specified as an **increasing** list of energy bounds, including the lower bound.
   Thus, for a :math:`G` groups there should be :math:`G+1` entries. For example, a typical two group structure is
   given by:

   .. code-block:: py

      parameters.homogenization_group_structure = [1.0E-11 * units.MeV, 6.25E-7 * units.MeV, 20.0 * units.MeV]

   The :mod:`core.energy_condensation` contains a number of pre-defined energy group structures.

   .. attention::

      All imported compose modules, as well as any imported lattice models must use the same energy group structure.

.. parameter:: subdivided_rows
   :type: :term:`dict`
   :default: ``None``

    Indicates which rows in the :param:`homogenization_grid` should be treated as segments of a single row. This
    parameter takes the form of a :term:`dict` object, with each key a single row tag, and values the list of row
    labels in :param:`homogenization_grid` that should be grouped together. For example, suppose that the row label
    ``'1'`` was split into ``'1a'`` and ``'1b'``:

    .. code-block:: py

       parameters.subdivided_rows = {'1': ['1a', '1b']}

   .. note::

      This parameter is only used when assemblies are segmented, that is, homogenized into pieces smaller than the
      full assembly size.

.. parameter:: subdivided_columns
   :type: :term:`dict`
   :default: ``None``

    Indicates which column in the :param:`homogenization_grid` should be treated as segments of a single column. This
    parameter takes the form of a :term:`dict` object, with each key a single column tag, and values the list of column
    labels in :param:`homogenization_grid` that should be grouped together. For example, suppose that the column label
    ``'A'`` was split into ``'A1'`` and ``'A2'``:

    .. code-block:: py

       parameters.subdivided_columns = {'A': ['A1', 'A2']}

   .. note::

      This parameter is only used when assemblies are segmented, that is, homogenized into pieces smaller than the
      full assembly size.

.. parameter:: sub_node_mesh
   :type: :term:`labeledgrid`
   :default: Empty

    Defines sub mesh structures for positions in the colorset. Each entry in the map is a list of length two,
    with the first entry a list of :math:`x` pitches, and the second a list of :math:`y` pitches. This then defines
    the two dimensional grid over which sub node fluxes and form factors will be calculated.

    The parameter is instantiated with an empty map, so instead of specifying an entire map, only the positions were
    sub-node fluxes are desired can be modified. For example,

    .. code-block:: py

       parameters.sub_node_mesh['B2'] = [[0.3, 0.4, 0.3], [0.3, 0.4, 0.3]]

    .. note::

       If a sub-mesh is defined, the nodal solver will be able to re-construct fluxes on a finer mesh in the core
       calculation.

.. parameter:: upscatter_correction
   :type: :term:`integer`
   :default: 0

   Largest group index (with 1 being the fast group) which are allowed to receive up-scatters. This correction is
   intended to avoid the situation were entire broad fast groups receive up-scatters due to a small amount of physical
   up-scattering in the lower energies within the group boundary. The correction method is flux weighted and preserves
   overall reaction rates.

   If this parameter is 0 (the default), no correction is performed.

   .. attention::

      All imported compose modules, as well as any imported lattice models must use the same value for this parameter.

.. parameter:: use_p1_side_fluxes
   :type: :term:`bool`
   :default: ``False``

   If this flag is set, the following expression will be used for side fluxes on node boundaries:

   .. math::

      2\left(J^+ + J^-\right)

   where :math:`J^+` and :math:`J^-` are the average in and out currents across the boundary respectively. When this
   option is used, in the downstream equivalence calculation, the partial currents :math:`J^+` and :math:`J^-` are
   preserved, as opposed to the boundary flux and net current.

   .. note::

       Since partial currents are generally easier to estimate when using Monte Carlo cross section generation, it is
       recommended that this flag be set to ``True``.
