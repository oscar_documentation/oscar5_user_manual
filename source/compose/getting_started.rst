Getting Started
***************

This section describes how to create a **cOMPoSe** input script, and the typical structure of the script.

Creating a template input module
================================

The :command:`manager` utility can be used to create a basic starting module for your cOMPoSe project. It is
used as follows:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager compose <module name> [options]

where *<module name>* is the name of your input script (without any extensions), and must adhere to
`python module naming requirements <https://python.org/dev/peps/pep-0008/#package-and-module-names>`_. The following
options are available:

.. program:: manager

.. option:: --project_name <str>

   Project name. Used to deduce some input and data file names, as well as the default :param:`working_directory`.

.. option:: --configuration <str>

   Name of configuration whose model should be used as basis for this project. Should be the name (without any extensions)
   of an existing module in your **configuration** package.

.. option:: --description <str>

   Short description of the project. Used for documentation purposes.

For example, the following will create the **experiments** module in the **compose** package, use the **cold**
configuration module:

.. code-block:: console

   $ oscar5 MY_REACTOR.manager compose experiments --config-file cold --description 'Nodal modal for startup experiments'


The cOMPoSE input script
========================


Import statements
-----------------

In order to import the cOMPoSe system infrastructure, add the following to the top of the script:

.. code-block:: py

    from cOMPOSe import *
    from core import *

This will make the parameters sets and application execution methods available, as well as the core functionality of
the pre-processing system.

If you are going to use an existing core configuration import it here, for instance,

.. code-block:: py

    from ..configurations.experiments import model

Finally, in order to save nodal representations for assemblies and the core configuration, the system needs
to know where assembly data is located. This is achieved by importing the assembly data base:

.. code-block:: py

    from ..model import assemblies


Parameters
----------

The bulk of the script contains input that configures the homogenization calculations. This is described in the
following sections.