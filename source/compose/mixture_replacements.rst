Adding Mixture Replacement Tests
********************************

Importing mixtures
==================

From external sources
---------------------

Externally produced **HED** files are included using the ``import_library`` method of a cut. The argument passed to
the call is the full path to the **HED**  file:

.. code-block:: py

   hed = cut.import_library('/path/to/file.HED')

The call returns a reference to the imported mixture, which can be used later, for instance when defining cut
`reconfigurations <Adding re-configured test cases>`_. There is no limit to the amount of external mixtures
that can be imported.

From other modules
------------------

Mixtures produced in other compose modules can be accessed by simply importing that module. For example,

.. code-block:: py

   import inf_1

   # reference to the mixture in row 0, column 0 of the 'act' cut in inf_1
   im = inf_1.act[0][0]


Adding re-configured test cases
===============================

For each cut, the generator will homogenize all channels in :param:`homogenization_grid` with a single transport
calculation. Thus, when equivalence theory is applied, average reaction rates and leakages should be preserved for
each node when moving to a nodal diffusion solution. See the section on `testing for equivalence <equivalence>`_.

A re-configured case is formed by replacing certain channels in the cut with mixtures homogenized in other environments.
This is a useful mechanism for estimating errors when a single loadable assembly mixture is placed in various
positions.

These cases are defined per cut, using the following interface:

.. code-block:: py

   case = cut.add_replacement_case(name, transformation_map=None, description=None)

where ``name`` is a string tag which is used to identify the replacement case on the `command line <reconf>`_, and
description just gives some additional information. The purpose of the ``transformation_map`` parameter will be
described later.

The above only registers a replacement case, actual replacements are performed using the ``replace`` method. It has
two major options for passing data, the first replaces all channels with :param:`homogenization_grid` matching a
particular token. For example,

.. code-block:: py

   import inf_1

   case = cut.add_replacement_case('MY-RECONF-1', description='Test replacement case')
   case.replace(1, inf_1.act['A1'])

will replace all channels in :param:`homogenization_grid` that has token ``1``, with the mixture in channel ``'A1'``
from the ``act`` cut in the ``inf_1`` module. You can string multiple ``replace`` calls together, to replace more
positions. For example,

.. code-block:: py

   import inf_1
   import inf_2

   case = cut.add_replacement_case('MY-RECONF-1', description='Test replacement case')
   case.replace(1, inf_1.act['A1'])
   case.replace(2, inf_2.act['A1'])

will also replace all channels in :param:`homogenization_grid` that has token ``2``, with the mixture in channel ``'A1'``
from the ``act`` cut in the ``inf_2`` module.

The second option is to pass a full :term:`labeledgrid`, with entries the replacement mixtures, and empty place holders
for position that should not be altered. For example:

.. code-block:: py

   import inf_1
   import inf_2

   m1 = inf_1.act['A1']
   m2 = inf_2.act['A1']

   case = cut.add_replacement_case('MY-RECONF-1', description='Test replacement case')

   rfmap = \
        [[   _,    3,   4,   5,   6,   7,   8],
         [ 'B',   m1,  m1,  m1,   _,  m1,   _],
         [ 'C',    _,  m1,  m2,  m1,  m2,  m1],
         [ 'D',   m1,  m1,  m1,   _,  m1,   _],
         [ 'E',    _,  m1,  m2,  m1,  m2,  m1],
         [ 'F',   m1,  m1,  m1,   _,  m1,   _],
         [ 'G',    _,  m1,  m2,  m1,  m2,  m1],
         [ 'H',   m1,  m1,  m1,  m1,  m1,   _]]

   case.replace(rfmap)

.. note::

   The map passed to ``replace`` must either be the full :param:`homogenization_grid`, or a valid sub grid thereof.


The ``transformation_map`` is a :term:`labeledgrid` whose entries are the transformations that should be applied to
replaced mixtures. Entries in this map depends on the orientation of new mixtures versus the orientation of the current
mixtures.

Frequently, using the heterogeneous transformation map will ensure that replaced assemblies will have the correct
orientation:

.. code-block:: py

   case = cut.add_replacement_case('MY-RECONF-1', description='Test replacement case',
                                   transformation_map=cut.model().transformation_map)


