"""
Basic driver utility to build the OSCAR-5 documentation set using sphinx. This utility also fetches examples from the
repositories.
"""

import os
import subprocess


def get_repo_file(repo, file, local):

    cmd = 'git archive --format=tar --remote=git@gitlab.com:{} HEAD {} | tar -xO > {}'.format(repo, file, local)
    subprocess.check_call(cmd, shell=True)


examples = [
    # --- MTR
    ('oscar5_benchmarks/SINGLE_MTR_ASSEMBLY.git', 'model/fuel_assembly.py', 'rapyds', 'basic_mtr.py'),  # generic
    ('oscar5_benchmarks/OPAL.git', 'model/standard_fuel.py', 'rapyds', 'ba_mtr.py'),  # plate with BA
    ('oscar5_benchmarks/IPEN.git', 'model/empty_miniplate_holder.py', 'rapyds', 'plate_holder.py'),  # plate with BA
    ('oscar5_benchmarks/IRR1.git', 'model/control_fuel_assembly.py', 'rapyds', 'mtr_intra.py'),  # plate with BA
    # -- LWR
    ('oscar5_benchmarks/BEAVRS.git', 'model/fuel_pin.py', 'rapyds', 'pin_pwr.py'),  # pins
    ('oscar5_benchmarks/BEAVRS.git', 'model/guide_pin.py', 'rapyds', 'guide_pin_pwr.py'),
    ('oscar5_benchmarks/BEAVRS.git', 'model/fuel_assembly.py', 'rapyds', 'pin_asm.py'),
    # -- HTR
    ('oscar5_benchmarks/PBMR400.git', 'model/core.py', 'rapyds', 'cor.py'),  # core
    ('oscar5_benchmarks/HTR10.git', 'model/pebble.py', 'rapyds', 'pebble.py'),  # coated particles
    # -- Generic
    ('oscar5_benchmarks/ETRR2.git', 'model/absorber_plate.py', 'rapyds', 'cntrl.py'),  # control assembly
    ('oscar5_benchmarks/MNR.git', 'model/beryllium_reflector.py', 'rapyds', 'refc_mtr.py'),  # reflector or pool


]


def fetch_examples():

    root = os.path.dirname(os.path.realpath(__file__))

    for repo, f, package, alias in examples:
        target = os.path.join(root, 'source', package, 'examples')
        os.makedirs(target, exist_ok=True)
        get_repo_file(repo, f, os.path.join(target, alias))


def build_docs():

    cmd = 'make html'
    subprocess.check_call(cmd, shell=True)


def build_leaflets():

    # installation
    root = os.path.dirname(os.path.realpath(__file__))

    dest = os.path.join(root, 'leaflets', 'installation')

    os.chdir(dest)

    cmd = 'make latexpdf'
    subprocess.check_call(cmd, shell=True)


if __name__ == '__main__':
    fetch_examples()
    build_docs()
    # build_leaflets()
